# `go-cronos`

A collection of tools I've made to do things for me on the Cronos chain

- [`go-cronos`](#go-cronos)
- [Usage](#usage)
  - [Setting up Telegram](#setting-up-telegram)
  - [Deployment on Digital Ocean](#deployment-on-digital-ocean)
- [Tools listing](#tools-listing)
  - [AES](#aes)
    - [Decrypt](#decrypt)
    - [Encrypt](#encrypt)
  - [Ebisus Bay](#ebisus-bay)
    - [Buy](#buy)
    - [Snipe](#snipe)
    - [Watch listings](#watch-listings)
    - [Watch sales](#watch-sales)
  - [tgutil](#tgutil)
    - [Listen](#listen)
    - [Send](#send)
- [API usage](#api-usage)
- [Contributing](#contributing)
  - [Building a binary](#building-a-binary)
  - [Building an image](#building-an-image)
  - [Building a utilities image](#building-a-utilities-image)
  - [Releasing images](#releasing-images)
  - [Overwriting Makefile configurations](#overwriting-makefile-configurations)
- [Thanks](#thanks)
- [Cheers](#cheers)

# Usage

You can run these tools using Go or you can build them into binaries for exporting.

To use these as Go tools, pull in the dependencies using `go mod vendor` and then use `go run ./cmd/${TOOL_NAME}` to start the tool. All tools use `spf13/cobra` for structuring, this means you can use `--help` to view inline information about any command.

## Setting up Telegram

Telegram is used for all notification functionality. To set up Telegram a utility tool `tgutil` is provided (located at [`./cmd/tgutil`](./cmd/tgutil)) that reflects back to you the chat ID.

1. Talk to the [`@BotFather`](https://t.me/botfather) and create a new bot for youyrself. You will receive an API token, this shall be referenced as `$TG_TOKEN` for the rest of this document
2. Start `tgutil` with the correct `$TG_TOKEN` set in your environment
3. Start your bot and you will receive a chat ID - this shall be referenced as `$TG_CHAT_ID` for the rest of this document

All services with notifications will reference these variables, you can either set the environment variables:

1. `TG_TOKEN`
2. `TG_CHAT_ID`

Or you can use the `--tg-token` and `--tg-chat-id`  flag to specify them. Flags are not recommended because they will stay in your shell history.

## Deployment on Digital Ocean

The services in this repository have been designed to work with DigitalOcean Apps. If you don't have an account yet, you can use [my referral code to sign up](https://m.do.co/c/c3b62cf39c7c) so I get some credits (please?).

# Tools listing

## AES

### Decrypt

This command decrypts a provided ciphertext (use this to test the decryption of your private key):

```sh
go run ./cmd/aes decrypt "ciphertext" -k "password";
```

### Encrypt

This command encrypts a provided plaintext (use this to encrypt your private key):

```sh
go run ./cmd/aes encrypt "plaintext" -k "password";
```

## Ebisus Bay

> Code found at [`./cmd/ebisusbay`](./cmd/ebisusbay)

### Buy

This command purchases the specified listing immediately at whatever price it is at.

```sh
# ensure the PRIVATE_KEY variable is populated,
# when running locally you can use the export. if
# running in a container, attach it to the env
export PRIVATE_KEY=abcd1234...;

# start the buyer
go run ./cmd/ebisusbay buy \
  --address $COLLECTION_ADDRESS \
  --token-id $TOKEN_ID;

# full example
go run ./cmd/ebisusbay buy \
  --address 0xA19bFcE9BaF34b92923b71D487db9D0D051a88F8 \
  --token-id 5302;
```

### Snipe

This command watches for listings in the collection `$COLLECTION_ADDRESS` and makes the purchase for you if a certain combination of traits `$TRAITS` reaches a floor price of `$PRICE_IN_CRO`.

To snipe, your private key is required. To facilitate doing this safely, encrypt it with the `aes` tool in this repository and use the ciphertext as the `PRIVATE_KEY` environment variable. You will also need to create a bot by talking to the [@BotFather on Telegram](https://t.me/botfather) to create a bot so that the bot can be used to get the password to your encrypted private keys.

1. Create bot with [@BotFather on Telegram](https://t.me/botfather)
2. The API keys that is given to you is your `TG_TOKEN` value
3. Run `TG_TOKEN=<your_api_token> go run ./cmd/tgutil listen` and open the link in the logs
4. Use `/start` to start the bot and the bot should give you a chat ID, this is your `TG_CHAT_ID` value
5. Run `go run ./cmd/aes encrypt "<your_private_key>" -k "<your_password>"`, a random string will appear, this is your `ENCRYPTED_PRIVATE_KEY` value
6. Set these variables in the environment you are running and start the bot as follows:

```sh
# export the environment variables
export ENCRYPTED_PRIVATE_KEY=abcd1234...;
export TG_TOKEN=12345...;
export TG_CHAT_ID=12345...;

# start the sniper on 'fat thumbs' mode
go run ./cmd/ebisusbay snipe \
  --address $COLLECTION_ADDRESS \
  --price $PRICE_IN_CRO;

# start the sniper to watch for specific traits
go run ./cmd/ebisusbay snipe \
  --address $COLLECTION_ADDRESS \
  --traits "$TRAITS" \
  --price $PRICE_IN_CRO;

# full example including traits selection
go run ./cmd/ebisusbay/ snipe \
  --address 0xA19bFcE9BaF34b92923b71D487db9D0D051a88F8 \
  --traits 'Background=Degen Blue,Fur=Purple Frankenstein,Mouth=Dirty Disdain'
  --price 10000;
```

> To skip the confirmation step, run the command with the `--yes` flag

After running the above, you will receive a message to enter your password in Telegram, do it (both request for password and password will be automatically deleted). Your bot is now live!

### Watch listings

This command watches for listings on Ebisus Bay and prints details of the listing when it detects new ones.

```
go run ./cmd/ebisusbay watch listings;
```

### Watch sales

This command watches for sales on Ebisus Bay and prints details of the sale when it detects new ones.

```
go run ./cmd/ebisusbay watch sales;
```

## tgutil

### Listen

This starts the bot on listen mode where you can talk to it and it will respond with a chat ID. This is the value of your `TG_CHAT_ID` environment variable.

```sh
export TG_TOKEN=abcd1234...

go run ./cmd/tgutil listen;
```

### Send

This tests sending a message to a chat ID and can help to debug whether the bot associated with your API key is able to send your message:

```sh
export TG_TOKEN=abcd1234...

go run ./cmd/tgutil send --tg-chat-id $TG_CHAT_ID
```

# API usage

Modules which can be used in your own project are in the `./pkg` directory. You can reference them by importing them into your Go code:

```go
import (
  // for Cronos-related automations
  "github.com/zephinzer/go-cronos/pkg/cronos"

  // for the ABI-based API of EbisusBay
  "github.com/zephinzer/go-cronos/pkg/ebisusbay"
  
  // for the HTTP-based API of EbisusBay
  "github.com/zephinzer/go-cronos/pkg/ebisusbayapi"
  
  // for encryption/decryption of stuff
  "github.com/zephinzer/go-cronos/pkg/secret"
)
```

# Contributing

## Building a binary

To build a binary for the command in `./cmd`, run:

```sh
make build app=...;

# example for ebisusbay
make build app=ebisusbay;
```

You'll get binaries in the `./bin` directory.

## Building an image

To build an image for the service `${APP}` located at `./cmd/${APP}`, run:

```sh
make image app=${APP};

# example for ebisusbay
make image app=ebisusbay;
```

You'll get an image `zephinzer/go-cronos-${app}:latest`.

## Building a utilities image

Utilities images are images used in Job/CronJob resources which are not directly related to business logic. Utility images are stored in `./build/*`, different from the primary `Dockerfile` in the root directory.

To build the utility image `${APP}`, run:

```sh
make utils_image app=${APP};

# example for mysqldump
make utils_image app=mysqldump;
```

## Releasing images

Services here are designed for deployment on Kubernetes. We use DockerHub for Docker image hosting.

To release a native service:

```sh
make release app=${APP};
```

To release a utilities service:

```sh
make utils_release app=${APP};
```

## Overwriting Makefile configurations

If you are forking this repository, you might want to push images to your own image repositories. To do this, you can create a new `./Makefile.properties` in the root directory and overwrite the following variables:

- `image_registry`
- `image_owner`
- `image_path`

These will be formed into `image_url` which consists of the above arguments. An example `Makefile.properties`:

```Makefile
image_registry = quay.io
image_owner = yourstruly
image_path = cronos-mc-cronos
```

This will result in the `image_url` being `quay.io/yourstruly/cronos-mc-cronos-${APP}`.

The `${APP}` should remain configurable for other Make recipes to work as expected so do not overwrite the `app` parameter.

# Thanks

- [Golang AES Encryption/Decryption example](https://gist.github.com/mickelsonm/e1bf365a149f3fe59119)

# Cheers
