ARG GO_VERSION=1.18
FROM golang:${GO_VERSION}-alpine AS build
RUN apk add --no-cache make g++ ca-certificates
WORKDIR /go/src/app
COPY ./go.mod ./go.sum ./
RUN go mod download -x
COPY ./cmd ./cmd
COPY ./internal ./internal
COPY ./pkg ./pkg
COPY ./Makefile ./
# this should be set by the build recipe in the Makefile
ARG APP=ebisusbay
ARG RELEASE_TAG=latest
ENV RELEASE_TAG=${RELEASE_TAG}
ARG RELEASE_VERSION=latest
ENV RELEASE_VERSION=${RELEASE_VERSION}
RUN make build app=${APP} release_version=${RELEASE_VERSION} release_tag=${RELEASE_TAG}
RUN mv ./bin/${APP}_${RELEASE_VERSION}-${RELEASE_TAG}_$(go env GOOS)_$(go env GOARCH) ./bin/${APP}
RUN mv ./bin/${APP}_${RELEASE_VERSION}-${RELEASE_TAG}_$(go env GOOS)_$(go env GOARCH).sha256 ./bin/${APP}.sha256

FROM scratch AS final
ARG APP=ebisusbay
ENV APP=${APP}
COPY --from=build /go/src/app/bin/${APP} /app
COPY --from=build /go/src/app/bin/${APP}.sha256 /app.sha256
COPY --from=build /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt
COPY --from=build /go/src/app/internal/migrations/* /go/src/app/cmd/${APP}/migrations/* /data/migrations/
ENTRYPOINT ["/app"]
LABEL repo_url https://github.com/zephinzer/go-cronos
LABEL maintainer ${APP}
