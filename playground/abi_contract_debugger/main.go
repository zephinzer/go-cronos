package abi_debugger

import (
	"bytes"
	"fmt"

	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/common"
)

// put your abi into this directory as a json file named
// contract.json and this embed below will pick it up
// - - -
//go:embed contract.json
var contractAbi []byte

func main() {
	contract, err := abi.JSON(bytes.NewReader(contractAbi))
	if err != nil {
		panic(fmt.Errorf("failed to read contract of package: %s", err))
	}

	fmt.Println("- - - methods - - -")
	for method, content := range contract.Methods {
		fmt.Printf("%s => %s (%s)\n\n", method, content.Sig, common.Bytes2Hex(content.ID))
	}
}
