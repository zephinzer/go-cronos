package main

import (
	"context"
	"math/big"

	"github.com/ethereum/go-ethereum/ethclient"
)

const (
	RpcUrl      = "https://rpc.vvs.finance"
	BlockNumber = 1
)

func main() {
	client, err := ethclient.Dial(RpcUrl)
	if err != nil {
		panic(err)
	}
	client.BlockByNumber(context.Background(), big.NewInt(BlockNumber))
}
