package abi_debugger

import (
	"bytes"
	"fmt"

	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/common"
)

// contactAbi
// TODO(user): put your abi into this directory as a json
// file named 'contract.json' and this embed below will
// pick it up
//
//go:embed contract.json
var contractAbi []byte

// inputData
// TODO(user): put your input data into a text file named
// 'input' and this embed below will pick it up for parsing
//
//go:embed input
var inputData []byte

// methodName: TODO(user): defines the method to call
const methodName = "aggregate"

func main() {
	contract, err := abi.JSON(bytes.NewReader(contractAbi))
	if err != nil {
		panic(fmt.Errorf("failed to read contract of package[ebisusbay]: %s", err))
	}

	fmt.Println("- - - methods - - -")
	for method, content := range contract.Methods {
		fmt.Printf("%s => %s (%s)\n\n", method, content.Sig, common.Bytes2Hex(content.ID))
	}

	unpackedData, err := contract.Methods[methodName].Inputs.Unpack(
		common.Hex2Bytes(string(inputData)),
	)
	if err != nil {
		fmt.Println(err)
	}

	// TODO(user): replace the below to correctly unpack the
	// expected data into a data structure

	realUnpackedData := unpackedData[0].([]struct {
		Target   common.Address `json:"target"`
		CallData []uint8        `json:"callData"`
	})
	for _, rudInstance := range realUnpackedData {
		fmt.Println("---")
		fmt.Println(rudInstance.Target)
		fmt.Println(common.Bytes2Hex([]byte(rudInstance.CallData)))
		fmt.Println("---")
	}
}
