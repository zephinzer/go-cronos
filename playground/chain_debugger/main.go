package chain_debugger

import (
	"fmt"
	"os"
	"sync"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/zephinzer/go-cronos/pkg/evmutils"
)

func main() {
	if err := GetCommand().Execute(); err != nil {
		logrus.Errorf("failed to complete successfully: %s", err)
		os.Exit(1)
	}
}

func GetCommand() *cobra.Command {
	command := cobra.Command{
		RunE: run,
	}
	return &command
}

func run(command *cobra.Command, args []string) error {
	var waiter sync.WaitGroup
	events := make(chan evmutils.WatchEvent, 100)
	waiter.Add(1)

	go func() {
		evmutils.WatchTransactions(evmutils.WatchTransactionsOpts{
			Events: events,
		})
	}()

	go func() {
		for {
			event := <-events
			switch event.Type {
			case evmutils.EventTransaction:
				// TODO(user): add in whatever you wanna play with
				// each transaction here
				fmt.Println("transaction received")
			}
		}
	}()

	waiter.Wait()
	return nil
}
