package ebisusbayapi

import (
	"fmt"
	"net/url"
	"path"
	"strings"
)

func GetRarity(collectionAddress string) (*RarityResponse, error) {
	appUrl, err := url.Parse(AppBaseUrl)
	if err != nil {
		return nil, fmt.Errorf("failed to parse app base url '%s': %s", ApiBaseUrl, err)
	}
	appUrl.Path = path.Join(appUrl.Path, PathFiles, collectionAddress, FilenameRarity)
	rarityUrl := strings.ToLower(appUrl.String())

	var response RarityResponse
	if err := send(sendOpts{
		TargetUrl:        rarityUrl,
		ResponseReceiver: &response,
	}); err != nil {
		return nil, fmt.Errorf("failed to send query: %s", err)
	}
	return &response, nil
}
