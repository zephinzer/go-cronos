package ebisusbayapi

type Direction string
type SortKey string

const (
	ApiBaseUrl = "https://api.ebisusbay.com"
	AppBaseUrl = "https://app.ebisusbay.com"
	// GraphqlBaseUrl is the original GraphQL endpoint that Ebisusbay used
	GraphqlBaseUrl = "https://graph.ebisusbay.com:8000"
	// GraphqlBizBaseUrl is the updated GraphQL endpoint we found on 2022-10-08.
	// Why they did this leaves me clueless
	GraphqlBizBaseUrl = "https://graph.ebisusbay.biz:8000"

	DirectionAscending  Direction = "asc"
	DirectionDescending Direction = "desc"

	FilenameRarity = "rarity.json"

	PathCollection  = "/fullcollections"
	PathCollections = "/collections"
	PathFiles       = "/files"
	PathListings    = "/listings"
	PathNft         = "/nft"
	PathOffers      = "/subgraphs/name/ebisusbay/offers"
	PathOffers2     = "/subgraphs/name/ebisusbay/offers2"

	QueryKeyAddress    = "address"
	QueryKeyCollection = "collection"
	QueryKeyDirection  = "direction"
	QueryKeyListingId  = "listingId"
	QueryKeyPage       = "page"
	QueryKeyPageSize   = "pageSize"
	QueryKeySortBy     = "sortBy"
	QueryKeyState      = "state"
	QueryKeyTokenId    = "tokenId"
	QueryKeyTraits     = "traits"

	SortKeyTotalVolume SortKey = "totalVolume"
	SortKeyPrice       SortKey = "price"
	SortKeyRank        SortKey = "rank"
	SortKeySaleTime    SortKey = "saleTime"
	SortKeyTokenID     SortKey = "id"

	StateSold = "1"
)
