package ebisusbayapi

import (
	"fmt"
	"net/url"
	"strings"
)

type GetCollectionsOpts struct {
	Address   string
	Direction Direction
	SortBy    SortKey
}

func (gco *GetCollectionsOpts) Validate() error {
	var errors []string

	if gco.Direction == "" {
		gco.Direction = DirectionDescending
	}

	if gco.SortBy == "" {
		gco.SortBy = SortKeyTotalVolume
	}

	if len(errors) > 0 {
		return fmt.Errorf("failed to validate GetCollectionOpts instance: ['%s']", strings.Join(errors, "', '"))
	}
	return nil
}

func GetCollections(opts GetCollectionsOpts) (*CollectionSummariesResponse, error) {
	if err := opts.Validate(); err != nil {
		return nil, fmt.Errorf("failed to receive a valid options object: %s", err)
	}
	baseUrl, err := url.Parse(ApiBaseUrl)
	if err != nil {
		return nil, fmt.Errorf("failed to parse api base url '%s': %s", ApiBaseUrl, err)
	}
	baseUrl.Path = PathCollections
	query := url.Values{}
	if opts.Address != "" {
		query.Add(QueryKeyCollection, opts.Address)
	}
	query.Add(QueryKeyDirection, string(opts.Direction))
	query.Add(QueryKeySortBy, string(opts.SortBy))
	baseUrl.RawQuery = query.Encode()
	collectionSummariesUrl := baseUrl.String()

	var response CollectionSummariesResponse
	if err := send(sendOpts{
		TargetUrl:        collectionSummariesUrl,
		ResponseReceiver: &response,
	}); err != nil {
		return nil, fmt.Errorf("failed to send query: %s", err)
	}

	response.Metadata.RequestUrl = collectionSummariesUrl
	return &response, nil
}
