package ebisusbayapi

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"time"
)

type sendOpts struct {
	TargetUrl        string
	ResponseReceiver interface{}
	RawResponse      *bytes.Buffer
	Body             []byte
}

func send(opts sendOpts) error {
	targetUrl := opts.TargetUrl
	var req *http.Request
	var err error
	if opts.Body != nil && len(opts.Body) > 0 {
		req, err = http.NewRequest(http.MethodPost, targetUrl, bytes.NewBuffer(opts.Body))
	} else {
		req, err = http.NewRequest(http.MethodGet, targetUrl, nil)
	}
	if err != nil {
		return fmt.Errorf("failed to create http request: %s", err)
	}
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return fmt.Errorf("failed to perform http request: %s", err)
	}
	if res.StatusCode == http.StatusTooManyRequests {
		<-time.After(3 * time.Second)
		return send(opts)
	}
	resBody, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return fmt.Errorf("failed to read response body: %s", err)
	}
	if opts.RawResponse != nil {
		io.Copy(opts.RawResponse, bytes.NewReader(resBody))
	}
	if err := json.Unmarshal(resBody, opts.ResponseReceiver); err != nil {
		var genericStructure map[string]interface{}
		json.Unmarshal(resBody, &genericStructure)
		prettyJson, err := json.MarshalIndent(genericStructure, "", "  ")
		if err != nil {
			return fmt.Errorf("failed to unmarshal response body into a collection response: %s | raw response follows:\n--BEGIN--\n%s\n--END--", err, string(resBody))
		}
		return fmt.Errorf("failed to unmarshal response body into a collection response: %s | raw response follows:\n--BEGIN--\n%s\n--END--", err, prettyJson)
	}

	return nil
}
