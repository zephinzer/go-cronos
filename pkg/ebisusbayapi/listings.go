package ebisusbayapi

import (
	"fmt"
	"net/url"
	"strconv"
)

type GetListingsOpts struct {
	// Address is the address of the collection
	Address string

	// TokenId is the asset ID of the collection
	TokenId int64
}

func GetListings(opts GetListingsOpts) (*ListingsResponse, error) {
	baseUrl, err := url.Parse(ApiBaseUrl)
	if err != nil {
		return nil, fmt.Errorf("failed to parse api base url '%s': %s", ApiBaseUrl, err)
	}
	baseUrl.Path = PathNft
	query := url.Values{}
	query.Add(QueryKeyCollection, opts.Address)
	query.Add(QueryKeyTokenId, strconv.FormatInt(opts.TokenId, 10))
	baseUrl.RawQuery = query.Encode()
	listingsUrl := baseUrl.String()

	var response ListingsResponse
	if err := send(sendOpts{
		TargetUrl:        listingsUrl,
		ResponseReceiver: &response,
	}); err != nil {
		return nil, fmt.Errorf("failed to send query: %s", err)
	}
	response.Metadata.RequestUrl = listingsUrl
	return &response, nil
}
