package ebisusbayapi

import (
	"fmt"
	"strconv"
	"strings"
	"time"
)

type Attributes []Attribute

type Attribute struct {
	TraitType string `json:"trait_type"`

	// Value can be either a number or string
	Value interface{} `json:"value"`

	Occurrence float64 `json:"occurrence"`
}

type Collection struct {
	Nfts        Nfts                         `json:"nfts"`
	Summary     CollectionSummary            `json:"summary"`
	TraitRarity map[string]map[string]Rarity `json:"traitRarity"`
}

type CollectionSummariesResponse struct {
	Status      int                 `json:"status"`
	Collections CollectionSummaries `json:"collections"`
	Metadata    requestMetadata     `json:"metadata"`
}

type CollectionResponse struct {
	Nfts       Nfts            `json:"nfts"`
	Page       int             `json:"page"`
	PageSize   int             `json:"pageSize"`
	Status     int             `json:"status"`
	TotalCount int             `json:"totalCount"`
	TotalPages int             `json:"totalPages"`
	Metadata   requestMetadata `json:"metadata"`
}

type CollectionsResponse struct {
	Status      int                 `json:"status"`
	Collections CollectionSummaries `json:"collections"`
	Metadata    requestMetadata     `json:"metadata"`
}

type CollectionSummaries []CollectionSummary

type CollectionSummary struct {
	Sales1D          string `json:"sales1d"`
	Sales7D          string `json:"sales7d"`
	Sales30D         string `json:"sales30d"`
	Volume1D         string `json:"volume1d"`
	Volume7D         string `json:"volume7d"`
	TotalFees        string `json:"totalFees"`
	Volume30D        string `json:"volume30d"`
	Collection       string `json:"collection"`
	FloorPrice       string `json:"floorPrice"`
	TotalVolume      string `json:"totalVolume"`
	NumberActive     string `json:"numberActive"`
	NumberOfSales    string `json:"numberOfSales"`
	TotalRoyalties   string `json:"totalRoyalties"`
	AverageSalePrice string `json:"averageSalePrice"`
}

type ListingResponse struct {
	Status   int             `json:"status"`
	Listings Listings        `json:"listings"`
	Metadata requestMetadata `json:"metadata"`
}

type ListingsResponse struct {
	Status   int             `json:"status"`
	Listings Listings        `json:"listings"`
	Nft      Nft             `json:"nft"`
	Metadata requestMetadata `json:"metadata"`
}

func (lr ListingsResponse) GetNewest() *Listing {
	latestListingIndex := -1
	for index, listing := range lr.Listings {
		if latestListingIndex == -1 || listing.ListingTime > lr.Listings[latestListingIndex].ListingTime {
			latestListingIndex = index
		}
	}
	if latestListingIndex == -1 {
		return nil
	}
	return &lr.Listings[latestListingIndex]
}

type Listings []Listing
type Listing struct {
	ListingId   int64  `json:"listingId"`
	ListingTime int64  `json:"listingTime"`
	Nft         *Nft   `json:"nft,omitempty"`
	NftAddress  string `json:"nftAddress"`
	NftId       string `json:"nftId"`
	Price       string `json:"price"`
	Purchaser   string `json:"purchaser"`
	SaleTime    int64  `json:"saleTime"`
	Seller      string `json:"seller"`
	Valid       bool   `json:"valid"`
}

type Nfts []Nft

type Nft struct {
	Address     string     `json:"address"`
	Attributes  Attributes `json:"attributes"`
	Description string     `json:"description"`

	// Edition can be either a number or a string
	Edition       interface{} `json:"edition"`
	ID            string      `json:"id"`
	Image         string      `json:"image"`
	Market        NftMarket   `json:"market"`
	Name          string      `json:"name"`
	NftAddress    string      `json:"nftAddress"`
	NftId         string      `json:"nftId"`
	OriginalImage string      `json:"original_image"`

	// Rank can be either a number or a string
	Rank  int         `json:"rank"`
	Score float64     `json:"score"`
	Slug  interface{} `json:"slug"`

	AnimationUrl string `json:"animationUrl"`
}

type NftMarket struct {
	ID    interface{} `json:"id"`
	Price string      `json:"price"`
	URI   string      `json:"uri"`
}

type OffersResponse struct {
	Data     *OffersData     `json:"data"`
	Metadata requestMetadata `json:"metadata"`
}

type OffersData struct {
	Offers Offers `json:"offers"`
}

type Offers []Offer

func (o Offers) GetLatestOfferBy(address string) *Offer {
	alreadySetOnce := false
	var latestOffer *Offer = nil
	for _, offer := range o {
		normalisedBuyerAddress := strings.ToLower(*offer.Buyer)
		normalisedTargetAddress := strings.ToLower(address)
		if normalisedBuyerAddress != normalisedTargetAddress {
			continue
		}
		if !alreadySetOnce {
			latestOffer = &offer
			alreadySetOnce = true
			continue
		}
		currentEpochTime, err := strconv.ParseInt(*offer.TimeCreated, 10, 64)
		if err != nil {
			continue
		}
		currentOfferCreatedAt := time.Unix(currentEpochTime, 0)
		latestEpochTime, err := strconv.ParseInt(*latestOffer.TimeCreated, 10, 64)
		if err != nil {
			continue
		}
		latestOfferCreatedAt := time.Unix(latestEpochTime, 0)
		if currentOfferCreatedAt.After(latestOfferCreatedAt) {
			latestOffer = &offer
		}
	}

	return latestOffer
}

type Offer struct {
	Buyer       *string `json:"buyer"`
	ID          string  `json:"id"`
	Hash        string  `json:"hash"`
	OfferIndex  string  `json:"offerIndex"`
	NftAddress  string  `json:"nftAddress"`
	NftId       string  `json:"nftId"`
	Price       string  `json:"price"`
	Seller      *string `json:"seller"`
	State       string  `json:"state"`
	TimeCreated *string `json:"timeCreated"`
	TimeUpdated *string `json:"timeUpdated"`
	TimeEnded   *string `json:"timeEnded"`
}

type RarityResponse map[string]map[string]Rarity

// ContainsTraits checks to see whether the provided watchedTraitMap
// contains a map of trait types and trait values that are compatible
// with this RarityResponse instance
func (rr RarityResponse) ContainsTraits(watchedTraitMap map[string]string) (bool, error) {
	for traitType, traitValue := range watchedTraitMap {
		if traitValueMap, ok := rr[traitType]; !ok {
			return false, fmt.Errorf("failed to find trait type '%s'", traitType)
		} else if _, ok := traitValueMap[traitValue]; !ok {
			return false, fmt.Errorf("failed to find trait value '%s'", traitValue)
		}
	}
	return true, nil
}

type Rarity struct {
	Count      int     `json:"count"`
	Occurrence float64 `json:"occurrence"`
}

type requestMetadata struct {
	RequestUrl   string `json:"requestUrl"`
	RequestBody  string `json:"requestBody"`
	ResponseBody string `json:"responseBody"`
}

type SalesResponse struct {
	Status     int             `json:"status"`
	Page       int             `json:"page"`
	TotalCount int             `json:"totalCount"`
	PageSize   int             `json:"pageSize"`
	TotalPages int             `json:"totalPages"`
	Listings   SalesListings   `json:"listings"`
	Metadata   requestMetadata `json:"metadata"`
}

type SalesListings []SalesListing

type SalesListing struct {
	ListingID   int              `json:"listingId"`
	Is1155      bool             `json:"is1155"`
	Invalid     int              `json:"invalid"`
	Valid       bool             `json:"valid"`
	Seller      string           `json:"seller"`
	Purchaser   string           `json:"purchaser"`
	State       int              `json:"state"`
	Price       string           `json:"price"`
	Royalty     string           `json:"royalty"`
	Fee         string           `json:"fee"`
	SaleTime    int              `json:"saleTime"`
	ListingTime int              `json:"listingTime"`
	NftID       string           `json:"nftId"`
	NftAddress  string           `json:"nftAddress"`
	Nft         *SalesListingNft `json:"nft"`
}

type SalesListingNft struct {
	NftID      string `json:"nftId"`
	NftAddress string `json:"nftAddress"`

	// Edition can be either a number or a string
	Edition       interface{} `json:"edition"`
	Name          string      `json:"name"`
	Image         string      `json:"image"`
	OriginalImage string      `json:"original_image"`
	Description   string      `json:"description"`

	// Rank can be either a number or a string
	Rank       int         `json:"rank"`
	Attributes *Attributes `json:"attributes"`
}

type Traits map[string][]string
