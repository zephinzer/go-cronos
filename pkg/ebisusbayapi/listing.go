package ebisusbayapi

import (
	"fmt"
	"net/url"
)

func GetListingById(listingId string) (*ListingResponse, error) {
	baseUrl, err := url.Parse(ApiBaseUrl)
	if err != nil {
		return nil, fmt.Errorf("failed to parse api base url '%s': %s", ApiBaseUrl, err)
	}
	baseUrl.Path = PathListings
	query := url.Values{}
	query.Add(QueryKeyListingId, listingId)
	baseUrl.RawQuery = query.Encode()
	listingUrl := baseUrl.String()

	var response ListingResponse
	if err := send(sendOpts{
		TargetUrl:        listingUrl,
		ResponseReceiver: &response,
	}); err != nil {
		return nil, fmt.Errorf("failed to send query: %s", err)
	}
	response.Metadata.RequestUrl = listingUrl
	return &response, nil
}
