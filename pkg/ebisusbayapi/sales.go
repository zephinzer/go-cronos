package ebisusbayapi

import (
	"fmt"
	"net/url"
	"strconv"
	"strings"
)

type GetSalesOpts struct {
	// Address of the collection
	Address string

	// Direction of ordering
	Direction Direction
	Page      uint64
	PageSize  uint64
	SortBy    SortKey
}

func (glo *GetSalesOpts) Validate() error {
	var errors []string

	if glo.Address == "" {
		errors = append(errors, "address property must be specified")
	}
	if glo.Direction == "" {
		glo.Direction = DirectionDescending
	}
	if glo.Page == 0 {
		glo.Page = 1
	}
	if glo.PageSize < 2 {
		glo.PageSize = 50
	}
	if glo.SortBy == "" {
		glo.SortBy = SortKeySaleTime
	}

	if len(errors) > 0 {
		return fmt.Errorf("failed to validate GetCollectionOpts instance: ['%s']", strings.Join(errors, "', '"))
	}
	return nil
}

func GetSales(opts GetSalesOpts) (*SalesResponse, error) {
	if err := opts.Validate(); err != nil {
		return nil, fmt.Errorf("failed to validate GetSalesOpts: %s", err)
	}
	baseUrl, err := url.Parse(ApiBaseUrl)
	if err != nil {
		return nil, fmt.Errorf("failed to parse api base url '%s': %s", ApiBaseUrl, err)
	}
	baseUrl.Path = PathListings
	query := url.Values{}
	query.Add(QueryKeyCollection, opts.Address)
	query.Add(QueryKeyDirection, string(DirectionDescending))
	query.Add(QueryKeySortBy, string(SortKeySaleTime))
	query.Add(QueryKeyPage, strconv.FormatUint(opts.Page, 10))
	query.Add(QueryKeyPageSize, strconv.FormatUint(opts.PageSize, 10))
	query.Add(QueryKeyState, StateSold)
	baseUrl.RawQuery = query.Encode()
	salesUrl := baseUrl.String()

	var response SalesResponse
	if err := send(sendOpts{
		TargetUrl:        salesUrl,
		ResponseReceiver: &response,
	}); err != nil {
		return nil, fmt.Errorf("failed to send query: %s", err)
	}

	response.Metadata.RequestUrl = salesUrl
	return &response, nil
}
