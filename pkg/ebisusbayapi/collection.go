package ebisusbayapi

import (
	"encoding/json"
	"fmt"
	"net/url"
	"strconv"
	"strings"
)

type GetCollectionOpts struct {
	Address   string
	Direction Direction
	Page      int
	PageSize  int
	SortBy    SortKey
	Traits    Traits
}

func (gco *GetCollectionOpts) Validate() error {
	var errors []string

	if gco.Address == "" {
		errors = append(errors, "address property must be specified")
	}
	if gco.Direction == "" {
		gco.Direction = DirectionAscending
	}
	if gco.Page == 0 {
		gco.Page = 1
	}
	if gco.PageSize == 0 {
		gco.PageSize = 25
	}
	if gco.SortBy == "" {
		gco.SortBy = SortKeyPrice
	}

	if len(errors) > 0 {
		return fmt.Errorf("failed to validate GetCollectionOpts instance: ['%s']", strings.Join(errors, "', '"))
	}
	return nil
}

func GetCollection(opts GetCollectionOpts) (*CollectionResponse, error) {
	if err := opts.Validate(); err != nil {
		return nil, fmt.Errorf("failed to receive a valid options object: %s", err)
	}
	baseUrl, err := url.Parse(ApiBaseUrl)
	if err != nil {
		return nil, fmt.Errorf("failed to parse api base url '%s': %s", ApiBaseUrl, err)
	}
	baseUrl.Path = PathCollection
	query := url.Values{}
	query.Add(QueryKeyAddress, opts.Address)
	query.Add(QueryKeyDirection, string(opts.Direction))
	query.Add(QueryKeyPage, strconv.Itoa(opts.Page))
	query.Add(QueryKeyPageSize, strconv.Itoa(opts.PageSize))
	query.Add(QueryKeySortBy, string(opts.SortBy))
	baseUrl.RawQuery = query.Encode()

	// this weird block below is because adding the query via the
	// query.Add(...) method causes the + symbol to be converted
	// to %2B which destroys the traits filter (this may change
	// in future, if this breaks, that should be it)
	var traitsQueryString string
	if opts.Traits != nil {
		traitsJson, err := json.Marshal(opts.Traits)
		if err == nil {
			traitsQueryString = url.QueryEscape(string(traitsJson))
		}
	}
	collectionUrl := baseUrl.String()
	if len(traitsQueryString) > 0 {
		collectionUrl += fmt.Sprintf("&%s=%s", QueryKeyTraits, traitsQueryString)
	}
	var response CollectionResponse
	if err := send(sendOpts{
		TargetUrl:        collectionUrl,
		ResponseReceiver: &response,
	}); err != nil {
		return nil, fmt.Errorf("failed to send query: %s", err)
	}

	response.Metadata.RequestUrl = collectionUrl
	return &response, nil
}
