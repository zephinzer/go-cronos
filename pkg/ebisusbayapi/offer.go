package ebisusbayapi

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/url"
)

type GetOffersOpts struct {
	// Address is the address of the collection
	Address string

	// TokenId is the asset ID of the collection
	TokenId int64
}

func GetOffers(opts GetOffersOpts) (*OffersResponse, error) {
	baseUrl, err := url.Parse(GraphqlBizBaseUrl)
	if err != nil {
		return nil, fmt.Errorf("failed to parse api base url '%s': %s", ApiBaseUrl, err)
	}
	baseUrl.Path = PathOffers2
	offerUrl := baseUrl.String()

	requestBody, err := json.Marshal(map[string]interface{}{
		"query": fmt.Sprintf(
			"query ($first: Int) {\n  offers(\n    first: 1000\n    where: {nftAddress: \"%s\", nftId: \"%v\"}\n  ) {\n    id\n    hash\n    offerIndex\n    nftAddress\n    nftId\n    buyer\n    seller\n    coinAddress\n    price\n    state\n    timeCreated\n    timeUpdated\n    timeEnded\n    __typename\n  }\n}",
			opts.Address,
			opts.TokenId,
		),
		"variables": struct{}{},
	})
	if err != nil {
		return nil, fmt.Errorf("failed to create request body: %s", err)
	}

	var response OffersResponse
	if err := send(sendOpts{
		TargetUrl:        offerUrl,
		ResponseReceiver: &response,
		Body:             requestBody,
	}); err != nil {
		return nil, fmt.Errorf("failed to send query: %s", err)
	}
	response.Metadata.RequestUrl = offerUrl
	return &response, nil
}

func GetOfferByHash(offerHash string) (*OffersResponse, error) {
	baseUrl, err := url.Parse(GraphqlBizBaseUrl)
	if err != nil {
		return nil, fmt.Errorf("failed to parse api base url '%s': %s", ApiBaseUrl, err)
	}
	baseUrl.Path = PathOffers2
	offerUrl := baseUrl.String()

	requestBody, err := json.Marshal(map[string]interface{}{
		"query": "query ($first: Int, $hash: String, $lastId: String) {\n  offers(first: $first, where: {hash: $hash, id_gt: $lastId}) {\n    id\n    hash\n    offerIndex\n    nftAddress\n    nftId\n    buyer\n    seller\n    coinAddress\n    price\n    state\n    timeCreated\n    timeUpdated\n    timeEnded\n    __typename\n  }\n}",
		"variables": map[string]interface{}{
			"hash":   offerHash,
			"first":  1000,
			"lastId": "",
		},
	})
	if err != nil {
		return nil, fmt.Errorf("failed to create request body: %s", err)
	}

	rawResponse := bytes.Buffer{}
	var response OffersResponse
	if err := send(sendOpts{
		TargetUrl:        offerUrl,
		RawResponse:      &rawResponse,
		ResponseReceiver: &response,
		Body:             requestBody,
	}); err != nil {
		return nil, fmt.Errorf("failed to send query: %s", err)
	}
	response.Metadata.RequestUrl = offerUrl
	response.Metadata.RequestBody = string(requestBody)
	response.Metadata.ResponseBody = rawResponse.String()
	return &response, nil
}

func GetWalletOffers(walletAddress string) (*OffersResponse, error) {
	baseUrl, err := url.Parse(GraphqlBaseUrl)
	if err != nil {
		return nil, fmt.Errorf("failed to parse api base url '%s': %s", ApiBaseUrl, err)
	}
	baseUrl.Path = PathOffers
	offerUrl := baseUrl.String()

	requestBody, err := json.Marshal(map[string]interface{}{
		"query": "query ($first: Int, $buyer: String, $state: String, $lastId: String) {\n  offers(first: $first, where: {buyer: $buyer, state: $state, id_gt: $lastId}) {\n    id\n    hash\n    offerIndex\n    nftAddress\n    nftId\n    buyer\n    seller\n    coinAddress\n    price\n    state\n    timeCreated\n    timeUpdated\n    timeEnded\n    __typename\n  }\n}",
		"variables": map[string]interface{}{
			"buyer":  walletAddress,
			"first":  1000,
			"lastId": "",
			"state":  "0",
		},
	})
	if err != nil {
		return nil, fmt.Errorf("failed to create request body: %s", err)
	}

	var response OffersResponse
	if err := send(sendOpts{
		TargetUrl:        offerUrl,
		ResponseReceiver: &response,
		Body:             requestBody,
	}); err != nil {
		return nil, fmt.Errorf("failed to send query: %s", err)
	}
	response.Metadata.RequestUrl = offerUrl
	return &response, nil
}
