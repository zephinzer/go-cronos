package erc20

import (
	"fmt"
	"math"
	"math/big"

	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/zephinzer/go-cronos/pkg/evmutils"
)

// Balance represents balances data for the requested
// token. Properties which are pointers
// contain values which might not be available in
// all contracts
type Balance struct {
	Address       common.Address `json:"address"`
	TokenAddress  common.Address `json:"tokenAddress"`
	TokenAmount   float64        `json:"tokenAmount"`
	TokenDecimals float64        `json:"tokenDecimals"`
	TokenSymbol   string         `json:"tokenSymbol"`
}

// GetBalanceEvent is the structure of events emitted by the
// GetBalance() method
type GetBalanceEvent struct {
	Message string
	IsError bool
	IsDone  bool
}

// GetBalanceOpts configures the GetBalance() method
type GetBalanceOpts struct {
	// TokenAddress is the address of the ERC20 token of interest
	TokenAddress common.Address

	// HolderAddress is the address of the wallet holding the
	// ERC20 tokens
	HolderAddress common.Address

	// Client provides an existing EVM-compatible client to
	// use to interact with the chain. If not provided, falls
	// back to creating a client based on the RpcUrl
	Client *ethclient.Client

	// Events provides a way for the GetBalance method to bubble
	// up emitted events from sub-functions
	Events chan GetBalanceEvent

	// RpcUrl defines the RPC URL to use to connect to the
	// chain. Ignored if .Client property is defined
	RpcUrl string
}

// GetBalance returns balanace information about an EVM-
// compatible address for a given ERC20 token address
//
// If the EVM client is not provided, a default one on
// Ethereum mainnet (or using RpcUrl if defined) will
// be created
func GetBalance(opts GetBalanceOpts) (*Balance, error) {
	// initialise return object
	balanceInstance := Balance{
		Address:      opts.HolderAddress,
		TokenAddress: opts.TokenAddress,
	}

	if opts.Events == nil {
		opts.Events = make(chan GetBalanceEvent, 10)
		defer close(opts.Events)
	}

	// intiialise client
	var client *ethclient.Client
	var chainId *big.Int
	if opts.Client == nil {
		rpcUrl := DefaultRpcUrl
		if opts.RpcUrl != "" {
			rpcUrl = opts.RpcUrl
		}
		var err error
		clientEvents := make(chan evmutils.NewClientEvent, 10)
		go func() {
			for {
				select {
				case event := <-clientEvents:
					if event.Error != nil {
						opts.Events <- GetBalanceEvent{
							Message: event.Error.Error(),
							IsError: true,
						}
					} else {
						opts.Events <- GetBalanceEvent{Message: event.Message}
					}
					continue
				default:
				}
			}
		}()
		if client, chainId, err = evmutils.NewClient(evmutils.NewClientOpts{
			Events:        clientEvents,
			RpcUrl:        rpcUrl,
			MaxRetries:    DefaultMaxRetries,
			RetryInterval: DefaultRetryInterval,
		}); err != nil {
			return nil, fmt.Errorf("failed to create a default evm client: %s", err)
		}
		opts.Events <- GetBalanceEvent{
			Message: fmt.Sprintf("connected to chain[%v] via rpc[%s]", chainId.Int64(), rpcUrl),
		}
		// close the client only if we create it, otherwise fk the consumer
		defer client.Close()
	} else {
		client = opts.Client
	}

	// initialise
	opts.Events <- GetBalanceEvent{Message: fmt.Sprintf("binding erc20 contract to address[%s]", opts.TokenAddress)}
	boundContract := bind.NewBoundContract(opts.TokenAddress, Contract, client, nil, nil)
	var results []interface{}

	results = []interface{}{}
	if err := boundContract.Call(&bind.CallOpts{}, &results, "decimals"); err != nil {
		return nil, fmt.Errorf("failed to call decimals(): %s", err)
	}
	decimalPlaces := results[0].(uint8)
	balanceInstance.TokenDecimals = math.Pow(10, float64(decimalPlaces))

	results = []interface{}{}
	if err := boundContract.Call(&bind.CallOpts{}, &results, "balanceOf", opts.HolderAddress); err != nil {
		return nil, fmt.Errorf("failed to call balanceOf(%s): %s", opts.HolderAddress, err)
	}
	tokenBalance := big.NewFloat(0).SetInt(results[0].(*big.Int))
	balanceInstance.TokenAmount, _ = big.NewFloat(0).Quo(tokenBalance, big.NewFloat(balanceInstance.TokenDecimals)).Float64()

	results = []interface{}{}
	if err := boundContract.Call(&bind.CallOpts{}, &results, "symbol"); err != nil {
		return nil, fmt.Errorf("failed to call symbol(): %s", err)
	}
	balanceInstance.TokenSymbol = results[0].(string)

	return &balanceInstance, nil
}
