package erc20

import "time"

const (
	DefaultRpcUrl         = "https://cloudflare-eth.com/"
	DefaultBinanceRpcUrl  = "https://bsc-dataseed3.binance.org/"
	DefaultCronosRpcUrl   = "https://evm.cronos.org/"
	DefaultFantomRpcUrl   = "https://rpc.ankr.com/fantom/"
	DefaultOptimismRpcUrl = "https://mainnet.optimism.io"
	DefaultPolygonRpcUrl  = "https://polygon-rpc.com/"

	DefaultMaxRetries    = 5
	DefaultRetryInterval = 1 * time.Second
)
