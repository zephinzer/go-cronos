package erc20

import (
	"bytes"
	_ "embed"
	"fmt"

	"github.com/ethereum/go-ethereum/accounts/abi"
)

//go:embed contract.json
var contract []byte

var Contract abi.ABI

func init() {
	var err error
	Contract, err = abi.JSON(bytes.NewReader(contract))
	if err != nil {
		panic(fmt.Errorf("failed to read contract of package[erc20]: %s", err))
	}
}
