package erc20

import (
	"fmt"
	"math"
	"math/big"

	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/zephinzer/go-cronos/internal/log"
	"github.com/zephinzer/go-cronos/pkg/evmutils"
)

// TokenInfo represents universal data about
// an ERC20 token. Properties which are pointers
// contain values which might not be available in
// all contracts
type TokenInfo struct {
	Address       common.Address  `json:"address"`
	Decimals      int             `json:"decimals"`
	Name          string          `json:"name"`
	Owner         *common.Address `json:"owner"`
	PreMineSupply *float64        `json:"preMineSupply"`
	Symbol        string          `json:"symbol"`
	TotalSupply   float64         `json:"totalSupply"`
}

// GetInfoEvent is the structure of events emitted by the
// GetInfo() method
type GetInfoEvent struct {
	Message string
	IsError bool
	IsDone  bool
}

// GetInfoOpts configures the GetInfo() method
type GetInfoOpts struct {
	// Address is the address of the ERC20 token of interest
	Address common.Address

	// Client provides an existing EVM-compatible client to
	// use to interact with the chain. If not provided, falls
	// back to creating a client based on the RpcUrl
	Client *ethclient.Client

	// Events provides a way for the GetInfo method to bubble
	// up emitted events from sub-functions
	Events chan GetInfoEvent

	// RpcUrl defines the RPC URL to use to connect to the
	// chain. Ignored if .Client property is defined
	RpcUrl string
}

// GetInfo returns information about a ERC20 token
// given its address. If the EVM client is not provided,
// a default one on Ethereum mainnet will be created
func GetInfo(opts GetInfoOpts) (*TokenInfo, error) {
	// initialise return object
	tokenInfo := TokenInfo{
		Address: opts.Address,
	}

	if opts.Events == nil {
		opts.Events = make(chan GetInfoEvent, 16)
		// no-op if events is not defined
		go func() {
			for {
				<-opts.Events
			}
		}()
		defer func() {
			defer close(opts.Events)
			opts.Events <- GetInfoEvent{IsDone: true}
		}()
	}

	// intiialise client
	var client *ethclient.Client
	var chainId *big.Int
	if opts.Client == nil {
		rpcUrl := DefaultRpcUrl
		if opts.RpcUrl != "" {
			rpcUrl = opts.RpcUrl
		}
		var err error
		clientEvents := make(chan evmutils.NewClientEvent, 10)
		go func() {
			for {
				select {
				case event := <-clientEvents:
					if event.Error != nil {
						opts.Events <- GetInfoEvent{
							Message: event.Error.Error(),
							IsError: true,
						}
					} else {
						opts.Events <- GetInfoEvent{Message: event.Message}
					}
					if event.Done {
						return
					}
					continue
				default:
				}
			}
		}()
		if client, chainId, err = evmutils.NewClient(evmutils.NewClientOpts{
			Events:        clientEvents,
			RpcUrl:        rpcUrl,
			MaxRetries:    DefaultMaxRetries,
			RetryInterval: DefaultRetryInterval,
		}); err != nil {
			return nil, fmt.Errorf("failed to create a default evm client: %s", err)
		}
		opts.Events <- GetInfoEvent{
			Message: fmt.Sprintf("connected to chain[%v] via rpc[%s]", chainId.Int64(), rpcUrl),
		}
		// close the client only if we create it, otherwise fk the consumer
		defer client.Close()
	} else {
		client = opts.Client
	}

	// initialise
	opts.Events <- GetInfoEvent{Message: fmt.Sprintf("binding erc20 contract to address[%s]", opts.Address)}
	boundContract := bind.NewBoundContract(opts.Address, Contract, client, nil, nil)
	var results []interface{}

	// get the symbol
	opts.Events <- GetInfoEvent{Message: fmt.Sprintf("getting symbol for address[%s]", opts.Address)}
	results = []interface{}{}

	if err := boundContract.Call(&bind.CallOpts{}, &results, "symbol"); err != nil {
		return nil, fmt.Errorf("failed to call symbol(): %s", err)
	}
	tokenInfo.Symbol = results[0].(string)

	// get the name
	opts.Events <- GetInfoEvent{Message: fmt.Sprintf("getting name for address[%s]", opts.Address)}
	results = []interface{}{}
	if err := boundContract.Call(&bind.CallOpts{}, &results, "name"); err != nil {
		return nil, fmt.Errorf("failed to call name(): %s", err)
	}
	tokenInfo.Name = results[0].(string)

	// get the number of decimal places to calculate the supplies later
	opts.Events <- GetInfoEvent{Message: fmt.Sprintf("getting decimal places for address[%s]", opts.Address)}
	results = []interface{}{}
	if err := boundContract.Call(&bind.CallOpts{}, &results, "decimals"); err != nil {
		return nil, fmt.Errorf("failed to call decimals(): %s", err)
	}
	decimals := results[0].(uint8)
	decimalPlaceDivisor := math.Pow(10, float64(decimals))
	tokenInfo.Decimals = int(decimals)

	// get the total supply of the token
	opts.Events <- GetInfoEvent{Message: fmt.Sprintf("getting total supply for address[%s]", opts.Address)}
	results = []interface{}{}
	if err := boundContract.Call(&bind.CallOpts{}, &results, "totalSupply"); err != nil {
		return nil, fmt.Errorf("failed to call totalSupply(): %s", err)
	}
	totalSupply := results[0].(*big.Int)
	totalSupplyFloat, _ := big.NewFloat(0).Quo(
		big.NewFloat(0).SetInt(totalSupply),
		big.NewFloat(decimalPlaceDivisor),
	).Float64()
	tokenInfo.TotalSupply = totalSupplyFloat

	// stuff that's potentially not defined

	// get the pre-mine supply
	opts.Events <- GetInfoEvent{Message: fmt.Sprintf("getting pre-mine supply for address[%s]", opts.Address)}
	tokenInfo.PreMineSupply = nil
	preMineSupply := big.NewInt(-1)
	var preMineSupplyFloat float64 = -1
	results = []interface{}{}
	if err := boundContract.Call(&bind.CallOpts{}, &results, "preMineSupply"); err == nil {
		preMineSupply = results[0].(*big.Int)
		preMineSupplyFloat, _ = big.NewFloat(0).Quo(
			big.NewFloat(0).SetInt(preMineSupply),
			big.NewFloat(decimalPlaceDivisor),
		).Float64()
		tokenInfo.PreMineSupply = &preMineSupplyFloat
	}

	// get the owner
	opts.Events <- GetInfoEvent{Message: fmt.Sprintf("getting owner for address[%s]", opts.Address)}
	results = []interface{}{}
	owner := common.Address{}
	if err := boundContract.Call(&bind.CallOpts{}, &results, "getOwner"); err != nil {
		log.Warnf("failed to call getOwner(): %s", err)
	} else {
		owner = results[0].(common.Address)
		tokenInfo.Owner = &owner
	}
	return &tokenInfo, nil
}
