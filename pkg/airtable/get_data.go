package airtable

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"path"
)

// airtableQueryResponse is the structure of the
// data returned by Airtable
type airtableQueryResponse struct {
	Records []interface{} `json:"records"`
	Offset  *string       `json:"offset"`
}

type Data struct {
	Error    error         `json:"error"`
	Metadata Metadata      `json:"metadata"`
	Records  []interface{} `json:"records"`
	Offset   *string       `json:"offset"`
}

type Metadata struct {
	Url string `json:"url"`
}

// GetDataOpts provides configurations for the GetData() method
type GetDataOpts struct {
	// ApiKey is the API key to use to access Airflow with
	ApiKey string

	// BaseId is the ID of the Airtable Base
	BaseId string

	// FilterByFormula is the formula to use to filter output by
	// Reference docs at https://support.airtable.com/docs/formula-field-reference
	FilterByFormula string

	// Offset is the offset to retrieve records starting from
	Offset string

	// TableId is the ID of the Airtable Table
	TableId string
}

// GetData retrieves data from Airtable
func GetData(opts GetDataOpts) Data {
	data := Data{}
	apiUrl, err := url.Parse(ApiBaseUrl)
	if err != nil {
		data.Error = fmt.Errorf("failed to parse base url: %s", err)
		return data
	}
	apiUrl.Path = path.Join(DefaultApiVersion, opts.BaseId, opts.TableId)
	if opts.Offset != "" {
		query := apiUrl.Query()
		query.Add("offset", opts.Offset)
		apiUrl.RawQuery = query.Encode()
	}
	if opts.FilterByFormula != "" {
		query := apiUrl.Query()
		query.Add("filterByFormula", opts.FilterByFormula)
		apiUrl.RawQuery = query.Encode()
	}
	data.Metadata = Metadata{
		Url: apiUrl.String(),
	}
	req, err := http.NewRequest(http.MethodGet, apiUrl.String(), nil)
	if err != nil {
		data.Error = fmt.Errorf("failed to create request: %s", err)
		return data
	}
	req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", opts.ApiKey))
	queryResponse := airtableQueryResponse{}
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		data.Error = fmt.Errorf("failed to perform request: %s", err)
		return data
	}
	responseBody, err := ioutil.ReadAll(res.Body)
	if err != nil {
		data.Error = fmt.Errorf("failed to read response body: %s", err)
		return data
	}
	if res.StatusCode != http.StatusOK {
		data.Error = fmt.Errorf("failed to successfully perform request (status code: %v): %s", res.StatusCode, string(responseBody))
		return data
	}
	if err := json.Unmarshal(responseBody, &queryResponse); err != nil {
		data.Error = fmt.Errorf("failed to unmarshal response body: %s", err)
		return data
	}
	data.Records = queryResponse.Records
	data.Offset = queryResponse.Offset
	return data
}
