package ebisusbay

import (
	"fmt"
	"math/big"
)

func NewMakePurchaseData(listingId int64) ([]byte, error) {
	makePurchase := MakePurchaseData{
		ListingID: big.NewInt(listingId),
	}
	packedData, err := Contract.Methods[MakePurchaseId].Inputs.Pack(makePurchase.ListingID)
	if err != nil {
		return nil, fmt.Errorf("failed to pack data for makePurchase txn: %s", err)
	}
	packedData = append(Contract.Methods[MakePurchaseId].ID, packedData...)
	return packedData, nil
}
