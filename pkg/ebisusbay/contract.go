package ebisusbay

import (
	"bytes"
	_ "embed"
	"fmt"
	"math/big"

	"github.com/ethereum/go-ethereum/accounts/abi"
)

//go:embed contract.json
var contract []byte

const (
	MakeListingId  string = "makeListing"
	MakePurchaseId string = "makePurchase"
)

var Contract abi.ABI
var ContractOffers abi.ABI

func init() {
	var err error
	Contract, err = abi.JSON(bytes.NewReader(contract))
	if err != nil {
		panic(fmt.Errorf("failed to read contract of package[ebisusbay]: %s", err))
	}
}

type MakeListingData struct {
	TokenAddress string
	TokenId      int64
	Amount       *big.Int
}

type MakePurchaseData struct {
	ListingID *big.Int
}
