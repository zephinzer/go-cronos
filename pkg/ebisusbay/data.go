package ebisusbay

import (
	"encoding/json"
	"fmt"

	_ "embed"
)

type StaticData struct {
	KnownContracts []KnownContract `json:"known_contracts"`
}

type KnownContract struct {
	Name     string                `json:"name"`
	Slug     string                `json:"slug"`
	Address  string                `json:"address"`
	Metadata KnownContractMetadata `json:"metadata"`
}

type KnownContractMetadata struct {
	Avatar      string  `json:"avatar"`
	Description string  `json:"description"`
	MaxSupply   int64   `json:"maxSupply"`
	Website     *string `json:"website"`
	Twitter     *string `json:"twitter"`
	Discord     *string `json:"discord"`
	Medium      *string `json:"medium"`
	Instagram   *string `json:"instagram"`
	Telegram    *string `json:"telegram"`
}

//go:embed data.json
var data []byte

// Data contains data from Ebisus Bay that is manually scraped
// from the website from a file named main.10a9b68b.chunk.js
// at time of publishing. Search for "known_contracts" in the
// Sources tab of the Chrome Developer Tools to find this data
// file - the current version could be outdated as new collections
// are added
var Data StaticData

func init() {
	if err := json.Unmarshal(data, &Data); err != nil {
		panic(fmt.Errorf("failed to parse known collections data: %s", err))
	}
}
