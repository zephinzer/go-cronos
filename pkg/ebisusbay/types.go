package ebisusbay

type abiContract abiFunctions

type abiFunctions []abiFunction
type abiFunction struct {
	Inputs          []abiFunctionInput `json:"inputs"`
	StateMutability string             `json:"stateMutability"`
	Name            string             `json:"name"`
	Type            string             `json:"type"`
}
type abiFunctionInput struct {
	InternalType string `json:"internalType`
	Name         string `json:"name"`
	Type         string `json:"type"`
}
