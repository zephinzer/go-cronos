// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package ebisusbay

import (
	"errors"
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// EbisusbayMetaData contains all meta data concerning the Ebisusbay contract.
var EbisusbayMetaData = &bind.MetaData{
	ABI: "[{\"inputs\":[{\"internalType\":\"address\",\"name\":\"tokenAddress\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"makeListing\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_id\",\"type\":\"uint256\"}],\"outputs\":[],\"stateMutability\":\"payable\",\"name\":\"makePurchase\",\"type\":\"function\"}]",
}

// EbisusbayABI is the input ABI used to generate the binding from.
// Deprecated: Use EbisusbayMetaData.ABI instead.
var EbisusbayABI = EbisusbayMetaData.ABI

// Ebisusbay is an auto generated Go binding around an Ethereum contract.
type Ebisusbay struct {
	EbisusbayCaller     // Read-only binding to the contract
	EbisusbayTransactor // Write-only binding to the contract
	EbisusbayFilterer   // Log filterer for contract events
}

// EbisusbayCaller is an auto generated read-only Go binding around an Ethereum contract.
type EbisusbayCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// EbisusbayTransactor is an auto generated write-only Go binding around an Ethereum contract.
type EbisusbayTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// EbisusbayFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type EbisusbayFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// EbisusbaySession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type EbisusbaySession struct {
	Contract     *Ebisusbay        // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// EbisusbayCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type EbisusbayCallerSession struct {
	Contract *EbisusbayCaller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts    // Call options to use throughout this session
}

// EbisusbayTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type EbisusbayTransactorSession struct {
	Contract     *EbisusbayTransactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts    // Transaction auth options to use throughout this session
}

// EbisusbayRaw is an auto generated low-level Go binding around an Ethereum contract.
type EbisusbayRaw struct {
	Contract *Ebisusbay // Generic contract binding to access the raw methods on
}

// EbisusbayCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type EbisusbayCallerRaw struct {
	Contract *EbisusbayCaller // Generic read-only contract binding to access the raw methods on
}

// EbisusbayTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type EbisusbayTransactorRaw struct {
	Contract *EbisusbayTransactor // Generic write-only contract binding to access the raw methods on
}

// NewEbisusbay creates a new instance of Ebisusbay, bound to a specific deployed contract.
func NewEbisusbay(address common.Address, backend bind.ContractBackend) (*Ebisusbay, error) {
	contract, err := bindEbisusbay(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &Ebisusbay{EbisusbayCaller: EbisusbayCaller{contract: contract}, EbisusbayTransactor: EbisusbayTransactor{contract: contract}, EbisusbayFilterer: EbisusbayFilterer{contract: contract}}, nil
}

// NewEbisusbayCaller creates a new read-only instance of Ebisusbay, bound to a specific deployed contract.
func NewEbisusbayCaller(address common.Address, caller bind.ContractCaller) (*EbisusbayCaller, error) {
	contract, err := bindEbisusbay(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &EbisusbayCaller{contract: contract}, nil
}

// NewEbisusbayTransactor creates a new write-only instance of Ebisusbay, bound to a specific deployed contract.
func NewEbisusbayTransactor(address common.Address, transactor bind.ContractTransactor) (*EbisusbayTransactor, error) {
	contract, err := bindEbisusbay(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &EbisusbayTransactor{contract: contract}, nil
}

// NewEbisusbayFilterer creates a new log filterer instance of Ebisusbay, bound to a specific deployed contract.
func NewEbisusbayFilterer(address common.Address, filterer bind.ContractFilterer) (*EbisusbayFilterer, error) {
	contract, err := bindEbisusbay(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &EbisusbayFilterer{contract: contract}, nil
}

// bindEbisusbay binds a generic wrapper to an already deployed contract.
func bindEbisusbay(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(EbisusbayABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Ebisusbay *EbisusbayRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Ebisusbay.Contract.EbisusbayCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Ebisusbay *EbisusbayRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Ebisusbay.Contract.EbisusbayTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Ebisusbay *EbisusbayRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Ebisusbay.Contract.EbisusbayTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Ebisusbay *EbisusbayCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Ebisusbay.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Ebisusbay *EbisusbayTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Ebisusbay.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Ebisusbay *EbisusbayTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Ebisusbay.Contract.contract.Transact(opts, method, params...)
}

// MakeListing is a paid mutator transaction binding the contract method 0x79c7550f.
//
// Solidity: function makeListing(address tokenAddress, uint256 tokenId, uint256 amount) returns()
func (_Ebisusbay *EbisusbayTransactor) MakeListing(opts *bind.TransactOpts, tokenAddress common.Address, tokenId *big.Int, amount *big.Int) (*types.Transaction, error) {
	return _Ebisusbay.contract.Transact(opts, "makeListing", tokenAddress, tokenId, amount)
}

// MakeListing is a paid mutator transaction binding the contract method 0x79c7550f.
//
// Solidity: function makeListing(address tokenAddress, uint256 tokenId, uint256 amount) returns()
func (_Ebisusbay *EbisusbaySession) MakeListing(tokenAddress common.Address, tokenId *big.Int, amount *big.Int) (*types.Transaction, error) {
	return _Ebisusbay.Contract.MakeListing(&_Ebisusbay.TransactOpts, tokenAddress, tokenId, amount)
}

// MakeListing is a paid mutator transaction binding the contract method 0x79c7550f.
//
// Solidity: function makeListing(address tokenAddress, uint256 tokenId, uint256 amount) returns()
func (_Ebisusbay *EbisusbayTransactorSession) MakeListing(tokenAddress common.Address, tokenId *big.Int, amount *big.Int) (*types.Transaction, error) {
	return _Ebisusbay.Contract.MakeListing(&_Ebisusbay.TransactOpts, tokenAddress, tokenId, amount)
}

// MakePurchase is a paid mutator transaction binding the contract method 0xc4175a44.
//
// Solidity: function makePurchase(uint256 _id) payable returns()
func (_Ebisusbay *EbisusbayTransactor) MakePurchase(opts *bind.TransactOpts, _id *big.Int) (*types.Transaction, error) {
	return _Ebisusbay.contract.Transact(opts, "makePurchase", _id)
}

// MakePurchase is a paid mutator transaction binding the contract method 0xc4175a44.
//
// Solidity: function makePurchase(uint256 _id) payable returns()
func (_Ebisusbay *EbisusbaySession) MakePurchase(_id *big.Int) (*types.Transaction, error) {
	return _Ebisusbay.Contract.MakePurchase(&_Ebisusbay.TransactOpts, _id)
}

// MakePurchase is a paid mutator transaction binding the contract method 0xc4175a44.
//
// Solidity: function makePurchase(uint256 _id) payable returns()
func (_Ebisusbay *EbisusbayTransactorSession) MakePurchase(_id *big.Int) (*types.Transaction, error) {
	return _Ebisusbay.Contract.MakePurchase(&_Ebisusbay.TransactOpts, _id)
}
