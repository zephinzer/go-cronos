package secret

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/md5"
	cryptorand "crypto/rand"
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io"
	"math"
	"math/rand"
	"time"
)

type encrypted struct {
	Data  string `json:"d"`
	Nonce int    `json:"n"`
	Time  int64  `json:"t"`
}

func init() {
	rand.Seed(time.Now().UnixNano())
}

func Encrypt(plaintext, key string) (string, error) {
	keyHash := []byte(hashKey(key)[:aes.BlockSize])
	toBeEncrypted := encrypted{
		Data:  plaintext,
		Nonce: rand.Intn(math.MaxInt),
		Time:  time.Now().UnixNano(),
	}
	data, err := json.Marshal(toBeEncrypted)
	if err != nil {
		return "", fmt.Errorf("failed to create encryption object: %s", err)
	}
	block, err := aes.NewCipher(keyHash)
	if err != nil {
		return "", fmt.Errorf("failed to create cipher block: %s", err)
	}
	ciphertext := make([]byte, aes.BlockSize+len(data))
	iv := ciphertext[:aes.BlockSize]
	if _, err := io.ReadFull(cryptorand.Reader, iv); err != nil {
		return "", fmt.Errorf("failed to create iv: %s", err)
	}
	stream := cipher.NewCFBEncrypter(block, iv)
	stream.XORKeyStream(ciphertext[aes.BlockSize:], data)
	return base64.URLEncoding.EncodeToString(ciphertext), nil
}

func Decrypt(ciphertext, key string) (string, error) {
	keyHash := []byte(hashKey(key)[:aes.BlockSize])
	decodedCiphertext, err := base64.URLEncoding.DecodeString(ciphertext)
	if err != nil {
		return "", fmt.Errorf("failed to decode ciphertext: %s", err)
	}
	block, err := aes.NewCipher(keyHash)
	if err != nil {
		return "", fmt.Errorf("failed to create cipher block: %s", err)
	}
	if len(decodedCiphertext) < aes.BlockSize {
		return "", fmt.Errorf("failed to receive a block of sufficient size")
	}
	iv := decodedCiphertext[:aes.BlockSize]
	decodedCiphertext = decodedCiphertext[aes.BlockSize:]
	stream := cipher.NewCFBDecrypter(block, iv)
	stream.XORKeyStream(decodedCiphertext, decodedCiphertext)
	var decrypted encrypted
	if err := json.Unmarshal(decodedCiphertext, &decrypted); err != nil {
		return "", fmt.Errorf("failed to parse decoded object: %s", err)
	}
	return decrypted.Data, nil
}

func hashKey(key string) string {
	keyHash := md5.Sum([]byte(key))
	hash := hex.EncodeToString(keyHash[:])
	return hash
}
