package cronos

import "time"

type RpcUrl string

const (
	DefaultRpc RpcUrl = RpcEvmCronos

	RpcEvmCronos RpcUrl = "https://evm-cronos.crypto.org"
	RpcMmf       RpcUrl = "https://mmf-rpc.xstaking.sg/"
	RpcNebkas    RpcUrl = "https://rpc.nebkas.ro/"
	RpcVvs       RpcUrl = "https://rpc.vvs.finance"
	RpcXstaking  RpcUrl = "https://cronosrpc-2.xstaking.sg"
)

const (
	DefaultWatchInterval = 1 * time.Second
)
