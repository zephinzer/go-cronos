package cronos

import (
	_ "embed"
	"fmt"
	"time"

	"github.com/ethereum/go-ethereum/crypto"
	"gopkg.in/yaml.v3"
)

//go:embed known_functions.yaml
var knownFunctions []byte

var TopicDirectoryInitDuration time.Duration
var TopicDirectory map[string]knownFunction

type knownFunctionList []knownFunction
type knownFunction struct {
	Format     string   `yaml:"format"`
	Method     string   `yaml:"method"`
	Params     []string `yaml:"params"`
	ParamTypes []string `yaml:"paramTypes"`
	Signature  string   `yaml:"signature"`
}

func init() {
	startedInitAt := time.Now()
	var functionList knownFunctionList
	if err := yaml.Unmarshal(knownFunctions, &functionList); err != nil {
		panic(fmt.Errorf("failed to parse known functions into intended data structure: %s", err))
	}
	TopicDirectory = map[string]knownFunction{}
	for _, function := range functionList {
		TopicDirectory[crypto.Keccak256Hash([]byte(function.Signature)).Hex()] = function
	}
	TopicDirectoryInitDuration = time.Now().Sub(startedInitAt)
}
