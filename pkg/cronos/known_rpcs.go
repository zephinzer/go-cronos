package cronos

import (
	_ "embed"

	"github.com/zephinzer/go-cronos/pkg/evmtypes"
	"gopkg.in/yaml.v3"
)

//go:embed known_rpcs.yaml
var knownRPCs []byte

var RPC evmtypes.RpcDirectory
var rpcFileStructure RpcFileStructure

type RpcFileStructure struct {
	RPCs map[string]string `json:"rpcs"`
}

func init() {
	if err := yaml.Unmarshal(knownRPCs, &rpcFileStructure); err != nil {
		panic(err)
	}
	RPC = evmtypes.RpcDirectory{}
	for rpcId, url := range rpcFileStructure.RPCs {
		RPC[rpcId] = &evmtypes.RPC{Url: url}
	}
}
