package cronos

import (
	_ "embed"
	"fmt"
	"strings"
	"time"

	"github.com/zephinzer/go-cronos/pkg/evmtypes"
	"gopkg.in/yaml.v2"
)

//go:embed known_addresses.yaml
var knownAddresses []byte

var DirectoryInitDuration time.Duration
var Directory evmtypes.AddressDirectory

type knownAddressList []knownAddress
type knownAddress struct {
	Address string   `json:"address" yaml:"address"`
	Slugs   []string `json:"slugs" yaml:"slugs"`
	Label   string   `json:"label" yaml:"label"`
	Type    string   `json:"type" yaml:"type"`
}

func init() {
	startedInitAt := time.Now()
	knownAddressListInstance := knownAddressList{}
	if err := yaml.Unmarshal(knownAddresses, &knownAddressListInstance); err != nil {
		panic(fmt.Errorf("failed to unmarshal known_addresses.yaml into a structure: %s", err))
	}
	Directory = evmtypes.AddressDirectory{}
	for _, knownAddressInstance := range knownAddressListInstance {
		address := knownAddressInstance.Address
		normalisedAddress := strings.ToLower(knownAddressInstance.Address)
		Directory[knownAddressInstance.Label] = &evmtypes.AddressDirectoryEntry{
			Address: address,
		}
		// if entry already exists, add label and its slugs
		if _, ok := Directory[address]; ok {
			Directory[address].Aliases = append(Directory[address].Aliases, knownAddressInstance.Label)
			if len(knownAddressInstance.Slugs) > 0 {
				Directory[address].Aliases = append(Directory[address].Aliases, knownAddressInstance.Slugs...)
				for _, slug := range knownAddressInstance.Slugs {
					Directory[slug] = &evmtypes.AddressDirectoryEntry{
						Address: address,
					}
				}
			}
		} else if !ok {
			addressEntry := &evmtypes.AddressDirectoryEntry{
				Address: address,
				Label:   knownAddressInstance.Label,
				Aliases: knownAddressInstance.Slugs,
				Type:    knownAddressInstance.Type,
			}
			Directory[address] = addressEntry
			Directory[normalisedAddress] = addressEntry
			for _, slug := range knownAddressInstance.Slugs {
				Directory[slug] = &evmtypes.AddressDirectoryEntry{
					Address: address,
				}
			}
		}
	}
	DirectoryInitDuration = time.Now().Sub(startedInitAt)
}
