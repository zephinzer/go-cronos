package cronoscanapi

type TransactionsResponse struct {
	Message string        `json:"message"`
	Result  []Transaction `json:"result"`
	Status  string        `json:"status"`
}

type Transactions []Transaction

func (t Transactions) Len() int {
	return len(t)
}

func (t Transactions) Less(i, j int) bool {
	return t[i].TimeStamp < t[j].TimeStamp
}

func (t Transactions) Swap(i, j int) {
	t[i], t[j] = t[j], t[i]
}

type Transaction struct {
	BlockHash         string `json:"blockHash"`
	BlockNumber       string `json:"blockNumber"`
	ContractAddress   string `json:"contractAddress"`
	Confirmations     string `json:"confirmations"`
	CumulativeGasUsed string `json:"cumulativeGasUsed"`
	From              string `json:"from"`
	Gas               string `json:"gas"`
	GasPrice          string `json:"gasPrice"`
	GasUsed           string `json:"gasUsed"`
	Hash              string `json:"hash"`
	Input             string `json:"input"`
	Nonce             string `json:"nonce"`
	To                string `json:"to"`
	TimeStamp         string `json:"timeStamp"`
	TokenName         string `json:"tokenName"`
	TokenDecimal      string `json:"tokenDecimal"`
	TokenSymbol       string `json:"tokenSymbol"`
	TransactionIndex  string `json:"transactionIndex"`
	Value             string `json:"value"`
}
