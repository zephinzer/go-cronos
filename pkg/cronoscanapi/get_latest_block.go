package cronoscanapi

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"
)

func GetLatestBlock(apiKey string) (uint64, error) {
	req, err := http.NewRequest("GET", fmt.Sprintf("https://api.cronoscan.com/api?module=block&action=getblocknobytime&timestamp=%v&closest=before&apikey=%s", time.Now().UTC().Unix(), apiKey), nil)
	if err != nil {
		return 0, fmt.Errorf("failed to create request: %s", err)
	}
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return 0, fmt.Errorf("failed to execute request: %s", err)
	}
	resBody, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return 0, fmt.Errorf("failed to read response body: %s", err)
	}
	var response map[string]interface{}
	if err := json.Unmarshal(resBody, &response); err != nil {
		return 0, fmt.Errorf("failed to unmarsal response (%s): %s", string(resBody), err)
	}
	latestBlockString, ok := response["result"].(string)
	if !ok {
		return 0, fmt.Errorf("failed to retrieve 'result' property as a string (response: %s)", string(resBody))
	}
	latestBlock, err := strconv.ParseUint(latestBlockString, 10, 64)
	if err != nil {
		return 0, fmt.Errorf("failed to parse '%s' into a uint64: %s", latestBlockString, err)
	}
	return latestBlock, nil
}
