package cronoscanapi

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"sort"
	"sync"
)

type GetTransactionsOpts struct {
	ContractAddress string
	FromBlock       uint
	TillBlock       uint
	ApiKey          string
}

func GetTransactions(opts GetTransactionsOpts) ([]Transaction, error) {
	// added this on 20220924 - this seemingly similar second trasactions is because the first doesn't return results that are minted (from 0x0000...0000)
	// which caused us to miss some sub-transactions like those which can be seen in https://cronoscan.com/tx/0x2683ba11bc9e62bc3d294a0c0730be8412f8fc53a0328ac9c952790f5a93f4f8
	// if somehow they fix this so that tokens minted become available in the above call, remove this block because it's confusing af frankly
	contractAddressUrl := fmt.Sprintf("https://api.cronoscan.com/api?module=account&action=tokentx&contractaddress=%s&startblock=%v&endblock=%v&sort=asc&apikey=%s", opts.ContractAddress, opts.FromBlock, opts.TillBlock, opts.ApiKey)
	addressUrl := fmt.Sprintf("https://api.cronoscan.com/api?module=account&action=tokentx&address=%s&startblock=%v&endblock=%v&sort=asc&apikey=%s", opts.ContractAddress, opts.FromBlock, opts.TillBlock, opts.ApiKey)

	errors := []string{}
	transactions := Transactions{}
	urls := []string{addressUrl, contractAddressUrl}
	incomingTransactions := make(chan []Transaction, len(urls))
	var waiter sync.WaitGroup
	go func() {
		for {
			incomingTransaction := <-incomingTransactions
			if incomingTransaction == nil {
				return
			}
			transactions = append(transactions, incomingTransaction...)
			waiter.Done()
		}
	}()
	for _, url := range urls {
		waiter.Add(1)
		go func(targetUrl string) {
			responseTransactions := []Transaction{}
			defer func() { incomingTransactions <- responseTransactions }()
			req, err := http.NewRequest("GET", targetUrl, nil)
			if err != nil {
				errors = append(errors, fmt.Sprintf("failed to create request: %s", err))
				return
			}
			res, err := http.DefaultClient.Do(req)
			if err != nil {
				errors = append(errors, fmt.Sprintf("failed to execute request: %s", err))
				return
			}
			resBody, err := ioutil.ReadAll(res.Body)
			if err != nil {
				errors = append(errors, fmt.Sprintf("failed to read response body: %s", err))
				return
			}
			var response TransactionsResponse
			if err := json.Unmarshal(resBody, &response); err != nil {
				errors = append(errors, fmt.Sprintf("failed to unmarsal response (%s): %s", string(resBody), err))
				return
			}
			responseTransactions = response.Result
		}(url)
	}
	waiter.Wait()
	close(incomingTransactions)
	sort.Sort(transactions)
	return transactions, nil
}
