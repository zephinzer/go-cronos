package evmtypes

type Chain struct {
	Currency  string           `json:"currency" yaml:"currency"`
	Id        int              `json:"id" yaml:"id"`
	Label     string           `json:"label" yaml:"label"`
	Directory AddressDirectory `json:"directory" yaml:"directory"`
	RPC       RpcDirectory     `json:"rpc" yaml:"rpc"`
}
