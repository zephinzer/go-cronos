package evmtypes

type AddressDirectory map[string]*AddressDirectoryEntry

type AddressDirectoryEntry struct {
	Address     string   `json:"address" yaml:"address"`
	Aliases     []string `json:"aliases" yaml:"aliases"`
	Description string   `json:"description" yaml:"description"`
	Label       string   `json:"label" yaml:"label"`
	Type        string   `json:"type" yaml:"type"`
}
