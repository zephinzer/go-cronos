package evmtypes

type RpcDirectory map[string]*RPC

type RPC struct {
	Url string `json:"url"`
}
