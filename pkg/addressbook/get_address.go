package addressbook

import (
	"fmt"

	"github.com/ethereum/go-ethereum/common"
)

func GetAddress(chainId string, identifier string) (*common.Address, error) {
	var address common.Address
	selectedChain, chainExists := Chain[chainId]
	if !chainExists {
		return nil, fmt.Errorf("failed to find chain[%s]", chainId)
	}
	directoryEntry, addressExists := selectedChain.Directory[identifier]
	if !addressExists {
		return nil, fmt.Errorf("failed to find address by identifier[%s]", identifier)
	}
	address = common.HexToAddress(directoryEntry.Address)
	return &address, nil
}
