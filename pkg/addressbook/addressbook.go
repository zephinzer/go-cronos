package addressbook

import (
	_ "embed"

	"github.com/zephinzer/go-cronos/pkg/cronos"
	"github.com/zephinzer/go-cronos/pkg/evmtypes"
	"gopkg.in/yaml.v3"
)

//go:embed known_chains.yaml
var knownChains []byte
var knownChainsFile ChainsFileStructure

var AvailableChains []string
var Chain map[string]*evmtypes.Chain
var ChainById map[int]*evmtypes.Chain

type ChainsFileStructure struct {
	Chains map[string]*evmtypes.Chain `json:"chains" yaml:"chains"`
}

func init() {
	if err := yaml.Unmarshal(knownChains, &knownChainsFile); err != nil {
		panic(err)
	}
	Chain = knownChainsFile.Chains
	ChainById = map[int]*evmtypes.Chain{}
	for chainKey, chainDetails := range Chain {
		ChainById[chainDetails.Id] = chainDetails
		AvailableChains = append(AvailableChains, chainKey)
	}
	// add static chain data below as needed
	Chain["cronos"].Directory = cronos.Directory
	Chain["cronos"].RPC = cronos.RPC
}
