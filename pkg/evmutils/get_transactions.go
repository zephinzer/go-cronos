package evmutils

import (
	"context"
	"fmt"
	"math/big"
	"strings"
	"time"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/sirupsen/logrus"
)

type GetTransactionsOpts struct {

	// Events is the channel where emitted transactions will be
	// sent to. Must be specified
	Events chan WatchEvent

	// Methods when specified will filter emitted transactions
	// to only methods with the indicated method hash
	Methods []string

	// Rpc is the selected RPC. If not specified, defaults to
	// cronos.RpcVvs
	Rpc RpcUrl

	// ToAddresses when specified will filter emitted transactions
	// to only those where the 'to' address equals an address in
	// this list. This works in **injunction** (logical OR) with
	// the FromAddresses property
	ToAddresses []string

	// FromAddresses when specified will filter emitted transactions
	// to only those where the 'from' address equals an address in
	// this list. This works in **injunction** (OR) with the
	// ToAddresses property
	FromAddresses []string

	// FromBlock specifies the first block which should be retrieved
	// and parsed
	FromBlock int64

	// ToBlock specifies the last block which should be retrieved
	// and parsed
	ToBlock int64

	// Interval is the duration between checks on the chain
	Interval time.Duration
}

func (gto *GetTransactionsOpts) Validate() error {
	errs := []string{}

	if gto.Events == nil {
		errs = append(errs, "missing events channel")
	}

	if gto.Rpc == "" {
		errs = append(errs, "missing rpc url")
	}

	if gto.Interval == 0 {
		gto.Interval = DefaultWatchInterval
	}

	if len(errs) > 0 {
		return fmt.Errorf("failed to ensure all options: ['%s']", strings.Join(errs, "', '"))
	}
	return nil
}

func GetTransactions(opts GetTransactionsOpts) {
	if err := opts.Validate(); err != nil {
		opts.Events <- WatchEvent{
			Type:  EventError,
			Error: fmt.Errorf("failed to validate options: %s", err),
		}
		return
	}

	// setup the filters
	var filterMethods map[string]struct{} = nil
	if opts.Methods != nil && len(opts.Methods) > 0 {
		filterMethods = map[string]struct{}{}
		for _, method := range opts.Methods {
			filterMethods[method] = struct{}{}
		}
	}
	var filterAddresses map[string]struct{} = nil
	if opts.ToAddresses != nil && len(opts.ToAddresses) > 0 {
		filterAddresses = map[string]struct{}{}
		for _, address := range opts.ToAddresses {
			filterAddresses[address] = struct{}{}
			filterAddresses[strings.ToLower(address)] = struct{}{}
		}
	}
	if opts.FromAddresses != nil && len(opts.FromAddresses) > 0 {
		filterAddresses = map[string]struct{}{}
		for _, address := range opts.FromAddresses {
			filterAddresses[address] = struct{}{}
			filterAddresses[strings.ToLower(address)] = struct{}{}
		}
	}

	// prepare the evm client
	client, err := ethclient.Dial(string(opts.Rpc))
	if err != nil {
		opts.Events <- WatchEvent{
			Type:  EventError,
			Error: fmt.Errorf("failed to connect to rpc[%s]: %s", string(opts.Rpc), err),
		}
		return
	}

	// get network details
	chainId, err := client.NetworkID(context.Background())
	if err != nil {
		opts.Events <- WatchEvent{
			Type:  EventWarning,
			Error: fmt.Errorf("failed to get chain id: %s", err),
		}
	}

	endingBlock := opts.ToBlock
	if endingBlock <= 0 {
		header, err := client.HeaderByNumber(context.Background(), nil)
		if err != nil {
			opts.Events <- WatchEvent{
				Type:  EventError,
				Error: fmt.Errorf("failed to get latest header: %s", err),
			}
			<-time.After(opts.Interval * 2)
		}
		endingBlock = header.Number.Int64()
	}

	// start the watch cycle
	for currentBlock := opts.FromBlock; currentBlock < endingBlock; currentBlock++ {
		header, err := client.HeaderByNumber(context.Background(), nil)
		if err != nil {
			opts.Events <- WatchEvent{
				Type:  EventError,
				Error: fmt.Errorf("failed to get latest header: %s", err),
			}
			<-time.After(opts.Interval * 2)
			continue
		}
		endingBlock = header.Number.Int64()

		block, err := client.BlockByNumber(context.Background(), big.NewInt(currentBlock))
		if err != nil {
			opts.Events <- WatchEvent{
				Type:  EventWarning,
				Error: fmt.Errorf("failed to get block data for block %v: %s", currentBlock, err),
			}
			logrus.Infof("adding block[%v] to missed blocks", currentBlock)
		}

		// process transactions
		for _, transaction := range block.Transactions() {
			txHash := transaction.Hash().String()
			txData := common.Bytes2Hex(transaction.Data())
			txCost := transaction.Cost()
			txValue := transaction.Value()
			if transaction.To() == nil {
				continue
			}
			txTo := transaction.To().Hex()
			if filterMethods != nil {
				if len(txData) >= 8 {
					methodSignature := txData[:8]
					if _, ok := filterMethods[methodSignature]; !ok {
						continue
					}
				}
			}
			txFrom := ""
			message, err := transaction.AsMessage(types.NewLondonSigner(chainId), nil)
			if err == nil {
				txFrom = message.From().Hex()
			}
			if filterAddresses != nil {
				_, fromExists := filterAddresses[strings.ToLower(txFrom)]
				_, toExists := filterAddresses[strings.ToLower(txTo)]
				if !fromExists && !toExists {
					continue
				}
			}
			var receipt *types.Receipt = nil
			var receiptError error
			for receipt == nil {
				receipt, receiptError = client.TransactionReceipt(context.Background(), transaction.Hash())
				if err != nil {
					opts.Events <- WatchEvent{
						Type:  EventError,
						Error: fmt.Errorf("failed to get tx receipt: %s", receiptError),
					}
					<-time.After(time.Millisecond * 200)
					continue
				}
			}
			blockNumber := receipt.BlockNumber
			txLogs := []TransactionLog{}
			if receipt.Logs != nil {
				for _, txLog := range receipt.Logs {
					topics := []string{}
					for _, txLogTopic := range txLog.Topics {
						topics = append(topics, txLogTopic.Hex())
					}
					txLogData := ""
					if txLog.Data != nil {
						txLogData = common.Bytes2Hex(txLog.Data)
					}
					txLogs = append(txLogs, TransactionLog{
						Address:  txLog.Address.Hex(),
						BlockId:  big.NewInt(0).SetUint64(txLog.BlockNumber),
						Data:     txLogData,
						Instance: txLog,
						Topics:   topics,
					})
				}
			}
			txType := transaction.Type()
			opts.Events <- WatchEvent{
				BlockId:    blockNumber,
				TxCost:     txCost,
				TxData:     txData,
				TxHash:     txHash,
				TxFrom:     txFrom,
				TxInstance: transaction,
				TxLogs:     txLogs,
				TxTo:       txTo,
				TxType:     uint64(txType),
				TxValue:    txValue,
				Type:       EventTransaction,
			}
		}

		<-time.After(opts.Interval)
	}
}
