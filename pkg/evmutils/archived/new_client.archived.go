package erc20

import (
	"fmt"
	"time"

	"github.com/ethereum/go-ethereum/ethclient"
)

// NewClientEvent provides a structure for events emitted by
// the NewClient() method
type NewClientEvent struct {
	Message string
	IsError bool
}

// NewClientOpts provides configuration for the NewClient method
type NewClientOpts struct {
	Events        chan NewClientEvent
	MaxRetries    int
	RetryInterval time.Duration
	RpcUrl        string
}

// NewClient creates a new client with a retry logic,
// events are emitted to the provided `.Events` property
// if provided. On success, returns an EVM client pointer,
// otherwise, the `error` field will be non-nil
func NewClient(opts NewClientOpts) (*ethclient.Client, error) {
	if opts.Events == nil {
		opts.Events = make(chan NewClientEvent, 10)
		defer close(opts.Events)
	}

	rpcUrl := opts.RpcUrl

	rpcMaxRetries := opts.MaxRetries
	if rpcMaxRetries <= 0 {
		rpcMaxRetries = 1
	}

	retryInterval := opts.RetryInterval
	if retryInterval <= 0 {
		retryInterval = 1 * time.Second
	}

	var clientInstance *ethclient.Client
	var dialError error

	opts.Events <- NewClientEvent{
		Message: fmt.Sprintf("connecting to rpc[%s]...", rpcUrl),
		IsError: false,
	}
	for clientInstance == nil {
		clientInstance, dialError = ethclient.Dial(rpcUrl)
		if dialError != nil {
			opts.Events <- NewClientEvent{
				Message: fmt.Sprintf("failed to dial rpc[%s]: %s", rpcUrl, dialError),
				IsError: true,
			}
			rpcMaxRetries--
			if rpcMaxRetries == 0 {
				return nil, fmt.Errorf("failed to reach rpc[%s]", rpcUrl)
			}
			opts.Events <- NewClientEvent{
				Message: fmt.Sprintf("dialing rpc again in %s", retryInterval.String()),
				IsError: true,
			}
		}
		<-time.After(retryInterval)
	}
	opts.Events <- NewClientEvent{
		Message: fmt.Sprintf("connected to rpc[%s] successfully", rpcUrl),
		IsError: false,
	}

	return clientInstance, nil
}
