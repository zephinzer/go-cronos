package evmutils

import (
	"context"
	"fmt"
	"math/big"
	"strings"
	"time"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/common/math"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/sirupsen/logrus"
)

type WatchEventType string

const (
	EventBlock       WatchEventType = "blk"
	EventError       WatchEventType = "err"
	EventWarning     WatchEventType = "warn"
	EventTransaction WatchEventType = "txn"
)

type WatchEvent struct {
	BlockId         *big.Int
	BlockInstance   *types.Block
	Error           error `json:"error"`
	InternalMessage string
	TxCost          *big.Int
	TxData          string
	TxFrom          string
	TxHash          string
	TxInstance      *types.Transaction
	TxLogs          []TransactionLog
	TxTo            string
	TxType          uint64
	TxValue         *big.Int
	Type            WatchEventType
}

type TransactionLog struct {
	Address  string
	BlockId  *big.Int
	Data     string
	Topics   []string
	Instance *types.Log
}

func (we WatchEvent) GetHumanValue() float64 {
	floatValue, _ := big.NewFloat(0).Quo(big.NewFloat(0).SetInt(we.TxValue), big.NewFloat(0).SetInt(math.BigPow(10, 18))).Float64()
	return floatValue
}

type WatchTransactionsOpts struct {

	// Events is the channel where emitted transactions will be
	// sent to. Must be specified
	Events chan WatchEvent

	// Methods when specified will filter emitted transactions
	// to only methods with the indicated method hash
	Methods []string

	// Rpc is the selected RPC. If not specified, defaults to
	// cronos.RpcVvs
	Rpc RpcUrl

	// ToAddresses when specified will filter emitted transactions
	// to only those where the 'to' address equals an address in
	// this list. This works in **injunction** (logical OR) with
	// the FromAddresses property
	ToAddresses []string

	// FromAddresses when specified will filter emitted transactions
	// to only those where the 'from' address equals an address in
	// this list. This works in **injunction** (OR) with the
	// ToAddresses property
	FromAddresses []string

	// Interval is the duration between checks on the chain
	Interval time.Duration
}

func (wto *WatchTransactionsOpts) Validate() error {
	errs := []string{}

	if wto.Events == nil {
		errs = append(errs, "missing events channel")
	}

	if wto.Rpc == "" {
		errs = append(errs, "missing rpc url")
	}

	if wto.Interval == 0 {
		wto.Interval = DefaultWatchInterval
	}

	if len(errs) > 0 {
		return fmt.Errorf("failed to ensure all options: ['%s']", strings.Join(errs, "', '"))
	}
	return nil
}

func WatchTransactions(opts WatchTransactionsOpts) {
	if err := opts.Validate(); err != nil {
		opts.Events <- WatchEvent{
			Type:  EventError,
			Error: fmt.Errorf("failed to validate options: %s", err),
		}
		return
	}

	// setup the filters
	var filterMethods map[string]struct{} = nil
	if opts.Methods != nil && len(opts.Methods) > 0 {
		filterMethods = map[string]struct{}{}
		for _, method := range opts.Methods {
			filterMethods[method] = struct{}{}
		}
	}
	var filterAddresses map[string]struct{} = nil
	if opts.ToAddresses != nil && len(opts.ToAddresses) > 0 {
		filterAddresses = map[string]struct{}{}
		for _, address := range opts.ToAddresses {
			filterAddresses[address] = struct{}{}
			filterAddresses[strings.ToLower(address)] = struct{}{}
		}
	}
	if opts.FromAddresses != nil && len(opts.FromAddresses) > 0 {
		filterAddresses = map[string]struct{}{}
		for _, address := range opts.FromAddresses {
			filterAddresses[address] = struct{}{}
			filterAddresses[strings.ToLower(address)] = struct{}{}
		}
	}

	// prepare the evm client
	client, err := ethclient.Dial(string(opts.Rpc))
	if err != nil {
		opts.Events <- WatchEvent{
			Type:  EventError,
			Error: fmt.Errorf("failed to connect to rpc[%s]: %s", string(opts.Rpc), err),
		}
		return
	}

	// get network details
	chainId, err := client.NetworkID(context.Background())
	if err != nil {
		opts.Events <- WatchEvent{
			Type:  EventWarning,
			Error: fmt.Errorf("failed to get chain id: %s", err),
		}
	}

	// start the watch cycle
	currentBlockNumber := big.NewInt(0)
	// set to true for the first time to process the current block we're on
	repeatIngestProcess := true
	missedBlocks := []*big.Int{}
	for {
		header, err := client.HeaderByNumber(context.Background(), nil)
		if err != nil {
			opts.Events <- WatchEvent{
				Type:  EventError,
				Error: fmt.Errorf("failed to get latest header: %s", err),
			}
			<-time.After(opts.Interval * 2)
			continue
		}
		blockDifference := big.NewInt(0)
		if !repeatIngestProcess {
			blockDifference = big.NewInt(0).Sub(header.Number, currentBlockNumber)
			if blockDifference.Int64() <= 1 {
				continue
			}
		}
		currentBlockNumber = big.NewInt(0).Sub(header.Number, big.NewInt(1))
		// example for makePurchase
		// currentBlockNumber = big.NewInt(3092652)
		// currentBlockNumber = big.NewInt(3367126)
		numberOfBlocks := blockDifference.Int64() - 1
		if numberOfBlocks < 1 {
			numberOfBlocks = 1
		}
		logrus.Infof("number of blocks to process: %v", numberOfBlocks)
		transactions := types.Transactions{}

		stillMissedBlocks := []*big.Int{}
		// attempt to clear missed blocks
		for _, missedBlock := range missedBlocks {
			logrus.Infof("processing missed block[%v]", missedBlock.String())
			block, err := client.BlockByNumber(context.Background(), missedBlock)
			if err != nil {
				opts.Events <- WatchEvent{
					Type:  EventWarning,
					Error: fmt.Errorf("failed to get latest block data for block %v: %s", missedBlock.Int64(), err),
				}
				logrus.Infof("adding block[%v] to missed blocks", missedBlock.String())
				stillMissedBlocks = append(stillMissedBlocks, missedBlock)
			} else {
				opts.Events <- WatchEvent{
					Type:          EventBlock,
					BlockId:       block.Number(),
					BlockInstance: block,
				}

				missedTransactions := block.Transactions()
				logrus.Infof("adding %v transactions from block[%v]", len(missedTransactions), missedBlock.String())
				transactions = append(transactions, missedTransactions...)
			}
		}
		missedBlocks = stillMissedBlocks

		// process current blocks
		for i := int64(0); i < numberOfBlocks; i++ {
			blockNumber := big.NewInt(0).Sub(currentBlockNumber, big.NewInt(i))
			block, err := client.BlockByNumber(context.Background(), blockNumber)
			if err != nil {
				opts.Events <- WatchEvent{
					Type:  EventWarning,
					Error: fmt.Errorf("failed to get latest block data for block %v: %s", blockNumber.Int64(), err),
				}
				logrus.Infof("adding block[%v] to missed blocks", blockNumber.String())
				missedBlocks = append(missedBlocks, blockNumber)
			} else {
				opts.Events <- WatchEvent{
					Type:          EventBlock,
					BlockId:       blockNumber,
					BlockInstance: block,
				}

				missedTransactions := block.Transactions()
				logrus.Infof("adding %v transactions from block[%v]", len(missedTransactions), blockNumber.String())
				transactions = append(missedTransactions, transactions...)
			}
		}

		// process transactions
		for _, transaction := range transactions {
			txHash := transaction.Hash().String()
			txData := common.Bytes2Hex(transaction.Data())
			txCost := transaction.Cost()
			txValue := transaction.Value()
			if transaction.To() == nil {
				continue
			}
			txTo := transaction.To().Hex()
			if filterMethods != nil {
				if len(txData) >= 8 {
					methodSignature := txData[:8]
					if _, ok := filterMethods[methodSignature]; !ok {
						continue
					}
				}
			}
			txFrom := ""
			message, err := transaction.AsMessage(types.NewLondonSigner(chainId), nil)
			if err == nil {
				txFrom = message.From().Hex()
			}
			if filterAddresses != nil {
				_, fromExists := filterAddresses[strings.ToLower(txFrom)]
				_, toExists := filterAddresses[strings.ToLower(txTo)]
				if !fromExists && !toExists {
					continue
				}
			}
			blockNumber := currentBlockNumber
			var receipt *types.Receipt = nil
			var receiptError error
			for receipt == nil {
				receipt, receiptError = client.TransactionReceipt(context.Background(), transaction.Hash())
				if err != nil {
					opts.Events <- WatchEvent{
						Type:  EventError,
						Error: fmt.Errorf("failed to get tx receipt: %s", receiptError),
					}
					<-time.After(time.Millisecond * 200)
					continue
				}
			}
			blockNumber = receipt.BlockNumber
			txLogs := []TransactionLog{}
			if receipt.Logs != nil {
				for _, txLog := range receipt.Logs {
					topics := []string{}
					for _, txLogTopic := range txLog.Topics {
						topics = append(topics, txLogTopic.Hex())
					}
					txLogData := ""
					if txLog.Data != nil {
						txLogData = common.Bytes2Hex(txLog.Data)
					}
					txLogs = append(txLogs, TransactionLog{
						Address:  txLog.Address.Hex(),
						BlockId:  big.NewInt(0).SetUint64(txLog.BlockNumber),
						Data:     txLogData,
						Instance: txLog,
						Topics:   topics,
					})
				}
			}
			txType := transaction.Type()
			opts.Events <- WatchEvent{
				BlockId:    blockNumber,
				TxCost:     txCost,
				TxData:     txData,
				TxHash:     txHash,
				TxFrom:     txFrom,
				TxInstance: transaction,
				TxLogs:     txLogs,
				TxTo:       txTo,
				TxType:     uint64(txType),
				TxValue:    txValue,
				Type:       EventTransaction,
			}
		}

		if repeatIngestProcess {
			repeatIngestProcess = false
		}
		<-time.After(opts.Interval)
	}
}
