package evmutils

import (
	"context"
	"crypto/ecdsa"
	"fmt"
	"math/big"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/ethereum/go-ethereum/ethclient"
)

type CreateTransactionOpts struct {
	Client     *ethclient.Client
	Data       []byte
	GasLimit   uint64
	PrivateKey *ecdsa.PrivateKey
	To         common.Address
	Value      *big.Int
}

func CreateSignedTransaction(opts CreateTransactionOpts) (*types.Transaction, error) {
	chainId, err := opts.Client.NetworkID(context.Background())
	if err != nil {
		return nil, fmt.Errorf("failed to get chain network id: %s", err)
	}
	txnNonce, err := opts.Client.PendingNonceAt(
		context.Background(),
		crypto.PubkeyToAddress(opts.PrivateKey.PublicKey),
	)
	if err != nil {
		return nil, fmt.Errorf("failed to get pending nonce: %s", err)
	}
	txnGasPrice, err := opts.Client.SuggestGasPrice(context.Background())
	if err != nil {
		return nil, fmt.Errorf("failed to get suggested gas price: %s", err)
	}

	txnData := types.DynamicFeeTx{
		ChainID:   chainId,
		Nonce:     txnNonce,
		To:        &opts.To,
		GasFeeCap: txnGasPrice,
		Gas:       opts.GasLimit,
		Value:     opts.Value,
		Data:      opts.Data,
	}
	signedTxnInstance, err := types.SignNewTx(
		opts.PrivateKey,
		types.NewLondonSigner(chainId),
		&txnData,
	)
	if err != nil {
		return nil, fmt.Errorf("failed to sign transaction: %s", err)
	}
	return signedTxnInstance, nil
}
