# `go-cronos`

- [`go-cronos`](#go-cronos)
- [Service Catalog](#service-catalog)
  - [Cronoscan](#cronoscan)
  - [Ebisusbay](#ebisusbay)
  - [Farmer](#farmer)
- [Configuration](#configuration)
  - [Database configurations](#database-configurations)
- [Contributing](#contributing)
  - [Setting up locally](#setting-up-locally)
    - [Dependencies](#dependencies)
    - [Using Docker Compose (recommended)](#using-docker-compose-recommended)
    - [Using Kubernetes (advanced)](#using-kubernetes-advanced)
  - [Building/Releasing](#buildingreleasing)
    - [Building binary locally](#building-binary-locally)
    - [Building the Docker image](#building-the-docker-image)
    - [Releasing the Docker image](#releasing-the-docker-image)
  - [Deploying](#deploying)

# Service Catalog

| Service         | Description                                                                                                       |
| --------------- | ----------------------------------------------------------------------------------------------------------------- |
| `aes`           | Does AES encryption/decryption for storage of private keys                                                        |
| `cronoscan`     | Used for harvesting data from Cronoscan                                                                           |
| `debug`         | Utility command to experiment with chain stuff                                                                    |
| `ebisusbay`     | Module that works with EbisusBay-only data                                                                        |
| `farmer`        | Godfather of data. This farms data from the chain itself for downstream services to have a database to query from |
| `tgutil`        | A Telegram bot that allows for checking of chat ID and other stuff like that                                      |
| `wallet-notifs` | Watches a wallet and notifies the specified Telegram user/group/channel of any transactions                       |

Services are categorised into levels depending on how far away they are from the source of truth:

- Level 1: Data-layer services which ingest raw data from service providers or the chain itself
- Level 2: Processing-layer services which interact with the raw data and produces data that fits an abstraction
- Level 3: Action-layer services which act as logical controllers that perform an action upon detection of trigger conditions from the processed data

Services not documented below are **use-at-your-own-risk** and are not orchestrated for deployment.

## Cronoscan

`cronoscan` is a level 1 service that runs as a job to harvest historical data for downstream data processing.

1. Run migrations using `cronoscan migrate`
2. Run the job periodically using `cronoscan ingest transactions`

Run `go run ./cmd/cronoscan help` for more information.

## Ebisusbay

`ebisusbay` is a level 2 service that grabs chain data from the master database and produces data interpreted for a marketplace dApp.

1. Run migrations using `ebisusbay migrate`
2. Run the contract testing service using `ebisusbay start checks`
2. Run the service using `ebisusbay start farmer`

Run `go run ./cmd/ebisusbay help` for more information.

## Farmer

`farmer` is a level 1 service that harvests data from the chain and puts them into the master database.

1. Run migrations using `farmer migrate`
2. Run the service using `farmer start`

Run `go run ./cmd/farmer help` for more information.

# Configuration

All workloads can be configured either by flags or environment variables.

The following section documents the standardised configurations across all services (not all services may adopt all of these)

## Database configurations

| Flag                   | Environment Variable Key | Default              | Description                                                                                  |
| ---------------------- | ------------------------ | -------------------- | -------------------------------------------------------------------------------------------- |
| `--db-username`        | `DB_USERNAME`            | *depends on service* | Username of the user to use for the service's primary database                               |
| `--db-password`        | `DB_PASSWORD`            | `"password"`         | Password of the user to use for the service's primary database                               |
| `--db-host`            | `DB_HOST`                | `"127.0.0.1"`        | Hostname where the database instance is reachable at                                         |
| `--db-name`            | `DB_NAME`                | *depends on service* | Name of the database schema to use                                                           |
| `--db-port`            | `DB_PORT`                | `3306`               | Port that the database instance is listening on                                              |
| `--db-master-username` | `DB_MASTER_USERNAME`     | *depends on service* | Username of the user to use for the service's master database                                |
| `--db-master-password` | `DB_MASTER_PASSWORD`     | `"password"`         | Password of the user to use for the service's master database                                |
| `--db-master-host`     | `DB_MASTER_HOST`         | `"127.0.0.1"`        | Hostname where the master database instance is reachable at                                  |
| `--db-master-name`     | `DB_MASTER_NAME`         | *depends on service* | Name of the master database schema to use                                                    |
| `--db-master-port`     | `DB_MASTER_PORT`         | `3306`               | Port that the database instance is listening on                                              |
| `--db-root-username`   | `DB_ROOT_USERNAME`       | `"root"`             | Name of the database instance's root user for creation of service-specific users and schemas |
| `--db-root-password`   | `DB_ROOT_PASSWORD`       | `"password"`         | Password of the database instance's root user                                                |


# Contributing

For the below section `${CMD}` will be the name of the service which can be found by listing the contents of `./cmd`. The service name is the name of the sub-directory in `./cmd`

## Setting up locally

### Dependencies

You will need to download and install most of the following tools:

- Docker
  - Via Docker Desktop: https://docs.docker.com/engine/install/#desktop
  - Linux: https://docs.docker.com/engine/install/#server
  - Verify with:
    ```sh
    docker --version;
    ```
- Docker Compose
  - Via Docker Desktop: https://docs.docker.com/compose/install/compose-desktop/
  - Linux: https://docs.docker.com/compose/install/compose-plugin/#install-using-the-repository
  - Verify with:
    ```sh
    docker-compose --version;
    ```
- Go
  - All platforms: https://go.dev/doc/install
  - Verify with:
    ```sh
    go version;
    ```
- Make
  - Windows: `choco install make`
  - MacOS: `brew install make` or `xcode-select --install`
  - Linux: `apt install make`
  - Verify with:
    ```sh
    make --version;
    ```

IF working on the code, you'll need to also run the following:

```sh
# pull in dependencies
make deps;
```

### Using Docker Compose (recommended)

Run the following to start support services locally via Docker Compose:

```sh
# setup
make env

# teardown - does not remove data
make env_down

# purge - removes data
make env_purge
```

Docker Compose deployment details are in [`./deploy/local-env`](./deploy/local-env)

### Using Kubernetes (advanced)

Run the following to bring up the Kubernetes cluster:

```sh
# setup
make k8s

# teardown - removes all data too
make k8s_down
```

Kubernetes deployment details are in [`./deploy/local-k8s`](./deploy/local-k8s)

## Building/Releasing

### Building binary locally

Built binaries will be in the `./bin` directory.

```sh
make build app=${CMD};
```

### Building the Docker image

```sh
make iamge app=${CMD};
```

### Releasing the Docker image

```sh
make release_git app=${CMD};
```

## Deploying

Deployments are done using Helm charts which can be found at [`./deploy/charts`](./deploy/charts).

Charts should be named according to the service they represent.

Navigate to the chart of interest and run:

```sh
helm upgrade --install ${RELEASE_NAME} ${CHART_NAME};
```
