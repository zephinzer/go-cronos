package healthcheck

import (
	"fmt"
	"net/http"

	"github.com/zephinzer/go-cronos/internal/log"
)

func StartServer(addr string, port uint16) {
	mux := http.NewServeMux()
	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("ok"))
	})
	bindInterface := fmt.Sprintf("%s:%v", addr, port)
	log.Infof("healthcheck server starting at '%s'...", bindInterface)
	if err := http.ListenAndServe(bindInterface, mux); err != nil {
		log.Warnf("failed to start healthcheck server")
	}
}
