package database

import (
	"database/sql"
	"fmt"
	"log"
	"os"
	"path"
	"strings"

	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/mysql"
	_ "github.com/golang-migrate/migrate/v4/source/file"
)

type MigrateOpts struct {
	DatabaseHost            string
	DatabasePort            uint64
	DatabaseName            string
	DatabaseUsername        string
	DatabasePassword        string
	DatabaseRootUsername    string
	DatabaseRootPassword    string
	DatabaseMigrationsPath  string
	DisableSchemaMigrations bool
	Log                     func(string, ...any)
}

func (mo MigrateOpts) GetMySqlDsn(useRoot ...bool) string {
	username := mo.DatabaseUsername
	password := mo.DatabasePassword
	schema := fmt.Sprintf("/%s", mo.DatabaseName)
	if len(useRoot) > 0 && useRoot[0] {
		username = mo.DatabaseRootUsername
		password = mo.DatabaseRootPassword
		schema = "/"
	}
	return fmt.Sprintf(
		"%s:%s@tcp(%s:%v)%s?multiStatements=true",
		username,
		password,
		mo.DatabaseHost,
		mo.DatabasePort,
		schema,
	)
}

func (mo MigrateOpts) GetMigrationsPath() (string, error) {
	migrationsPath := mo.DatabaseMigrationsPath
	if !path.IsAbs(migrationsPath) {
		cwd, err := os.Getwd()
		if err != nil {
			return "", fmt.Errorf("failed to get the current working directory: %s", err)
		}
		migrationsPath = path.Join(cwd, migrationsPath)
	}
	if pathInfo, err := os.Lstat(migrationsPath); err != nil {
		return "", fmt.Errorf("failed to get path[%s]: %s", migrationsPath, err)
	} else if !pathInfo.IsDir() {
		return "", fmt.Errorf("provided path[%s] is not a directory", migrationsPath)
	}
	return migrationsPath, nil
}

// Migrate standardises the migration process across all services
// and does a few things:
//
// 1. Create a new user for the service
//
// 2. Create the service's requested database schema
//
// 3. Grant the newly created user permissions to run operations on the newly created database schema
//
// 4. Run the service's local migrations on the newly created database schema
//
// Use this function in the various service's migrate command
func Migrate(opts MigrateOpts) error {
	logInfo := func(f string, a ...any) { log.Printf(f, a...) }
	if opts.Log != nil {
		logInfo = opts.Log
	}

	logInfo("creating root database session...")
	rootConnection, err := sql.Open("mysql", opts.GetMySqlDsn(true))
	if err != nil {
		return fmt.Errorf("failed to create a db connection for root user: %s", err)
	}
	logInfo("ensuring database exists...")
	if _, err = rootConnection.Exec(
		fmt.Sprintf("CREATE SCHEMA IF NOT EXISTS `%s`", opts.DatabaseName),
	); err != nil {
		return fmt.Errorf("failed to create schema: %s", err)
	}
	var existingUser string
	var existingUserError error
	logInfo("querying existing database users...")
	if existingUserError = rootConnection.QueryRow("SELECT user FROM mysql.user WHERE user=?", opts.DatabaseUsername).Scan(&existingUser); existingUserError != nil && existingUserError != sql.ErrNoRows {
		return fmt.Errorf("failed to query existing users: %s", err)
	}
	if existingUser == "" && existingUserError == sql.ErrNoRows {
		logInfo("creating database user...")
		if _, err := rootConnection.Exec(
			fmt.Sprintf("CREATE USER IF NOT EXISTS `%s`@`%%` IDENTIFIED BY '%s';", opts.DatabaseUsername, opts.DatabasePassword),
		); err != nil {
			return fmt.Errorf("failed to create user: %s", err)
		}
	}
	logInfo("granting database user privileges...")
	if _, err := rootConnection.Exec(
		fmt.Sprintf("GRANT ALL ON %s.* TO '%s'@'%%';", opts.DatabaseName, opts.DatabaseUsername),
	); err != nil {
		return fmt.Errorf("failed to grant user permissions: %s", err)
	}
	logInfo("flushing privileges...")
	if _, err := rootConnection.Exec("FLUSH PRIVILEGES;"); err != nil {
		return fmt.Errorf("failed to flush user permissions: %s", err)
	}
	rootConnection.Close()

	logInfo("connecting to database...")
	connection, err := sql.Open("mysql", opts.GetMySqlDsn())
	if err != nil {
		return fmt.Errorf("failed to create a db connection: %s", err)
	}
	defer connection.Close()

	logInfo("initialising database migrations...")
	driver, err := mysql.WithInstance(connection, &mysql.Config{})
	if err != nil {
		return fmt.Errorf("failed to create a db driver: %s", err)
	}

	if !opts.DisableSchemaMigrations {
		logInfo("loading database migrations...")
		migrationsPath, err := opts.GetMigrationsPath()
		if err != nil {
			return fmt.Errorf("failed to retrieve migrations: %s", err)
		}
		migration, err := migrate.NewWithDatabaseInstance(fmt.Sprintf("file://%s", migrationsPath), "mysql", driver)
		if err != nil {
			return fmt.Errorf("failed to create migration instance: %s", err)
		}
		defer migration.Close()

		logInfo("executing database migrations...")
		version, dirty, err := migration.Version()
		logInfo("current database version[%v] dirty[%v] err[%v]", version, dirty, err)
		if err != nil {
			if !strings.Contains(err.Error(), "no migration") {
				return fmt.Errorf("failed to get the current version: %s", err)
			}
		} else if dirty {
			return fmt.Errorf("not contiuning until dirty flag is cleared: %s", err)
		}

		if err := migration.Up(); err != nil {
			if !strings.Contains(err.Error(), "no change") {
				return fmt.Errorf("failed to run migrations: %s", err)
			}
		}

		version, dirty, err = migration.Version()
		logInfo("current database version[%v] dirty[%v] err[%v]", version, dirty, err)
		if err != nil {
			if !strings.Contains(err.Error(), "no migration") {
				return fmt.Errorf("failed to get the current version: %s", err)
			}
		} else if dirty {
			return fmt.Errorf("not contiuning until dirty flag is cleared: %s", err)
		}
	}

	return nil
}
