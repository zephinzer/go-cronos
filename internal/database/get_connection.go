package database

import (
	"database/sql"
	"fmt"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

type GetConnectionLogger func(string, ...any)

type GetConnectionOpts struct {
	Username string
	Password string
	Host     string
	Port     uint64
	Database string

	RetryInterval time.Duration
	MaxRetryCount uint64
	Log           GetConnectionLogger
	LogError      GetConnectionLogger
}

func GetConnection(opts GetConnectionOpts) (*sql.DB, error) {
	logInfo := func(string, ...any) {}
	if opts.Log != nil {
		logInfo = opts.Log
	}
	logError := func(string, ...any) {}
	if opts.LogError != nil {
		logError = opts.LogError
	}

	publicDsn := fmt.Sprintf(
		"%s@%s:%v/%s",
		opts.Username,
		opts.Host,
		opts.Port,
		opts.Database,
	)
	logInfo("connecting to db[%s]", publicDsn)
	connectionDsn := fmt.Sprintf(
		"%s:%s@tcp(%s:%v)/%s?parseTime=true",
		opts.Username,
		opts.Password,
		opts.Host,
		opts.Port,
		opts.Database,
	)
	db, err := sql.Open("mysql", connectionDsn)
	if err != nil {
		return nil, fmt.Errorf("failed to open db connection: %s", err)
	}
	pingError := db.Ping()
	maxRetries := opts.MaxRetryCount
	retryInterval := opts.RetryInterval
	for pingError != nil {
		if pingError != nil {
			logError("failed to ping db[%s]", publicDsn)
			maxRetries--
			if maxRetries == 0 {
				return nil, fmt.Errorf("failed to reach db[%s] for %v counts", publicDsn, opts.MaxRetryCount)
			}
			logInfo("pinging db again in %s", retryInterval.String())
		}
		<-time.After(retryInterval)
		pingError = db.Ping()
	}
	logInfo("connected to database successfully")
	return db, nil
}
