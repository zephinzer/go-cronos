/*
Package log is a utility module that provides logging
capabilities for internal use. Logs that are emitted via this
package are NOT for workload execution logs or audit trails
*/
package log
