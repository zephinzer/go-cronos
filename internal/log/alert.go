package log

import (
	"fmt"
	"os"
	"strconv"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

type AlertType string

const (
	AlertTelegramChatIdKey = "LOG_ALERT_TELEGRAM_CHAT_ID"
	AlertTelegramTokenKey  = "LOG_ALERT_TELEGRAM_TOKEN"

	AlertTypeError AlertType = "error"
	AlertTypeInfo  AlertType = "info"
)

func Alert(message string, messageType AlertType) error {
	switch messageType {
	case AlertTypeError:
		Errorf(message)
	case AlertTypeInfo:
		fallthrough
	default:
		Infof(message)
	}
	telegramChatId, err := strconv.ParseInt(os.Getenv(AlertTelegramChatIdKey), 10, 64)
	if err != nil {
		return fmt.Errorf("failed to parse provided envvar[%s][%v] into an integer: %s", AlertTelegramChatIdKey, os.Getenv(AlertTelegramChatIdKey), err)
	}
	telegramToken := os.Getenv(AlertTelegramTokenKey)
	if telegramChatId == 0 {
		return fmt.Errorf("failed to find value of envvar[%s]", AlertTelegramChatIdKey)
	}
	if telegramToken == "" {
		return fmt.Errorf("failed to find value of envvar[%s]", AlertTelegramTokenKey)
	}
	botInstance, err := tgbotapi.NewBotAPI(telegramToken)
	if err != nil {
		return fmt.Errorf("failed to instantiate a telegram bot instance: %s", err)
	}
	switch messageType {
	case AlertTypeError:
		message = "⚠️ #ERROR ⚠️\n```\n" + message + "\n```"
	case AlertTypeInfo:
		fallthrough
	default:
		message = "ℹ️ #INFO ℹ️\n```\n" + message + "\n```"
	}
	messageInstance := tgbotapi.NewMessage(telegramChatId, message)
	messageInstance.ParseMode = "markdown"
	messageInstance.DisableNotification = messageType != AlertTypeError
	if _, err := botInstance.Send(messageInstance); err != nil {
		return fmt.Errorf("failed to send message: %s", err)
	}
	return nil
}
