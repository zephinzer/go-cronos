package log

import (
	"io"
	"os"

	"github.com/sirupsen/logrus"
	"github.com/zephinzer/go-cronos/internal/constants"
)

var DefaultOutputTo = os.Stderr

type InitOpts struct {
	Format          Format
	Level           Level
	OutputTo        io.Writer
	TimestampFormat string
}

func (o InitOpts) GetOutputTo() io.Writer {
	if o.OutputTo == nil {
		return DefaultOutputTo
	}
	return o.OutputTo
}

func (o InitOpts) GetTimestampFormat() string {
	if len(o.TimestampFormat) == 0 {
		return constants.TimestampYmdHMSz
	}
	return o.TimestampFormat
}

func Init(opts InitOpts) {
	Logger.SetLevel(logLevelsMap[opts.Level])
	Logger.SetOutput(opts.GetOutputTo())
	Logger.SetReportCaller(true)
	switch opts.Format {
	case LogFormatText:
		Logger.SetFormatter(&logrus.TextFormatter{
			CallerPrettyfier:       PrettifyCaller,
			TimestampFormat:        opts.GetTimestampFormat(),
			FullTimestamp:          true,
			QuoteEmptyFields:       true,
			DisableSorting:         true,
			DisableLevelTruncation: true,
		})
	case LogFormatJSON:
		Logger.SetFormatter(&logrus.JSONFormatter{
			CallerPrettyfier: PrettifyCaller,
			TimestampFormat:  opts.GetTimestampFormat(),
		})
	default:
		Logger.SetFormatter(&DefaultFormatter)
	}
}
