package alias

// Alias stores the logical structure for an alias
// instance
type Alias struct {
	Address     string `json:"address" gorm:"uniqueIndex;type:varchar(46);notNull"`
	Chain       string `json:"chain" gorm:"type:varchar(32);notNull"`
	Description string `json:"description"`
	Label       string `json:"label" gorm:"type:varchar(64)"`
	Type        string `json:"type" gorm:"type:varchar(16)"`
}
