package constants

const (
	// TimestampYmdHMS uses format "2006-01-02 15:04:05"
	TimestampYmdHMS = "2006-01-02 15:04:05"

	// TimestampYmdHMSz uses format "2006-01-02 15:04:05 -0700"
	TimestampYmdHMSz = "2006-01-02 15:04:05 -0700"

	// TimestampMySql uses format "2006-01-02 15:04:05.999999"
	TimestampMySql = "2006-01-02 15:04:05.999999"
)
