package erc721events

import (
	"context"
	"fmt"
	"math/big"
	"strings"
	"time"

	"github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/ethclient"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/spf13/cobra"
	"github.com/zephinzer/go-cronos/cmd/farmer/common/farmer"
	"github.com/zephinzer/go-cronos/internal/database"
	"github.com/zephinzer/go-cronos/internal/log"
	"github.com/zephinzer/go-cronos/pkg/erc721"
)

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:   "erc721events",
		Short: "Loads ERC721 events",
		RunE:  runE,
	}
	conf.ApplyToCobra(&command)
	return &command
}

func runE(command *cobra.Command, args []string) error {
	log.Infof("starting farmer...")
	configurationInstance, configError := InitConfiguration(conf)
	if configError != nil {
		return fmt.Errorf("failed to receive a valid configuration: %s", configError)
	}

	retryInterval := time.Second * 5
	dbMaxRetries := 10
	rpcMaxRetries := 10
	chainIdMaxretries := 10

	// test the rpc connection
	log.Infof("connecting to rpc[%s]...", configurationInstance.RpcUrl)
	var client *ethclient.Client
	var dialError error
	for client == nil {
		client, dialError = ethclient.Dial(configurationInstance.RpcUrl)
		if dialError != nil {
			log.Errorf("failed to dial rpc[%s]: %s", configurationInstance.RpcUrl, dialError)
			rpcMaxRetries--
			if rpcMaxRetries == 0 {
				return fmt.Errorf("failed to reach rpc[%s]", configurationInstance.RpcUrl)
			}
			log.Infof("dialing rpc again in %s", retryInterval.String())
		}
		<-time.After(retryInterval)
	}
	log.Infof("connected to rpc successfully")

	log.Info("retrieving chain id...")
	var chainId *big.Int
	var chainIdError error
	for chainId == nil {
		chainId, chainIdError = client.ChainID(context.Background())
		if chainIdError != nil {
			log.Warnf("failed to get chain id: %s", chainIdError)
			chainIdMaxretries--
			if chainIdMaxretries == 0 {
				return fmt.Errorf("failed to get chain id: %s", chainIdError)
			}
			log.Infof("getting chain id again in %s", retryInterval.String())
		}
		<-time.After(retryInterval)
	}
	log.Infof("retrieved chain id[%s]", chainId.String())
	log.Infof("verifying provided address '%s'", configurationInstance.Address.String())
	if bytecode, err := client.CodeAt(context.Background(), configurationInstance.Address, nil); err != nil || len(bytecode) == 0 {
		return fmt.Errorf("failed to receive a contract address: %s", err)
	}
	defer client.Close()

	// connect to database
	log.Infof("connecting to db at %s:%v...", configurationInstance.DatabaseHost, configurationInstance.DatabasePort)
	db, err := database.GetConnection(database.GetConnectionOpts{
		Username:      configurationInstance.DatabaseUsername,
		Password:      configurationInstance.DatabasePassword,
		Host:          configurationInstance.DatabaseHost,
		Port:          uint64(configurationInstance.DatabasePort),
		Database:      configurationInstance.DatabaseName,
		RetryInterval: retryInterval,
		MaxRetryCount: uint64(dbMaxRetries),
		Log:           func(l string, a ...any) { log.Infof(l, a...) },
		LogError:      func(l string, a ...any) { log.Warnf(l, a...) },
	})
	if err != nil {
		return fmt.Errorf("failed to create db connection: %s", err)
	}

	if configurationInstance.TillBlock == 0 {
		latestBlockInstance, err := client.BlockByNumber(context.Background(), nil)
		if err != nil {
			return fmt.Errorf("failed to get the latest block: %s", err)
		}
		configurationInstance.TillBlock = latestBlockInstance.Number().Int64()
	}
	if configurationInstance.FromBlock == 0 {
		configurationInstance.FromBlock = configurationInstance.TillBlock - configurationInstance.BatchSize
	}

	fromBlock := big.NewInt(configurationInstance.FromBlock)
	toBlock := big.NewInt(configurationInstance.TillBlock)
	targetContract := configurationInstance.Address

	boundContract := bind.NewBoundContract(targetContract, erc721.Contract, client, nil, nil)
	tokenSymbolResult := []interface{}{}
	if err := boundContract.Call(&bind.CallOpts{}, &tokenSymbolResult, "symbol"); err != nil {
		return fmt.Errorf("failed to call symbol(): %s", err)
	}
	tokenSymbol := tokenSymbolResult[0].(string)
	log.Infof("symbol: $%s\n", tokenSymbol)

	batchSize := configurationInstance.BatchSize
	maxBatchSize := configurationInstance.TillBlock - configurationInstance.FromBlock
	if maxBatchSize < batchSize {
		batchSize = maxBatchSize
	}

	errors := []string{}
	log.Infof("retrieving all events from block[%v] to block[%v] in btaches of %v...", fromBlock.Int64(), toBlock.Int64(), batchSize)
	for startingBlock := fromBlock.Int64(); startingBlock < toBlock.Int64(); startingBlock += batchSize {
		endingBlock := startingBlock + batchSize - 1
		currentBlock, err := client.BlockNumber(context.Background())
		if err != nil {
			log.Warnf("failed to get current block number: %s", err)
		}
		log.Infof("block info: from[%v] to[%v] current[%v]", startingBlock, endingBlock, currentBlock)
		if currentBlock != 0 && endingBlock > int64(currentBlock) {
			endingBlock = int64(currentBlock) - 1
		}
		log.Infof("processing block[%v] to block[%v]...", startingBlock, endingBlock)
		logs, err := client.FilterLogs(context.Background(), ethereum.FilterQuery{
			FromBlock: big.NewInt(startingBlock),
			ToBlock:   big.NewInt(endingBlock),
			Addresses: []common.Address{targetContract},
		})
		if err != nil {
			return fmt.Errorf("failed to get filtered logs from block[%v] to block[%v] for contract[%s]: %s", fromBlock.Int64(), toBlock.Int64(), targetContract.String(), err)
		}
		log.Infof("received and processing %v logs...", len(logs))
		erc721events := farmer.Erc721Events{}
		blockTimestampMap, err := prepareBlockTimestamps(client, startingBlock, endingBlock)
		if err != nil {
			return fmt.Errorf("failed to get block timestamps: %s", err)
		}
		for _, logInstance := range logs {
			log.Infof("processing erc721 logs for tx[%s]", logInstance.TxHash.String())
			tx, _, err := client.TransactionByHash(context.Background(), logInstance.TxHash)
			if err != nil {
				log.Warnf("failed to get tx[%s]: %s", logInstance.TxHash.Hex(), err)
				continue
			}
			event, err := erc721.Contract.EventByID(logInstance.Topics[0])
			if err != nil {
				log.Warnf("failed to read erc721 event by id: %s", err)
				continue
			}
			txFrom := "UNKNOWN"
			msg, err := tx.AsMessage(types.NewLondonSigner(chainId), nil)
			if err == nil {
				txFrom = msg.From().Hex()
			}
			fmt.Printf("<%s> called on %s by %s at %v\n", event.Name, configurationInstance.Address.Hex(), txFrom, blockTimestampMap[int64(logInstance.BlockNumber)])
			switch event.Name {
			case "ApprovalForAll": // for marketplaces or rugs
				data, err := erc721.Contract.Unpack(event.Name, logInstance.Data)
				if err != nil {
					return fmt.Errorf("failed to unpack approval data: %s", err)
				}
				approved := data[0].(bool)
				owner := common.HexToAddress(logInstance.Topics[1].Hex()).Hex()
				spender := common.HexToAddress(logInstance.Topics[2].Hex()).Hex()
				log.Infof("approval for spending of all %s from %s by %s set to %v",
					tokenSymbol,
					owner[:6]+"..."+owner[len(owner)-4:],
					spender[:6]+"..."+spender[len(spender)-4:],
					approved,
				)
				erc721events = append(erc721events, farmer.Erc721Event{
					Address:         configurationInstance.Address.Hex(),
					AddressBy:       txFrom,
					AddressFrom:     owner,
					AddressTo:       spender,
					Approval:        approved,
					TransactionHash: logInstance.TxHash.Hex(),
					BlockNumber:     logInstance.BlockNumber,
					Timestamp:       blockTimestampMap[int64(logInstance.BlockNumber)],
					Index:           uint64(logInstance.Index),
					Type:            event.Name,
					Symbol:          tokenSymbol,
				})
			case "Approval":
				fmt.Println(logInstance.TxHash.String())
				addressFrom := common.HexToAddress(logInstance.Topics[1].Hex()).Hex()
				tokenId := logInstance.Topics[3].Big().Uint64()
				log.Infof("approval to transfer %s[%v] by %s",
					tokenSymbol,
					tokenId,
					addressFrom[:6]+"..."+addressFrom[len(addressFrom)-4:],
				)
				erc721events = append(erc721events, farmer.Erc721Event{
					Address:         configurationInstance.Address.Hex(),
					AddressBy:       txFrom,
					AddressFrom:     addressFrom,
					TokenId:         tokenId,
					TransactionHash: logInstance.TxHash.Hex(),
					BlockNumber:     logInstance.BlockNumber,
					Timestamp:       blockTimestampMap[int64(logInstance.BlockNumber)],
					Index:           uint64(logInstance.Index),
					Type:            event.Name,
					Symbol:          tokenSymbol,
				})
			case "Transfer":
				addressFrom := common.HexToAddress(logInstance.Topics[1].Hex()).Hex()
				addressTo := common.HexToAddress(logInstance.Topics[2].Hex()).Hex()
				tokenId := logInstance.Topics[3].Big().Uint64()
				log.Infof("transfer of %s[%v] from %s to %s",
					tokenSymbol,
					tokenId,
					addressFrom[:6]+"..."+addressFrom[len(addressFrom)-4:],
					addressTo[:6]+"..."+addressTo[len(addressTo)-4:],
				)
				erc721events = append(erc721events, farmer.Erc721Event{
					Address:         configurationInstance.Address.Hex(),
					AddressBy:       txFrom,
					AddressFrom:     addressFrom,
					AddressTo:       addressTo,
					TokenId:         tokenId,
					TransactionHash: logInstance.TxHash.Hex(),
					BlockNumber:     logInstance.BlockNumber,
					Timestamp:       blockTimestampMap[int64(logInstance.BlockNumber)],
					Index:           uint64(logInstance.Index),
					Type:            event.Name,
					Symbol:          tokenSymbol,
				})
			}
		}
		log.Infof("processed %v events from %v logs, inserting into db...", len(erc721events), len(logs))
		// x, _ := json.MarshalIndent(erc721events, "", "  ")
		// fmt.Println(string(x))
		if err := erc721events.Insert(db); err != nil {
			log.Warnf("failed to process events from block[%v] to block[%v]: %s", currentBlock, endingBlock, err)
			errors = append(errors, err.Error())
		}
	}
	if len(errors) > 0 {
		return fmt.Errorf("failed to process all events successfully:\n  - %s", strings.Join(errors, "\n  - "))
	}

	return nil
}
