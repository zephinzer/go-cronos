package erc721events

import (
	"context"
	"fmt"
	"math/big"
	"time"

	"github.com/ethereum/go-ethereum/ethclient"
)

func prepareBlockTimestamps(client *ethclient.Client, startingBlock, endingBlock int64) (map[int64]time.Time, error) {
	// 31 - 10 = 21
	totalBlockCount := endingBlock - startingBlock
	// [10]
	intervals := []int64{}
	intervalCount := int64(10)
	// 21 / 10 = 2
	intervalSize := totalBlockCount / int64(intervalCount)

	// 10,12,14,16,18,20,22,24,26,28
	for i := int64(0); i < intervalCount; i++ { // 0,1,2,3,4,5,6,7,8,9
		intervals = append(intervals, startingBlock+(intervalSize*i))
	}
	// this is for the last stretch to the specified endingBlock
	intervals = append(intervals, startingBlock+(intervalSize*intervalCount))
	intervals = append(intervals, endingBlock)

	blockTimestamps := map[int64]time.Time{}
	errors := []string{}
	for i := 1; i < len(intervals); i++ {
		startBlockNumber := big.NewInt(intervals[i-1])
		startBlock, err := client.BlockByNumber(context.Background(), startBlockNumber)
		if err != nil {
			return nil, fmt.Errorf("failed to get block[%v]: %s", startBlockNumber.Int64(), err)
		}
		endBlockNumber := big.NewInt(intervals[i])
		endBlock, err := client.BlockByNumber(context.Background(), endBlockNumber)
		if err != nil {
			return nil, fmt.Errorf("failed to get block[%v]: %s", endBlockNumber.Int64(), err)
		}
		durationBetweenBlocks := endBlock.Time() - startBlock.Time()
		blockCount := intervals[i] - intervals[i-1]
		if blockCount == 0 {
			blockCount = 1
		}
		averageDurationOfBlock := int64(durationBetweenBlocks / uint64(blockCount))
		for j := int64(0); j <= blockCount; j++ {
			blockTimestamps[startBlockNumber.Int64()+int64(j)] = time.Unix(
				int64(startBlock.Time())+
					(int64(j)*averageDurationOfBlock), 0)
		}
	}

	if len(errors) > 0 {
		return blockTimestamps, fmt.Errorf("failed to prepare all blocktimestamps")
	}
	return blockTimestamps, nil
}
