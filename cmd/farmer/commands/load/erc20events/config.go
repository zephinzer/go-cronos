package erc20events

import (
	"fmt"
	"regexp"

	"github.com/ethereum/go-ethereum/common"
	"github.com/usvc/go-config"
	"github.com/zephinzer/go-cronos/cmd/farmer/common/configuration"
)

const (
	ConfigKeyAddress   = "address"
	ConfigKeyBatchSize = "batch-size"
	ConfigKeyFromBlock = "from-block"
	ConfigKeyRpcUrl    = "rpc-url"
	ConfigKeyTillBlock = "till-block"

	DefaultBatchSize = 2000
	DefaultRpcUrl    = "https://evm.cronos.org"
)

var conf = config.Map{
	configuration.KeyDatabaseHost:     configuration.GetDatabaseHost(),
	configuration.KeyDatabasePort:     configuration.GetDatabasePort(),
	configuration.KeyDatabaseName:     configuration.GetDatabaseName(),
	configuration.KeyDatabaseUsername: configuration.GetDatabaseUsername(),
	configuration.KeyDatabasePassword: configuration.GetDatabasePassword(),
	ConfigKeyAddress: &config.String{
		Shorthand: "A",
		Usage:     "retrieve events from the contract at this address",
	},
	ConfigKeyBatchSize: &config.Uint{
		Shorthand: "b",
		Usage:     "number of blocks to harvest at once (max 2000)",
		Default:   DefaultBatchSize,
	},
	ConfigKeyFromBlock: &config.Uint{
		Shorthand: "f",
		Usage:     "starting block (inclusive) number",
	},
	ConfigKeyRpcUrl: &config.String{
		Shorthand: "r",
		Usage:     "RPC URL to use",
		Default:   DefaultRpcUrl,
	},
	ConfigKeyTillBlock: &config.Uint{
		Shorthand: "t",
		Usage:     "final block (inclusive) number",
	},
}

func InitConfiguration(conf config.Map) (*commandConfiguration, error) {
	address := conf.GetString(ConfigKeyAddress)
	if !regexp.MustCompile("^0x[0-9a-fA-F]{40}$").MatchString(address) {
		return nil, fmt.Errorf("failed to receive a valid address (got '%s')", address)
	}
	batchSize := int64(conf.GetUint(ConfigKeyBatchSize))
	if batchSize > 2000 {
		batchSize = 2000
	}
	contractAddress := common.HexToAddress(address)
	configurationInstance := &commandConfiguration{
		Address:          contractAddress,
		BatchSize:        batchSize,
		DatabaseHost:     conf.GetString(configuration.KeyDatabaseHost),
		DatabasePort:     conf.GetUint(configuration.KeyDatabasePort),
		DatabaseUsername: conf.GetString(configuration.KeyDatabaseUsername),
		DatabasePassword: conf.GetString(configuration.KeyDatabasePassword),
		DatabaseName:     conf.GetString(configuration.KeyDatabaseName),
		FromBlock:        int64(conf.GetUint(ConfigKeyFromBlock)),
		RpcUrl:           conf.GetString(ConfigKeyRpcUrl),
		TillBlock:        int64(conf.GetUint(ConfigKeyTillBlock)),
	}
	return configurationInstance, nil
}

type commandConfiguration struct {
	Address          common.Address
	BatchSize        int64
	DatabaseHost     string
	DatabasePort     uint
	DatabaseUsername string
	DatabasePassword string
	DatabaseName     string
	FromBlock        int64
	RpcUrl           string
	TillBlock        int64
}

func (c *commandConfiguration) GetMySqlDsn() string {
	return fmt.Sprintf(
		"%s:%s@tcp(%s:%v)/%s",
		c.DatabaseUsername,
		c.DatabasePassword,
		c.DatabaseHost,
		c.DatabasePort,
		c.DatabaseName,
	)
}

func (c commandConfiguration) GetPublicMySqlConnectionString() string {
	return fmt.Sprintf(
		"%s@%s:%v/%s",
		c.DatabaseUsername,
		c.DatabaseHost,
		c.DatabasePort,
		c.DatabaseName,
	)
}
