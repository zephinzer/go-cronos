package erc20stats

import (
	"fmt"
	"io/ioutil"

	"github.com/ethereum/go-ethereum/common"
	"github.com/usvc/go-config"
	"github.com/zephinzer/go-cronos/cmd/farmer/common/configuration"
	"github.com/zephinzer/go-cronos/internal/log"
	"github.com/zephinzer/go-cronos/pkg/addressbook"
	"github.com/zephinzer/go-cronos/pkg/evmutils"
	"gopkg.in/yaml.v2"
)

const (
	ConfigKeyConfigPath = "config"
	ConfigKeyRpcUrl     = "rpc-url"

	DefaultBatchSize = 2000
	DefaultRpcUrl    = "https://evm.cronos.org"
)

var conf = config.Map{
	configuration.KeyDatabaseHost:     configuration.GetDatabaseHost(),
	configuration.KeyDatabasePort:     configuration.GetDatabasePort(),
	configuration.KeyDatabaseName:     configuration.GetDatabaseName(),
	configuration.KeyDatabaseUsername: configuration.GetDatabaseUsername(),
	configuration.KeyDatabasePassword: configuration.GetDatabasePassword(),
	ConfigKeyConfigPath: &config.String{
		Shorthand: "c",
		Usage:     "path to the configuration file",
	},
	ConfigKeyRpcUrl: &config.String{
		Shorthand: "r",
		Usage:     "RPC URL to use",
		Default:   DefaultRpcUrl,
	},
}

type fileConfiguration []tokenStatsTargetId

// tokenStatsTarget should be the address or alias (as recorded
// in the known_collections.yaml)
type tokenStatsTargetId string

// InitConfiguration sets up the internal configuration class instance
// which should guarantee that values are valid. If an invalid value is
// found, this function should return with an error and `nil` as the
// first value
func InitConfiguration(conf config.Map) (*commandConfiguration, error) {
	rpcUrl := conf.GetString(ConfigKeyRpcUrl)
	client, chainId, err := evmutils.NewClient(evmutils.NewClientOpts{
		RpcUrl: rpcUrl,
	})
	if err != nil {
		return nil, fmt.Errorf("failed to connect to provided rpc[%s]: %s", rpcUrl, err)
	}
	defer client.Close()
	selectedChain := addressbook.ChainById[int(chainId.Int64())]
	configFilePath := conf.GetString(ConfigKeyConfigPath)
	if len(configFilePath) == 0 {
		return nil, fmt.Errorf("failed to receive path to configuration file (use --%s)", ConfigKeyConfigPath)
	}
	configFileContents, err := ioutil.ReadFile(configFilePath)
	if err != nil {
		return nil, fmt.Errorf("failed to read config file at '%s': %s", configFilePath, err)
	}
	fileConfigurationInstance := fileConfiguration{}
	if err := yaml.Unmarshal(configFileContents, &fileConfigurationInstance); err != nil {
		return nil, fmt.Errorf("failed to parse config file at '%s': %s\n\nconfig content:\n%s", configFilePath, err, string(configFileContents))
	}

	tokenStatsTargetsInstance := tokenStatsTargets{}
	for _, tokenStatsTargetId := range fileConfigurationInstance {
		if directoryEntry, ok := selectedChain.Directory[string(tokenStatsTargetId)]; ok {
			tokenStatsTargetsInstance = append(tokenStatsTargetsInstance, tokenStatsTarget{
				Id:      string(tokenStatsTargetId),
				Address: common.HexToAddress(directoryEntry.Address),
			})
			continue
		}
		log.Warnf("failed to identify '%s' in our known addresses", tokenStatsTargetId)
	}

	configurationInstance := &commandConfiguration{
		DatabaseHost:     conf.GetString(configuration.KeyDatabaseHost),
		DatabasePort:     conf.GetUint(configuration.KeyDatabasePort),
		DatabaseUsername: conf.GetString(configuration.KeyDatabaseUsername),
		DatabasePassword: conf.GetString(configuration.KeyDatabasePassword),
		DatabaseName:     conf.GetString(configuration.KeyDatabaseName),
		RpcUrl:           rpcUrl,
		Targets:          tokenStatsTargetsInstance,
	}
	return configurationInstance, nil

}

type tokenStatsTargets []tokenStatsTarget

type tokenStatsTarget struct {
	Id      string         `json:"id"`
	Address common.Address `json:"address"`
}

// commandConfiguration is the internally utilised configuration
// object to get guaranteed valid configuration values from
type commandConfiguration struct {
	DatabaseHost     string
	DatabasePort     uint
	DatabaseUsername string
	DatabasePassword string
	DatabaseName     string
	RpcUrl           string
	Targets          []tokenStatsTarget
}
