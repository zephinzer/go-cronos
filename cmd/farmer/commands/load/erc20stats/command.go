package erc20stats

import (
	"encoding/json"
	"fmt"
	"math"
	"math/big"
	"time"

	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/spf13/cobra"
	"github.com/zephinzer/go-cronos/cmd/farmer/common/farmer"
	"github.com/zephinzer/go-cronos/internal/database"
	"github.com/zephinzer/go-cronos/internal/log"
	"github.com/zephinzer/go-cronos/pkg/erc20"
	"github.com/zephinzer/go-cronos/pkg/evmutils"
)

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:   "erc20stats",
		Short: "Loads ERC20 token statistics for the target tokens",
		RunE:  runE,
	}
	conf.ApplyToCobra(&command)
	return &command
}

func runE(command *cobra.Command, args []string) error {
	log.Infof("starting erc20 statistics loader...")
	configurationInstance, configError := InitConfiguration(conf)
	if configError != nil {
		return fmt.Errorf("failed to receive a valid configuration: %s", configError)
	}

	retryInterval := time.Second * 5
	dbMaxRetries := 10
	rpcMaxRetries := 10

	// test the rpc connection
	log.Infof("connecting to rpc[%s]...", configurationInstance.RpcUrl)
	events := make(chan evmutils.NewClientEvent, 10)
	go func() {
		done := false
		for !done {
			select {
			case event := <-events:
				done = event.Done
				if event.Error != nil {
					log.Errorf("%s", event.Error)
				} else {
					log.Infof("%s", event.Message)
				}
			default:
			}
		}
	}()
	client, chainId, rpcError := evmutils.NewClient(evmutils.NewClientOpts{
		Events:     events,
		MaxRetries: rpcMaxRetries,
		RpcUrl:     configurationInstance.RpcUrl,
	})
	if rpcError != nil {
		return fmt.Errorf("failed to create an evm client: %s", rpcError)
	}
	log.Infof("connected to chain[%v] via rpc[%s]", chainId, configurationInstance.RpcUrl)
	defer client.Close()

	// connect to database
	log.Infof("connecting to db at %s:%v...", configurationInstance.DatabaseHost, configurationInstance.DatabasePort)
	db, err := database.GetConnection(database.GetConnectionOpts{
		Username:      configurationInstance.DatabaseUsername,
		Password:      configurationInstance.DatabasePassword,
		Host:          configurationInstance.DatabaseHost,
		Port:          uint64(configurationInstance.DatabasePort),
		Database:      configurationInstance.DatabaseName,
		RetryInterval: retryInterval,
		MaxRetryCount: uint64(dbMaxRetries),
		Log:           func(l string, a ...any) { log.Infof(l, a...) },
		LogError:      func(l string, a ...any) { log.Warnf(l, a...) },
	})
	if err != nil {
		return fmt.Errorf("failed to create db connection: %s", err)
	}

	erc20statistics := farmer.Erc20Statistics{}
	for _, target := range configurationInstance.Targets {
		erc20statistic := farmer.Erc20Statistic{
			Address: target.Address.String(),
		}
		boundContract := bind.NewBoundContract(target.Address, erc20.Contract, client, nil, nil)
		var results []interface{}
		if err := boundContract.Call(&bind.CallOpts{}, &results, "decimals"); err != nil {
			return fmt.Errorf("failed to call decimals(): %s", err)
		}
		decimals := results[0].(uint8)
		decimalPlaceDivisor := math.Pow(10, float64(decimals))
		erc20statistic.Decimals = int(decimals)

		results = []interface{}{}
		if err := boundContract.Call(&bind.CallOpts{}, &results, "totalSupply"); err != nil {
			return fmt.Errorf("failed to call totalSupply(): %s", err)
		}
		totalSupply := results[0].(*big.Int)
		totalSupplyFloat, _ := big.NewFloat(0).Quo(
			big.NewFloat(0).SetInt(totalSupply),
			big.NewFloat(decimalPlaceDivisor),
		).Float64()
		erc20statistic.TotalSupply = totalSupplyFloat

		results = []interface{}{}
		if err := boundContract.Call(&bind.CallOpts{}, &results, "symbol"); err != nil {
			return fmt.Errorf("failed to call symbol(): %s", err)
		}
		symbol := results[0].(string)
		erc20statistic.Symbol = symbol
		erc20statistics = append(erc20statistics, erc20statistic)
	}

	x, _ := json.MarshalIndent(erc20statistics, "", "  ")
	fmt.Println(string(x))

	if err := erc20statistics.Insert(db); err != nil {
		return fmt.Errorf("failed to insert %v balances into the database: %s", len(erc20statistics), err)
	}
	return nil
}
