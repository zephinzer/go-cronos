package balances

import (
	"fmt"
	"io/ioutil"

	"github.com/ethereum/go-ethereum/common"
	"github.com/usvc/go-config"
	"github.com/zephinzer/go-cronos/cmd/farmer/common/configuration"
	"github.com/zephinzer/go-cronos/internal/log"
	"github.com/zephinzer/go-cronos/pkg/addressbook"
	"github.com/zephinzer/go-cronos/pkg/evmutils"
	"gopkg.in/yaml.v3"
)

const (
	ConfigKeyConfigPath = "config"
	ConfigKeyRpcUrl     = "rpc-url"

	DefaultBatchSize = 2000
	DefaultRpcUrl    = "https://evm.cronos.org"
)

var conf = config.Map{
	configuration.KeyDatabaseHost:     configuration.GetDatabaseHost(),
	configuration.KeyDatabasePort:     configuration.GetDatabasePort(),
	configuration.KeyDatabaseName:     configuration.GetDatabaseName(),
	configuration.KeyDatabaseUsername: configuration.GetDatabaseUsername(),
	configuration.KeyDatabasePassword: configuration.GetDatabasePassword(),
	ConfigKeyConfigPath: &config.String{
		Shorthand: "c",
		Usage:     "path to the configuration file",
	},
	ConfigKeyRpcUrl: &config.String{
		Shorthand: "r",
		Usage:     "RPC URL to use",
		Default:   DefaultRpcUrl,
	},
}

type fileConfiguration []tokenHolderTarget

type tokenHolderTarget struct {
	Holder string `json:"holder" yaml:"holder"`
	Token  string `json:"token" yaml:"token"`
}

func InitConfiguration(conf config.Map) (*commandConfiguration, error) {
	rpcUrl := conf.GetString(ConfigKeyRpcUrl)
	client, chainId, err := evmutils.NewClient(evmutils.NewClientOpts{
		RpcUrl: rpcUrl,
	})
	if err != nil {
		return nil, fmt.Errorf("failed to connect to provided rpc[%s]: %s", rpcUrl, err)
	}
	defer client.Close()
	selectedChain := addressbook.ChainById[int(chainId.Int64())]
	configFilePath := conf.GetString(ConfigKeyConfigPath)
	if len(configFilePath) == 0 {
		return nil, fmt.Errorf("failed to receive path to configuration file (use --%s)", ConfigKeyConfigPath)
	}
	configFileContents, err := ioutil.ReadFile(configFilePath)
	if err != nil {
		return nil, fmt.Errorf("failed to read config file at '%s': %s", configFilePath, err)
	}
	fileConfigurationInstance := fileConfiguration{}
	if err := yaml.Unmarshal(configFileContents, &fileConfigurationInstance); err != nil {
		return nil, fmt.Errorf("failed to parse config file at '%s': %s\n\nconfig content:\n%s", configFilePath, err, string(configFileContents))
	}
	targetsInstance := targetsConfig{}
	for _, tokenHolder := range fileConfigurationInstance {
		// log.Infof("holder label: %s", tokenHolder.Holder)
		var holderLabel string
		var holderAddress common.Address

		if holder, ok := selectedChain.Directory[tokenHolder.Holder]; !ok {
			if common.IsHexAddress(tokenHolder.Holder) {
				holderLabel = tokenHolder.Holder
				holderAddress = common.HexToAddress(tokenHolder.Holder)
			} else {
				log.Warnf("failed to identify provided holder: %s", tokenHolder.Holder)
				continue
			}
		} else {
			holderLabel = tokenHolder.Holder
			holderAddress = common.HexToAddress(holder.Address)
		}
		// log.Infof("holder: %s", holder.Address)
		// log.Infof("token label: %s", tokenHolder.Token)
		var tokenLabel string
		var tokenAddress common.Address
		token, ok := selectedChain.Directory[tokenHolder.Token]
		if !ok {
			if common.IsHexAddress(tokenHolder.Token) {
				tokenLabel = tokenHolder.Token
				tokenAddress = common.HexToAddress(tokenHolder.Token)
			} else {
				log.Warnf("failed to identify provided token: %s", tokenHolder.Token)
				continue
			}
		} else {
			tokenLabel = tokenHolder.Token
			tokenAddress = common.HexToAddress(token.Address)
		}
		// log.Infof("token: %s", token.Address)

		targetsInstance = append(targetsInstance, targetConfig{
			Holder:      holderAddress,
			HolderLabel: holderLabel,
			Token:       tokenAddress,
			TokenLabel:  tokenLabel,
		})
	}

	configurationInstance := &commandConfiguration{
		DatabaseHost:     conf.GetString(configuration.KeyDatabaseHost),
		DatabasePort:     conf.GetUint(configuration.KeyDatabasePort),
		DatabaseUsername: conf.GetString(configuration.KeyDatabaseUsername),
		DatabasePassword: conf.GetString(configuration.KeyDatabasePassword),
		DatabaseName:     conf.GetString(configuration.KeyDatabaseName),
		RpcUrl:           rpcUrl,
		Targets:          targetsInstance,
	}
	return configurationInstance, nil
}

type targetsConfig []targetConfig

type targetConfig struct {
	Holder      common.Address
	HolderLabel string
	Token       common.Address
	TokenLabel  string
}

type commandConfiguration struct {
	DatabaseHost     string
	DatabasePort     uint
	DatabaseUsername string
	DatabasePassword string
	DatabaseName     string
	RpcUrl           string
	Targets          targetsConfig
}

func (c *commandConfiguration) GetMySqlDsn() string {
	return fmt.Sprintf(
		"%s:%s@tcp(%s:%v)/%s",
		c.DatabaseUsername,
		c.DatabasePassword,
		c.DatabaseHost,
		c.DatabasePort,
		c.DatabaseName,
	)
}

func (c commandConfiguration) GetPublicMySqlConnectionString() string {
	return fmt.Sprintf(
		"%s@%s:%v/%s",
		c.DatabaseUsername,
		c.DatabaseHost,
		c.DatabasePort,
		c.DatabaseName,
	)
}
