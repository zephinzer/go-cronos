package balances

import (
	"context"
	"fmt"
	"math"
	"math/big"
	"time"

	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/ethclient"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/spf13/cobra"
	"github.com/zephinzer/go-cronos/cmd/farmer/common/farmer"
	"github.com/zephinzer/go-cronos/internal/database"
	"github.com/zephinzer/go-cronos/internal/log"
	"github.com/zephinzer/go-cronos/pkg/erc20"
)

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:   "balances",
		Short: "Loads token balance of a holder given a list of token-holder pairs",
		RunE:  runE,
	}
	conf.ApplyToCobra(&command)
	return &command
}

func runE(command *cobra.Command, args []string) error {
	log.Infof("starting balance loader...")
	configurationInstance, configError := InitConfiguration(conf)
	if configError != nil {
		return fmt.Errorf("failed to receive a valid configuration: %s", configError)
	}

	retryInterval := time.Second * 5
	dbMaxRetries := 10
	rpcMaxRetries := 10
	chainIdMaxretries := 10

	// test the rpc connection
	log.Infof("connecting to rpc[%s]...", configurationInstance.RpcUrl)
	var client *ethclient.Client
	var dialError error
	for client == nil {
		client, dialError = ethclient.Dial(configurationInstance.RpcUrl)
		if dialError != nil {
			log.Errorf("failed to dial rpc[%s]: %s", configurationInstance.RpcUrl, dialError)
			rpcMaxRetries--
			if rpcMaxRetries == 0 {
				return fmt.Errorf("failed to reach rpc[%s]", configurationInstance.RpcUrl)
			}
			log.Infof("dialing rpc again in %s", retryInterval.String())
		}
		<-time.After(retryInterval)
	}
	log.Infof("connected to rpc successfully")

	log.Info("retrieving chain id...")
	var chainId *big.Int
	var chainIdError error
	for chainId == nil {
		chainId, chainIdError = client.ChainID(context.Background())
		if chainIdError != nil {
			log.Warnf("failed to get chain id: %s", chainIdError)
			chainIdMaxretries--
			if chainIdMaxretries == 0 {
				return fmt.Errorf("failed to get chain id: %s", chainIdError)
			}
			log.Infof("getting chain id again in %s", retryInterval.String())
		}
		<-time.After(retryInterval)
	}
	log.Infof("retrieved chain id[%s]", chainId.String())
	defer client.Close()

	// connect to database
	log.Infof("connecting to db at %s:%v...", configurationInstance.DatabaseHost, configurationInstance.DatabasePort)
	db, err := database.GetConnection(database.GetConnectionOpts{
		Username:      configurationInstance.DatabaseUsername,
		Password:      configurationInstance.DatabasePassword,
		Host:          configurationInstance.DatabaseHost,
		Port:          uint64(configurationInstance.DatabasePort),
		Database:      configurationInstance.DatabaseName,
		RetryInterval: retryInterval,
		MaxRetryCount: uint64(dbMaxRetries),
		Log:           func(l string, a ...any) { log.Infof(l, a...) },
		LogError:      func(l string, a ...any) { log.Warnf(l, a...) },
	})
	if err != nil {
		return fmt.Errorf("failed to create db connection: %s", err)
	}

	erc20balances := farmer.Erc20Balances{}
	for _, target := range configurationInstance.Targets {
		log.Infof("retrieving balance of token[%s] for wallet[%s]...", target.TokenLabel, target.HolderLabel)
		boundContract := bind.NewBoundContract(target.Token, erc20.Contract, client, nil, nil)
		decimalsResult := []interface{}{}
		if err := boundContract.Call(&bind.CallOpts{}, &decimalsResult, "decimals"); err != nil {
			return fmt.Errorf("failed to call decimals(): %s", err)
		}
		decimalPlaces := decimalsResult[0].(uint8)
		decimalPlaceDivisor := math.Pow(10, float64(decimalPlaces))
		log.Infof("decimal places: %v", decimalPlaces)
		var balance []interface{}
		if err := boundContract.Call(&bind.CallOpts{}, &balance, "balanceOf", target.Holder); err != nil {
			return fmt.Errorf("failed to call balanceOf(): %s", err)
		}
		tokenSymbolResult := []interface{}{}
		if err := boundContract.Call(&bind.CallOpts{}, &tokenSymbolResult, "symbol"); err != nil {
			return fmt.Errorf("failed to call symbol(): %s", err)
		}
		tokenSymbol := tokenSymbolResult[0].(string)
		tokenBalance := big.NewFloat(0).SetInt(balance[0].(*big.Int))
		tokenBalanceHumanReadable := big.NewFloat(0).Quo(tokenBalance, big.NewFloat(decimalPlaceDivisor))
		log.Infof("wallet[%s] has %s %s", target.HolderLabel, tokenBalanceHumanReadable.String(), tokenSymbol)

		tokenBalanceValue, _ := tokenBalanceHumanReadable.Float64()
		erc20balances = append(erc20balances, farmer.Erc20Balance{
			Address:        target.Holder.Hex(),
			AddressLabel:   target.HolderLabel,
			BalanceAddress: target.Token.Hex(),
			BalanceAmount:  tokenBalanceValue,
			BalanceSymbol:  tokenSymbol,
		})
	}

	// x, _ := json.MarshalIndent(erc20balances, "", "  ")
	// fmt.Println(string(x))

	if err := erc20balances.Insert(db); err != nil {
		return fmt.Errorf("failed to insert %v balances into the database: %s", len(erc20balances), err)
	}

	return nil
}
