package info

import (
	"fmt"
	"io/ioutil"

	"github.com/ethereum/go-ethereum/common"
	"github.com/usvc/go-config"
	"github.com/zephinzer/go-cronos/cmd/farmer/common/configuration"
	"github.com/zephinzer/go-cronos/internal/log"
	"github.com/zephinzer/go-cronos/pkg/addressbook"
	"github.com/zephinzer/go-cronos/pkg/evmutils"
	"gopkg.in/yaml.v2"
)

const (
	ConfigKeyConfigPath = "config"
	ConfigKeyOutput     = "output"
	ConfigKeyRpcUrl     = "rpc-url"

	DefaultBatchSize = 2000
	DefaultRpcUrl    = "https://evm.cronos.org"
)

var conf = config.Map{
	configuration.KeyDatabaseHost:     configuration.GetDatabaseHost(),
	configuration.KeyDatabasePort:     configuration.GetDatabasePort(),
	configuration.KeyDatabaseName:     configuration.GetDatabaseName(),
	configuration.KeyDatabaseUsername: configuration.GetDatabaseUsername(),
	configuration.KeyDatabasePassword: configuration.GetDatabasePassword(),
	ConfigKeyConfigPath: &config.String{
		Shorthand: "c",
		Usage:     "path to the configuration file",
	},
	ConfigKeyOutput: &config.String{
		Shorthand: "o",
		Usage:     "defines the output format",
		Default:   "db",
	},
	ConfigKeyRpcUrl: &config.String{
		Shorthand: "r",
		Usage:     "RPC URL to use",
		Default:   DefaultRpcUrl,
	},
}

// InitConfiguration sets up the internal configuration class instance
// which should guarantee that values are valid. If an invalid value is
// found, this function should return with an error and `nil` as the
// first value
func InitConfiguration(conf config.Map) (*internalConfiguration, error) {
	rpcUrl := conf.GetString(ConfigKeyRpcUrl)
	client, chainId, err := evmutils.NewClient(evmutils.NewClientOpts{
		RpcUrl: rpcUrl,
	})
	if err != nil {
		return nil, fmt.Errorf("failed to connect to provided rpc[%s]: %s", rpcUrl, err)
	}
	defer client.Close()
	selectedChain := addressbook.ChainById[int(chainId.Int64())]
	configFilePath := conf.GetString(ConfigKeyConfigPath)
	if len(configFilePath) == 0 {
		return nil, fmt.Errorf("failed to receive path to configuration file (use --%s)", ConfigKeyConfigPath)
	}
	configFileContents, err := ioutil.ReadFile(configFilePath)
	if err != nil {
		return nil, fmt.Errorf("failed to read config file at '%s': %s", configFilePath, err)
	}
	fileConfigurationInstance := fileConfiguration{}
	if err := yaml.Unmarshal(configFileContents, &fileConfigurationInstance); err != nil {
		return nil, fmt.Errorf("failed to parse config file at '%s': %s\n\nconfig content:\n%s", configFilePath, err, string(configFileContents))
	}

	burnAddresses := []common.Address{}
	for _, burnAddress := range fileConfigurationInstance.Global.BurnAddresses {
		var address string
		if directoryEntry, ok := selectedChain.Directory[burnAddress]; ok {
			address = directoryEntry.Address
		}
		if !common.IsHexAddress(address) {
			log.Warnf("failed to resolve burn address '%s', skipping...", burnAddress)
		}
		burnAddresses = append(burnAddresses, common.HexToAddress(address))
	}

	targetsInstance := targets{}
	for _, token := range fileConfigurationInstance.Tokens {
		targetInstance := target{}
		if common.IsHexAddress(token.Id) {
			targetInstance.Address = common.HexToAddress(token.Id)
		} else if directoryEntry, ok := selectedChain.Directory[token.Id]; ok {
			targetInstance.Address = common.HexToAddress(directoryEntry.Address)
		} else {
			log.Warnf("failed to resolve token id '%s', skipping...", token.Id)
			continue
		}

		targetInstance.BurnAddresses = append(targetInstance.BurnAddresses, burnAddresses...)
		for _, burnAddress := range token.BurnAddresses {
			var address string
			if directoryEntry, ok := selectedChain.Directory[burnAddress]; ok {
				address = directoryEntry.Address
			}
			if !common.IsHexAddress(address) {
				log.Warnf("failed to resolve burn address '%s', skipping...", burnAddress)
				continue
			}
			targetInstance.BurnAddresses = append(targetInstance.BurnAddresses, common.HexToAddress(address))
		}

		for _, ssContract := range token.SsContracts {
			var address string
			if directoryEntry, ok := selectedChain.Directory[ssContract]; ok {
				address = directoryEntry.Address
			}
			if !common.IsHexAddress(address) {
				log.Warnf("failed to resolve single stake contract '%s', skipping...", ssContract)
				continue
			}
			targetInstance.SsContracts = append(targetInstance.SsContracts, common.HexToAddress(address))

		}

		for _, lpContract := range token.LpContracts {
			var address string
			if directoryEntry, ok := selectedChain.Directory[lpContract]; ok {
				address = directoryEntry.Address
			}
			if !common.IsHexAddress(address) {
				log.Warnf("failed to resolve liquidity pool contract '%s', skipping...", lpContract)
				continue
			}
			targetInstance.LpContracts = append(targetInstance.LpContracts, common.HexToAddress(address))

		}

		for _, daoAddress := range token.DaoAddresses {
			var address string
			if directoryEntry, ok := selectedChain.Directory[daoAddress]; ok {
				address = directoryEntry.Address
			}
			if !common.IsHexAddress(address) {
				log.Warnf("failed to resolve dao address '%s', skipping...", daoAddress)
				continue
			}
			targetInstance.DaoAddresses = append(targetInstance.DaoAddresses, common.HexToAddress(address))
		}

		targetsInstance = append(targetsInstance, targetInstance)
	}

	configurationInstance := &internalConfiguration{
		DatabaseHost:     conf.GetString(configuration.KeyDatabaseHost),
		DatabasePort:     conf.GetUint(configuration.KeyDatabasePort),
		DatabaseUsername: conf.GetString(configuration.KeyDatabaseUsername),
		DatabasePassword: conf.GetString(configuration.KeyDatabasePassword),
		DatabaseName:     conf.GetString(configuration.KeyDatabaseName),
		Output:           conf.GetString(ConfigKeyOutput),
		RpcUrl:           rpcUrl,
		Targets:          targetsInstance,
	}
	return configurationInstance, nil

}

// fileConfiguration contains the expected structure of the
// file-based configuration
type fileConfiguration struct {
	// Global contains the global configuration for this service
	Global GlobalConfiguration `json:"global" yaml:"global"`

	// Tokens contains a list of target tokens which we should
	// scrape information for
	Tokens Tokens `json:"tokens" yaml:"tokens"`
}

// GlobalConfiguration contains the global configuration for
// this service
type GlobalConfiguration struct {
	// BurnAddresses is a list of burn addresses that will be
	// applied to all target tokens specified in the global
	// .Tokens property
	BurnAddresses []string `json:"burnAddresses" yaml:"burnAddresses"`
}

// Tokens is a list of Token instances
type Tokens []Token

// Token contains the configuration for the a single logical
// token which we are intending to track
type Token struct {
	// Id is an identifer for the token, this can be an identifier
	// from our `known_addresses.yaml` allowlist file or the raw
	// addresses. No validation is done if this is a raw address
	Id string `json:"id" yaml:"id"`

	// SsContracts is a list of contracts where single-staking
	// of the token can happen
	SsContracts []string `json:"ssContracts" yaml:"ssContracts"`

	// LpContracts is a list of liquditiy pool token addresses
	LpContracts []string `json:"lpContracts" yaml:"lpContracts"`

	// BurnAddresses is a list of wallet addresses which the
	// devs use as burn addresses
	BurnAddresses []string `json:"burnAddresses" yaml:"burnAddresses"`

	// DaoAddresses is a list of wallet addresses which the
	// devs use as the DAO
	DaoAddresses []string `json:"daoAddresses" yaml:"daoAddresses"`
}

// internalConfiguration is the internally utilised configuration
// object to get guaranteed valid configuration values from
type internalConfiguration struct {
	DatabaseHost     string
	DatabasePort     uint
	DatabaseUsername string
	DatabasePassword string
	DatabaseName     string
	Output           string
	RpcUrl           string
	Targets          targets
}

type targets []target

type target struct {
	Address       common.Address
	SsContracts   []common.Address
	LpContracts   []common.Address
	DaoAddresses  []common.Address
	BurnAddresses []common.Address
}
