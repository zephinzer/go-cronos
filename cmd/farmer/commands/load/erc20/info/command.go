package info

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"time"

	"github.com/spf13/cobra"
	"github.com/zephinzer/go-cronos/cmd/farmer/common/farmer"
	"github.com/zephinzer/go-cronos/internal/database"
	"github.com/zephinzer/go-cronos/internal/log"
	"github.com/zephinzer/go-cronos/pkg/addressbook"
	"github.com/zephinzer/go-cronos/pkg/erc20"
	"github.com/zephinzer/go-cronos/pkg/evmutils"
)

const helpText = `
This sub-command loads ERC20 token supplies in multiple wallets with
the goal of estimating circulating token supply
`

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:     "info",
		Aliases: []string{"i"},
		Short:   "Loads ERC20 token information for the target tokens",
		Long:    helpText,
		RunE:    runE,
	}
	conf.ApplyToCobra(&command)
	return &command
}

func runE(command *cobra.Command, args []string) error {
	log.Infof("starting erc20 info loader...")
	configurationInstance, configError := InitConfiguration(conf)
	if configError != nil {
		return fmt.Errorf("failed to receive a valid configuration: %s", configError)
	}

	retryInterval := time.Second * 5
	dbMaxRetries := 10

	// create the rpc connection
	events := make(chan evmutils.NewClientEvent, 10)
	go func() {
		done := false
		for !done {
			select {
			case event := <-events:
				done = event.Done
				if event.Error != nil {
					log.Errorf("%s", event.Error)
				} else {
					log.Infof("%s", event.Message)
				}
			default:
			}
		}
	}()
	client, chainId, rpcError := evmutils.NewClient(evmutils.NewClientOpts{
		Events:     events,
		MaxRetries: 3,
		RpcUrl:     configurationInstance.RpcUrl,
	})
	if rpcError != nil {
		return fmt.Errorf("failed to create an evm client: %s", rpcError)
	}
	log.Infof("connected to chain[%v] via rpc[%s]", chainId, configurationInstance.RpcUrl)
	defer client.Close()
	selectedChain := addressbook.ChainById[int(chainId.Int64())]

	var db *sql.DB
	if configurationInstance.Output == "db" {
		var dbError error
		// connect to database
		log.Infof("connecting to db at %s:%v...", configurationInstance.DatabaseHost, configurationInstance.DatabasePort)
		if db, dbError = database.GetConnection(database.GetConnectionOpts{
			Username:      configurationInstance.DatabaseUsername,
			Password:      configurationInstance.DatabasePassword,
			Host:          configurationInstance.DatabaseHost,
			Port:          uint64(configurationInstance.DatabasePort),
			Database:      configurationInstance.DatabaseName,
			RetryInterval: retryInterval,
			MaxRetryCount: uint64(dbMaxRetries),
			Log:           func(l string, a ...any) { log.Infof(l, a...) },
			LogError:      func(l string, a ...any) { log.Warnf(l, a...) },
		}); dbError != nil {
			return fmt.Errorf("failed to create db connection: %s", dbError)
		}
	}

	x, _ := json.MarshalIndent(configurationInstance, "", "  ")
	log.Infof("configuration: %s", string(x))

	erc20Infos := farmer.Erc20Infos{}
	for _, target := range configurationInstance.Targets {
		erc20TokenInfo, err := erc20.GetInfo(erc20.GetInfoOpts{
			Address: target.Address,
			Client:  client,
		})
		if err != nil {
			log.Warnf("failed to get token info for address[%s]: %s", target.Address, err)
			continue
		}
		erc20Info := farmer.Erc20Info{
			Address:             erc20TokenInfo.Address.Hex(),
			Decimals:            erc20TokenInfo.Decimals,
			Symbol:              erc20TokenInfo.Symbol,
			TotalSupply:         erc20TokenInfo.TotalSupply,
			SingleStakeBalances: []farmer.Erc20BalanceInfo{},
			LpStakeBalances:     []farmer.Erc20BalanceInfo{},
			BurnBalances:        []farmer.Erc20BalanceInfo{},
			DaoBalances:         []farmer.Erc20BalanceInfo{},
		}
		for _, daoAddress := range target.DaoAddresses {
			balanceInfo, err := erc20.GetBalance(erc20.GetBalanceOpts{
				TokenAddress:  erc20TokenInfo.Address,
				HolderAddress: daoAddress,
				Client:        client,
			})
			if err != nil {
				log.Warnf("failed to get balance of token[%s] at address[%s]: %s", erc20TokenInfo.Address.Hex(), daoAddress.Hex(), err)
				continue
			}
			var label string
			if directoryEntry, ok := selectedChain.Directory[daoAddress.Hex()]; ok {
				label = directoryEntry.Label
			}
			erc20Info.DaoBalances = append(erc20Info.DaoBalances, farmer.Erc20BalanceInfo{
				Address: daoAddress.Hex(),
				Label:   label,
				Supply:  balanceInfo.TokenAmount,
			})
			erc20Info.DaoTotal += balanceInfo.TokenAmount
			log.Infof("address[%s] has %v $%s tokens", daoAddress.Hex(), balanceInfo.TokenAmount, balanceInfo.TokenSymbol)
		}
		for _, lpStakeAddress := range target.LpContracts {
			balanceInfo, err := erc20.GetBalance(erc20.GetBalanceOpts{
				TokenAddress:  erc20TokenInfo.Address,
				HolderAddress: lpStakeAddress,
				Client:        client,
			})
			if err != nil {
				log.Warnf("failed to get balance of token[%s] at address[%s]: %s", erc20TokenInfo.Address.Hex(), lpStakeAddress.Hex(), err)
				continue
			}
			var label string
			if directoryEntry, ok := selectedChain.Directory[lpStakeAddress.Hex()]; ok {
				label = directoryEntry.Label
			}
			erc20Info.LpStakeBalances = append(erc20Info.LpStakeBalances, farmer.Erc20BalanceInfo{
				Address: lpStakeAddress.Hex(),
				Label:   label,
				Supply:  balanceInfo.TokenAmount,
			})
			erc20Info.LpStakeTotal += balanceInfo.TokenAmount
			log.Infof("address[%s] has %v $%s tokens", lpStakeAddress.Hex(), balanceInfo.TokenAmount, balanceInfo.TokenSymbol)
		}
		for _, singleStakeAddress := range target.SsContracts {
			balanceInfo, err := erc20.GetBalance(erc20.GetBalanceOpts{
				TokenAddress:  erc20TokenInfo.Address,
				HolderAddress: singleStakeAddress,
				Client:        client,
			})
			if err != nil {
				log.Warnf("failed to get balance of token[%s] at address[%s]: %s", erc20TokenInfo.Address.Hex(), singleStakeAddress.Hex(), err)
				continue
			}

			var label string
			if directoryEntry, ok := selectedChain.Directory[singleStakeAddress.Hex()]; ok {
				label = directoryEntry.Label
			}
			erc20Info.SingleStakeBalances = append(erc20Info.SingleStakeBalances, farmer.Erc20BalanceInfo{
				Address: singleStakeAddress.Hex(),
				Label:   label,
				Supply:  balanceInfo.TokenAmount,
			})
			erc20Info.SingleStakeTotal += balanceInfo.TokenAmount
			log.Infof("address[%s] has %v $%s tokens", singleStakeAddress.Hex(), balanceInfo.TokenAmount, balanceInfo.TokenSymbol)
		}
		for _, burnAddress := range target.BurnAddresses {
			balanceInfo, err := erc20.GetBalance(erc20.GetBalanceOpts{
				TokenAddress:  erc20TokenInfo.Address,
				HolderAddress: burnAddress,
				Client:        client,
			})
			if err != nil {
				log.Warnf("failed to get balance of token[%s] at address[%s]: %s", erc20TokenInfo.Address.Hex(), burnAddress.Hex(), err)
				continue
			}
			var label string
			if directoryEntry, ok := selectedChain.Directory[burnAddress.Hex()]; ok {
				label = directoryEntry.Label
			}
			erc20Info.BurnBalances = append(erc20Info.BurnBalances, farmer.Erc20BalanceInfo{
				Address: burnAddress.Hex(),
				Label:   label,
				Supply:  balanceInfo.TokenAmount,
			})
			erc20Info.BurnTotal += balanceInfo.TokenAmount
			log.Infof("address[%s] has %v $%s tokens", burnAddress.Hex(), balanceInfo.TokenAmount, balanceInfo.TokenSymbol)
		}
		erc20Infos = append(erc20Infos, erc20Info)
	}

	x, _ = json.MarshalIndent(erc20Infos, "", "  ")
	log.Infof("output: %s", string(x))

	if err := erc20Infos.Insert(db); err != nil {
		return fmt.Errorf("failed to insert %v balances into the database: %s", len(erc20Infos), err)
	}
	return nil
}
