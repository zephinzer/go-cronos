package erc20

import (
	"github.com/spf13/cobra"
	"github.com/zephinzer/go-cronos/cmd/farmer/commands/load/erc20/info"
)

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:   "erc20",
		Short: "Trigger jobs to load ERC20-related data",
		RunE:  runE,
	}
	command.AddCommand(info.GetCommand())
	return &command
}

func runE(command *cobra.Command, args []string) error {
	return command.Help()
}
