package load

import (
	"github.com/spf13/cobra"
	"github.com/zephinzer/go-cronos/cmd/farmer/commands/load/balances"
	"github.com/zephinzer/go-cronos/cmd/farmer/commands/load/erc20"
	"github.com/zephinzer/go-cronos/cmd/farmer/commands/load/erc20events"
	"github.com/zephinzer/go-cronos/cmd/farmer/commands/load/erc20stats"
	"github.com/zephinzer/go-cronos/cmd/farmer/commands/load/erc721events"
)

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:   "load",
		Short: "Trigger jobs to load historical data from the chain",
		RunE:  runE,
	}
	command.AddCommand(balances.GetCommand())
	command.AddCommand(erc20.GetCommand())
	command.AddCommand(erc20events.GetCommand())
	command.AddCommand(erc20stats.GetCommand())
	command.AddCommand(erc721events.GetCommand())
	return &command
}

func runE(command *cobra.Command, args []string) error {
	return command.Help()
}
