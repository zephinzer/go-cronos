package transactions

import (
	"fmt"

	"github.com/usvc/go-config"
	"github.com/zephinzer/go-cronos/cmd/farmer/common/configuration"
)

const (
	ConfigKeyDisableTxLogging = "disable-tx-logging"
	ConfigKeyFromBlock        = "from-block"
	ConfigKeyRpcUrl           = "rpc-url"
	ConfigKeyTillBlock        = "till-block"

	DefaultBatchSize = 2000
	DefaultRpcUrl    = "https://evm.cronos.org"
)

var conf = config.Map{
	configuration.KeyDatabaseHost:     configuration.GetDatabaseHost(),
	configuration.KeyDatabasePort:     configuration.GetDatabasePort(),
	configuration.KeyDatabaseName:     configuration.GetDatabaseName(),
	configuration.KeyDatabaseUsername: configuration.GetDatabaseUsername(),
	configuration.KeyDatabasePassword: configuration.GetDatabasePassword(),
	ConfigKeyDisableTxLogging: &config.Bool{
		Shorthand: "L",
		Usage:     "when specified, transaction logs will not be recorded to the database",
	},
	ConfigKeyFromBlock: &config.Uint{
		Shorthand: "f",
		Usage:     "starting block (inclusive) number",
	},
	ConfigKeyRpcUrl: &config.String{
		Shorthand: "r",
		Usage:     "RPC URL to use",
		Default:   DefaultRpcUrl,
	},
	ConfigKeyTillBlock: &config.Uint{
		Shorthand: "t",
		Usage:     "final block (inclusive) number",
	},
}

func InitConfiguration(conf config.Map) (*commandConfiguration, error) {
	configurationInstance := &commandConfiguration{
		DatabaseHost:                 conf.GetString(configuration.KeyDatabaseHost),
		DatabasePort:                 conf.GetUint(configuration.KeyDatabasePort),
		DatabaseUsername:             conf.GetString(configuration.KeyDatabaseUsername),
		DatabasePassword:             conf.GetString(configuration.KeyDatabasePassword),
		DatabaseName:                 conf.GetString(configuration.KeyDatabaseName),
		FromBlock:                    int64(conf.GetUint(ConfigKeyFromBlock)),
		IsTransactionLoggingDisabled: conf.GetBool(ConfigKeyDisableTxLogging),
		RpcUrl:                       conf.GetString(ConfigKeyRpcUrl),
		TillBlock:                    int64(conf.GetUint(ConfigKeyTillBlock)),
	}
	return configurationInstance, nil
}

type commandConfiguration struct {
	DatabaseHost                 string
	DatabasePort                 uint
	DatabaseUsername             string
	DatabasePassword             string
	DatabaseName                 string
	FromBlock                    int64
	IsTransactionLoggingDisabled bool
	RpcUrl                       string
	TillBlock                    int64
}

func (c *commandConfiguration) GetMySqlDsn() string {
	return fmt.Sprintf(
		"%s:%s@tcp(%s:%v)/%s",
		c.DatabaseUsername,
		c.DatabasePassword,
		c.DatabaseHost,
		c.DatabasePort,
		c.DatabaseName,
	)
}

func (c commandConfiguration) GetPublicMySqlConnectionString() string {
	return fmt.Sprintf(
		"%s@%s:%v/%s",
		c.DatabaseUsername,
		c.DatabaseHost,
		c.DatabasePort,
		c.DatabaseName,
	)
}
