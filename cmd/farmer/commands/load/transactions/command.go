package transactions

import (
	"context"
	"fmt"
	"math/big"
	"sync"
	"time"

	"github.com/ethereum/go-ethereum/ethclient"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/spf13/cobra"
	"github.com/zephinzer/go-cronos/cmd/farmer/common/farmer"
	"github.com/zephinzer/go-cronos/internal/database"
	"github.com/zephinzer/go-cronos/internal/log"
	"github.com/zephinzer/go-cronos/pkg/evmutils"
)

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:   "transactions",
		Short: "Loads chain transactions",
		RunE:  runE,
	}
	conf.ApplyToCobra(&command)
	return &command
}

func runE(command *cobra.Command, args []string) error {
	log.Infof("starting farmer...")
	configurationInstance, configError := InitConfiguration(conf)
	if configError != nil {
		return fmt.Errorf("failed to receive a valid configuration: %s", configError)
	}

	retryInterval := time.Second * 5
	dbMaxRetries := 10
	rpcMaxRetries := 10
	chainIdMaxretries := 10

	// test the rpc connection
	log.Infof("connecting to rpc[%s]...", configurationInstance.RpcUrl)
	var client *ethclient.Client
	var dialError error
	for client == nil {
		client, dialError = ethclient.Dial(configurationInstance.RpcUrl)
		if dialError != nil {
			log.Errorf("failed to dial rpc[%s]: %s", configurationInstance.RpcUrl, dialError)
			rpcMaxRetries--
			if rpcMaxRetries == 0 {
				return fmt.Errorf("failed to reach rpc[%s]", configurationInstance.RpcUrl)
			}
			log.Infof("dialing rpc again in %s", retryInterval.String())
		}
		<-time.After(retryInterval)
	}
	log.Infof("connected to rpc successfully")

	log.Info("retrieving chain id...")
	var chainId *big.Int
	var chainIdError error
	for chainId == nil {
		chainId, chainIdError = client.ChainID(context.Background())
		if chainIdError != nil {
			log.Warnf("failed to get chain id: %s", chainIdError)
			chainIdMaxretries--
			if chainIdMaxretries == 0 {
				return fmt.Errorf("failed to get chain id: %s", chainIdError)
			}
			log.Infof("getting chain id again in %s", retryInterval.String())
		}
		<-time.After(retryInterval)
	}
	log.Infof("retrieved chain id[%s]", chainId.String())
	defer client.Close()

	// connect to database
	log.Infof("connecting to db at %s:%v...", configurationInstance.DatabaseHost, configurationInstance.DatabasePort)
	db, err := database.GetConnection(database.GetConnectionOpts{
		Username:      configurationInstance.DatabaseUsername,
		Password:      configurationInstance.DatabasePassword,
		Host:          configurationInstance.DatabaseHost,
		Port:          uint64(configurationInstance.DatabasePort),
		Database:      configurationInstance.DatabaseName,
		RetryInterval: retryInterval,
		MaxRetryCount: uint64(dbMaxRetries),
		Log:           func(l string, a ...any) { log.Infof(l, a...) },
		LogError:      func(l string, a ...any) { log.Warnf(l, a...) },
	})
	if err != nil {
		return fmt.Errorf("failed to create db connection: %s", err)
	}

	if configurationInstance.TillBlock == 0 {
		latestBlockInstance, err := client.BlockByNumber(context.Background(), nil)
		if err != nil {
			return fmt.Errorf("failed to get the latest block: %s", err)
		}
		configurationInstance.TillBlock = latestBlockInstance.Number().Int64()
	}
	if configurationInstance.FromBlock == 0 {
		configurationInstance.FromBlock = configurationInstance.TillBlock - 2000
	}

	fromBlock := big.NewInt(configurationInstance.FromBlock)
	toBlock := big.NewInt(configurationInstance.TillBlock)
	evmutils.GetTransactions(evmutils.GetTransactionsOpts{
		Rpc: evmutils.RpcUrl(configurationInstance.RpcUrl),
	})

	watchEvents := make(chan evmutils.WatchEvent, 100)
	var waiter sync.WaitGroup
	waiter.Add(1)
	go func() {
		defer waiter.Done()
		for {
			watchEvent := <-watchEvents
			switch watchEvent.Type {
			case evmutils.EventBlock:
				blockInstance, err := farmer.NewBlock(watchEvent.BlockInstance)
				if err != nil {
					log.Errorf("failed to process block[%v]: %s", watchEvent.BlockId, err)
					continue
				}
				if err := blockInstance.Insert(db); err != nil {
					log.Errorf("failed to insert block[%v]: %s", watchEvent.BlockId, err)
				}
				log.Infof("block[%v]", watchEvent.BlockId.String())
			case evmutils.EventError:
				log.Warnf("error: %s", watchEvent.Error.Error())
			case evmutils.EventWarning:
				log.Warnf("error: %s", watchEvent.Error.Error())
			case evmutils.EventTransaction:
				transactionInstance, err := farmer.NewTransaction(watchEvent.TxInstance, watchEvent.BlockId.Uint64(), chainId)
				if err != nil {
					log.Errorf("failed to process tx[%v]: %s", watchEvent.TxHash, err)
					continue
				}
				if err := transactionInstance.Insert(db); err != nil {
					log.Errorf("failed to insert tx[%v]: %s", watchEvent.TxHash, err)
					continue
				}
				log.Infof("block[%v]: tx[%v]", watchEvent.BlockId.String(), watchEvent.TxHash)
				if !configurationInstance.IsTransactionLoggingDisabled {
					for index, txLog := range watchEvent.TxLogs {
						logInstance, err := farmer.NewLog(txLog.Instance, watchEvent.TxHash, uint64(index))
						if err != nil {
							log.Errorf("failed to process log[%v] of tx[%v]", txLog.Address, watchEvent.TxHash)
							continue
						}
						if err := logInstance.Insert(db); err != nil {
							log.Errorf("failed to insert log[%v] of tx[%v]", txLog.Address, watchEvent.TxHash)
							continue
						}
						log.Infof("block[%v]: tx[%v]: log[%v]: %s", watchEvent.BlockId.String(), watchEvent.TxHash[:6]+".."+watchEvent.TxHash[len(watchEvent.TxHash)-4:], index, txLog.Address)
					}
				}
			}
		}
	}()

	// start the watch cycle
	go evmutils.GetTransactions(evmutils.GetTransactionsOpts{
		Events:    watchEvents,
		Interval:  time.Second * 1,
		FromBlock: fromBlock.Int64(),
		ToBlock:   toBlock.Int64(),
		Rpc:       evmutils.RpcUrl(configurationInstance.RpcUrl),
	})

	waiter.Wait()

	return nil
}
