package migrate

import (
	"github.com/usvc/go-config"
	"github.com/zephinzer/go-cronos/cmd/farmer/common/configuration"
)

var conf = config.Map{
	configuration.KeyDatabaseHost:           configuration.GetDatabaseHost(),
	configuration.KeyDatabasePort:           configuration.GetDatabasePort(),
	configuration.KeyDatabaseName:           configuration.GetDatabaseName(),
	configuration.KeyDatabaseUsername:       configuration.GetDatabaseUsername(),
	configuration.KeyDatabasePassword:       configuration.GetDatabasePassword(),
	configuration.KeyDatabaseRootUsername:   configuration.GetDatabaseRootUsername(),
	configuration.KeyDatabaseRootPassword:   configuration.GetDatabaseRootPassword(),
	configuration.KeyDatabaseMigrationsPath: configuration.GetDatabaseMigrationsPath(),
}

func InitConfiguration(conf config.Map) (*commandConfiguration, error) {
	configurationInstance := &commandConfiguration{
		DatabaseHost:           conf.GetString(configuration.KeyDatabaseHost),
		DatabasePort:           conf.GetUint(configuration.KeyDatabasePort),
		DatabaseUsername:       conf.GetString(configuration.KeyDatabaseUsername),
		DatabasePassword:       conf.GetString(configuration.KeyDatabasePassword),
		DatabaseRootUsername:   conf.GetString(configuration.KeyDatabaseRootUsername),
		DatabaseRootPassword:   conf.GetString(configuration.KeyDatabaseRootPassword),
		DatabaseName:           conf.GetString(configuration.KeyDatabaseName),
		DatabaseMigrationsPath: conf.GetString(configuration.KeyDatabaseMigrationsPath),
	}
	return configurationInstance, nil
}

type commandConfiguration struct {
	DatabaseHost           string
	DatabasePort           uint
	DatabaseUsername       string
	DatabasePassword       string
	DatabaseRootUsername   string
	DatabaseRootPassword   string
	DatabaseName           string
	DatabaseMigrationsPath string
}
