package sweep

import (
	"context"
	"fmt"
	"math/big"
	"time"

	"github.com/ethereum/go-ethereum/core/types"
	"github.com/spf13/cobra"
	"github.com/zephinzer/go-cronos/cmd/farmer/common/farmer"
	"github.com/zephinzer/go-cronos/internal/database"
	"github.com/zephinzer/go-cronos/internal/log"
	"github.com/zephinzer/go-cronos/pkg/evmutils"
)

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:   "sweep",
		Short: "Performs a backward search for missing blocks and fills in the transactions",
		Long:  "Performs a backward search for missing blocks and fills in the transactions. Meant to be run as a job to fill up the gaps left out by the main farmer 'start' command",
		RunE:  runE,
	}
	conf.ApplyToCobra(&command)
	return &command
}

func runE(command *cobra.Command, args []string) error {
	log.Infof("starting sweeper...")
	configurationInstance, configError := InitConfiguration(conf)
	if configError != nil {
		return fmt.Errorf("failed to receive a valid configuration: %s", configError)
	}
	retryInterval := time.Second * 5
	maxRetries := 10

	events := make(chan evmutils.NewClientEvent, 10)
	go func() {
		for {
			eventInstance := <-events
			switch true {
			case eventInstance.Done:
				return
			case eventInstance.Error != nil:
				log.Warn(eventInstance.Error)
			case len(eventInstance.Message) > 0:
				log.Info(eventInstance.Message)
			}
		}
	}()
	client, chainId, err := evmutils.NewClient(evmutils.NewClientOpts{
		RpcUrl:        configurationInstance.RpcUrl,
		RetryInterval: retryInterval,
		MaxRetries:    maxRetries,
		Events:        events,
	})
	fmt.Println(chainId)
	defer client.Close()

	// connect to database
	log.Infof("connecting to db at %s:%v...", configurationInstance.DatabaseHost, configurationInstance.DatabasePort)
	db, err := database.GetConnection(database.GetConnectionOpts{
		Username:      configurationInstance.DatabaseUsername,
		Password:      configurationInstance.DatabasePassword,
		Host:          configurationInstance.DatabaseHost,
		Port:          uint64(configurationInstance.DatabasePort),
		Database:      configurationInstance.DatabaseName,
		RetryInterval: retryInterval,
		MaxRetryCount: uint64(maxRetries),
		Log:           func(l string, a ...any) { log.Infof(l, a...) },
		LogError:      func(l string, a ...any) { log.Warnf(l, a...) },
	})
	if err != nil {
		return fmt.Errorf("failed to create db connection: %s", err)
	}

	var latestBlock int64
	maxRow := db.QueryRow("SELECT MAX(number) FROM blocks")
	if err := maxRow.Scan(&latestBlock); err != nil {
		return fmt.Errorf("failed to get latest block number: %s", err)
	}
	oldestBlock := latestBlock - int64(configurationInstance.BlockCount)
	log.Infof("sweeping transactions from block[%v] > block[%v]", latestBlock, oldestBlock)

	transactionsCounter := 0
	for i := latestBlock; i >= oldestBlock; i-- {
		var err error = nil
		var currentBlock *types.Block = nil
		for currentBlock == nil {
			currentBlock, err = client.BlockByNumber(context.Background(), big.NewInt(i))
			if err != nil {
				log.Warnf("failed to get block by number: %s", err)
				<-time.After(500 * time.Millisecond)
				continue
			}
			break
		}
		log.Infof("retrieved current block[%v]", currentBlock.Number().String())
		blockInstance, err := farmer.NewBlock(currentBlock)
		if err != nil {
			log.Warnf("failed to process block[%v]: %s", i, err)
			continue
		}

		if err := blockInstance.Insert(db); err != nil {
			log.Warnf("failed to insert block[%v]: %s", i, err)
			continue
		}

		transactions := currentBlock.Transactions()
		log.Infof("retrieved %v transactions from current block[%v]", len(transactions), currentBlock.Number().String())
		for _, transaction := range transactions {
			transactionInstance, err := farmer.NewTransaction(transaction, blockInstance.Number.Uint64(), chainId)
			if err != nil {
				log.Warnf("failed to process tx[%s]: %s", transaction.Hash().String(), err)
			}
			if err := transactionInstance.Insert(db); err != nil {
				log.Warnf("failed to insert tx[%v]: %s", transaction.Hash().String(), err)
				continue
			}
			if !configurationInstance.IsTransactionLoggingDisabled {
				receipt, err := client.TransactionReceipt(context.Background(), transaction.Hash())
				if err != nil {
					log.Warnf("failed to get receipt for tx[%x]: %s", transaction.Hash().String(), err)
					continue
				}
				logInstances := farmer.Logs{}
				for logIndex, logEntry := range receipt.Logs {
					logInstance, err := farmer.NewLog(logEntry, transaction.Hash().String(), uint64(logIndex))
					if err != nil {
						log.Warnf("failed to process log[%v] of tx[%v]: %s", logEntry.Index, transaction.Hash().String(), err)
						continue
					}
					logInstances = append(logInstances, *logInstance)
				}
				if err := logInstances.Insert(db); err != nil {
					log.Warnf("failed to insert %v logs into the db: %s", len(logInstances), err)
					continue
				}
				log.Infof("retrieved %v logs from tx[%v] in current block[%v]", len(logInstances), transaction.Hash().String(), currentBlock.Number().String())
			}
		}
		log.Infof("completed inserting %v transactions from block[%v]", len(transactions), blockInstance.Number.Uint64())
		transactionsCounter += len(transactions)
	}

	log.Infof("completed verification of %s transactions", transactionsCounter)

	return nil
}
