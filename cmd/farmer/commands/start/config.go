package start

import (
	"fmt"
	"strings"

	"github.com/usvc/go-config"
	"github.com/zephinzer/go-cronos/cmd/farmer/common/configuration"
)

const (
	ConfigKeyAddresses        = "addresses"
	ConfigKeyDisableTxLogging = "disable-tx-logging"
	ConfigKeyRpcUrl           = "rpc-url"

	DefaultRpcUrl = "https://rpc.vvs.finance"
)

var conf = config.Map{
	configuration.KeyDatabaseHost:     configuration.GetDatabaseHost(),
	configuration.KeyDatabasePort:     configuration.GetDatabasePort(),
	configuration.KeyDatabaseName:     configuration.GetDatabaseName(),
	configuration.KeyDatabaseUsername: configuration.GetDatabaseUsername(),
	configuration.KeyDatabasePassword: configuration.GetDatabasePassword(),
	ConfigKeyAddresses: &config.StringSlice{
		Shorthand: "A",
		Usage:     "when specified, only records transactions to/from this address",
	},
	ConfigKeyDisableTxLogging: &config.Bool{
		Shorthand: "L",
		Usage:     "when specified, transaction logs will not be recorded to the database",
	},
	ConfigKeyRpcUrl: &config.String{
		Shorthand: "r",
		Usage:     "RPC URL to use",
		Default:   DefaultRpcUrl,
	},
}

func InitConfiguration(conf config.Map) (*commandConfiguration, error) {
	configurationInstance := &commandConfiguration{
		AddressMap:                   map[string]bool{"_": false},
		DatabaseHost:                 conf.GetString(configuration.KeyDatabaseHost),
		DatabasePort:                 conf.GetUint(configuration.KeyDatabasePort),
		DatabaseUsername:             conf.GetString(configuration.KeyDatabaseUsername),
		DatabasePassword:             conf.GetString(configuration.KeyDatabasePassword),
		DatabaseName:                 conf.GetString(configuration.KeyDatabaseName),
		IsTransactionLoggingDisabled: conf.GetBool(ConfigKeyDisableTxLogging),
		RpcUrl:                       conf.GetString(ConfigKeyRpcUrl),
	}
	addresses := conf.GetStringSlice(ConfigKeyAddresses)
	if len(addresses) > 0 {
		configurationInstance.AddressMap = map[string]bool{}
	}
	for _, address := range addresses {
		configurationInstance.AddressMap[strings.ToLower(address)] = true
	}
	return configurationInstance, nil
}

type commandConfiguration struct {
	AddressMap                   map[string]bool
	DatabaseHost                 string
	DatabasePort                 uint
	DatabaseUsername             string
	DatabasePassword             string
	DatabaseName                 string
	IsTransactionLoggingDisabled bool
	RpcUrl                       string
}

func (c *commandConfiguration) GetMySqlDsn() string {
	return fmt.Sprintf(
		"%s:%s@tcp(%s:%v)/%s",
		c.DatabaseUsername,
		c.DatabasePassword,
		c.DatabaseHost,
		c.DatabasePort,
		c.DatabaseName,
	)
}

func (c commandConfiguration) GetPublicMySqlConnectionString() string {
	return fmt.Sprintf(
		"%s@%s:%v/%s",
		c.DatabaseUsername,
		c.DatabaseHost,
		c.DatabasePort,
		c.DatabaseName,
	)
}

func (c commandConfiguration) IsAddressTargetted(targetAddress string) bool {
	if isSet, exists := c.AddressMap["_"]; exists && !isSet {
		// if no addresses are defined, always say yes
		return true
	}
	isTargetted, exists := c.AddressMap[strings.ToLower(targetAddress)]
	return exists && isTargetted
}
