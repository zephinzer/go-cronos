CREATE TABLE IF NOT EXISTS `erc20_info` (
  `address` VARCHAR(42) NOT NULL,
  `decimals` INT NOT NULL,
  `symbol` VARCHAR(16) NOT NULL,
  `total_supply` FLOAT NOT NULL,
  `burn_balances` TEXT NOT NULL,
  `burn_total` FLOAT NOT NULL,
  `dao_balances` TEXT NOT NULL,
  `dao_total` FLOAT NOT NULL,
  `lp_stake_balances` TEXT NOT NULL,
  `lp_stake_total` FLOAT NOT NULL,
  `single_stake_balances` TEXT NOT NULL,
  `single_stake_total` FLOAT NOT NULL,
  `timestamp` TIMESTAMP DEFAULT NOW()
);
