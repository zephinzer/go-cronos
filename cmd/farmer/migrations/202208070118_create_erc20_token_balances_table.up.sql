CREATE TABLE IF NOT EXISTS `erc20_token_balances` (
  id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  address VARCHAR(42) NOT NULL,
  address_label TEXT NOT NULL,
  balance_address VARCHAR(42) NOT NULL,
  balance_amount FLOAT NOT NULL,
  balance_symbol VARCHAR(24) NOT NULL,
  timestamp TIMESTAMP DEFAULT NOW(),

  UNIQUE(id),
  PRIMARY KEY(`id`)
);
