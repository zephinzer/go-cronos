CREATE TABLE IF NOT EXISTS `transactions` (
  id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  hash VARCHAR(68) NOT NULL,
  address_from VARCHAR(44) NOT NULL,
  address_to VARCHAR(44),
  block_number BIGINT UNSIGNED NOT NULL,
  data TEXT,
  cost FLOAT NOT NULL DEFAULT(-1),
  gas BIGINT NOT NULL DEFAULT(-1),
  gas_fee_cap BIGINT NOT NULL DEFAULT(-1),
  gas_price BIGINT NOT NULL DEFAULT(-1),
  gas_tip_cap BIGINT NOT NULL DEFAULT(-1),
  value FLOAT NOT NULL DEFAULT(-1),
  type INT,
  last_modified TIMESTAMP NOT NULL DEFAULT NOW(),

  PRIMARY KEY (id),
  UNIQUE INDEX (hash),
  CONSTRAINT FK_block_number_blocks_number FOREIGN KEY (block_number) REFERENCES blocks(number) ON DELETE CASCADE
);
