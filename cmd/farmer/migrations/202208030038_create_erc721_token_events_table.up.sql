CREATE TABLE IF NOT EXISTS `erc721_token_events` (
  id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  address VARCHAR(42) NOT NULL,
  address_by VARCHAR(42),
  address_from VARCHAR(42) NOT NULL,
  address_to VARCHAR(42),
  approval TINYINT(1),
  token_id BIGINT UNSIGNED,
  transaction_hash VARCHAR(67) NOT NULL,
  block_number BIGINT UNSIGNED NOT NULL,
  timestamp TIMESTAMP,
  type VARCHAR(67) NOT NULL,
  value FLOAT NOT NULL,
  `index` TINYINT UNSIGNED,
  symbol VARCHAR(24),
  last_modified TIMESTAMP NOT NULL DEFAULT NOW(),

  UNIQUE(id),
  PRIMARY KEY(`block_number`, `transaction_hash`, `index`)
);
