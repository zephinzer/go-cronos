CREATE TABLE IF NOT EXISTS `logs` (
  id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  address VARCHAR(42) NOT NULL,
  block_number BIGINT NOT NULL,
  transaction_hash VARCHAR(68) NOT NULL,
  log_index INT(4) NOT NULL,
  data TEXT,
  topics_count INT NOT NULL DEFAULT(-1),
  topic_0 VARCHAR(128),
  topic_1 VARCHAR(128),
  topic_2 VARCHAR(128),
  topic_3 VARCHAR(128),
  last_modified TIMESTAMP NOT NULL DEFAULT NOW(),

  PRIMARY KEY (id),
  CONSTRAINT FK_transaction_hash_transactions_hash FOREIGN KEY (transaction_hash) REFERENCES transactions(hash) ON DELETE CASCADE
);
