CREATE TABLE IF NOT EXISTS `erc20_statistics` (
  address VARCHAR(42) NOT NULL,
  decimals INT NOT NULL,
  symbol VARCHAR(16) NOT NULL,
  total_supply FLOAT NOT NULL,
  timestamp TIMESTAMP DEFAULT NOW()
);
