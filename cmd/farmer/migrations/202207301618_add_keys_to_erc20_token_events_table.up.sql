START TRANSACTION;
ALTER TABLE `erc20_token_events`
  DROP PRIMARY KEY,
  CHANGE `id` `id` BIGINT NULL,
  ADD PRIMARY KEY(`block_number`, `transaction_hash`, `index`);
COMMIT;
