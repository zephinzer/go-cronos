package main

import (
	"fmt"
	"os"
	"strings"

	"github.com/spf13/cobra"
	"github.com/zephinzer/go-cronos/cmd/farmer/commands/load"
	"github.com/zephinzer/go-cronos/cmd/farmer/commands/migrate"
	"github.com/zephinzer/go-cronos/cmd/farmer/commands/start"
	"github.com/zephinzer/go-cronos/cmd/farmer/commands/sweep"
	"github.com/zephinzer/go-cronos/internal/constants"
	"github.com/zephinzer/go-cronos/internal/log"
)

const appId = "farmer"

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:   "farmer <sub-command>",
		Short: "Transaction farmer to load a database with targetted transaction data",
		RunE:  runE,
	}
	command.Version = constants.Version
	command.AddCommand(load.GetCommand())
	command.AddCommand(migrate.GetCommand())
	command.AddCommand(start.GetCommand())
	command.AddCommand(sweep.GetCommand())
	return &command
}

func runE(command *cobra.Command, args []string) error {
	command.Help()
	return nil
}

func main() {
	log.Alert(fmt.Sprintf("%s[%s] started with arguments['%s']", appId, constants.Version, strings.Join(os.Args, "', '")), log.AlertTypeInfo)
	if err := GetCommand().Execute(); err != nil {
		if alertErr := log.Alert(fmt.Sprintf("%s[%s '%s'] failed to execute successfully: \n\n%s", appId, constants.Version, strings.Join(os.Args, "', '"), err), log.AlertTypeError); alertErr != nil {
			log.Errorf("failed to send alert: %s", alertErr)
		}
		// os.Exit(constants.ExitCodeNotOk)
		return
	}
}
