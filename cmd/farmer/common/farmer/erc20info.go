package farmer

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"strings"
	"time"
)

const erc20InfoInsertStmtTemplate = `
INSERT INTO erc20_info (
	address,
	decimals,
	symbol,
	total_supply,
	burn_balances,
	burn_total,
	dao_balances,
	dao_total,
	lp_stake_balances,
	lp_stake_total,
	single_stake_balances,
	single_stake_total
) VALUES (
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?
)
`

type Erc20Infos []Erc20Info

func (erc20i Erc20Infos) Insert(db *sql.DB) error {
	errors := []string{}
	bulkInsertStmt, err := db.Prepare(erc20InfoInsertStmtTemplate)
	if err != nil {
		return fmt.Errorf("failed to prepare sql '%s': %s", erc20InfoInsertStmtTemplate, err)
	}
	for _, erc20Info := range erc20i {
		if err := erc20Info.Insert(db, bulkInsertStmt); err != nil {
			errors = append(errors, err.Error())
		}
	}
	if len(errors) > 0 {
		return fmt.Errorf("failed to complete all insertions successfully:\n - %s", strings.Join(errors, "\n - "))
	}
	return nil
}

type Erc20Info struct {
	Address             string             `json:"address"`
	Decimals            int                `json:"decimals"`
	Symbol              string             `json:"symbol"`
	TotalSupply         float64            `json:"totalSupply"`
	BurnBalances        []Erc20BalanceInfo `json:"burnBalances"`
	BurnTotal           float64            `json:"burnTotal"`
	DaoBalances         []Erc20BalanceInfo `json:"daoBalances"`
	DaoTotal            float64            `json:"daoTotal"`
	LpStakeBalances     []Erc20BalanceInfo `json:"lpStakeBalances"`
	LpStakeTotal        float64            `json:"lpStakeTotal"`
	SingleStakeBalances []Erc20BalanceInfo `json:"singleStakeBalances"`
	SingleStakeTotal    float64            `json:"singleStakeTotal"`
	Timestamp           time.Time          `json:"timestamp"`
}

func (erc20i Erc20Info) Insert(db *sql.DB, useStmt ...*sql.Stmt) error {
	var preparedStmt *sql.Stmt
	if useStmt != nil && len(useStmt) == 0 {
		preparedStmt, err := db.Prepare(erc20InfoInsertStmtTemplate)
		if err != nil {
			return fmt.Errorf("failed to prepare sql '%s': %s", erc20InfoInsertStmtTemplate, err)
		}
		defer preparedStmt.Close()
	} else {
		preparedStmt = useStmt[0]
	}

	burnBalances, _ := json.Marshal(erc20i.BurnBalances)
	daoBalances, _ := json.Marshal(erc20i.DaoBalances)
	lpStakeBalances, _ := json.Marshal(erc20i.LpStakeBalances)
	singleStakeBalances, _ := json.Marshal(erc20i.SingleStakeBalances)

	if _, err := preparedStmt.Exec(
		erc20i.Address,
		erc20i.Decimals,
		erc20i.Symbol,
		erc20i.TotalSupply,
		string(burnBalances),
		erc20i.BurnTotal,
		string(daoBalances),
		erc20i.DaoTotal,
		string(lpStakeBalances),
		erc20i.LpStakeTotal,
		string(singleStakeBalances),
		erc20i.SingleStakeTotal,
	); err != nil {
		return fmt.Errorf("failed to insert erc20 info into the db: %s", err)
	}
	return nil
}

type Erc20BalanceInfo struct {
	Address string  `json:"address"`
	Label   string  `json:"label"`
	Supply  float64 `json:"supply"`
}
