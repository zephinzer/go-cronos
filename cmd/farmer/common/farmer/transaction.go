package farmer

import (
	"database/sql"
	"fmt"
	"math/big"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/common/math"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/go-sql-driver/mysql"
	"github.com/zephinzer/go-cronos/internal/log"
)

func NewTransaction(transactionInstance *types.Transaction, blockNumber uint64, chainId *big.Int) (*Transaction, error) {
	var transactionFrom common.Address
	if message, err := transactionInstance.AsMessage(types.NewLondonSigner(chainId), nil); err == nil {
		transactionFrom = message.From()
	}
	costCro, _ := big.NewFloat(0).Quo(big.NewFloat(0).SetInt(transactionInstance.Cost()), big.NewFloat(0).SetInt(math.BigPow(10, 18))).Float64()
	valueCro, _ := big.NewFloat(0).Quo(big.NewFloat(0).SetInt(transactionInstance.Value()), big.NewFloat(0).SetInt(math.BigPow(10, 18))).Float64()

	var data *string
	rawData := transactionInstance.Data()
	if rawData != nil {
		hexData := common.Bytes2Hex(rawData)
		data = &hexData
	}

	var transactionTo *string
	if transactionInstance.To() != nil {
		rawTransactionTo := transactionInstance.To().Hex()
		transactionTo = &rawTransactionTo
	}

	return &Transaction{
		AddressFrom: transactionFrom.Hex(),
		AddressTo:   transactionTo,
		BlockNumber: blockNumber,
		Cost:        costCro,
		Data:        data,
		Gas:         transactionInstance.Gas(),
		GasFeeCap:   transactionInstance.GasFeeCap().Uint64(),
		GasPrice:    transactionInstance.GasPrice().Uint64(),
		GasTipCap:   transactionInstance.GasTipCap().Uint64(),
		Hash:        transactionInstance.Hash().Hex(),
		Type:        int64(transactionInstance.Type()),
		Value:       valueCro,
	}, nil
}

type Transaction struct {
	AddressFrom string  `json:"addressFrom"`
	AddressTo   *string `json:"addressTo"`
	BlockNumber uint64  `json:"blockNumber"`
	Cost        float64 `json:"cost"`
	Data        *string `json:"data"`
	Gas         uint64  `json:"gas"`
	GasFeeCap   uint64  `json:"gasFeeCap"`
	GasPrice    uint64  `json:"gasPrice"`
	GasTipCap   uint64  `json:"gasTipCap"`
	Hash        string  `json:"hash"`
	Type        int64   `json:"type"`
	Value       float64 `json:"value"`
}

func (t *Transaction) Insert(db *sql.DB) error {
	dbStatement, err := db.Prepare(`
INSERT INTO transactions (
	hash,
	address_from,
	address_to,
	block_number,
	data,
	cost,
	gas,
	gas_fee_cap,
	gas_price,
	gas_tip_cap,
	value,
	type
) VALUES (
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?
)
`)
	if err != nil {
		return fmt.Errorf("failed to prepare database insert statement: %s", err)
	}
	defer dbStatement.Close()
	result, err := dbStatement.Exec(
		t.Hash,
		t.AddressFrom,
		t.AddressTo,
		t.BlockNumber,
		t.Data,
		t.Cost,
		t.Gas,
		t.GasFeeCap,
		t.GasPrice,
		t.GasTipCap,
		t.Value,
		t.Type,
	)
	if err != nil {
		if mysqlError, ok := err.(*mysql.MySQLError); ok && mysqlError.Number == 1062 {
			log.Infof("tx[%s] already exists", t.Hash)
			return nil
		}
		return fmt.Errorf("failed to execute db tx: %s", err)
	}
	rowsAffected, err := result.RowsAffected()
	if err != nil {
		log.Warnf("failed to get number of rows affected: %s", err)
	} else {
		log.Debugf("%v row(s) affected: transaction[%s] of block[%v]", rowsAffected, t.Hash, t.BlockNumber)
	}
	return nil
}
