package farmer

import (
	"database/sql"
	"fmt"
	"strings"
)

const erc20StatisticsInsertStmtTemplate = `
INSERT INTO erc20_statistics (
	address,
	decimals,
	symbol,
	total_supply
) VALUES (
	?,
	?,
	?,
	?
)
`

// Erc20Statistics is a slice of Erc20Statistic with convenience functions
type Erc20Statistics []Erc20Statistic

func (erc20Statistics Erc20Statistics) Insert(db *sql.DB) error {
	bulkInsertStmt, err := db.Prepare(erc20StatisticsInsertStmtTemplate)
	if err != nil {
		return fmt.Errorf("failed to insert %v items into the db: %s", len(erc20Statistics), err)
	}
	defer bulkInsertStmt.Close()
	errors := []string{}
	for _, statistic := range erc20Statistics {
		if err := statistic.Insert(db, bulkInsertStmt); err != nil {
			errors = append(errors, fmt.Sprintf("failed to insert statistics of token[%s] with symbol '%s': %s", statistic.Address, statistic.Symbol, err))
		}
	}
	if len(errors) > 0 {
		return fmt.Errorf("failed to insert all (%v failed): ['%s']", len(errors), strings.Join(errors, "', '"))
	}
	return nil
}

// Erc20Statistic represents an ERC20 token statistic snapshot
type Erc20Statistic struct {
	Address     string  `json:"address"`
	Decimals    int     `json:"decimals"`
	Symbol      string  `json:"symbol"`
	TotalSupply float64 `json:"total_supply"`
}

func (e20s Erc20Statistic) Insert(db *sql.DB, useStmt ...*sql.Stmt) error {
	var stmt *sql.Stmt = nil
	if useStmt != nil && len(useStmt) > 0 {
		stmt = useStmt[0]
	}
	if stmt == nil {
		stmt, err := db.Prepare(erc20StatisticsInsertStmtTemplate)
		if err != nil {
			return fmt.Errorf("failed to prepare erc20 statistic db insertion: %s", err)
		}
		defer stmt.Close()
	}

	if _, err := stmt.Exec(
		e20s.Address,
		e20s.Decimals,
		e20s.Symbol,
		e20s.TotalSupply,
	); err != nil {
		return fmt.Errorf("failed to insert erc20 statistic into the db: %s", err)
	}

	return nil
}
