package farmer

import (
	"database/sql"
	"fmt"
	"math/big"
	"strconv"
	"strings"
	"time"

	"github.com/ethereum/go-ethereum/core/types"
	"github.com/go-sql-driver/mysql"
	"github.com/zephinzer/go-cronos/internal/log"
)

func NewBlock(blockInstance *types.Block) (*Block, error) {
	blockSizeText := strings.Split(blockInstance.Size().String(), " ")
	blockSize, err := strconv.ParseFloat(blockSizeText[0], 64)
	if err != nil {
		log.Warnf("failed to parse float: %s", err)
	} else {
		switch blockSizeText[1] {
		case "TiB":
			blockSize = blockSize * 1024 * 1024 * 1024 * 1024
		case "GiB":
			blockSize = blockSize * 1024 * 1024 * 1024
		case "MiB":
			blockSize = blockSize * 1024 * 1024
		case "KiB":
			blockSize = blockSize * 1024
		}
	}
	return &Block{
		Number:         blockInstance.Number(),
		Hash:           blockInstance.Hash().Hex(),
		GasUsed:        blockInstance.GasUsed(),
		BlockSizeBytes: int64(blockSize),
		ReceivedAt:     time.Unix(int64(blockInstance.Time()), 0),
	}, nil
}

type Block struct {
	Number         *big.Int  `json:"number"`
	Hash           string    `json:"hash"`
	GasUsed        uint64    `json:"gasUsed"`
	BlockSizeBytes int64     `json:"blockSizeBytes"`
	ReceivedAt     time.Time `json:"receivedAt"`
	LastModified   time.Time `json:"lastModified"`
}

func (b *Block) Insert(db *sql.DB) error {
	dbStatement, err := db.Prepare(`
INSERT INTO blocks (
	number,
	hash,
	gas_used,
	block_size_bytes,
	received_at
) VALUES (
	?,
	?,
	?,
	?,
	?
)
`)
	if err != nil {
		return fmt.Errorf("failed to prepare database insert statement: %s", err)
	}
	defer dbStatement.Close()
	result, err := dbStatement.Exec(
		b.Number.Uint64(),
		b.Hash,
		b.GasUsed,
		b.BlockSizeBytes,
		b.ReceivedAt,
	)
	if err != nil {
		if mysqlError, ok := err.(*mysql.MySQLError); ok && mysqlError.Number == 1062 {
			log.Infof("block[%v] already exists: %s", b.Number.Uint64(), err)
			return nil
		}
		return fmt.Errorf("failed to execute db tx: %s", err)
	}
	rowsAffected, err := result.RowsAffected()
	if err != nil {
		log.Warnf("failed to get number of rows affected: %s", err)
	} else {
		log.Debugf("%v row(s) affected: block[%v]", rowsAffected, b.Number.String())
	}
	return nil
}
