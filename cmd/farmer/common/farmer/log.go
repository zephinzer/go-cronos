package farmer

import (
	"database/sql"
	"fmt"
	"strings"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/zephinzer/go-cronos/internal/log"
)

const transactionLogInsertStmtTemplate = `
REPLACE INTO logs (
	address,
	block_number,
	transaction_hash,
	log_index,
	data,
	topics_count,
	topic_0,
	topic_1,
	topic_2,
	topic_3
) VALUES (
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?
)
`

func NewLog(logInstance *types.Log, transactionHash string, logIndex uint64) (*Log, error) {
	returnedLogInstance := &Log{
		Address:         logInstance.Address.String(),
		BlockNumber:     logInstance.BlockNumber,
		TransactionHash: transactionHash,
		TopicsCount:     int64(len(logInstance.Topics)),
		LogIndex:        logIndex,
	}
	switch true {
	case len(logInstance.Topics) > 3:
		returnedLogInstance.Topic3 = logInstance.Topics[3].Hex()
		fallthrough
	case len(logInstance.Topics) > 2:
		returnedLogInstance.Topic2 = logInstance.Topics[2].Hex()
		fallthrough
	case len(logInstance.Topics) > 1:
		returnedLogInstance.Topic1 = logInstance.Topics[1].Hex()
		fallthrough
	case len(logInstance.Topics) > 0:
		returnedLogInstance.Topic0 = logInstance.Topics[0].Hex()
	}
	if logInstance.Data != nil && len(logInstance.Data) > 0 {
		returnedLogInstance.Data = common.Bytes2Hex(logInstance.Data)
	}

	return returnedLogInstance, nil
}

type Logs []Log

func (logs Logs) Insert(db *sql.DB) error {
	bulkInsertStmt, err := db.Prepare(transactionLogInsertStmtTemplate)
	if err != nil {
		return fmt.Errorf("failed to insert %v items into the db: %s", len(logs), err)
	}
	defer bulkInsertStmt.Close()
	errors := []string{}
	for _, logInstance := range logs {
		if err := logInstance.Insert(db, bulkInsertStmt); err != nil {
			errors = append(errors, fmt.Sprintf("failed to insert log: %s", err))
		}
	}
	if len(errors) > 0 {
		return fmt.Errorf("failed to insert all (%v failed): ['%s']", len(errors), strings.Join(errors, "', '"))
	}
	return nil
}

type Log struct {
	Address         string `json:"address"`
	BlockNumber     uint64 `json:"blockNumber"`
	TransactionHash string `json:"transactionHash"`
	LogIndex        uint64 `json:"logIndex"`
	Data            string `json:"data"`
	TopicsCount     int64  `json:"topicsCount"`
	Topic0          string `json:"topic0"`
	Topic1          string `json:"topic1"`
	Topic2          string `json:"topic2"`
	Topic3          string `json:"topic3"`
}

func (l Log) Insert(db *sql.DB, useStmt ...*sql.Stmt) error {
	var stmt *sql.Stmt = nil
	var err error
	if useStmt != nil && len(useStmt) > 0 {
		stmt = useStmt[0]
	}
	if stmt == nil {
		if stmt, err = db.Prepare(transactionLogInsertStmtTemplate); err != nil {
			return fmt.Errorf("failed to prepare database insert statement: %s", err)
		}
		defer stmt.Close()
	}
	result, err := stmt.Exec(
		l.Address,
		l.BlockNumber,
		l.TransactionHash,
		l.LogIndex,
		l.Data,
		l.TopicsCount,
		l.Topic0,
		l.Topic1,
		l.Topic2,
		l.Topic3,
	)
	if err != nil {
		return fmt.Errorf("failed to execute db transaction: %s", err)
	}
	rowsAffected, err := result.RowsAffected()
	if err != nil {
		log.Warnf("failed to get number of rows affected: %s", err)
	} else {
		log.Debugf("%v row(s) affected: log[%v] of tx[%v]", rowsAffected, l.LogIndex, l.TransactionHash)
	}
	return nil
}
