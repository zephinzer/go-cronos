package farmer

import (
	"database/sql"
	"fmt"
	"strings"
	"time"
)

const erc20InsertStmtTemplate = `
REPLACE INTO erc20_token_events (
	address,
	address_from,
	address_to,
	transaction_hash,
	block_number,
	timestamp,
	` + "`" + "index" + "`" + `,
	type,
	value,
	symbol
) VALUES (
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?
)
`

// Erc20Events is a slice of Erc20Events with convenience functions
type Erc20Events []Erc20Event

func (erc20Events Erc20Events) Insert(db *sql.DB) error {
	errors := []string{}
	bulkInsertStmt, err := db.Prepare(erc20InsertStmtTemplate)
	if err != nil {
		return fmt.Errorf("failed to insert %v items into the db: %s", len(erc20Events), err)
	}
	defer bulkInsertStmt.Close()
	for _, event := range erc20Events {
		if err := event.Insert(db, bulkInsertStmt); err != nil {
			errors = append(errors, err.Error())
		}
	}
	if len(errors) > 0 {
		return fmt.Errorf("failed to complete all insertions successfully:\n - %s", strings.Join(errors, "\n - "))
	}
	return nil
}

// Erc20Event represents an ERC20 token transaction
type Erc20Event struct {
	Address         string    `json:"address"`
	AddressFrom     string    `json:"addressFrom"`
	AddressTo       string    `json:"addressTo"`
	TransactionHash string    `json:"transactionHash"`
	BlockNumber     uint64    `json:"blockNumber"`
	Timestamp       time.Time `json:"timestamp"`
	Index           uint64    `json:"index"`
	Type            string    `json:"type"`
	Value           float64   `json:"value"`
	Symbol          string    `json:"symbol"`
}

func (e20e Erc20Event) Insert(db *sql.DB, useStmt ...*sql.Stmt) error {
	var stmt *sql.Stmt = nil
	if useStmt != nil && len(useStmt) > 0 {
		stmt = useStmt[0]
	}
	if stmt == nil {
		stmt, err := db.Prepare(erc20InsertStmtTemplate)
		if err != nil {
			return fmt.Errorf("failed to prepare erc20 event db insertion: %s", err)
		}
		defer stmt.Close()
	}
	if _, err := stmt.Exec(
		e20e.Address,
		e20e.AddressFrom,
		e20e.AddressTo,
		e20e.TransactionHash,
		e20e.BlockNumber,
		e20e.Timestamp,
		e20e.Index,
		e20e.Type,
		e20e.Value,
		e20e.Symbol,
	); err != nil {
		return fmt.Errorf("failed to insert erc20 event from tx[%s] with value[%v] into the db: %s", e20e.TransactionHash, e20e.Value, err)
	}
	return nil
}
