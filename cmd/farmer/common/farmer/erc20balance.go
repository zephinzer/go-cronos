package farmer

import (
	"database/sql"
	"fmt"
	"strings"
)

const erc20BalanceInsertStmtTemplate = `
REPLACE INTO erc20_token_balances (
	address,
	address_label,
	balance_address,
	balance_amount,
	balance_symbol
) VALUES (
	?,
	?,
	?,
	?,
	?
)
`

// Erc20Balances is a slice of Erc20Balance with convenience functions
type Erc20Balances []Erc20Balance

func (erc20Balances Erc20Balances) Insert(db *sql.DB) error {
	bulkInsertStmt, err := db.Prepare(erc20BalanceInsertStmtTemplate)
	if err != nil {
		return fmt.Errorf("failed to insert %v items into the db: %s", len(erc20Balances), err)
	}
	defer bulkInsertStmt.Close()
	errors := []string{}
	for _, balance := range erc20Balances {
		if err := balance.Insert(db, bulkInsertStmt); err != nil {
			errors = append(errors, fmt.Sprintf("failed to insert balance of token[%s] in wallet[%s]: %s", balance.BalanceSymbol, balance.Address, err))
		}
	}
	if len(errors) > 0 {
		return fmt.Errorf("failed to insert all (%v failed): ['%s']", len(errors), strings.Join(errors, "', '"))
	}
	return nil
}

// Erc20Balance represents an ERC20 token transaction
type Erc20Balance struct {
	Address        string  `json:"address"`
	AddressLabel   string  `json:"addressLabel"`
	BalanceAddress string  `json:"balanceAddress"`
	BalanceAmount  float64 `json:"balanceAmount"`
	BalanceSymbol  string  `json:"balanceSymbol"`
}

func (e20b Erc20Balance) Insert(db *sql.DB, useStmt ...*sql.Stmt) error {
	var stmt *sql.Stmt = nil
	if useStmt != nil && len(useStmt) > 0 {
		stmt = useStmt[0]
	}
	if stmt == nil {
		stmt, err := db.Prepare(erc20BalanceInsertStmtTemplate)
		if err != nil {
			return fmt.Errorf("failed to prepare erc20 balance db insertion: %s", err)
		}
		defer stmt.Close()
	}

	if _, err := stmt.Exec(
		e20b.Address,
		e20b.AddressLabel,
		e20b.BalanceAddress,
		e20b.BalanceAmount,
		e20b.BalanceSymbol,
	); err != nil {
		return fmt.Errorf("failed to insert erc20 balance into the db: %s", err)
	}

	return nil
}
