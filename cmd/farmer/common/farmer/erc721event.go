package farmer

import (
	"database/sql"
	"fmt"
	"time"
)

const erc721InsertStmtTemplate = `
REPLACE INTO erc721_token_events (
	address,
	address_by,
	address_from,
	address_to,
	approval,
	token_id,
	transaction_hash,
	block_number,
	timestamp,
	` + "`" + "index" + "`" + `,
	type,
	value,
	symbol
) VALUES (
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?
)
`

// Erc721Events is a slice of Erc721Events with convenience functions
type Erc721Events []Erc721Event

func (erc721Events Erc721Events) Insert(db *sql.DB) error {
	bulkInsertStmt, err := db.Prepare(erc721InsertStmtTemplate)
	if err != nil {
		return fmt.Errorf("failed to insert %v items into the db: %s", len(erc721Events), err)
	}
	defer bulkInsertStmt.Close()
	for _, event := range erc721Events {
		event.Insert(db, bulkInsertStmt)
	}
	return nil
}

// Erc721Event represents an ERC721 token transaction
type Erc721Event struct {
	Address         string    `json:"address"`
	AddressBy       string    `json:"addressBy"`
	AddressFrom     string    `json:"addressFrom"`
	AddressTo       string    `json:"addressTo"`
	Approval        bool      `json:"approval"`
	TokenId         uint64    `json:"tokenId"`
	TransactionHash string    `json:"transactionHash"`
	BlockNumber     uint64    `json:"blockNumber"`
	Timestamp       time.Time `json:"timestamp"`
	Index           uint64    `json:"index"`
	Type            string    `json:"type"`
	Value           float64   `json:"value"`
	Symbol          string    `json:"symbol"`
}

func (e721e Erc721Event) Insert(db *sql.DB, useStmt ...*sql.Stmt) error {
	var stmt *sql.Stmt = nil
	if useStmt != nil && len(useStmt) > 0 {
		stmt = useStmt[0]
	}
	if stmt == nil {
		stmt, err := db.Prepare(erc721InsertStmtTemplate)
		if err != nil {
			return fmt.Errorf("failed to prepare erc721 event db insertion: %s", err)
		}
		defer stmt.Close()
	}
	if _, err := stmt.Exec(
		e721e.Address,
		e721e.AddressBy,
		e721e.AddressFrom,
		e721e.AddressTo,
		e721e.Approval,
		e721e.TokenId,
		e721e.TransactionHash,
		e721e.BlockNumber,
		e721e.Timestamp,
		e721e.Index,
		e721e.Type,
		e721e.Value,
		e721e.Symbol,
	); err != nil {
		return fmt.Errorf("failed to insert erc721 event into the db: %s", err)
	}
	return nil
}
