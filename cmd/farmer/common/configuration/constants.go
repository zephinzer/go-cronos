package configuration

import "github.com/usvc/go-config"

const (
	KeyDatabaseHost           = "db-host"
	KeyDatabasePort           = "db-port"
	KeyDatabaseName           = "db-name"
	KeyDatabaseUsername       = "db-username"
	KeyDatabasePassword       = "db-password"
	KeyDatabaseRootUsername   = "db-root-username"
	KeyDatabaseRootPassword   = "db-root-password"
	KeyDatabaseMigrationsPath = "db-migrations-path"

	DefaultDatabaseHost         = "127.0.0.1"
	DefaultDatabaseName         = "farmer"
	DefaultDatabasePort         = 3306
	DefaultDatabaseUsername     = "farmer"
	DefaultDatabasePassword     = "password"
	DefaultDatabaseRootUsername = "root"
	DefaultDatabaseRootPassword = "password"
)

func GetDatabaseHost() *config.String {
	return &config.String{
		Shorthand: "H",
		Usage:     "Hostname of the database instance",
		Default:   DefaultDatabaseHost,
	}
}
func GetDatabasePort() *config.Uint {
	return &config.Uint{
		Shorthand: "P",
		Usage:     "Port the database instance is listening on",
		Default:   DefaultDatabasePort,
	}
}
func GetDatabaseName() *config.String {
	return &config.String{
		Shorthand: "N",
		Usage:     "Name of the database schema to use",
		Default:   DefaultDatabaseName,
	}
}
func GetDatabaseUsername() *config.String {
	return &config.String{
		Shorthand: "u",
		Usage:     "Username to login to the database",
		Default:   DefaultDatabaseUsername,
	}
}
func GetDatabasePassword() *config.String {
	return &config.String{
		Shorthand: "p",
		Usage:     "Password to login to the database",
		Default:   DefaultDatabasePassword,
	}
}
func GetDatabaseRootUsername() *config.String {
	return &config.String{
		Shorthand: "z",
		Usage:     "Username to create the database schema",
		Default:   DefaultDatabaseRootUsername,
	}
}
func GetDatabaseRootPassword() *config.String {
	return &config.String{
		Shorthand: "Z",
		Usage:     "Password to create the database schema",
		Default:   DefaultDatabaseRootPassword,
	}
}
func GetDatabaseMigrationsPath() *config.String {
	return &config.String{
		Shorthand: "m",
		Usage:     "Path to migration files",
		Default:   "./data/migrations",
	}
}
