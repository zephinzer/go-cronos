package send

import "github.com/usvc/go-config"

const (
	ConfigKeyTelegramBotApiKey = "tg-api-key"
	ConfigKeyChatId            = "chat-id"
	ConfigKeyMessage           = "message"
)

var conf = config.Map{
	ConfigKeyTelegramBotApiKey: &config.String{
		Usage: "Bot API key for Telegram",
	},
	ConfigKeyChatId: &config.String{
		Usage: "Chat ID of the Telegram chat",
	},
	ConfigKeyMessage: &config.String{
		Usage: "Message content of the Telegram message",
	},
}
