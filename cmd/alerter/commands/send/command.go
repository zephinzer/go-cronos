package send

import (
	"fmt"
	"strconv"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"github.com/spf13/cobra"
	"github.com/zephinzer/go-cronos/internal/log"
)

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:   "send",
		Short: "sends a message immediately to the target chat ID",
		RunE:  runE,
	}
	conf.ApplyToCobra(&command)
	return &command
}

func runE(command *cobra.Command, args []string) error {
	telegramBotApiKey := conf.GetString(ConfigKeyTelegramBotApiKey)
	botInstance, err := tgbotapi.NewBotAPI(telegramBotApiKey)
	if err != nil {
		return fmt.Errorf("failed to created bot instance: %s", err)
	}
	log.Infof("bot id: %v", botInstance.Self.ID)
	log.Infof("bot name: %v", botInstance.Self.UserName)
	log.Infof("bot url: https://t.me/%v", botInstance.Self.UserName)

	chatId := conf.GetString(ConfigKeyChatId)
	messageText := conf.GetString(ConfigKeyMessage)

	var telegramMessage tgbotapi.Chattable
	parsedChatId, err := strconv.ParseInt(chatId, 10, 64)
	if err != nil { // this could happen if it's a public channel without a numeric id
		msgInstance := tgbotapi.NewMessageToChannel(chatId, messageText)
		msgInstance.ParseMode = "markdown"
		telegramMessage = msgInstance
	}
	if parsedChatId != 0 {
		msgInstance := tgbotapi.NewMessage(parsedChatId, messageText)
		msgInstance.ParseMode = "markdown"
		telegramMessage = msgInstance
	}
	receipt, err := botInstance.Send(telegramMessage)
	if err != nil {
		log.Warnf("failed to send message to chat[%v]: %s", chatId, err)
	}
	log.Infof("sent message[%v] to chat[%v]", receipt.MessageID, receipt.Chat.ID)

	return nil
}
