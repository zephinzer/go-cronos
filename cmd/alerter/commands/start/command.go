package start

import (
	"fmt"
	"strconv"
	"sync"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"github.com/spf13/cobra"
	"github.com/zephinzer/go-cronos/internal/log"
)

type Listener func(chan Message)

type Message struct {
	ChatId string
	Text   string
}

var (
	messages  chan Message
	listeners []Listener
)

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:   "start",
		Short: "starts the alerter service",
		RunE:  runE,
	}
	conf.ApplyToCobra(&command)
	listeners = append(listeners, pulse)
	return &command
}

func runE(command *cobra.Command, args []string) error {
	telegramBotApiKey := conf.GetString(ConfigKeyTelegramBotApiKey)
	botInstance, err := tgbotapi.NewBotAPI(telegramBotApiKey)
	if err != nil {
		return fmt.Errorf("failed to created bot instance: %s", err)
	}
	log.Infof("bot id: %v", botInstance.Self.ID)
	log.Infof("bot name: %v", botInstance.Self.UserName)
	log.Infof("bot url: https://t.me/%v", botInstance.Self.UserName)

	messages = make(chan Message, 1024)

	var waiter sync.WaitGroup

	waiter.Add(1)
	for _, listenerInstance := range listeners {
		go listenerInstance(messages)
	}
	go func() {
		for {
			messageInstance := <-messages
			log.Infof("received message request to chat[%s]", messageInstance.ChatId)
			var telegramMessage tgbotapi.Chattable
			messageText := messageInstance.Text
			parsedChatId, err := strconv.ParseInt(messageInstance.ChatId, 10, 64)
			if err != nil { // this could happen if it's a public channel without a numeric id
				telegramMessage = tgbotapi.NewMessageToChannel(messageInstance.ChatId, messageText)
			}
			if parsedChatId != 0 {
				telegramMessage = tgbotapi.NewMessage(parsedChatId, messageText)
			}
			go func(msg tgbotapi.Chattable) {
				receipt, err := botInstance.Send(msg)
				if err != nil {
					log.Warnf("failed to send tg to chat[%v]: %s", messageInstance.ChatId, err)
				}
				log.Infof("sent tg message[%v] to chat[%v]", receipt.MessageID, receipt.Chat.ID)
			}(telegramMessage)
		}
	}()

	waiter.Wait()
	return nil
}
