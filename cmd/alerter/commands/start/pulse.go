package start

import "time"

func pulse(messageStream chan Message) {
	for {
		messageStream <- Message{
			ChatId: "267230627",
			Text:   "hello world",
		}
		<-time.After(5 * time.Second)
	}
}
