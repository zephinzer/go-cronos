package start

import "github.com/usvc/go-config"

const (
	ConfigKeyTelegramBotApiKey = "tg-api-key"
)

var conf = config.Map{
	ConfigKeyTelegramBotApiKey: &config.String{
		Usage: "Bot API key for Telegram",
	},
}
