package listen

import (
	"fmt"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/usvc/go-config"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

const (
	ConfigTelegramApiKey = "tg-api-key"
)

var conf = config.Map{
	ConfigTelegramApiKey: &config.String{
		Usage: "telegram bot api token (get it from https://t.me/BotFather)",
	},
}

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:   "listen",
		Short: "Listen for chat messages and responds to them with the chat ID",
		RunE:  run,
	}
	conf.ApplyToCobra(&command)
	return &command
}

func run(command *cobra.Command, args []string) error {
	bot, err := tgbotapi.NewBotAPI(conf.GetString(ConfigTelegramApiKey))
	if err != nil {
		return fmt.Errorf("failed to create new telegram bot instance: %s", err)
	}

	logrus.Infof("bot is at https://t.me/%s", bot.Self.UserName)
	updates := tgbotapi.NewUpdate(0)
	updates.Timeout = 60

	updatesChannel := bot.GetUpdatesChan(updates)

	for update := range updatesChannel {
		if update.ChannelPost != nil {
			logrus.Infof("[%s] %s", update.ChannelPost.Chat.UserName, update.ChannelPost.Text)
			msg := tgbotapi.NewMessage(
				update.ChannelPost.Chat.ID,
				fmt.Sprintf("this chat id is `%v`", update.ChannelPost.Chat.ID),
			)
			msg.ReplyToMessageID = update.ChannelPost.MessageID
			msg.ParseMode = "markdown"
			bot.Send(msg)
		}
		if update.Message != nil {
			logrus.Infof("[%s] %s", update.Message.From.UserName, update.Message.Text)

			msg := tgbotapi.NewMessage(
				update.Message.Chat.ID,
				fmt.Sprintf("this chat id is `%v`", update.Message.Chat.ID),
			)
			msg.ReplyToMessageID = update.Message.MessageID
			msg.ParseMode = "markdown"
			bot.Send(msg)
		}
	}
	return nil
}
