package main

import (
	"github.com/spf13/cobra"
	"github.com/zephinzer/go-cronos/cmd/alerter/commands/listen"
	"github.com/zephinzer/go-cronos/cmd/alerter/commands/send"
	"github.com/zephinzer/go-cronos/cmd/alerter/commands/start"
	"github.com/zephinzer/go-cronos/internal/constants"
	"github.com/zephinzer/go-cronos/internal/log"
)

func main() {
	err := GetCommand().Execute()
	if err != nil {
		log.Errorf("failed to execute successfully: %s", err)
	}
}

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:   "alerter <sub-command>",
		Short: "Alerter service that sends alerts based on triggers",
		RunE:  runE,
	}
	command.Version = constants.Version
	command.AddCommand(listen.GetCommand())
	command.AddCommand(send.GetCommand())
	command.AddCommand(start.GetCommand())
	return &command
}

func runE(command *cobra.Command, args []string) error {
	command.Help()
	return nil
}
