package transactions

import (
	"fmt"
	"strings"
	"time"

	"github.com/ethereum/go-ethereum/common"
	"github.com/spf13/cobra"
	"github.com/zephinzer/go-cronos/cmd/cronoscan/common/cronoscan"
	"github.com/zephinzer/go-cronos/cmd/cronoscan/common/types"
	"github.com/zephinzer/go-cronos/internal/database"
	"github.com/zephinzer/go-cronos/internal/log"
	"github.com/zephinzer/go-cronos/pkg/addressbook"
	"github.com/zephinzer/go-cronos/pkg/cronoscanapi"
)

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:     "transactions <contract-identifier>",
		Aliases: []string{"tx", "txs"},
		Short:   "Parses data output from Cronoscan given a contract address and sends it into the database",
		RunE:    runE,
	}
	conf.ApplyToCobra(&command)
	return &command
}

func runE(command *cobra.Command, args []string) error {
	if len(args) == 0 {
		return fmt.Errorf("failed to receive a <contract-identifier> (specify it as an argument)")
	}
	batchSize := conf.GetUint(ConfigKeyBatchSize)
	contractAddress, err := addressbook.GetAddress("cronos", args[0])
	if err != nil {
		log.Warnf("failed to get contract[%s] from address book: %s", args[0], err)
		targetAddress := common.HexToAddress(args[0])
		contractAddress = &targetAddress
	}
	cronoscanApiKey := conf.GetString(ConfigKeyCronoscanApiKey)
	fromBlock := conf.GetUint(ConfigKeyFromBlock)
	tillBlock := conf.GetUint(ConfigKeyTillBlock)

	if tillBlock == 0 {
		for tillBlock == 0 {
			latestBlock, err := cronoscanapi.GetLatestBlock(cronoscanApiKey)
			if err != nil {
				log.Warnf("failed to get latest block: %s", err)
				retryAfterDuration := 3 * time.Second
				log.Infof("retrying in %v", retryAfterDuration)
				time.Sleep(retryAfterDuration)
				continue
			}
			tillBlock = uint(latestBlock)
			log.Infof("till block not specified, using block[%v]", tillBlock)
		}
	}

	if fromBlock == 0 {
		fromBlock = tillBlock - batchSize
		log.Infof("from block not specified, using block[%v]", fromBlock)
	}

	log.Infof("from block[%v] to block[%v]", fromBlock, tillBlock)

	// TODO(joseph) - set these as some const/conf
	retryInterval := time.Second * 5
	dbMaxRetries := 10

	// connect to database
	log.Infof("connecting to db at %s:%v...", conf.GetString(ConfigKeyDatabaseHost), conf.GetUint(ConfigKeyDatabasePort))
	db, err := database.GetConnection(database.GetConnectionOpts{
		Username:      conf.GetString(ConfigKeyDatabaseUsername),
		Password:      conf.GetString(ConfigKeyDatabasePassword),
		Host:          conf.GetString(ConfigKeyDatabaseHost),
		Port:          uint64(conf.GetUint(ConfigKeyDatabasePort)),
		Database:      conf.GetString(ConfigKeyDatabaseName),
		RetryInterval: retryInterval,
		MaxRetryCount: uint64(dbMaxRetries),
		Log:           func(l string, a ...any) { log.Infof(l, a...) },
		LogError:      func(l string, a ...any) { log.Warnf(l, a...) },
	})
	if err != nil {
		return fmt.Errorf("failed to create db connection: %s", err)
	}
	defer db.Close()

	errors := []string{}
	for currentBlock := fromBlock; currentBlock < tillBlock; currentBlock += batchSize {
		upToBlock := currentBlock + batchSize - 1
		if upToBlock > tillBlock {
			upToBlock = tillBlock
		}
		log.Infof("retrieving transactions from block[%v] to block[%v]...", currentBlock, upToBlock)
		transactionsInstance, err := cronoscanapi.GetTransactions(cronoscanapi.GetTransactionsOpts{
			ContractAddress: contractAddress.String(),
			FromBlock:       currentBlock,
			TillBlock:       upToBlock,
			ApiKey:          cronoscanApiKey,
		})
		if err != nil {
			errString := fmt.Sprintf("failed to get transactions: %s", err)
			log.Warn(errString)
			errors = append(errors, errString)
			continue
		}
		log.Infof("received %v transactions from cronoscan", len(transactionsInstance))

		nonLpTransactions := []cronoscanapi.Transaction{}
		lpTransactions := []cronoscanapi.Transaction{}
		log.Info("separating transactions involving an lp token...")
		for _, transactionInstance := range transactionsInstance {
			fmt.Printf("token symbol[%s] in tx[%s] at timestamp[%v]\n", transactionInstance.TokenSymbol, transactionInstance.Hash, transactionInstance.TimeStamp)
			if isLp, ok := LpTokenMap[transactionInstance.TokenSymbol]; ok && isLp {
				lpTransactions = append(lpTransactions, transactionInstance)
				continue
			}
			nonLpTransactions = append(nonLpTransactions, transactionInstance)
		}

		log.Infof("inserting %v non-lp and %v lp transactions...", len(nonLpTransactions), len(lpTransactions))
		insertableTransactionsInstance := types.NewTransactions(nonLpTransactions)
		log.Infof("retrieved non-lp %v transactions", len(insertableTransactionsInstance))
		if err := insertableTransactionsInstance.Insert(db, cronoscan.TableTransactions); err != nil {
			errString := fmt.Sprintf("failed to successfully insert all non-lp transactions into the database: %s", err)
			log.Warn(errString)
			errors = append(errors, errString)
			continue
		}

		insertableTransactionsInstance = types.NewTransactions(lpTransactions)
		log.Infof("retrieved lp %v transactions", len(insertableTransactionsInstance))
		if err := insertableTransactionsInstance.Insert(db, cronoscan.TableLpTransactions); err != nil {
			errString := fmt.Sprintf("failed to successfully insert all lp transactions into the database: %s", err)
			log.Warn(errString)
			errors = append(errors, errString)
			continue
		}
	}

	if len(errors) > 0 {
		return fmt.Errorf("failed to complete successfully (%v operations failed):\n  - %s", len(errors), strings.Join(errors, "\n  - "))
	}

	return nil
}
