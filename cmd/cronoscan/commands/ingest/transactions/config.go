package transactions

import "github.com/usvc/go-config"

var (
	LpTokenMap map[string]bool = map[string]bool{
		"ANN-LP":     true,
		"CGS-LP":     true,
		"CNO-LP":     true,
		"CROS-USDC":  true,
		"CROS-WCRO":  true,
		"Crona-LP":   true,
		"ELP":        true,
		"Hop-LP":     true,
		"MEERKAT-LP": true,
		"Strong-LP":  true,
		"UNI-V2":     true,
		"VVS-LP":     true,
	}
)

const (
	ConfigKeyBatchSize            = "batch-size"
	ConfigKeyCronoscanApiKey      = "cronoscan-api-key"
	ConfigKeyDatabaseUsername     = "db-username"
	ConfigKeyDatabasePassword     = "db-password"
	ConfigKeyDatabaseHost         = "db-host"
	ConfigKeyDatabasePort         = "db-port"
	ConfigKeyDatabaseName         = "db-name"
	ConfigKeyDatabaseRootUsername = "db-root-username"
	ConfigKeyDatabaseRootPassword = "db-root-password"
	ConfigKeyFromBlock            = "from-block"
	ConfigKeyTillBlock            = "till-block"

	DefaultBatchSize            = 500
	DefaultDatabaseUsername     = "cronoscan"
	DefaultDatabasePassword     = "password"
	DefaultDatabaseHost         = "127.0.0.1"
	DefaultDatabasePort         = 3306
	DefaultDatabaseName         = "cronoscan"
	DefaultDatabaseRootUsername = "user"
	DefaultDatabaseRootPassword = "password"
)

var conf = config.Map{
	ConfigKeyBatchSize: &config.Uint{
		Shorthand: "b",
		Usage:     "batch size in terms of number of blocks",
		Default:   DefaultBatchSize,
	},
	ConfigKeyCronoscanApiKey: &config.String{
		Shorthand: "k",
		Usage:     "api key for cronoscan (get from https://cronoscan.com/myapikey)",
	},
	ConfigKeyDatabaseUsername: &config.String{
		Shorthand: "u",
		Usage:     "username of the database user",
		Default:   DefaultDatabaseUsername,
	},
	ConfigKeyDatabasePassword: &config.String{
		Shorthand: "p",
		Usage:     "password of the database user",
		Default:   DefaultDatabasePassword,
	},
	ConfigKeyDatabaseHost: &config.String{
		Shorthand: "H",
		Usage:     "hostname of the database",
		Default:   DefaultDatabaseHost,
	},
	ConfigKeyDatabasePort: &config.Uint{
		Shorthand: "P",
		Usage:     "port the database instance is listening on",
		Default:   DefaultDatabasePort,
	},
	ConfigKeyDatabaseName: &config.String{
		Shorthand: "n",
		Usage:     "name of the database schema to use",
		Default:   DefaultDatabaseName,
	},
	ConfigKeyDatabaseRootUsername: &config.String{
		Shorthand: "z",
		Usage:     "Username to create the database schema",
		Default:   DefaultDatabaseRootUsername,
	},
	ConfigKeyDatabaseRootPassword: &config.String{
		Shorthand: "Z",
		Usage:     "Password to create the database schema",
		Default:   DefaultDatabaseRootPassword,
	},
	ConfigKeyFromBlock: &config.Uint{
		Shorthand: "f",
		Usage:     "starting block (inclusive) number",
	},
	ConfigKeyTillBlock: &config.Uint{
		Shorthand: "t",
		Usage:     "final block (inclusive) number",
	},
}
