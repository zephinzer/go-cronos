package ingest

import (
	"github.com/spf13/cobra"
	"github.com/zephinzer/go-cronos/cmd/cronoscan/commands/ingest/transactions"
)

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:   "ingest",
		Short: "Parses data output from Cronoscan and sends it into the database",
		RunE:  runE,
	}
	command.AddCommand(transactions.GetCommand())
	return &command
}

func runE(command *cobra.Command, args []string) error {
	return command.Help()
}
