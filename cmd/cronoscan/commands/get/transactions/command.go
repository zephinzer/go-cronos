package transactions

import (
	"encoding/json"
	"fmt"

	"github.com/spf13/cobra"
	"github.com/zephinzer/go-cronos/pkg/cronoscanapi"
)

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:     "transactions",
		Aliases: []string{"tx"},
		Short:   "Retrieves transaction data from Cronoscan and prints it",
		RunE:    runE,
	}
	conf.ApplyToCobra(&command)
	return &command
}

func runE(command *cobra.Command, args []string) error {
	apiKey := conf.GetString(ConfigKeyCronoscanApiKey)
	contractAddress := conf.GetString(ConfigKeyContractAddress)
	fromBlock := conf.GetUint(ConfigKeyFromBlock)
	tillBlock := conf.GetUint(ConfigKeyTillBlock)

	transactions, err := cronoscanapi.GetTransactions(cronoscanapi.GetTransactionsOpts{
		ApiKey:          apiKey,
		ContractAddress: contractAddress,
		FromBlock:       fromBlock,
		TillBlock:       tillBlock,
	})
	if err != nil {
		return fmt.Errorf("failed to get transactions for contract[%s] from block[%v] to block[%v]: %s", contractAddress, fromBlock, tillBlock, err)
	}
	x, _ := json.MarshalIndent(transactions, "", "  ")
	fmt.Println(string(x))
	return nil
}
