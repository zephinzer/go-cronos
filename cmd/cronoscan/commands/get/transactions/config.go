package transactions

import "github.com/usvc/go-config"

const (
	ConfigKeyContractAddress = "contract-address"
	ConfigKeyCronoscanApiKey = "cronoscan-api-key"
	ConfigKeyFromBlock       = "from-block"
	ConfigKeyTillBlock       = "till-block"
)

var conf = config.Map{
	ConfigKeyContractAddress: &config.String{
		Shorthand: "a",
		Usage:     "contract address of the token",
	},
	ConfigKeyCronoscanApiKey: &config.String{
		Shorthand: "k",
		Usage:     "api key for cronoscan (get from https://cronoscan.com/myapikey)",
	},
	ConfigKeyFromBlock: &config.Uint{
		Shorthand: "f",
		Usage:     "starting block (inclusive) number",
	},
	ConfigKeyTillBlock: &config.Uint{
		Shorthand: "t",
		Usage:     "final block (inclusive) number",
	},
}
