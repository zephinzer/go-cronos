package get

import (
	"github.com/spf13/cobra"
	"github.com/zephinzer/go-cronos/cmd/cronoscan/commands/get/transactions"
)

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:     "get",
		Aliases: []string{"g"},
		Short:   "Retrieves data from Cronoscan and pastes it",
		RunE:    runE,
	}
	command.AddCommand(transactions.GetCommand())
	return &command
}

func runE(command *cobra.Command, args []string) error {
	return command.Help()
}
