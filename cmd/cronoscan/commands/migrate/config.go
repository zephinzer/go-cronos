package migrate

import (
	"github.com/usvc/go-config"
	"github.com/zephinzer/go-cronos/cmd/cronoscan/common/configuration"
)

var conf = config.Map{
	configuration.KeyDatabaseHost:           configuration.GetDatabaseHost(),
	configuration.KeyDatabasePort:           configuration.GetDatabasePort(),
	configuration.KeyDatabaseUsername:       configuration.GetDatabaseUsername(),
	configuration.KeyDatabasePassword:       configuration.GetDatabasePassword(),
	configuration.KeyDatabaseName:           configuration.GetDatabaseName(),
	configuration.KeyDatabaseRootUsername:   configuration.GetDatabaseRootUsername(),
	configuration.KeyDatabaseRootPassword:   configuration.GetDatabaseRootPassword(),
	configuration.KeyDatabaseMigrationsPath: configuration.GetDatabaseMigrationsPath(),
}

func InitConfiguration(conf config.Map) (*commandConfiguration, error) {
	configurationInstance := &commandConfiguration{
		DatabaseHost:           conf.GetString(configuration.KeyDatabaseHost),
		DatabasePort:           conf.GetUint(configuration.KeyDatabasePort),
		DatabaseUsername:       conf.GetString(configuration.KeyDatabaseUsername),
		DatabasePassword:       conf.GetString(configuration.KeyDatabasePassword),
		DatabaseName:           conf.GetString(configuration.KeyDatabaseName),
		DatabaseRootUsername:   conf.GetString(configuration.KeyDatabaseRootUsername),
		DatabaseRootPassword:   conf.GetString(configuration.KeyDatabaseRootPassword),
		DatabaseMigrationsPath: conf.GetString(configuration.KeyDatabaseMigrationsPath),
	}
	return configurationInstance, nil
}

type commandConfiguration struct {
	DatabaseHost           string
	DatabasePort           uint
	DatabaseUsername       string
	DatabasePassword       string
	DatabaseName           string
	DatabaseRootUsername   string
	DatabaseRootPassword   string
	DatabaseMigrationsPath string
}
