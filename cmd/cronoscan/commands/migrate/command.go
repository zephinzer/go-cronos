package migrate

import (
	"fmt"

	"github.com/spf13/cobra"
	"github.com/zephinzer/go-cronos/internal/database"
	"github.com/zephinzer/go-cronos/internal/log"
)

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:   "migrate [flags...]",
		Short: "Migrates the database for Cronoscan API transaction farming",
		RunE:  runE,
	}
	conf.ApplyToCobra(&command)
	return &command
}

func runE(command *cobra.Command, args []string) error {
	log.Infof("starting database migrations...")
	configurationInstance, configError := InitConfiguration(conf)
	if configError != nil {
		return fmt.Errorf("failed to receive a valid configuration: %s", configError)
	}

	return database.Migrate(database.MigrateOpts{
		DatabaseHost:           configurationInstance.DatabaseHost,
		DatabasePort:           uint64(configurationInstance.DatabasePort),
		DatabaseUsername:       configurationInstance.DatabaseUsername,
		DatabasePassword:       configurationInstance.DatabasePassword,
		DatabaseRootUsername:   configurationInstance.DatabaseRootUsername,
		DatabaseRootPassword:   configurationInstance.DatabaseRootPassword,
		DatabaseName:           configurationInstance.DatabaseName,
		DatabaseMigrationsPath: configurationInstance.DatabaseMigrationsPath,
		Log:                    func(f string, a ...any) { log.Infof(f, a...) },
	})
}
