package prune

import (
	"github.com/spf13/cobra"
	"github.com/zephinzer/go-cronos/cmd/cronoscan/commands/prune/lps"
)

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:   "prune",
		Short: "Prunes data output from Cronoscan from the database",
		RunE:  runE,
	}
	command.AddCommand(lps.GetCommand())
	return &command
}

func runE(command *cobra.Command, args []string) error {
	return command.Help()
}
