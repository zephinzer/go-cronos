package lps

import (
	"context"
	"fmt"
	"strings"
	"time"

	"github.com/spf13/cobra"
	"github.com/zephinzer/go-cronos/cmd/cronoscan/common/cronoscan"
	"github.com/zephinzer/go-cronos/internal/constants"
	"github.com/zephinzer/go-cronos/internal/database"
	"github.com/zephinzer/go-cronos/internal/log"
)

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:   "lps",
		Short: "Prunes LP data from Cronoscan from the database",
		RunE:  runE,
	}
	conf.ApplyToCobra(&command)
	return &command
}

func runE(command *cobra.Command, args []string) error {
	retryInterval := 6 * time.Second
	dbMaxRetries := 10

	// connect to database
	log.Infof("connecting to db at %s:%v...", conf.GetString(ConfigKeyDatabaseHost), conf.GetUint(ConfigKeyDatabasePort))
	db, err := database.GetConnection(database.GetConnectionOpts{
		Username:      conf.GetString(ConfigKeyDatabaseUsername),
		Password:      conf.GetString(ConfigKeyDatabasePassword),
		Host:          conf.GetString(ConfigKeyDatabaseHost),
		Port:          uint64(conf.GetUint(ConfigKeyDatabasePort)),
		Database:      conf.GetString(ConfigKeyDatabaseName),
		RetryInterval: retryInterval,
		MaxRetryCount: uint64(dbMaxRetries),
		Log:           func(l string, a ...any) { log.Infof(l, a...) },
		LogError:      func(l string, a ...any) { log.Warnf(l, a...) },
	})
	if err != nil {
		return fmt.Errorf("failed to create db connection: %s", err)
	}
	defer db.Close()

	sinceSecondsAgo := conf.GetUint(ConfigKeySinceSecondsAgo)
	since := time.Now().UTC().Add(-1 * time.Duration(sinceSecondsAgo) * time.Second)
	sinceFormat := since.Format(constants.TimestampYmdHMS)
	log.Infof("pruning transactions table since %s", sinceFormat)
	targetSelection := fmt.Sprintf(
		"SELECT DISTINCT(hash) FROM %s WHERE last_modified > ?",
		cronoscan.TableLpTransactions,
	)
	targetSelectionStmt, err := db.PrepareContext(context.Background(), targetSelection)
	if err != nil {
		return fmt.Errorf("failed to prepare db stmt that selects for transaction hashes: %s", err)
	}
	rows, err := targetSelectionStmt.Query(sinceFormat)
	if err != nil {
		return fmt.Errorf("failed to query for unique lp transaction hashes: %s", err)
	}
	hashes := []string{}
	for rows.Next() {
		var lpHash string
		rows.Scan(&lpHash)
		hashes = append(hashes, lpHash)
	}
	log.Infof("found %v unique lp tx hashes", len(hashes))
	hashPlaceholders := []string{}
	for i := 0; i < len(hashes); i++ {
		hashPlaceholders = append(hashPlaceholders, "?")
	}

	if len(hashes) == 0 {
		log.Infof("no unique lp tx hashes found, this is it, bye!")
		return nil
	}

	batchSize := int(conf.GetUint(ConfigKeyTransactionBatchSize))
	for i := 0; i < len(hashes); i += batchSize {
		fromIndex := i
		toIndex := i + batchSize
		if toIndex > len(hashes) {
			toIndex = len(hashes)
		}
		targetHashes := hashes[fromIndex:toIndex]
		targetHashesPlaceholder := hashPlaceholders[fromIndex:toIndex]
		transactionTransfer := fmt.Sprintf(`
		INSERT INTO %s (
			block_number,
			contract_address,
			hash,
			address_from,
			address_to,
			transaction_index,
			time_stamp,
			token_symbol,
			token_decimal,
			value,
			last_modified
		) SELECT
			block_number,
			contract_address,
			hash,
			address_from,
			address_to,
			transaction_index,
			time_stamp,
			token_symbol,
			token_decimal,
			value,
			last_modified
		FROM %s WHERE last_modified > ? AND hash IN (%s)
		ON DUPLICATE KEY UPDATE %s.last_modified=%s.last_modified
			`,
			cronoscan.TableLpTransactions,
			cronoscan.TableTransactions,
			strings.Join(targetHashesPlaceholder, ","),
			cronoscan.TableLpTransactions,
			cronoscan.TableTransactions,
		)
		transactionTransferStmt, err := db.PrepareContext(context.Background(), transactionTransfer)
		if err != nil {
			return fmt.Errorf("failed to prepare transaction transfer statement: %s", err)
		}

		transactionsSinceFormat := since.Format(constants.TimestampYmdHMS)
		transactionTransferArgs := []interface{}{transactionsSinceFormat}
		for _, targetHash := range targetHashes {
			transactionTransferArgs = append(transactionTransferArgs, targetHash)
		}
		if res, err := transactionTransferStmt.Exec(transactionTransferArgs...); err != nil {
			return fmt.Errorf("failed to move lp transactions into lp table: %s", err)
		} else {
			rowsAffected, err := res.RowsAffected()
			if err != nil {
				log.Warnf("failed to get number of rows affected: %s", err)
			} else {
				log.Infof("copied %v rows from '%s' into '%s'", rowsAffected, cronoscan.TableTransactions, cronoscan.TableLpTransactions)
			}
		}

		log.Infof("removing %v transaction hashes from table[%s]", len(targetHashes), cronoscan.TableLpTransactions)
		transactionDeletion := fmt.Sprintf(
			`DELETE FROM %s WHERE last_modified > ? AND hash IN (%s)`,
			cronoscan.TableTransactions,
			strings.Join(targetHashesPlaceholder, ","),
		)
		transactionDeletionStmt, err := db.PrepareContext(context.Background(), transactionDeletion)
		if res, err := transactionDeletionStmt.Exec(transactionTransferArgs...); err != nil {
			return fmt.Errorf("failed to remove lp transactions from non-lp table: %s", err)
		} else {
			rowsAffected, err := res.RowsAffected()
			if err != nil {
				log.Warnf("failed to get number of rows affected: %s", err)
			} else {
				log.Infof("deleted %v rows from '%s'", rowsAffected, cronoscan.TableTransactions)
			}
		}
	}

	return nil
}
