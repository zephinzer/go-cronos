package lps

import "github.com/usvc/go-config"

const (
	ConfigKeyDatabaseUsername     = "db-username"
	ConfigKeyDatabasePassword     = "db-password"
	ConfigKeyDatabaseHost         = "db-host"
	ConfigKeyDatabasePort         = "db-port"
	ConfigKeyDatabaseName         = "db-name"
	ConfigKeySinceSecondsAgo      = "since-seconds-ago"
	ConfigKeyTransactionBatchSize = "tx-batch-size"

	DefaultDatabaseUsername     = "cronoscan"
	DefaultDatabasePassword     = "password"
	DefaultDatabaseHost         = "127.0.0.1"
	DefaultDatabasePort         = 3306
	DefaultDatabaseName         = "cronoscan"
	DefaultSinceSecondsAgo      = 60 * 20
	DefaultTransactionBatchSize = 50
)

var conf = config.Map{
	ConfigKeyDatabaseUsername: &config.String{
		Shorthand: "u",
		Usage:     "username of the database user",
		Default:   DefaultDatabaseUsername,
	},
	ConfigKeyDatabasePassword: &config.String{
		Shorthand: "p",
		Usage:     "password of the database user",
		Default:   DefaultDatabasePassword,
	},
	ConfigKeyDatabaseHost: &config.String{
		Shorthand: "H",
		Usage:     "hostname of the database",
		Default:   DefaultDatabaseHost,
	},
	ConfigKeyDatabasePort: &config.Uint{
		Shorthand: "P",
		Usage:     "port the database instance is listening on",
		Default:   DefaultDatabasePort,
	},
	ConfigKeyDatabaseName: &config.String{
		Shorthand: "n",
		Usage:     "name of the database schema to use",
		Default:   DefaultDatabaseName,
	},
	ConfigKeySinceSecondsAgo: &config.Uint{
		Shorthand: "s",
		Usage:     "prune lp transactions since seconds ago",
		Default:   DefaultSinceSecondsAgo,
	},
	ConfigKeyTransactionBatchSize: &config.Uint{
		Shorthand: "b",
		Usage:     "maximum number of transaction hashes to remove in one database transaction",
		Default:   DefaultTransactionBatchSize,
	},
}
