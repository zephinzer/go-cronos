package cronoscan

import "github.com/zephinzer/go-cronos/internal/constants"

const (
	// CronosDecimals is the number of deciamsl of the
	// CRO coin
	CronosDecimals = 18

	// TableTransactions is the name of the table which
	// the `cronoscan` command uses; it's prefixed so
	// that it won't override `transactions` tables of
	// other services
	TableTransactions = "croscan_transactions"

	// TableLpTransactions is the name of the table for
	// LP-associated transactions to go to
	TableLpTransactions = "lp_transactions"

	TimestampYmdHMS  = constants.TimestampYmdHMS
	TimestampYmdHMSz = constants.TimestampYmdHMSz
	TimestampMySql   = constants.TimestampMySql
)
