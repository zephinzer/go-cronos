package types

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"math"
	"strconv"
	"strings"
	"time"

	"github.com/go-sql-driver/mysql"
	"github.com/zephinzer/go-cronos/internal/constants"
	"github.com/zephinzer/go-cronos/pkg/cronoscanapi"
)

// insertionStatementTemplate is the SQL statement we are
// running to insert the provided transaction into our database
const insertionStatementTemplate = `
INSERT INTO %s (
	block_number,
	contract_address,
	hash,
	address_from,
	address_to,
	transaction_index,
	time_stamp,
	token_symbol,
	token_decimal,
	value
) VALUES (
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?
)
`

// Transactions is a convenience structure for holding
// a slice of transactions that can be inserted into our
// dtabase
type Transactions []Transaction

// NewTransactions is a convenience function for converting
// raw cronoscanapi.Transaction instances into our database
// insertable Transaction instances
func NewTransactions(txs []cronoscanapi.Transaction) Transactions {
	insertableTxTransactions := Transactions{}
	for _, txInstance := range txs {
		insertableTxTransactions = append(
			insertableTxTransactions,
			Transaction(txInstance),
		)
	}
	return insertableTxTransactions
}

// Insert does a bulk insert of all transactions in the
// represented slice of Transaction instances
func (t Transactions) Insert(db *sql.DB, tableName string) error {
	transactionStmt, err := db.Prepare(fmt.Sprintf(insertionStatementTemplate, tableName)) // constants.TableTransactions))
	if err != nil {
		return fmt.Errorf("failed to prepare the transaction insertion statement: %s", err)
	}
	defer transactionStmt.Close()
	errors := []string{}
	for _, transactionInstance := range t {
		if err := transactionInstance.Insert(db, tableName, transactionStmt); err != nil {
			errors = append(errors, err.Error())
		}
	}
	if len(errors) > 0 {
		return fmt.Errorf("failed to bulk insert transactions: ['%s']", strings.Join(errors, "', '"))
	}
	return nil
}

// Transaction represents a transaction which can be
// inserted into our database
type Transaction cronoscanapi.Transaction

// Insert does a single insert of this Transaction instance
func (t Transaction) Insert(db *sql.DB, tableName string, stmt ...*sql.Stmt) error {
	var transactionStmt *sql.Stmt = nil
	if stmt != nil && len(stmt) > 0 {
		transactionStmt = stmt[0]
	}
	if transactionStmt == nil {
		var err error
		if transactionStmt, err = db.Prepare(fmt.Sprintf(insertionStatementTemplate, tableName)); err != nil { // constants.TableTransactions)); err != nil {
			return fmt.Errorf("failed to prepare the transaction insertion statement: %s", err)
		}
		defer transactionStmt.Close()
	}
	blockNumber, err := strconv.ParseUint(t.BlockNumber, 10, 64)
	if err != nil {
		return fmt.Errorf("failed to convert block number '%s' into an unsigned integer: %s", t.BlockNumber, err)
	}
	timeStampEpoch, err := strconv.ParseInt(t.TimeStamp, 10, 64)
	if err != nil {
		return fmt.Errorf("failed to convert timestamp '%s' into an unsigned intger: %s", t.TimeStamp, err)
	}
	timestamp := time.Unix(timeStampEpoch, 0).UTC().Format(constants.TimestampMySql)
	tokenDecimal, _ := strconv.ParseInt(t.TokenDecimal, 10, 64)
	value, err := strconv.ParseFloat(t.Value, 64)
	if err != nil {
		return fmt.Errorf("failed to convert value '%s' into a float: %s", t.Value, err)
	}
	value = float64(value / float64(math.Pow(10, float64(tokenDecimal))))
	transactionIndex, _ := strconv.Atoi(t.TransactionIndex)
	execArgs := []any{
		blockNumber,
		t.ContractAddress,
		t.Hash,
		t.From,
		t.To,
		transactionIndex,
		timestamp,
		t.TokenSymbol,
		tokenDecimal,
		value,
	}
	if _, err = transactionStmt.Exec(execArgs...); err != nil {
		if mysqlError, ok := err.(*mysql.MySQLError); ok && mysqlError.Number == 1062 {
			// ignore if its a duplicate since we don't mind
			return nil
		}
		jsonData, _ := json.Marshal(execArgs)
		return fmt.Errorf("failed to insert into database: %s (data: %v)", err, string(jsonData))
	}
	return nil
}
