package main

import (
	"fmt"
	"os"
	"strings"

	"github.com/spf13/cobra"
	"github.com/zephinzer/go-cronos/cmd/cronoscan/commands/get"
	"github.com/zephinzer/go-cronos/cmd/cronoscan/commands/ingest"
	"github.com/zephinzer/go-cronos/cmd/cronoscan/commands/migrate"
	"github.com/zephinzer/go-cronos/cmd/cronoscan/commands/prune"
	"github.com/zephinzer/go-cronos/internal/constants"
	"github.com/zephinzer/go-cronos/internal/log"
)

const appId = "cronoscan"

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:   "cronoscan",
		Short: "Utility tool to do things with the Cronoscan API",
		RunE:  run,
	}
	command.AddCommand(get.GetCommand())
	command.AddCommand(ingest.GetCommand())
	command.AddCommand(migrate.GetCommand())
	command.AddCommand(prune.GetCommand())
	return &command
}

func run(command *cobra.Command, args []string) error {
	command.Help()
	return nil
}

func main() {
	log.Alert(fmt.Sprintf("%s[%s] started with arguments['%s']", appId, constants.Version, strings.Join(os.Args, "', '")), log.AlertTypeInfo)
	if err := GetCommand().Execute(); err != nil {
		if alertErr := log.Alert(fmt.Sprintf("%s[%s '%s'] failed to execute successfully: \n\n%s", appId, constants.Version, strings.Join(os.Args, "', '"), err), log.AlertTypeError); alertErr != nil {
			log.Errorf("failed to send alert: %s", alertErr)
		}
		os.Exit(constants.ExitCodeNotOk)
		return
	}
}
