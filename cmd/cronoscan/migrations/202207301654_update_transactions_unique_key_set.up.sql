START TRANSACTION;
ALTER TABLE `croscan_transactions`
  ADD COLUMN `transaction_index` INT UNSIGNED NOT NULL AFTER `address_to`,
  DROP INDEX `hash`,
  DROP PRIMARY KEY,
  CHANGE `id` `id` BIGINT NULL,
  ADD PRIMARY KEY(`hash`, `transaction_index`);
COMMIT;
