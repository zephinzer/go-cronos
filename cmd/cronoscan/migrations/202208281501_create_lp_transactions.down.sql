START TRANSACTION;
DROP TABLE `lp_transactions`;
ALTER TABLE `croscan_transactions`
  DROP PRIMARY KEY,
  ADD PRIMARY KEY(`contract_address`, `hash`, `address_from`, `address_to`);
COMMIT;
