START TRANSACTION;
ALTER TABLE `croscan_transactions`
  DROP PRIMARY KEY,
  ADD PRIMARY KEY(`hash`, `transaction_index`);
COMMIT;
