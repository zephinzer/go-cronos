CREATE TABLE IF NOT EXISTS `croscan_transactions` (
  id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  block_number BIGINT UNSIGNED,
  contract_address VARCHAR(44) NOT NULL,
  hash VARCHAR(68) NOT NULL,
  address_from VARCHAR(44) NOT NULL,
  address_to VARCHAR(44),
  time_stamp TIMESTAMP,
  token_symbol VARCHAR(12),
  token_decimal INT,
  value FLOAT NOT NULL DEFAULT(-1),
  last_modified TIMESTAMP NOT NULL DEFAULT NOW(),

  PRIMARY KEY(id),
  UNIQUE INDEX(hash)
);
