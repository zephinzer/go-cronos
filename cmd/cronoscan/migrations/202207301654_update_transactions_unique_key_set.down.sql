START TRANSACTION;
ALTER TABLE `croscan_transactions`
  DROP PRIMARY KEY,
  CHANGE `id` `id` BIGINT AUTO_INCREMENT,
  ADD PRIMARY KEY(`id`),
  ADD CONSTRAINT hash_unique UNIQUE(`hash`),
  DROP COLUMN `transaction_index`;
COMMIT;
