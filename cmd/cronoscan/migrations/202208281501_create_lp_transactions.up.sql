START TRANSACTION;
ALTER TABLE `croscan_transactions`
  DROP PRIMARY KEY,
  ADD PRIMARY KEY(`contract_address`, `hash`, `address_from`, `address_to`,`token_symbol`);
CREATE TABLE IF NOT EXISTS `lp_transactions` (
  block_number BIGINT UNSIGNED,
  contract_address VARCHAR(44) NOT NULL,
  hash VARCHAR(68) NOT NULL,
  address_from VARCHAR(44) NOT NULL,
  address_to VARCHAR(44),
  transaction_index INT UNSIGNED NOT NULL,
  time_stamp TIMESTAMP,
  token_symbol VARCHAR(12),
  token_decimal INT,
  value FLOAT NOT NULL DEFAULT(-1),
  last_modified TIMESTAMP NOT NULL DEFAULT NOW(),

  PRIMARY KEY (`contract_address`,`hash`,`address_from`,`address_to`,`token_symbol`)
);
COMMIT;
