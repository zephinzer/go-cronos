package rpc

import (
	"context"
	"fmt"
	"math/big"
	"net/url"
	"strings"

	"github.com/spf13/cobra"
	"github.com/zephinzer/go-cronos/internal/log"
	"github.com/zephinzer/go-cronos/pkg/evmutils"
)

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:   "rpc <rpc url>",
		Short: "Retrieves details of the provided RPC",
		RunE:  runE,
	}
	return &command
}

func runE(command *cobra.Command, args []string) error {
	errors := []string{}
	defer func() {
		if r := recover(); r != nil {
			if err, ok := r.(error); ok {
				log.Errorf("failed to execute successfully: %s", err)
			}
		}
		if len(errors) > 0 {
			log.Errorf("some features may not be available:\n- %s", strings.Join(errors, "\n- "))
		}
	}()
	if len(args) == 0 {
		return fmt.Errorf("failed to receive an rpc url")
	}
	rpcUrl := args[0]
	parsedRpcUrl, urlParseError := url.Parse(rpcUrl)
	if urlParseError != nil {
		return fmt.Errorf("failed to parse rpc url: %s", urlParseError)
	}
	if parsedRpcUrl.Scheme == "" {
		return fmt.Errorf("failed to receive a scheme in rpc[%s]", rpcUrl)
	}
	if parsedRpcUrl.Host == "" {
		return fmt.Errorf("failed to receive a host in rpc[%s]", rpcUrl)
	}

	client, chainId, err := evmutils.NewClient(evmutils.NewClientOpts{
		RpcUrl: rpcUrl,
	})
	if err != nil {
		return fmt.Errorf("failed to create new evm client with rpc[%s]: %s", rpcUrl, err)
	}
	log.Infof("connected to rpc[%s] on chain with id[%v]", rpcUrl, chainId.Int64())

	currentBlockNumber, err := client.BlockNumber(context.Background())
	if err != nil {
		errors = append(errors, fmt.Sprintf("failed to get latest block number: %s", err.Error()))
	}
	log.Infof("current block number: %v", currentBlockNumber)

	blockInstance, err := client.BlockByNumber(context.Background(), big.NewInt(1))
	if err != nil {
		errors = append(errors, fmt.Sprintf("failed to get block[1]: %s", err))
	}
	if blockInstance != nil {
		retrievedBlockNumber := blockInstance.Number().Int64()
		if retrievedBlockNumber == 1 {
			log.Infof("successfully retrieved block[1]")
		} else {
			log.Warnf("retrieved block[%v]", retrievedBlockNumber)
		}
	}

	if len(errors) > 0 {
		return fmt.Errorf("rpc does not support some features:\n- %s", strings.Join(errors, "\n- "))
	}

	return nil
}
