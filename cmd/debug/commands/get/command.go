package get

import (
	"github.com/spf13/cobra"
	"github.com/zephinzer/go-cronos/cmd/debug/commands/get/address"
	"github.com/zephinzer/go-cronos/cmd/debug/commands/get/addresses"
	"github.com/zephinzer/go-cronos/cmd/debug/commands/get/block"
	"github.com/zephinzer/go-cronos/cmd/debug/commands/get/chains"
	"github.com/zephinzer/go-cronos/cmd/debug/commands/get/erc20"
	"github.com/zephinzer/go-cronos/cmd/debug/commands/get/erc721"
	"github.com/zephinzer/go-cronos/cmd/debug/commands/get/erc721collection"
	"github.com/zephinzer/go-cronos/cmd/debug/commands/get/logs"
	"github.com/zephinzer/go-cronos/cmd/debug/commands/get/rpc"
	"github.com/zephinzer/go-cronos/cmd/debug/commands/get/rpcs"
	"github.com/zephinzer/go-cronos/cmd/debug/commands/get/transaction"
)

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:  "get",
		RunE: runE,
	}
	command.AddCommand(address.GetCommand())
	command.AddCommand(addresses.GetCommand())
	command.AddCommand(block.GetCommand())
	command.AddCommand(chains.GetCommand())
	command.AddCommand(erc20.GetCommand())
	command.AddCommand(erc721.GetCommand())
	command.AddCommand(erc721collection.GetCommand())
	command.AddCommand(logs.GetCommand())
	command.AddCommand(rpc.GetCommand())
	command.AddCommand(rpcs.GetCommand())
	command.AddCommand(transaction.GetCommand())
	return &command
}

func runE(command *cobra.Command, args []string) error {
	return command.Help()
}
