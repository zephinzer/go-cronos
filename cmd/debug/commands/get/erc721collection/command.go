package erc721collection

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math/big"
	"net/http"
	"os"
	"path"
	"strings"
	"sync"
	"time"

	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/spf13/cobra"
	"github.com/usvc/go-config"
	"github.com/zephinzer/go-cronos/internal/log"
	"github.com/zephinzer/go-cronos/pkg/addressbook"
	"github.com/zephinzer/go-cronos/pkg/erc721"
)

const (
	ConfigKeyChain       = "chain"
	ConfigKeyOutput      = "output"
	ConfigKeyRpcUrl      = "rpc-url"
	ConfigKeyTotalSupply = "total-supply"
)

var conf = config.Map{
	ConfigKeyChain: &config.String{
		Shorthand: "c",
		Usage:     fmt.Sprintf("slug of chain (available options: [%s])", strings.Join(addressbook.AvailableChains, ", ")),
		Default:   "cronos",
	},
	ConfigKeyOutput: &config.String{
		Shorthand: "o",
		Usage:     "defines path to the output file",
		Default:   "./",
	},
	ConfigKeyRpcUrl: &config.String{
		Shorthand: "u",
		Usage:     "specifies the RPC URL",
		Default:   "https://rpc.vvs.finance/",
	},
	ConfigKeyTotalSupply: &config.Int{
		Shorthand: "s",
		Usage:     "override the output of calling the contract's totalSupply() (useful for contracts which randomise the mint ID)",
	},
}

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:   "erc721collection <collection-identifier>",
		Short: "Retrieves details about the ERC721 token at the provided contract address",
		RunE:  runE,
	}
	conf.ApplyToCobra(&command)
	return &command
}

func runE(command *cobra.Command, args []string) error {
	client, err := ethclient.Dial(conf.GetString("rpc-url"))
	if err != nil {
		panic(err)
	}
	defer client.Close()
	var collectionIdentifier string
	chainIdentifier := conf.GetString(ConfigKeyChain)
	if len(args) > 0 {
		collectionIdentifier = args[0]
	} else {
		return fmt.Errorf("failed to receive <token-identifier>")
	}
	contractAddress, err := addressbook.GetAddress(chainIdentifier, collectionIdentifier)
	if err != nil {
		if common.IsHexAddress(collectionIdentifier) {
			collectionAddress := common.HexToAddress(collectionIdentifier)
			contractAddress = &collectionAddress
		} else {
			return fmt.Errorf("failed to get address of token[%s] on chain[%s]: %s", collectionIdentifier, chainIdentifier, err)
		}
	}

	boundContract := bind.NewBoundContract(
		*contractAddress,
		erc721.Contract,
		client,
		nil,
		nil,
	)

	var results []interface{}
	if err := boundContract.Call(&bind.CallOpts{}, &results, "name"); err != nil {
		return fmt.Errorf("failed to call name(): %s", err)
	}
	name := results[0].(string)

	results = []interface{}{}
	if err := boundContract.Call(&bind.CallOpts{}, &results, "symbol"); err != nil {
		return fmt.Errorf("failed to call symbol(): %s", err)
	}
	symbol := results[0].(string)

	results = []interface{}{}
	if err := boundContract.Call(&bind.CallOpts{}, &results, "totalSupply"); err != nil {
		return fmt.Errorf("failed to call totalSupply(): %s", err)
	}
	totalSupply := results[0].(*big.Int)
	configuredTotalSupply := conf.GetInt("total-supply")
	if configuredTotalSupply > 0 {
		totalSupply = big.NewInt(int64(configuredTotalSupply))
	}

	data := map[string]interface{}{
		"address":     contractAddress.String(),
		"name":        name,
		"symbol":      symbol,
		"totalSupply": totalSupply.Int64(),
		"tokens":      []interface{}{},
	}

	var waiter sync.WaitGroup
	waiter.Add(1)

	var fullCollectionMetadata []interface{}
	done := make(chan bool)
	tokenDataCollector := make(chan map[string]interface{}, totalSupply.Int64())
	fullErrors := []error{}
	errors := make(chan error, totalSupply.Int64())
	go func() {
		log.Infof("starting collation process...")
		for {
			if len(fullCollectionMetadata)+len(fullErrors) >= int(totalSupply.Int64()) {
				waiter.Done()
				return
			}
			select {
			case err := <-errors:
				log.Warnf("failed to process token: %s", err)
				fullErrors = append(fullErrors, err)
			case isDone := <-done:
				if isDone {
					break
				}
			case tokenData := <-tokenDataCollector:
				fullCollectionMetadata = append(fullCollectionMetadata, tokenData)
			}
		}
	}()

	for i := int64(1); i <= totalSupply.Int64(); i++ {
		go getTokenUri(i, boundContract, tokenDataCollector, errors)
		<-time.After(300 * time.Millisecond)
	}
	waiter.Wait()
	close(errors)
	close(tokenDataCollector)

	data["tokens"] = fullCollectionMetadata

	dataAsJson, err := json.MarshalIndent(data, "", "  ")
	if err != nil {
		return fmt.Errorf("failed to marshal data as json: %s", err)
	}

	filename := "./" + contractAddress.Hex() + ".json"
	filename = path.Join(conf.GetString(ConfigKeyOutput), filename)

	if err := ioutil.WriteFile(filename, dataAsJson, os.ModePerm); err != nil {
		return fmt.Errorf("failed to write results to '%s': %s", filename, err)
	}

	return nil
}

func getTokenUri(tokenId int64, contract *bind.BoundContract, tokenDataCollector chan map[string]interface{}, errors chan error) {
	log.Infof("processing token[%v]...", tokenId)
	results := []interface{}{}
	if err := contract.Call(&bind.CallOpts{}, &results, "tokenURI", big.NewInt(tokenId)); err != nil {
		errorInstance := fmt.Errorf("failed to call tokenURI(%s): %s", big.NewInt(tokenId).String(), err)
		log.Warn(errorInstance)
		if !strings.Contains(err.Error(), "nonexistent token") {
			<-time.After(500 * time.Millisecond)
			getTokenUri(tokenId, contract, tokenDataCollector, errors)
		} else {
			errors <- errorInstance
		}
		return
	}
	tokenUri := results[0].(string)
	if strings.Index(tokenUri, "ipfs://") == 0 {
		tokenUri = strings.Replace(tokenUri, "ipfs://", "https://ipfs.io/ipfs/", 1)
	}
	req, err := http.NewRequest(http.MethodGet, tokenUri, nil)
	if err != nil {
		errors <- fmt.Errorf("failed to get metadata for token[%v]: %s", tokenId, err)
		return
	}
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		errors <- fmt.Errorf("failed to perform http request for token[%v]: %s", tokenId, err)
		return
	}
	resBody, err := ioutil.ReadAll(res.Body)
	if err != nil {
		errors <- fmt.Errorf("failed to read response body for token[%v]: %s", tokenId, err)
		return
	}
	var metadata map[string]interface{}
	if err := json.Unmarshal(resBody, &metadata); err != nil {
		errors <- fmt.Errorf("metadata is not in json for token[%v]: %s", tokenId, err)
		return
	}
	tokenDataCollector <- metadata
	log.Infof("processed token[%v]", tokenId)
}
