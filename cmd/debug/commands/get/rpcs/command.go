package rpcs

import (
	"encoding/json"
	"fmt"
	"strings"

	"github.com/spf13/cobra"
	"github.com/usvc/go-config"
	"github.com/zephinzer/go-cronos/pkg/addressbook"
)

var conf = config.Map{
	"chain": &config.String{
		Shorthand: "c",
		Usage:     fmt.Sprintf("slug of target chain (available options: [%s])", strings.Join(addressbook.AvailableChains, ", ")),
		Default:   "cronos",
	},
}

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:   "rpcs",
		Short: "Retrieves all rpcs for the provided chain",
		RunE:  runE,
	}
	conf.ApplyToCobra(&command)
	return &command
}

func runE(command *cobra.Command, args []string) error {
	selectedChainSlug := conf.GetString("chain")
	selectedChain, ok := addressbook.Chain[selectedChainSlug]
	if !ok {
		return fmt.Errorf("failed to find chain[%s]", selectedChainSlug)
	}
	if selectedChain.RPC == nil {
		return fmt.Errorf("failed to find rpcs for chain[%s] ('%s')", selectedChainSlug, selectedChain.Label)
	}
	x, _ := json.MarshalIndent(selectedChain.RPC, "", "  ")
	fmt.Println(string(x))
	return nil
}
