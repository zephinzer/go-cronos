package erc20

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strings"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/olekukonko/tablewriter"
	"github.com/spf13/cobra"
	"github.com/usvc/go-config"
	"github.com/zephinzer/go-cronos/internal/log"
	"github.com/zephinzer/go-cronos/pkg/addressbook"
	"github.com/zephinzer/go-cronos/pkg/erc20"
)

const (
	ConfigKeyChain  = "chain"
	ConfigKeyFormat = "format"
	ConfigKeyRpcUrl = "rpc-url"
)

var conf = config.Map{
	ConfigKeyChain: &config.String{
		Shorthand: "c",
		Usage:     fmt.Sprintf("slug of chain (available options: [%s])", strings.Join(addressbook.AvailableChains, ", ")),
		Default:   "cronos",
	},
	ConfigKeyFormat: &config.String{
		Shorthand: "f",
		Usage:     "one of [text, json]",
		Default:   "json",
	},
	ConfigKeyRpcUrl: &config.String{
		Shorthand: "u",
		Usage:     "specifies the RPC URL",
		Default:   "https://rpc.vvs.finance/",
	},
}

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:   "erc20 <token-identifier>",
		Short: "Retrieves details about the ERC20 token at the provided contract address",
		RunE:  runE,
	}
	conf.ApplyToCobra(&command)
	return &command
}

func runE(command *cobra.Command, args []string) error {
	client, err := ethclient.Dial(conf.GetString(ConfigKeyRpcUrl))
	if err != nil {
		return fmt.Errorf("failed to connect to rpc: %s", err)
	}
	defer client.Close()

	var tokenIdentifier string
	chainIdentifier := conf.GetString(ConfigKeyChain)
	if len(args) > 0 {
		tokenIdentifier = args[0]
	} else {
		return fmt.Errorf("failed to receive <token-identifier>")
	}
	contractAddress, err := addressbook.GetAddress(chainIdentifier, tokenIdentifier)
	if err != nil {
		if common.IsHexAddress(tokenIdentifier) {
			collectionAddress := common.HexToAddress(tokenIdentifier)
			contractAddress = &collectionAddress
		} else {
			return fmt.Errorf("failed to get address of token[%s] on chain[%s]: %s", tokenIdentifier, chainIdentifier, err)
		}
	}

	events := make(chan erc20.GetInfoEvent, 10)
	defer close(events)
	go func() {
		for {
			select {
			case event := <-events:
				if event.IsError {
					log.Warn(event.Message)
				} else {
					log.Info(event.Message)
				}
				continue
			default:
			}
		}
	}()
	tokenInfo, err := erc20.GetInfo(erc20.GetInfoOpts{
		Address: *contractAddress,
		// NOTE: enable this for logging/debugging
		// Events:  events,
		RpcUrl: conf.GetString(ConfigKeyRpcUrl),
	})
	if err != nil {
		return fmt.Errorf("failed to get erc20 token information: %s", err)
	}

	var outputData bytes.Buffer

	data := map[string]interface{}{
		"address":       tokenInfo.Address.String(),
		"decimals":      tokenInfo.Decimals,
		"name":          tokenInfo.Name,
		"owner":         *tokenInfo.Owner,
		"preMineSupply": *tokenInfo.PreMineSupply,
		"symbol":        tokenInfo.Symbol,
		"totalSupply":   tokenInfo.TotalSupply,
	}
	switch conf.GetString(ConfigKeyFormat) {
	case "json":
		dataAsJson, err := json.MarshalIndent(data, "", "  ")
		if err != nil {
			panic(err)
		}
		outputData = *bytes.NewBuffer(dataAsJson)
	case "text":
		fallthrough
	default:
		table := tablewriter.NewWriter(&outputData)
		table.SetAlignment(tablewriter.ALIGN_LEFT)
		table.SetHeader([]string{"property", "value"})
		table.Render()
	}

	fmt.Println(outputData.String())

	return nil
}
