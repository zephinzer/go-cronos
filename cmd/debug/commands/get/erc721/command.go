package erc721

import (
	"bytes"
	"encoding/json"
	"fmt"
	"math/big"
	"strings"

	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/olekukonko/tablewriter"
	"github.com/spf13/cobra"
	"github.com/usvc/go-config"
	"github.com/zephinzer/go-cronos/pkg/addressbook"
	"github.com/zephinzer/go-cronos/pkg/erc721"
)

const (
	ConfigKeyChain   = "chain"
	ConfigKeyFormat  = "format"
	ConfigKeyRpcUrl  = "rpc-url"
	ConfigKeyTokenId = "token-id"
)

var conf = config.Map{
	ConfigKeyChain: &config.String{
		Shorthand: "c",
		Usage:     fmt.Sprintf("slug of chain (available options: [%s])", strings.Join(addressbook.AvailableChains, ", ")),
		Default:   "cronos",
	},
	ConfigKeyTokenId: &config.Int{
		Shorthand: "I",
		Usage:     "specifies a token id",
		Default:   1,
	},
	ConfigKeyFormat: &config.String{
		Shorthand: "f",
		Usage:     "one of [text, json]",
		Default:   "json",
	},
	ConfigKeyRpcUrl: &config.String{
		Shorthand: "u",
		Usage:     "specifies the RPC URL",
		Default:   "https://rpc.vvs.finance/",
	},
}

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:   "erc721 <collection-identifier>",
		Short: "Retrieves details about the ERC721 token at the provided contract address",
		RunE:  runE,
	}
	conf.ApplyToCobra(&command)
	return &command
}

func runE(command *cobra.Command, args []string) error {
	client, err := ethclient.Dial(conf.GetString(ConfigKeyRpcUrl))
	if err != nil {
		panic(err)
	}
	defer client.Close()

	var collectionIdentifier string
	chainIdentifier := conf.GetString(ConfigKeyChain)
	if len(args) > 0 {
		collectionIdentifier = args[0]
	} else {
		return fmt.Errorf("failed to receive <token-identifier>")
	}
	contractAddress, err := addressbook.GetAddress(chainIdentifier, collectionIdentifier)
	if err != nil {
		if common.IsHexAddress(collectionIdentifier) {
			collectionAddress := common.HexToAddress(collectionIdentifier)
			contractAddress = &collectionAddress
		} else {
			return fmt.Errorf("failed to get address of token[%s] on chain[%s]: %s", collectionIdentifier, chainIdentifier, err)
		}
	}

	boundContract := bind.NewBoundContract(
		*contractAddress,
		erc721.Contract,
		client,
		nil,
		nil,
	)
	tokenId := conf.GetInt(ConfigKeyTokenId)

	var results []interface{}
	if err := boundContract.Call(&bind.CallOpts{}, &results, "name"); err != nil {
		return fmt.Errorf("failed to call name(): %s", err)
	}
	name := results[0].(string)

	results = []interface{}{}
	if err := boundContract.Call(&bind.CallOpts{}, &results, "symbol"); err != nil {
		return fmt.Errorf("failed to call symbol(): %s", err)
	}
	symbol := results[0].(string)

	results = []interface{}{}
	if err := boundContract.Call(&bind.CallOpts{}, &results, "totalSupply"); err != nil {
		return fmt.Errorf("failed to call totalSupply(): %s", err)
	}
	totalSupply := results[0].(*big.Int)

	var tokenUri string
	if tokenId != 0 {
		tokenIdParam := big.NewInt(int64(tokenId))
		results = []interface{}{}
		if err := boundContract.Call(&bind.CallOpts{}, &results, "tokenURI", tokenIdParam); err != nil {
			return fmt.Errorf("failed to call tokenURI(): %s", err)
		}
		tokenUri = results[0].(string)
	}

	var outputData bytes.Buffer

	data := map[string]interface{}{
		"address":     contractAddress.String(),
		"name":        name,
		"symbol":      symbol,
		"tokenUri":    tokenUri,
		"totalSupply": totalSupply.Int64(),
	}
	switch conf.GetString("format") {
	case "json":
		dataAsJson, err := json.MarshalIndent(data, "", "  ")
		if err != nil {
			panic(err)
		}
		outputData = *bytes.NewBuffer(dataAsJson)
	case "text":
		fallthrough
	default:
		table := tablewriter.NewWriter(&outputData)
		table.SetAlignment(tablewriter.ALIGN_LEFT)
		table.SetHeader([]string{"property", "value"})
		table.Render()
	}

	fmt.Println(outputData.String())

	return nil
}
