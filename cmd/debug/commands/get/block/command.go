package block

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"math/big"
	"strconv"
	"strings"
	"time"

	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/olekukonko/tablewriter"
	"github.com/spf13/cobra"
	"github.com/usvc/go-config"
)

var conf = config.Map{
	"format": &config.String{
		Shorthand: "f",
		Usage:     "one of [text, json]",
		Default:   "text",
	},
	"rpc-url": &config.String{
		Shorthand: "u",
		Usage:     "specifies the RPC URL",
		Default:   "https://rpc.vvs.finance/",
	},
}

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:   "block <block-number>",
		Short: "Retrieves transactions from the latest block, if no block number is specified, the block number is returned",
		RunE:  runE,
	}
	conf.ApplyToCobra(&command)
	return &command
}

func runE(command *cobra.Command, args []string) error {
	client, err := ethclient.Dial(conf.GetString("rpc-url"))
	if err != nil {
		panic(err)
	}
	defer client.Close()
	var blockNumber uint64
	if len(args) == 0 {
		blockNumber, err = client.BlockNumber(context.Background())
		if blockNumber > 0 {
			blockNumber -= 1
		}
	} else {
		blockNumber, err = strconv.ParseUint(args[0], 10, 64)
	}
	if err != nil {
		panic(err)
	}
	blockInstance, err := client.BlockByNumber(context.Background(), big.NewInt(int64(blockNumber)))
	if err != nil {
		panic(err)
	}
	transactions := blockInstance.Transactions()
	transactionHashes := []string{}
	for _, transaction := range transactions {
		transactionHashes = append(transactionHashes, transaction.Hash().Hex())
	}

	var outputData bytes.Buffer

	data := map[string]interface{}{
		"blockNumber":       blockInstance.Number().Int64(),
		"blockHash":         blockInstance.Hash().Hex(),
		"parentHash":        blockInstance.ParentHash().Hex(),
		"uncleHash":         blockInstance.UncleHash().Hex(),
		"miner":             blockInstance.Coinbase().Hex(),
		"nonce":             blockInstance.Nonce(),
		"transactionsCount": len(blockInstance.Transactions()),
		"blockSizeBytes":    blockInstance.Size(),
		"processedAt":       time.Unix(int64(blockInstance.Time()), 0).Format(time.RFC1123),
		"difficulty":        blockInstance.Difficulty(),
		"baseFee":           blockInstance.BaseFee(),
		"gasLimit":          blockInstance.GasLimit(),
		"gasUsed":           blockInstance.GasUsed(),
		"transactions":      transactionHashes,
	}
	switch conf.GetString("format") {
	case "json":
		dataAsJson, err := json.MarshalIndent(data, "", "  ")
		if err != nil {
			panic(err)
		}
		outputData = *bytes.NewBuffer(dataAsJson)
	case "text":
		fallthrough
	default:
		table := tablewriter.NewWriter(&outputData)
		table.SetAlignment(tablewriter.ALIGN_LEFT)
		table.SetHeader([]string{"property", "value"})
		table.Append([]string{"block number", blockInstance.Number().String()})
		table.Append([]string{"block hash", blockInstance.Hash().Hex()})
		table.Append([]string{"transactions count", strconv.Itoa(len(blockInstance.Transactions()))})
		table.Append([]string{"block size", blockInstance.Size().TerminalString()})
		table.Append([]string{"processed at", time.Unix(int64(blockInstance.Time()), 0).Format(time.RFC1123)})
		table.Append([]string{"difficulty", blockInstance.Difficulty().String()})
		table.Append([]string{"base fee", blockInstance.BaseFee().String()})
		table.Append([]string{"gas limit", strconv.FormatUint(blockInstance.GasLimit(), 10)})
		table.Append([]string{"gas used", strconv.FormatUint(blockInstance.GasUsed(), 10)})
		table.Append([]string{"transaction hashes", strings.Join(transactionHashes, ", ")})
		table.Render()
	}

	fmt.Println(outputData.String())

	return nil
}
