package addresses

import (
	"encoding/json"
	"fmt"
	"strings"

	"github.com/spf13/cobra"
	"github.com/usvc/go-config"
	"github.com/zephinzer/go-cronos/internal/log"
	"github.com/zephinzer/go-cronos/pkg/addressbook"
	"github.com/zephinzer/go-cronos/pkg/evmtypes"
)

const (
	ConfigKeyChain = "chain"
)

var conf = config.Map{
	ConfigKeyChain: &config.String{
		Shorthand: "c",
		Usage:     fmt.Sprintf("slug of chain (available options: [%s])", strings.Join(addressbook.AvailableChains, ", ")),
		Default:   "cronos",
	},
}

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:     "addresses",
		Aliases: []string{"address", "add"},
		RunE:    runE,
	}
	conf.ApplyToCobra(&command)
	return &command
}

func runE(command *cobra.Command, args []string) error {
	selectedChainSlug := conf.GetString(ConfigKeyChain)
	selectedChain := addressbook.Chain[selectedChainSlug]
	addresses := []*evmtypes.AddressDirectoryEntry{}
	if selectedChain == nil {
		log.Warnf("failed to find chain[%s], retrieving addresses from all chains", selectedChainSlug)
		for _, chainInstance := range addressbook.Chain {
			for _, directoryEntry := range chainInstance.Directory {
				addresses = append(addresses, directoryEntry)
			}
		}
	} else {
		for _, directoryEntry := range selectedChain.Directory {
			addresses = append(addresses, directoryEntry)
		}
	}
	x, _ := json.MarshalIndent(addresses, "", "  ")
	fmt.Println(string(x))
	return nil
}
