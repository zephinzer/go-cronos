package logs

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"math/big"

	"github.com/ethereum/go-ethereum/common"
	"github.com/olekukonko/tablewriter"
	"github.com/spf13/cobra"
	"github.com/usvc/go-config"
	"github.com/zephinzer/go-cronos/pkg/addressbook"
	"github.com/zephinzer/go-cronos/pkg/cronos"
	"github.com/zephinzer/go-cronos/pkg/evmutils"
)

var conf = config.Map{
	"format": &config.String{
		Shorthand: "f",
		Usage:     "one of [text, json]",
		Default:   "text",
	},
	"rpc-url": &config.String{
		Shorthand: "u",
		Usage:     "specifies the RPC URL",
		Default:   "https://rpc.vvs.finance/",
	},
}

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:  "logs <transaction-hash>",
		RunE: runE,
	}
	conf.ApplyToCobra(&command)
	return &command
}

func runE(command *cobra.Command, args []string) error {
	client, chainId, err := evmutils.NewClient(evmutils.NewClientOpts{
		RpcUrl: conf.GetString("rpc-url"),
	})
	if err != nil {
		panic(err)
	}
	defer client.Close()

	directory := addressbook.ChainById[int(chainId.Int64())].Directory

	if len(args) == 0 {
		return fmt.Errorf("you need to pass in a transaction hash")
	}
	transactionHash := common.HexToHash(args[0])

	receipt, err := client.TransactionReceipt(context.Background(), transactionHash)
	if err != nil {
		panic(err)
	}
	logs := receipt.Logs

	data := []map[string]interface{}{}
	for index, txLog := range logs {
		topics := []string{}
		data = append(data, map[string]interface{}{
			"address": txLog.Address.String(),
			"block":   txLog.BlockNumber,
			"data":    nil,
		})
		for _, topic := range txLog.Topics {
			topics = append(topics, topic.Hex())
		}
		data[index]["topics"] = topics
		if txLog.Data != nil && len(txLog.Data) > 0 {
			data[index]["data"] = common.Bytes2Hex(txLog.Data)
		}
	}

	var outputData bytes.Buffer

	switch conf.GetString("format") {
	case "json":
		outputJson, err := json.MarshalIndent(data, "", "  ")
		if err != nil {
			return fmt.Errorf("failed to marshal data: %s", err)
		}
		fmt.Println(string(outputJson))
	case "text":
		fallthrough
	default:
		table := tablewriter.NewWriter(&outputData)
		table.SetHeader([]string{"property", "value"})
		table.SetAlignment(tablewriter.ALIGN_LEFT)
		for _, logEntry := range data {
			address := logEntry["address"].(string)
			if addressEntry, exists := directory[address]; exists {
				address = addressEntry.Label
			}
			topics := logEntry["topics"].([]string)
			methodHash := topics[0]
			translation := "N/A"
			if knownFunction, exists := cronos.TopicDirectory[methodHash]; exists {
				args := []any{address}
				for index, topic := range topics[1:] {
					switch knownFunction.ParamTypes[index] {
					case "address":
						value := common.HexToAddress(topic).String()
						if addressEntry, exists := directory[value]; exists {
							value = addressEntry.Label
						}
						args = append(args, value)
					case "uint256":
						value := big.NewInt(0).SetBytes(common.Hex2Bytes(string(topic[2:]))).String()
						args = append(args, value)
					default:
						args = append(args, topic)
					}
				}
				translation = fmt.Sprintf(knownFunction.Format, args...)
			}
			table.Append([]string{"english", translation})
		}
		table.Render()
	}

	fmt.Println(outputData.String())

	for hash, function := range cronos.TopicDirectory {
		fmt.Printf("%s: %s\n", hash, function.Method)
	}

	return nil
}
