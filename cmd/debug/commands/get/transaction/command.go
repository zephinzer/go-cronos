package transaction

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"math/big"
	"strconv"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/common/math"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/olekukonko/tablewriter"
	"github.com/spf13/cobra"
	"github.com/usvc/go-config"
)

var conf = config.Map{
	"format": &config.String{
		Shorthand: "f",
		Usage:     "one of [text, json]",
		Default:   "text",
	},
	"rpc-url": &config.String{
		Shorthand: "u",
		Usage:     "specifies the RPC URL",
		Default:   "https://rpc.vvs.finance/",
	},
}

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:  "transaction <transaction-hash>",
		RunE: runE,
	}
	conf.ApplyToCobra(&command)
	return &command
}

func runE(command *cobra.Command, args []string) error {
	client, err := ethclient.Dial(conf.GetString("rpc-url"))
	if err != nil {
		panic(err)
	}
	defer client.Close()
	if len(args) == 0 {
		return fmt.Errorf("you need to pass in a transaction hash")
	}
	transactionHash := common.HexToHash(args[0])

	var transaction *types.Transaction
	var isPending bool = true
	var getTxErr error

	for isPending {
		transaction, isPending, getTxErr = client.TransactionByHash(context.Background(), transactionHash)
		if getTxErr != nil {
			panic(getTxErr)
		}
	}

	chainId, err := client.ChainID(context.Background())
	if err != nil {
		panic(err)
	}

	var transactionFrom common.Address
	if message, err := transaction.AsMessage(types.NewLondonSigner(chainId), nil); err == nil {
		transactionFrom = message.From()
	}
	receipt, err := client.TransactionReceipt(context.Background(), transactionHash)
	if err != nil {
		panic(err)
	}

	logs := receipt.Logs

	costCro, _ := big.NewFloat(0).Quo(big.NewFloat(0).SetInt(transaction.Cost()), big.NewFloat(0).SetInt(math.BigPow(10, 18))).Float64()
	valueCro, _ := big.NewFloat(0).Quo(big.NewFloat(0).SetInt(transaction.Value()), big.NewFloat(0).SetInt(math.BigPow(10, 18))).Float64()

	data := map[string]interface{}{
		"blockNumber": receipt.BlockNumber,
		"blockHash":   receipt.BlockHash,
		"url":         fmt.Sprintf("https://cronoscan.com/tx/%s", transaction.Hash().String()),
		"cost":        costCro,
		"from":        transactionFrom.Hex(),
		"hash":        transaction.Hash(),
		"valueCro":    valueCro,
		"to":          transaction.To().Hex(),
		"gas":         transaction.Gas(),
		"gasFeeCap":   transaction.GasFeeCap(),
		"gasPrice":    transaction.GasPrice(),
		"gasTipCap":   transaction.GasTipCap(),
		"type":        transaction.Type(),
		"nonce":       transaction.Nonce(),
		"method":      common.Bytes2Hex(transaction.Data())[:8],
		"data":        common.Bytes2Hex(transaction.Data())[8:],
		"logsCount":   len(logs),
	}

	var outputData bytes.Buffer

	switch conf.GetString("format") {
	case "json":
		outputJson, err := json.MarshalIndent(data, "", "  ")
		if err != nil {
			return fmt.Errorf("failed to marshal data: %s", err)
		}
		fmt.Println(string(outputJson))
	case "text":
		fallthrough
	default:
		table := tablewriter.NewWriter(&outputData)
		table.SetHeader([]string{"property", "value"})
		table.SetAlignment(tablewriter.ALIGN_LEFT)
		table.Append([]string{"url", fmt.Sprintf("https://cronoscan.com/tx/%s", transaction.Hash().String())})
		table.Append([]string{"cost", transaction.Cost().String()})
		table.Append([]string{"from", transactionFrom.Hex()})
		table.Append([]string{"to", transaction.To().Hex()})
		table.Append([]string{"gas", strconv.FormatUint(transaction.Gas(), 10)})
		table.Append([]string{"nonce", strconv.FormatUint(transaction.Nonce(), 10)})
		table.Append([]string{"method", common.Bytes2Hex(transaction.Data())[:8]})
		table.Append([]string{"data", common.Bytes2Hex(transaction.Data())[8:]})
		table.Append([]string{"internal txns count", strconv.Itoa(len(logs))})

		for index, txLog := range logs {
			table.Append([]string{fmt.Sprintf("internal txn [%v]", index), txLog.Address.Hex() + "=>" + common.Bytes2Hex(txLog.Data)})
			for topicIndex, topic := range txLog.Topics {
				table.Append([]string{fmt.Sprintf("internal txn [%v] topic [%v]", index, topicIndex), topic.String()})
			}
		}
		table.Render()
	}

	fmt.Println(outputData.String())

	return nil
}
