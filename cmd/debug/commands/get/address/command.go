package address

import (
	"encoding/json"
	"fmt"
	"strings"

	"github.com/spf13/cobra"
	"github.com/usvc/go-config"
	"github.com/zephinzer/go-cronos/pkg/addressbook"
)

const (
	ConfigKeyChain = "chain"
)

var conf = config.Map{
	ConfigKeyChain: &config.String{
		Shorthand: "c",
		Usage:     fmt.Sprintf("slug of chain (available options: [%s])", strings.Join(addressbook.AvailableChains, ", ")),
		Default:   "cronos",
	},
}

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:   "address <target-address>",
		Short: "Retrieves an address",
		RunE:  runE,
	}
	conf.ApplyToCobra(&command)
	return &command
}

func runE(command *cobra.Command, args []string) error {
	if len(args) < 1 {
		return fmt.Errorf("failed to find expected <target-address>")
	}
	selectedAddress := args[0]
	selectedChainSlug := conf.GetString(ConfigKeyChain)
	selectedChain, ok := addressbook.Chain[selectedChainSlug]
	if !ok {
		return fmt.Errorf("failed to find chain[%s]", selectedChainSlug)
	}
	if selectedChain.Directory == nil {
		return fmt.Errorf("failed to find directory for chain[%s] ('%s')", selectedChainSlug, selectedChain.Label)
	}
	directoryEntry, ok := selectedChain.Directory[selectedAddress]
	if !ok {
		return fmt.Errorf("failed to find directory entry for chain[%s] and address[%s]", selectedChainSlug, selectedAddress)
	}
	primaryDirectoryEntry := selectedChain.Directory[directoryEntry.Address]
	x, _ := json.MarshalIndent(primaryDirectoryEntry, "", "  ")
	fmt.Println(string(x))
	return nil
}
