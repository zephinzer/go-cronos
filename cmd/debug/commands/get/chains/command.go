package chains

import (
	"encoding/json"
	"fmt"

	"github.com/spf13/cobra"
	"github.com/zephinzer/go-cronos/pkg/addressbook"
)

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:   "chains",
		Short: "Retrieves all known chains",
		RunE:  runE,
	}
	return &command
}

func runE(command *cobra.Command, args []string) error {
	chainsData := addressbook.Chain
	for key, _ := range chainsData {
		chainsData[key].Directory = nil
		chainsData[key].RPC = nil
	}
	x, _ := json.MarshalIndent(chainsData, "", "  ")
	fmt.Println(string(x))
	return nil
}
