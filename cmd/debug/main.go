package main

import (
	"github.com/spf13/cobra"
	"github.com/zephinzer/go-cronos/cmd/debug/commands/get"
)

func main() {
	GetCommand().Execute()
}

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:  "debug",
		RunE: runE,
	}
	command.AddCommand(get.GetCommand())
	return &command
}

func runE(command *cobra.Command, args []string) error {
	command.Help()
	return nil
}
