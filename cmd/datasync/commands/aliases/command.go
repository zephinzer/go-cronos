package aliases

import (
	"encoding/json"
	"fmt"
	"strings"

	"github.com/spf13/cobra"
	"github.com/zephinzer/go-cronos/cmd/datasync/common/datasync"
	"github.com/zephinzer/go-cronos/internal/log"
	"github.com/zephinzer/go-cronos/pkg/airtable"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:  "aliases",
		RunE: runE,
	}
	conf.ApplyToCobra(&command)
	return &command
}

func runE(command *cobra.Command, args []string) error {
	done := false
	offset := ""
	aliasesInstance := []datasync.Aliases{}

	for !done {
		data := airtable.GetData(airtable.GetDataOpts{
			ApiKey:  conf.GetString(ConfigKeyAirtableApiKey),
			BaseId:  conf.GetString(ConfigKeyAirtableBaseId),
			Offset:  offset,
			TableId: conf.GetString(ConfigKeyAirtableTableId),
		})
		if data.Error != nil {
			return fmt.Errorf("failed to get data from airtable: %s", data.Error)
		}
		for _, aliasInstance := range data.Records {
			airtableAliasInstance, err := datasync.NewAliasesFromAirtableRecord(aliasInstance)
			if err != nil {
				log.Warnf("failed to convert airtable record into a datasync.Aliases: %s", err)
			} else {
				aliasesInstance = append(aliasesInstance, airtableAliasInstance.Fields)
			}
		}
		if data.Offset == nil {
			done = true
		} else {
			offset = *data.Offset
		}
	}

	if conf.GetString(ConfigKeyOutput) == "stdout" {
		x, _ := json.MarshalIndent(aliasesInstance, "", "  ")
		fmt.Println(string(x))
		return nil
	}

	dbUsername := conf.GetString(ConfigKeyDatabaseUsername)
	dbPassword := conf.GetString(ConfigKeyDatabasePassword)
	dbHost := conf.GetString(ConfigKeyDatabaseHost)
	dbPort := conf.GetUint(ConfigKeyDatabasePort)
	dbName := conf.GetString(ConfigKeyDatabaseName)
	log.Infof("connecting to database[%s:%v/%s] with user[%s]...", dbHost, dbPort, dbName, dbUsername)
	db, err := gorm.Open(mysql.New(mysql.Config{
		DSN: fmt.Sprintf(
			"%s:%s@tcp(%s:%v)/%s?charset=utf8mb4&parseTime=True",
			dbUsername,
			dbPassword,
			dbHost,
			dbPort,
			dbName,
		),
	}))
	if err != nil {
		return fmt.Errorf(
			"failed to open a db connection to %s@%s:%v/%s via gorm: %s",
			dbUsername,
			dbHost,
			dbPort,
			dbName,
			err,
		)
	}

	log.Infof("inserting %v aliases...", len(aliasesInstance))
	errors := []string{}
	for index, aliasInstance := range aliasesInstance {
		log.Infof("processing alias[%v]...", index)
		aliasData := aliasInstance
		gormTx := db.Clauses(clause.OnConflict{
			Columns:   []clause.Column{{Name: "address"}},
			DoUpdates: clause.AssignmentColumns([]string{"label", "chain", "type", "description", "updated_at"}),
		}).Create(&aliasData)

		if gormTx.Error != nil {
			log.Warnf("failed to insert alias[%s] from chain[%s] (label[%s])", aliasData.Address, aliasData.Chain, aliasData.Label)
			errors = append(errors, fmt.Sprintf("failed to insert alias[%s]: %s", aliasData.Address, gormTx.Error))
		}
	}

	if len(errors) > 0 {
		return fmt.Errorf("failed to insert all aliases: ['%s']", strings.Join(errors, "', '"))
	}

	return nil
}
