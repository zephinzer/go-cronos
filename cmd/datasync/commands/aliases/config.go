package aliases

import (
	"fmt"
	"strings"

	"github.com/usvc/go-config"
	"github.com/zephinzer/go-cronos/pkg/addressbook"
)

const (
	ConfigKeyAirtableApiKey  = "airtable-api-key"
	ConfigKeyAirtableBaseId  = "airtable-base-id"
	ConfigKeyAirtableTableId = "airtable-table-id"

	ConfigKeyChain = "chain"

	ConfigKeyDatabaseUsername = "db-username"
	ConfigKeyDatabasePassword = "db-password"
	ConfigKeyDatabaseHost     = "db-host"
	ConfigKeyDatabasePort     = "db-port"
	ConfigKeyDatabaseName     = "db-name"

	ConfigKeyOutput = "output"

	DefaultDatabaseUsername = "datasync"
	DefaultDatabasePassword = "password"
	DefaultDatabaseHost     = "127.0.0.1"
	DefaultDatabasePort     = 3306
	DefaultDatabaseName     = "farmer"
)

var conf = config.Map{
	ConfigKeyAirtableApiKey: &config.String{
		Shorthand: "a",
		Usage:     "airtable api key",
	},
	ConfigKeyAirtableBaseId: &config.String{
		Shorthand: "b",
		Usage:     "airtable base id",
	},
	ConfigKeyAirtableTableId: &config.String{
		Shorthand: "t",
		Usage:     "airtable table id",
	},
	ConfigKeyChain: &config.String{
		Shorthand: "c",
		Usage:     fmt.Sprintf("slug of chain (available options: [%s])", strings.Join(addressbook.AvailableChains, ", ")),
		Default:   "cronos",
	},
	ConfigKeyDatabaseUsername: &config.String{
		Shorthand: "u",
		Usage:     "username of the database user",
		Default:   DefaultDatabaseUsername,
	},
	ConfigKeyDatabasePassword: &config.String{
		Shorthand: "p",
		Usage:     "password of the database user",
		Default:   DefaultDatabasePassword,
	},
	ConfigKeyDatabaseHost: &config.String{
		Shorthand: "H",
		Usage:     "hostname of the database",
		Default:   DefaultDatabaseHost,
	},
	ConfigKeyDatabasePort: &config.Uint{
		Shorthand: "P",
		Usage:     "port the database instance is listening on",
		Default:   DefaultDatabasePort,
	},
	ConfigKeyDatabaseName: &config.String{
		Shorthand: "n",
		Usage:     "name of the database schema to use",
		Default:   DefaultDatabaseName,
	},
	ConfigKeyOutput: &config.String{
		Shorthand: "o",
		Usage:     "specify where to send synced data to",
		Default:   "db",
	},
}
