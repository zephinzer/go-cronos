package migrate

import (
	"fmt"

	"github.com/spf13/cobra"
	"github.com/zephinzer/go-cronos/cmd/datasync/common/datasync"
	"github.com/zephinzer/go-cronos/internal/database"
	"github.com/zephinzer/go-cronos/internal/log"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:   "migrate",
		Short: "runs a migration on the database tables needed for datasync to work",
		RunE:  runE,
	}
	conf.ApplyToCobra(&command)
	return &command
}

func runE(command *cobra.Command, args []string) error {
	dbUsername := conf.GetString(ConfigKeyDatabaseUsername)
	dbPassword := conf.GetString(ConfigKeyDatabasePassword)
	dbHost := conf.GetString(ConfigKeyDatabaseHost)
	dbPort := conf.GetUint(ConfigKeyDatabasePort)
	dbName := conf.GetString(ConfigKeyDatabaseName)
	dbRootUsername := conf.GetString(ConfigKeyDatabaseRootUsername)
	dbRootPassword := conf.GetString(ConfigKeyDatabaseRootPassword)

	database.Migrate(database.MigrateOpts{
		DatabaseHost:            dbHost,
		DatabasePort:            uint64(dbPort),
		DatabaseUsername:        dbUsername,
		DatabasePassword:        dbPassword,
		DatabaseRootUsername:    dbRootUsername,
		DatabaseRootPassword:    dbRootPassword,
		DatabaseName:            dbName,
		DisableSchemaMigrations: true,
		Log:                     func(f string, a ...any) { log.Infof(f, a...) },
	})
	log.Infof("initialising gorm for auto-migrations...")
	db, err := gorm.Open(mysql.New(mysql.Config{
		DSN: fmt.Sprintf(
			"%s:%s@tcp(%s:%v)/%s?charset=utf8mb4&parseTime=True",
			dbUsername,
			dbPassword,
			dbHost,
			dbPort,
			dbName,
		),
	}))
	if err != nil {
		return fmt.Errorf("failed to open a db connection via gorm: %s", err)
	}
	log.Infof("executing database auto-migrations for alias records")
	if err := db.AutoMigrate(&datasync.Aliases{}); err != nil {
		return fmt.Errorf("failed to auto-migrate aliases table: %s", err)
	}
	return nil
}
