package migrate

import (
	"github.com/usvc/go-config"
)

const (
	ConfigKeyDatabaseUsername     = "db-username"
	ConfigKeyDatabasePassword     = "db-password"
	ConfigKeyDatabaseHost         = "db-host"
	ConfigKeyDatabasePort         = "db-port"
	ConfigKeyDatabaseName         = "db-name"
	ConfigKeyDatabaseRootUsername = "db-root-username"
	ConfigKeyDatabaseRootPassword = "db-root-password"

	DefaultDatabaseHost         = "127.0.0.1"
	DefaultDatabaseName         = "farmer"
	DefaultDatabasePort         = 3306
	DefaultDatabaseUsername     = "datasync"
	DefaultDatabasePassword     = "password"
	DefaultDatabaseRootUsername = "root"
	DefaultDatabaseRootPassword = "password"
)

var conf = config.Map{
	ConfigKeyDatabaseUsername: &config.String{
		Shorthand: "u",
		Usage:     "username of the database user",
		Default:   DefaultDatabaseUsername,
	},
	ConfigKeyDatabasePassword: &config.String{
		Shorthand: "p",
		Usage:     "password of the database user",
		Default:   DefaultDatabasePassword,
	},
	ConfigKeyDatabaseHost: &config.String{
		Shorthand: "H",
		Usage:     "hostname of the database",
		Default:   DefaultDatabaseHost,
	},
	ConfigKeyDatabasePort: &config.Uint{
		Shorthand: "P",
		Usage:     "port the database instance is listening on",
		Default:   DefaultDatabasePort,
	},
	ConfigKeyDatabaseName: &config.String{
		Shorthand: "n",
		Usage:     "name of the database schema to use",
		Default:   DefaultDatabaseName,
	},
	ConfigKeyDatabaseRootUsername: &config.String{
		Usage:   "username of the root database user to create schemas",
		Default: DefaultDatabaseRootUsername,
	},
	ConfigKeyDatabaseRootPassword: &config.String{
		Usage:   "password of the root database user to create schemas",
		Default: DefaultDatabaseRootPassword,
	},
}
