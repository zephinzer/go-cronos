package datasync

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/zephinzer/go-cronos/internal/alias"
	"gorm.io/gorm"
)

type Aliases struct {
	alias.Alias
	gorm.Model
}

type AliasesData struct {
	CreatedTime time.Time `json:"createdTime"`
	Fields      Aliases   `json:"fields"`
	Id          string    `json:"id"`
}

func NewAliasesFromAirtableRecord(data interface{}) (*AliasesData, error) {
	jsonData, err := json.Marshal(data)
	if err != nil {
		return nil, fmt.Errorf("failed to marshal data to json: %s", err)
	}
	aliasesInstance := AliasesData{}
	if err := json.Unmarshal(jsonData, &aliasesInstance); err != nil {
		return nil, fmt.Errorf("failed to unmarshal data to an aliases instance: %s", err)
	}
	return &aliasesInstance, nil
}
