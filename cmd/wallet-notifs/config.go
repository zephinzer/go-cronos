package main

import (
	"github.com/usvc/go-config"
	"github.com/zephinzer/go-cronos/pkg/cronos"
)

const (
	CommsChannelTelegram = "telegram"

	ConfigKeyAddresses      = "addresses"
	ConfigKeyCommsChannel   = "comms-channel"
	ConfigKeyRpcUrl         = "rpc-url"
	ConfigKeyTelegramChatId = "tg-chat-id"
	ConfigKeyTelegramToken  = "tg-token"

	DefaultRpcUrl = cronos.RpcXstaking
)

var conf = config.Map{
	ConfigKeyAddresses: &config.StringSlice{
		Shorthand: "a",
		Usage:     "list of hex wallet address to watch",
	},
	ConfigKeyCommsChannel: &config.String{
		Shorthand: "c",
		Usage:     "comms channel slug",
	},
	ConfigKeyRpcUrl: &config.String{
		Shorthand: "r",
		Usage:     "rpc url to use",
	},
	ConfigKeyTelegramChatId: &config.Int{
		Usage: "chat id to send notifications to",
	},
	ConfigKeyTelegramToken: &config.String{
		Usage: "telegram bot api token (talk to @BotFather at https://t.me/botfather to get this)",
	},
}
