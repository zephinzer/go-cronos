package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
	"sync"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/zephinzer/go-cronos/pkg/addressbook"
	"github.com/zephinzer/go-cronos/pkg/evmutils"
)

func main() {
	if err := GetCommand().Execute(); err != nil {
		logrus.Errorf("failed to complete successfully: %s", err)
		os.Exit(1)
	}
}

func GetCommand() *cobra.Command {
	command := cobra.Command{
		RunE: run,
	}
	conf.ApplyToCobra(&command)
	return &command
}

func run(command *cobra.Command, args []string) error {
	rpcUrl := conf.GetString(ConfigKeyRpcUrl)
	client, chainId, err := evmutils.NewClient(evmutils.NewClientOpts{
		RpcUrl: rpcUrl,
	})
	if err != nil {
		return fmt.Errorf("failed to connect to provided rpc[%s]: %s", rpcUrl, err)
	}
	client.Close()
	selectedChain := addressbook.ChainById[int(chainId.Int64())]
	targetAddresses := conf.GetStringSlice(ConfigKeyAddresses)
	targetAddressMap := map[string]bool{}
	for _, targetAddress := range targetAddresses {
		normalisedAddress := strings.ToLower(targetAddress)
		targetAddressMap[normalisedAddress] = true
		logrus.Infof("setting watch for wallet[%s]", targetAddress)
	}
	commsChannel := conf.GetString(ConfigKeyCommsChannel)
	var telegramToken *string = nil
	var telegramChatId *int64 = nil
	switch commsChannel {
	case CommsChannelTelegram:
		tt := conf.GetString(ConfigKeyTelegramToken)
		if tt != "" {
			telegramToken = &tt
		}
		tc := int64(conf.GetInt(ConfigKeyTelegramChatId))
		if tc != 0 {
			telegramChatId = &tc
		}
	default:
		logrus.Warnf("no communications channel specified, no notifications will be sent")
	}

	var telegramBotInstance *tgbotapi.BotAPI = nil
	if telegramToken != nil {
		var newBotError error
		telegramBotInstance, newBotError = tgbotapi.NewBotAPI(*telegramToken)
		if newBotError != nil {
			logrus.Warnf("failed to initialise telegram bot: %s", newBotError)
		}
	}
	if telegramBotInstance != nil {
		logrus.Infof("sending notification to chat[%v] using bot[%s]", *telegramChatId, telegramBotInstance.Self.UserName)
		walletNames := []string{}
		for _, targetAddress := range targetAddresses {
			if knownAddress, exists := selectedChain.Directory[targetAddress]; exists {
				walletLabel := knownAddress.Label
				if len(knownAddress.Aliases) > 0 {
					walletLabel = fmt.Sprintf("%s (AKA %s)", walletLabel, strings.Join(knownAddress.Aliases, ", "))
				}
				walletNames = append(walletNames, "*"+walletLabel+"*: `"+targetAddress+"`")
			} else {
				walletNames = append(walletNames, "`"+targetAddress+"`")
			}
		}
		message := tgbotapi.NewMessage(*telegramChatId, fmt.Sprintf(
			"👀 Now watching the following wallets for transactions\n\n%s",
			strings.Join(walletNames, "\n"),
		))
		message.ParseMode = "markdown"
		messageOutput, err := telegramBotInstance.Send(message)
		if err != nil {
			logrus.Warnf("failed to send message to chat[%v]: %s", *telegramChatId, err)
		} else {
			logrus.Infof("sent message[%v] to chat[%v]", messageOutput.MessageID, *telegramChatId)
			pinMessage := tgbotapi.PinChatMessageConfig{
				ChatID:              *telegramChatId,
				MessageID:           messageOutput.MessageID,
				DisableNotification: true,
			}
			if _, err := telegramBotInstance.Send(pinMessage); err != nil {
				logrus.Warnf("failed to pin message[%v]: %s", messageOutput.MessageID, err)
			}
		}
	}

	var waiter sync.WaitGroup
	events := make(chan evmutils.WatchEvent, 100)
	waiter.Add(1)

	go func() {
		evmutils.WatchTransactions(evmutils.WatchTransactionsOpts{
			Events: events,
			Rpc:    evmutils.RpcUrl(rpcUrl),
		})
	}()

	go func() {
		for {
			event := <-events
			switch event.Type {
			case evmutils.EventTransaction:
				var fromLabel, toLabel string
				if fromAddress, ok := selectedChain.Directory[event.TxFrom]; !ok {
					fromLabel = event.TxFrom[:6] + ".." + event.TxFrom[len(event.TxFrom)-4:]
				} else {
					fromLabel = fromAddress.Label
				}
				if toAddress, ok := selectedChain.Directory[event.TxTo]; !ok {
					toLabel = event.TxTo[:6] + ".." + event.TxTo[len(event.TxTo)-4:]
				} else {
					toLabel = toAddress.Label
				}
				methodHash := "        "
				if len(event.TxData) > 8 {
					methodHash = event.TxData[:8]
				}
				fmt.Println(event.TxHash + "[" + methodHash + "] >> \033[1m" + fromLabel + "\033[0m -> \033[1m" + toLabel + "\033[0m : " + strconv.FormatFloat(event.GetHumanValue(), 'f', 2, 64) + " CRO")
				if len(targetAddresses) > 0 {
					normalisedAddress := strings.ToLower(event.TxFrom)
					if shouldNotify, exists := targetAddressMap[normalisedAddress]; exists && shouldNotify && telegramBotInstance != nil && telegramChatId != nil {
						methodId := "NONE"
						if len(event.TxData) > 8 {
							methodId = event.TxData[:8]
						}
						message := tgbotapi.NewMessage(
							*telegramChatId,
							fmt.Sprintf(
								"ℹ️ *%s* transacted with address `%s` (%s) using method `%s` for %v CRO",
								fromLabel,
								event.TxTo,
								toLabel,
								methodId,
								event.GetHumanValue(),
							),
						)
						message.ReplyMarkup = tgbotapi.NewInlineKeyboardMarkup(
							tgbotapi.NewInlineKeyboardRow(
								tgbotapi.NewInlineKeyboardButtonURL("Wallet", fmt.Sprintf("https://cronoscan.com/address/%s", event.TxFrom)),
								tgbotapi.NewInlineKeyboardButtonURL("Transaction", fmt.Sprintf("https://cronoscan.com/tx/%s", event.TxHash)),
							),
						)
						message.ParseMode = "markdown"
						messageOutput, err := telegramBotInstance.Send(message)
						if err != nil {
							logrus.Warnf("failed to send notification message: %s", err)
							continue
						}
						logrus.Infof("sent message[%v] to chat[%v]", messageOutput.MessageID, *telegramChatId)
					}
				}
			}
		}
	}()
	waiter.Wait()
	return nil
}
