package main

import (
	"fmt"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/zephinzer/go-cronos/cmd/aes/decrypt"
	"github.com/zephinzer/go-cronos/cmd/aes/encrypt"
)

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:   "aes",
		Short: "Utility tool for encrypting and decrypting a string",
		RunE:  run,
	}
	command.AddCommand(decrypt.GetCommand())
	command.AddCommand(encrypt.GetCommand())
	return &command
}

func run(command *cobra.Command, args []string) error {
	return nil
}

func main() {

	command := GetCommand()
	if err := command.Execute(); err != nil {
		command.Help()
		fmt.Println("")
		logrus.Errorf("failed to complete: %s", err)
	}
}
