package encrypt

import (
	"fmt"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/usvc/go-config"
	"github.com/zephinzer/go-cronos/pkg/secret"
)

const (
	ConfigKey = "key"
)

var conf = config.Map{
	ConfigKey: &config.String{
		Shorthand: "k",
		Usage:     "key for encrypting ciphertext",
	},
}

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:   "encrypt <plaintext> -k <key>",
		Short: "Encrypts a plaintext into a ciphertext",
		RunE:  run,
	}
	conf.ApplyToCobra(&command)
	return &command
}

func run(command *cobra.Command, args []string) error {
	if len(args) == 0 {
		return fmt.Errorf("failed to find a plaintext to encrypt")
	}
	plaintext := args[0]
	key := conf.GetString(ConfigKey)
	if len(key) == 0 {
		return fmt.Errorf("failed to find a valid key")
	}
	ciphertext, err := secret.Encrypt(plaintext, key)
	if err != nil {
		return fmt.Errorf("failed to encrypt key: %s", err)
	}
	fmt.Println(ciphertext)
	return nil
}

func main() {

	command := GetCommand()
	if err := command.Execute(); err != nil {
		command.Help()
		fmt.Println("")
		logrus.Errorf("failed to complete: %s", err)
	}
}
