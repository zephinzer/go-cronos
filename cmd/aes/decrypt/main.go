package decrypt

import (
	"fmt"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/usvc/go-config"
	"github.com/zephinzer/go-cronos/pkg/secret"
)

const (
	ConfigKey = "key"
)

var conf = config.Map{
	ConfigKey: &config.String{
		Shorthand: "k",
		Usage:     "key for decrypting ciphertext",
	},
}

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:   "decrypt <ciphertext> -k <key>",
		Short: "Decrypts a ciphertext into plaintext",
		RunE:  run,
	}

	conf.ApplyToCobra(&command)
	return &command
}

func run(command *cobra.Command, args []string) error {
	if len(args) == 0 {
		return fmt.Errorf("failed to find a ciphertext to encrypt")
	}
	ciphertext := args[0]
	key := conf.GetString(ConfigKey)
	if len(key) == 0 {
		return fmt.Errorf("failed to find a valid key")
	}
	plaintext, err := secret.Decrypt(ciphertext, key)
	if err != nil {
		return fmt.Errorf("failed to encrypt key: %s", err)
	}
	fmt.Println(plaintext)
	return nil
}

func main() {

	command := GetCommand()
	if err := command.Execute(); err != nil {
		command.Help()
		fmt.Println("")
		logrus.Errorf("failed to complete: %s", err)
	}
}
