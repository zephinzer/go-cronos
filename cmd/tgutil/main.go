package main

import (
	"fmt"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/usvc/go-config"
	"github.com/zephinzer/go-cronos/cmd/tgutil/listen"
	"github.com/zephinzer/go-cronos/cmd/tgutil/send"
)

const (
	ConfigTelegramToken = "tg-token"
)

var conf = config.Map{
	ConfigTelegramToken: &config.String{
		Usage: "telegram bot api token (get it from https://t.me/BotFather)",
	},
}

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:   "tgutil",
		Short: "Utility tool for telegram functionality",
		RunE:  run,
	}
	command.AddCommand(listen.GetCommand())
	command.AddCommand(send.GetCommand())
	conf.ApplyToCobra(&command)
	return &command
}

func run(command *cobra.Command, args []string) error {
	command.Help()
	return nil
}

func main() {

	command := GetCommand()
	if err := command.Execute(); err != nil {
		command.Help()
		fmt.Println("")
		logrus.Errorf("failed to complete: %s", err)
	}
}
