package send

import (
	"fmt"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/usvc/go-config"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

const (
	ConfigTelegramChatId = "tg-chat-id"
	ConfigTelegramToken  = "tg-token"
)

var conf = config.Map{
	ConfigTelegramChatId: &config.Int{
		Usage: "chat id to send a test message to",
	},
	ConfigTelegramToken: &config.String{
		Usage: "telegram bot api token (get it from https://t.me/BotFather)",
	},
}

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:  "send",
		RunE: run,
	}
	conf.ApplyToCobra(&command)
	return &command
}

func run(command *cobra.Command, args []string) error {
	telegramToken := conf.GetString(ConfigTelegramToken)
	if telegramToken == "" {
		return fmt.Errorf("failed to receive a non-empty telegram token")
	}
	bot, err := tgbotapi.NewBotAPI(telegramToken)
	if err != nil {
		return fmt.Errorf("failed to create new telegram bot instance: %s", err)
	}
	logrus.Infof("bot is at https://t.me/%s", bot.Self.UserName)
	chatId := int64(conf.GetInt(ConfigTelegramChatId))
	if chatId == 0 {
		return fmt.Errorf("failed to receive a valid chat id")
	}
	msg := tgbotapi.NewMessage(chatId, "test")
	sentMessage, err := bot.Send(msg)
	if err != nil {
		return fmt.Errorf("failed to send message: %s", err)
	}
	logrus.Info("message 'test' sent successfully")
	deleteMessage := tgbotapi.NewDeleteMessage(chatId, sentMessage.MessageID)
	if _, err := bot.Send(deleteMessage); err != nil {
		return fmt.Errorf("failed to delete message: %s", err)
	}

	return nil
}
