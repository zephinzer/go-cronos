package constants

const (
	CommsChannelTelegram = "telegram"

	FlagAddress             = "address"
	FlagAddresses           = "addresses"
	FlagCommsChannel        = "comms-channel"
	FlagEncryptedPrivateKey = "encrypted-private-key"
	FlagPrice               = "price"
	FlagRank                = "rank"
	FlagRpcUrl              = "rpc-url"
	FlagTelegramChatId      = "tg-chat-id"
	FlagTelegramToken       = "tg-token"
	FlagTraits              = "traits"
	FlagYes                 = "yes"
)
