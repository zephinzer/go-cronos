package event

import (
	"fmt"
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"github.com/sirupsen/logrus"
	"github.com/zephinzer/go-cronos/cmd/ebisusbay-original/common/telegram"
	"github.com/zephinzer/go-cronos/pkg/cronos"
)

func HandleBlock(event cronos.WatchEvent) {
	internalMessage := ""
	if event.InternalMessage != "" {
		internalMessage = " (" + event.InternalMessage + ")"
	}
	logrus.Infof("reached block %s%s", event.BlockId, internalMessage)
}

type EventEmitter struct {
	telegramBotInstance *tgbotapi.BotAPI
	telegramChatId      int64
}

func HandleError(event cronos.WatchEvent, emitter ...EventEmitter) {
	logrus.Warnf("received error: %s", event.Error)
	if emitter != nil && len(emitter) > 0 {
		switch true {
		case emitter[0].telegramBotInstance != nil && emitter[0].telegramChatId != 0:
			telegram.SendSystemLog(
				emitter[0].telegramBotInstance,
				emitter[0].telegramChatId,
				fmt.Sprintf("received error: %s", event.Error),
			)
		}
	}
	<-time.After(1 * time.Second)
}

func HandleWarning(event cronos.WatchEvent) {
	logrus.Warnf("received warning: %s", event.Error)
}
