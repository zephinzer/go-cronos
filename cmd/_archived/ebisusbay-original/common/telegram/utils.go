package telegram

import (
	"fmt"
	"math/big"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/common/math"
	"github.com/ethereum/go-ethereum/core/types"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"github.com/sirupsen/logrus"
	"github.com/zephinzer/go-cronos/pkg/ebisusbayapi"
)

const systemMessagePrefix = "⚙️ SYSTEM MESSAGE\n\n"
const warningMessagePrefix = "🚨 WARNING\n\n"

func SendSystemLog(withBot *tgbotapi.BotAPI, toChatId int64, systemLogMessage string) {
	if len(systemLogMessage) > (2048) {
		systemLogMessage = systemLogMessage[:2048] + "... (truncated)"
	}
	message := tgbotapi.NewMessage(toChatId, fmt.Sprintf("%s`%s`", systemMessagePrefix, systemLogMessage))
	message.ParseMode = "markdown"
	messageOutput, err := withBot.Send(message)
	if err != nil {
		logrus.Warnf("failed to send telegram message to chat[%v]: %s", toChatId, err)
	} else {
		logrus.Infof("sent message[%v] to chat[%v]", messageOutput.MessageID, toChatId)
	}
}

func SendListingNotification(withBot *tgbotapi.BotAPI, toChatId int64, listingResponse *ebisusbayapi.ListingsResponse, listing *ebisusbayapi.Listing, listingPriceHuman int64) {
	message := tgbotapi.NewMessage(toChatId, fmt.Sprintf(
		"[🔔](%s)* %s listed at %v CRO* (Rank: %v)\n",
		listingResponse.Nft.Image,
		listingResponse.Nft.Name,
		listingPriceHuman,
		listingResponse.Nft.Rank,
	))
	message.ParseMode = "markdown"
	message.ReplyMarkup = tgbotapi.NewInlineKeyboardMarkup(
		tgbotapi.NewInlineKeyboardRow(
			tgbotapi.NewInlineKeyboardButtonURL("Browser", fmt.Sprintf("https://app.ebisusbay.com/collection/%s/%v", listingResponse.Nft.NftAddress, listingResponse.Nft.NftId)),
			tgbotapi.NewInlineKeyboardButtonURL("Metamask", fmt.Sprintf("https://metamask.app.link/dapp/app.ebisusbay.com/collection/%s/%v", listingResponse.Nft.NftAddress, listingResponse.Nft.NftId)),
		),
	)
	messageOutput, err := withBot.Send(message)
	if err != nil {
		logrus.Warnf("failed to send telegram message to chat[%v]", toChatId)
	} else {
		logrus.Infof("successfully sent message[%v] to chat[%v]", messageOutput.MessageID, toChatId)
	}
}

func SendPurchaseNotification(withBot *tgbotapi.BotAPI, toChatId int64, listingResponse *ebisusbayapi.ListingResponse, listing *ebisusbayapi.Listing, transaction *types.Transaction) {
	transactionCostHuman := big.NewInt(0).Div(transaction.Cost(), math.BigPow(10, 18)).Int64()
	transactionValueHuman := big.NewInt(0).Div(transaction.Value(), math.BigPow(10, 18)).Int64()
	message := tgbotapi.NewMessage(toChatId, fmt.Sprintf(
		"🔔 Making a purchase of %s at %v CRO\n\nListing URL: %s\nTransaction cost: %v CRO\nTransaction value: %v CRO\nRaw data: `%s`",
		listingResponse.Nft.Name,
		transactionValueHuman,
		listingResponse.Nft.Market.URI,
		transactionCostHuman,
		transactionValueHuman,
		"0x"+common.Bytes2Hex(transaction.Data()),
	))
	message.ParseMode = "markdown"
	message.ReplyMarkup = tgbotapi.NewInlineKeyboardMarkup(
		tgbotapi.NewInlineKeyboardRow(
			tgbotapi.NewInlineKeyboardButtonURL("Browser", fmt.Sprintf("https://app.ebisusbay.com/collection/%s/%v", listingResponse.Nft.NftAddress, listingResponse.Nft.NftId)),
			tgbotapi.NewInlineKeyboardButtonURL("Metamask", fmt.Sprintf("https://metamask.app.link/dapp/app.ebisusbay.com/collection/%s/%v", listingResponse.Nft.NftAddress, listingResponse.Nft.NftId)),
		),
	)
	if messageOutput, err := withBot.Send(message); err != nil {
		logrus.Warnf("failed to send telegram message to chat[%v]", toChatId)
	} else {
		logrus.Infof("successfully sent message[%v] to chat[%v]", messageOutput.MessageID, toChatId)
	}
}

func SendWarning(withBot *tgbotapi.BotAPI, toChatId int64, warningMessage string) int {
	if len(warningMessage) > (2048) {
		warningMessage = warningMessage[:2048] + "... (truncated)"
	}
	message := tgbotapi.NewMessage(toChatId, fmt.Sprintf("%s%s", warningMessagePrefix, warningMessage))
	message.ParseMode = "markdown"
	messageOutput, err := withBot.Send(message)
	if err != nil {
		logrus.Warnf("failed to send telegram message to chat[%v]: %s", toChatId, err)
	} else {
		logrus.Infof("sent message[%v] to chat[%v]", messageOutput.MessageID, toChatId)
	}
	return messageOutput.MessageID
}
