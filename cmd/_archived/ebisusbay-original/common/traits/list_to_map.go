package traits

import (
	"fmt"
	"strings"
)

func ListToMap(list []string) (map[string]string, error) {
	mapOutput := map[string]string{}
	for _, watchedTrait := range list {
		traitPair := strings.Split(watchedTrait, "=")
		if len(traitPair) != 2 {
			return nil, fmt.Errorf("failed to receive a valid trait for '%s', it should be 'trait category=trait value'", traitPair)
		}
		traitType := traitPair[0]
		traitValue := traitPair[1]
		mapOutput[traitType] = traitValue
	}
	return mapOutput, nil
}
