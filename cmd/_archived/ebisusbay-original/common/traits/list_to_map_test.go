package traits

import (
	"testing"

	"github.com/stretchr/testify/suite"
)

type ListToMapTests struct {
	suite.Suite
}

func TestListToMap(t *testing.T) {
	suite.Run(t, &ListToMapTests{})
}

func (s *ListToMapTests) TestListToMap() {
	list := []string{
		"trait1=value1",
		"trait 2=value 2",
		"Trait 3=Value 3",
	}
	output, err := ListToMap(list)
	s.Nil(err)
	s.EqualValues("value1", output["trait1"])
	s.EqualValues("value 2", output["trait 2"])
	s.EqualValues("Value 3", output["Trait 3"])
}
