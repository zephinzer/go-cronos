package watch

import (
	"github.com/spf13/cobra"
	"github.com/zephinzer/go-cronos/cmd/ebisusbay-original/commands/watch/listings"
	"github.com/zephinzer/go-cronos/cmd/ebisusbay-original/commands/watch/sales"
)

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:   "watch",
		Short: "Watches for events on Ebisus Bay",
		RunE:  run,
	}
	command.AddCommand(listings.GetCommand())
	command.AddCommand(sales.GetCommand())
	return &command
}

func run(command *cobra.Command, args []string) error {
	command.Help()
	return nil
}
