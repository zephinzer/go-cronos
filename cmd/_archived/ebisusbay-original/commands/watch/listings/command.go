package listings

import (
	"encoding/hex"
	"fmt"
	"math/big"
	"net/url"
	"path"
	"strconv"
	"sync"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/common/math"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/usvc/go-config"
	"github.com/zephinzer/go-cronos/cmd/ebisusbay-original/common/constants"
	"github.com/zephinzer/go-cronos/cmd/ebisusbay-original/common/event"
	"github.com/zephinzer/go-cronos/cmd/ebisusbay-original/common/telegram"
	"github.com/zephinzer/go-cronos/internal/healthcheck"
	"github.com/zephinzer/go-cronos/pkg/ebisusbay"
	"github.com/zephinzer/go-cronos/pkg/ebisusbayapi"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

var conf = config.Map{
	constants.FlagAddress: &config.String{
		Shorthand: "a",
		Usage:     "collection address to watch for new listings",
	},
	constants.FlagPrice: &config.Int{
		Shorthand: "M",
		Usage:     "when specified, only tokens costing less than this amount in cro will be processed",
	},
	constants.FlagRank: &config.String{
		Shorthand: "r",
		Usage:     "when specified, only tokens with a rank below this number will be processed",
	},
	constants.FlagTraits: &config.StringSlice{
		Shorthand: "t",
		Usage:     "when specified, only tokens with these traits will be processed",
	},
	constants.FlagCommsChannel: &config.String{
		Shorthand: "n",
		Usage:     "notification channel type",
	},
	constants.FlagTelegramChatId: &config.Int{
		Usage: "telegram channel id to notify",
	},
	constants.FlagTelegramToken: &config.String{
		Usage: "telegram bot api token (get it from https://t.me/BotFather)",
	},
}

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:   "listings",
		Short: "Watch for new listings on Ebisus Bay",
		RunE:  run,
	}
	conf.ApplyToCobra(&command)
	return &command
}

func run(command *cobra.Command, args []string) error {
	var telegramBotInstance *tgbotapi.BotAPI = nil
	var telegramChatId int64 = 0
	commsChannel := conf.GetString(constants.FlagCommsChannel)
	if commsChannel != "" {
		switch commsChannel {
		case constants.CommsChannelTelegram:
			logrus.Infof("initialising telegram bot...")
			tgToken := conf.GetString(constants.FlagTelegramToken)
			if tgToken == "" {
				return fmt.Errorf("failed to receive a valid telegram token")
			}
			var tgbotError error
			telegramBotInstance, tgbotError = tgbotapi.NewBotAPI(tgToken)
			if tgbotError != nil {
				return fmt.Errorf("failed to create a new telegram bot instance: %s", tgbotError)
			}
			telegramChatId = int64(conf.GetInt(constants.FlagTelegramChatId))
			if telegramChatId == 0 {
				return fmt.Errorf("failed to receive a valid telegram chat id")
			}
			logrus.Infof("configured to send messages to chat[%v] via bot[@%s]", telegramChatId, telegramBotInstance.Self.UserName)
		default:
			return fmt.Errorf("failed to find a compatible notification channel ('%s' is invalid)", commsChannel)
		}
	}

	targetAddress := conf.GetString(constants.FlagAddress)
	var targetPrice *big.Int = nil
	maxPrice := conf.GetInt(constants.FlagPrice)
	if maxPrice > 0 {
		targetPrice = big.NewInt(0).Mul(big.NewInt(int64(maxPrice)), math.BigPow(10, 18))
	}

	ebisusBayUrl, urlError := url.Parse(ebisusbayapi.AppBaseUrl)
	if urlError != nil {
		return fmt.Errorf("failed to parse the ebisus bay app url: %s", urlError)
	}

	watchEvents := make(chan evmutils.WatchEvent, 100)
	var waiter sync.WaitGroup
	var err error
	waiter.Add(1)
	go func() {
		defer waiter.Done()
		for {
			watchEvent := <-watchEvents
			switch watchEvent.Type {
			case evmutils.EventBlock:
				event.HandleBlock(watchEvent)
			case evmutils.EventError:
				event.HandleError(watchEvent)
			case evmutils.EventWarning:
				event.HandleWarning(watchEvent)
			case evmutils.EventTransaction:
				go func(event evmutils.WatchEvent) {
					decodedData, err := hex.DecodeString(event.TxData[8:])
					if err != nil {
						logrus.Warnf("failed to decode txn data: %s", err)
						return
					}
					parsedData, err := ebisusbay.Contract.Methods["makeListing"].Inputs.Unpack(decodedData)
					if err != nil {
						logrus.Warnf("failed to parse txn data: %s", err)
						return
					}
					newListing := ebisusbay.MakeListingData{
						TokenAddress: "0x" + common.Bytes2Hex(parsedData[0].(common.Address).Bytes()),
						TokenId:      parsedData[1].(*big.Int).Int64(),
						Amount:       parsedData[2].(*big.Int),
					}

					tokenPriceHuman := big.NewInt(0).Div(newListing.Amount, math.BigPow(10, 18))
					listingPriceHuman := big.NewInt(0).Div(newListing.Amount, math.BigPow(10, 18)).Int64()
					listingUrl := *ebisusBayUrl
					listingUrl.Scheme = "https"
					listingUrl.Path = path.Join("/collection", newListing.TokenAddress, strconv.FormatInt(newListing.TokenId, 10))
					logrus.Infof("found new listing at %s for %v cro", listingUrl.String(), tokenPriceHuman.Int64())

					// check addresses
					if targetAddress != "" {
						if newListing.TokenAddress != targetAddress {
							logrus.Infof("skipped token[%v] of collection[%s], not in watched collection[%s]", newListing.TokenId, newListing.TokenAddress, targetAddress)
							return
						}
					}

					// check price filter
					if targetPrice != nil {
						targetPriceHuman := big.NewInt(0).Div(targetPrice, math.BigPow(10, 18))
						logrus.Infof("skipped token[%v] of collection[%s], token price (%v cro) exceeds target price (%v cro)", newListing.TokenId, newListing.TokenAddress, tokenPriceHuman.Int64(), targetPriceHuman.Int64())
						return
					}

					listingData, err := ebisusbayapi.GetListings(ebisusbayapi.GetListingsOpts{
						Address: newListing.TokenAddress,
						TokenId: newListing.TokenId,
					})
					if err != nil {
						logrus.Warnf("failed to get listing information: %s\n\n%v", err, listingData)
						fmt.Printf(
							"token #%v from collection '%s' listed for %v cro\n",
							newListing.TokenId,
							newListing.TokenAddress,
							listingPriceHuman,
						)
						fmt.Printf("> [cronoscan](https://cronoscan.com/tx/%s)\n", event.TxHash)
						fmt.Printf("> [ebisusbay collection](https://app.ebisusbay.com/collection/%s)\n", newListing.TokenAddress)
						fmt.Printf("> [ebisusbay item](https://app.ebisusbay.com/collection/%s/%v)\n", newListing.TokenAddress, newListing.TokenId)
						return
					}
					targetListing := listingData.GetNewest()
					if targetListing == nil {
						logrus.Infof("skipped token[%v] of collection[%s], no listings found\n", newListing.TokenId, newListing.TokenAddress)
						return
					}
					fmt.Printf(
						"%s (token id: %s) listed for %v cro\n",
						listingData.Nft.Name,
						listingData.Nft.NftId,
						listingPriceHuman,
					)
					fmt.Printf("> [cronoscan](https://cronoscan.com/tx/%s)\n", event.TxHash)
					fmt.Printf("> [ebisusbay collection](https://app.ebisusbay.com/collection/%s)\n", newListing.TokenAddress)
					fmt.Printf("> [ebisusbay item](https://app.ebisusbay.com/collection/%s/%v)\n", newListing.TokenAddress, newListing.TokenId)
					fmt.Printf("> [ebisusbay listing](https://app.ebisusbay.com/listing/%v)\n", targetListing.ListingId)
					if telegramBotInstance != nil {
						telegram.SendListingNotification(telegramBotInstance, telegramChatId, listingData, targetListing, listingPriceHuman)
					}
				}(watchEvent)
			}
		}
	}()
	listingSignature := common.Bytes2Hex(ebisusbay.Contract.Methods["makeListing"].ID)
	go evmutils.WatchTransactions(evmutils.WatchTransactionsOpts{
		Events:      watchEvents,
		Methods:     []string{string(listingSignature)},
		ToAddresses: []string{constants.EbisusBayContractAddress},
		Rpc:         evmutils.RpcVvs,
	})
	go healthcheck.StartServer("0.0.0.0", 8000)
	waiter.Wait()
	if err != nil {
		return fmt.Errorf("failed to maintain chain watching: %s", err)
	}

	return nil
}
