package sales

import (
	"encoding/hex"
	"fmt"
	"math/big"
	"sync"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/common/math"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/zephinzer/go-cronos/cmd/ebisusbay-original/common/constants"
	"github.com/zephinzer/go-cronos/cmd/ebisusbay-original/common/event"
	"github.com/zephinzer/go-cronos/pkg/ebisusbay"
	"github.com/zephinzer/go-cronos/pkg/ebisusbayapi"
)

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:   "sales",
		Short: "Watch for new sales on Ebisus Bay",
		RunE:  run,
	}
	return &command
}

func run(command *cobra.Command, args []string) error {
	watchEvents := make(chan evmutils.WatchEvent, 100)

	var waiter sync.WaitGroup
	var err error
	waiter.Add(1)
	go func() {
		defer waiter.Done()
		for {
			watchEvent := <-watchEvents
			switch watchEvent.Type {
			case evmutils.EventBlock:
				event.HandleBlock(watchEvent)
			case evmutils.EventError:
				event.HandleError(watchEvent)
			case evmutils.EventWarning:
				event.HandleWarning(watchEvent)
			case evmutils.EventTransaction:
				go func(event evmutils.WatchEvent) {
					decodedData, err := hex.DecodeString(event.TxData[8:])
					if err != nil {
						logrus.Warnf("failed to decode txn data: %s", err)
						return
					}
					parsedData, err := ebisusbay.Contract.Methods["makePurchase"].Inputs.Unpack(decodedData)
					if err != nil {
						logrus.Warnf("failed to parse txn data: %s", err)
						return
					}
					newSale := ebisusbay.MakePurchaseData{
						ListingID: parsedData[0].(*big.Int),
					}
					pricePaid := big.NewInt(0).Div(event.TxValue, math.BigPow(10, 18)).Int64()
					listingData, err := ebisusbayapi.GetListingById(newSale.ListingID.String())
					if err != nil {
						logrus.Warnf("failed to get listing information: %s", err)
						fmt.Printf(
							"listing %v sold for %v cro\n",
							newSale.ListingID.Int64(),
							pricePaid,
						)
						fmt.Printf("> [cronoscan](https://cronoscan.com/tx/%s)\n", event.TxHash)
						fmt.Printf("> [ebisusbay listing](https://app.ebisusbay.com/listing/%v)\n", newSale.ListingID.Int64())
						return
					}
					listing := listingData.Listings[0]
					fmt.Printf(
						"'%s' (token id: %s) sold for %v cro\n",
						listingData.Nft.Name,
						listing.NftId,
						pricePaid,
					)
					fmt.Printf("> [cronoscan](https://cronoscan.com/tx/%s)\n", event.TxHash)
					fmt.Printf("> [ebisusbay collection](https://app.ebisusbay.com/collection/%s)\n", listing.NftAddress)
					fmt.Printf("> [ebisusbay item](https://app.ebisusbay.com/collection/%s/%v)\n", listing.NftAddress, listing.NftId)
					fmt.Printf("> [ebisusbay listing](https://app.ebisusbay.com/listing/%v)\n", listing.ListingId)
				}(watchEvent)
			}
		}
	}()
	listingSignature := common.Bytes2Hex(ebisusbay.Contract.Methods["makePurchase"].ID)
	go evmutils.WatchTransactions(evmutils.WatchTransactionsOpts{
		Events:      watchEvents,
		Methods:     []string{string(listingSignature)},
		ToAddresses: []string{constants.EbisusBayContractAddress},
	})
	waiter.Wait()
	if err != nil {
		return fmt.Errorf("failed to maintain chain watching: %s", err)
	}

	return nil
}
