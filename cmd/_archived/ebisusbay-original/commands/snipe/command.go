package snipe

import (
	"fmt"
	"sync"
	"time"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/ethereum/go-ethereum/ethclient"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/usvc/go-config"
	"github.com/zephinzer/go-cronos/cmd/ebisusbay-original/common/constants"
	"github.com/zephinzer/go-cronos/cmd/ebisusbay-original/common/event"
	"github.com/zephinzer/go-cronos/internal/healthcheck"
	"github.com/zephinzer/go-cronos/pkg/ebisusbay"
	"github.com/zephinzer/go-cronos/pkg/secret"
)

var conf = config.Map{
	constants.FlagAddress: &config.String{
		Shorthand: "a",
		Usage:     "specifies the collection address on ebisus bay to watch",
	},
	constants.FlagAddresses: &config.StringSlice{
		Shorthand: "A",
		Usage:     fmt.Sprintf("specifies multiple collection addresses on ebisus bay to watch (if this is specified, --%s is ignored", constants.FlagAddress),
	},
	constants.FlagCommsChannel: &config.String{
		Shorthand: "c",
		Usage:     "specifies the decryption channel for you to send a key to decrypt the private key",
	},
	constants.FlagEncryptedPrivateKey: &config.String{
		Shorthand: "P",
		Usage:     "specifies the private key of your account",
	},
	constants.FlagPrice: &config.Float{
		Shorthand: "p",
		Usage:     "specifies a price in CRO below which the purchase will be made",
	},
	constants.FlagRpcUrl: &config.String{
		Shorthand: "R",
		Usage:     "specifies the rpc url to use",
		Default:   string(evmutils.DefaultRpc),
	},
	constants.FlagTelegramChatId: &config.Int{
		Usage: fmt.Sprintf("chat id of the telegram chat. required when %s is set to '%s'", constants.FlagCommsChannel, constants.CommsChannelTelegram),
	},
	constants.FlagTelegramToken: &config.String{
		Usage: fmt.Sprintf("api token for the telegram bot (get it from https://t.me/botfather). required when %s is set to '%s'", constants.FlagCommsChannel, constants.CommsChannelTelegram),
	},
	constants.FlagRank: &config.Int{
		Shorthand: "r",
		Usage:     "specifies a rank below which the purchase will be made",
	},
	constants.FlagTraits: &config.StringSlice{
		Shorthand: "t",
		Usage:     "array of comma delimited (trait_name=trait_value)",
	},
	constants.FlagYes: &config.Bool{
		Shorthand: "y",
		Usage:     "when specified, skips the user-confirmation - use for automated usages",
	},
}

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:   "snipe",
		Short: "Snipe pieces from a specified collection on Ebisus Bay",
		RunE:  run,
	}
	conf.ApplyToCobra(&command)
	return &command
}

func run(command *cobra.Command, args []string) error {
	// sanity checks on input
	encryptedPrivateKey := conf.GetString(constants.FlagEncryptedPrivateKey)
	if encryptedPrivateKey == "" {
		return fmt.Errorf("failed to find a private key, specify it with the flag '--%s <encrypted_private_key>'", constants.FlagEncryptedPrivateKey)
	}
	commsChannel := conf.GetString(constants.FlagCommsChannel)
	if commsChannel == "" {
		return fmt.Errorf("failed to receive a supported decryption channel, only '%s' is supported currently", constants.CommsChannelTelegram)
	}
	logrus.Infof("selected decryption channel: %s", commsChannel)

	var telegramChatId int64
	var telegramToken string

	switch commsChannel {
	case constants.CommsChannelTelegram:
		telegramChatId = int64(conf.GetInt(constants.FlagTelegramChatId))
		if telegramChatId == 0 {
			return fmt.Errorf("failed to find a valid telegram chat id")
		}
		logrus.Infof("telegram chat id: %v", telegramChatId)
		telegramToken = conf.GetString(constants.FlagTelegramToken)
		if telegramToken == "" {
			return fmt.Errorf("failed to find a valid api token for telegram")
		}
	}
	targetRpcUrl := conf.GetString(constants.FlagRpcUrl)
	client, dialErr := ethclient.Dial(targetRpcUrl)
	if dialErr != nil {
		return fmt.Errorf("failed to connect to rpc[%s]: %s", targetRpcUrl, dialErr)
	}
	client.Close()

	// confirm with user this is what they want
	if err := validateAndConfirmWithUser(); err != nil {
		return fmt.Errorf("failed to confirm snipe with user: %s", err)
	}
	targetAddress := conf.GetString(constants.FlagAddress)
	targetAddresses := conf.GetStringSlice(constants.FlagAddresses)
	targetPrice := conf.GetFloat(constants.FlagPrice)
	targetRank := conf.GetInt(constants.FlagRank)
	targetTraits := conf.GetStringSlice(constants.FlagTraits)

	var telegramBotInstance *tgbotapi.BotAPI
	// decrypt the private key
	var privateKeyString string
	logrus.Infof("decrypting user's private key...")
	switch commsChannel {
	case constants.CommsChannelTelegram:
		var newBotError error
		telegramBotInstance, newBotError = tgbotapi.NewBotAPI(telegramToken)
		if newBotError != nil {
			return fmt.Errorf("failed to instantiate a telegram bot instance: %s", newBotError)
		}
		messageInstance := tgbotapi.NewMessage(telegramChatId, "👋🏼 EbisusBay Sniper!\n\n*Reply to this message with the password for your private key* (_both messages will be deleted later_)")
		messageInstance.ReplyMarkup = tgbotapi.ForceReply{ForceReply: true}
		messageInstance.ParseMode = "markdown"
		logrus.Infof("sending request for user password to chat[%v]", telegramChatId)
		messageOutput, err := telegramBotInstance.Send(messageInstance)
		if err != nil {
			return fmt.Errorf("failed to send message to get user password: %s", err)
		}

		// get user password to decrypt private key
		var userPassword string
		updatesChannel := telegramBotInstance.GetUpdatesChan(tgbotapi.NewUpdate(0))
		logrus.Infof("waiting for response from chat[%v]...", telegramChatId)
		for update := range updatesChannel {
			if update.Message.ReplyToMessage != nil && update.Message.ReplyToMessage.MessageID == messageOutput.MessageID {
				userPassword = update.Message.Text
				deleteAction := tgbotapi.NewDeleteMessage(telegramChatId, update.Message.MessageID)
				if _, err := telegramBotInstance.Send(deleteAction); err != nil {
					logrus.Warnf("(might have) failed to delete message with user's password in chat[%v]: %s", telegramChatId, err)
				}
				deleteAction = tgbotapi.NewDeleteMessage(telegramChatId, messageOutput.MessageID)
				if _, err := telegramBotInstance.Send(deleteAction); err != nil {
					logrus.Warnf("(might have) failed to delete message with request for user's password in chat[%v]: %s", telegramChatId, err)
				}
				break
			} else {
				messageInstance = tgbotapi.NewMessage(telegramChatId, "⚠️ Send your password as a reply to this message")
				messageInstance.ReplyToMessageID = messageOutput.MessageID
				if _, err := telegramBotInstance.Send(messageInstance); err != nil {
					logrus.Warnf("(might have) failed to send message to chat[%v]: %s", telegramChatId, err)
				}
			}
		}
		telegramBotInstance.StopReceivingUpdates()
		var decryptErr error
		privateKeyString, decryptErr = secret.Decrypt(encryptedPrivateKey, userPassword)
		if decryptErr != nil {
			return fmt.Errorf("failed to decrypt the private key: %s", decryptErr)
		}
	}

	// create connection to rpc server
	defer client.Close()
	client, dialErr = ethclient.Dial(targetRpcUrl)
	if dialErr != nil {
		return fmt.Errorf("failed to connect to rpc[%s]: %s", targetRpcUrl, dialErr)
	}

	// grab private key of the configured address (config.yaml address => ./secrets/keys.json key => get value)
	privateKey, privateKeyError := crypto.HexToECDSA(privateKeyString)
	if privateKeyError != nil {
		return fmt.Errorf("failed to parse private keys: %s", privateKeyError)
	}
	userAddress := crypto.PubkeyToAddress(privateKey.PublicKey)
	logrus.Infof("sniping from wallet[%s]", userAddress.String())

	// this is a channel to receive all the watch events
	watchEvents := make(chan evmutils.WatchEvent, 100)
	var waiter sync.WaitGroup
	var err error
	waiter.Add(1)

	// start healthcheck server for digitalocean
	go healthcheck.StartServer("0.0.0.0", 8000)

	// start the process that checks every minute if the wallet
	// has sufficient balance
	go startWalletBalanceChecker(walletBalanceCheckerOpts{
		Every:               time.Second * 60,
		EvmClient:           client,
		PrivateKey:          privateKey,
		TargetPriceCro:      targetPrice,
		TelegramBotInstance: telegramBotInstance,
		TelegramChatId:      telegramChatId,
	})

	// start the process that listens for events from the
	// chain watcher (started later further below)
	go func() {
		defer waiter.Done()
		for {
			watchEvent := <-watchEvents
			switch watchEvent.Type {
			case evmutils.EventBlock:
				event.HandleBlock(watchEvent)
			case evmutils.EventError:
				event.HandleError(watchEvent)
			case evmutils.EventWarning:
				event.HandleWarning(watchEvent)
			case evmutils.EventTransaction:
				go handleTransaction(handleTransactionOpts{
					Client:              client,
					Event:               watchEvent,
					PrivateKey:          privateKey,
					TargetAddress:       targetAddress,
					TargetAddresses:     targetAddresses,
					TargetPrice:         targetPrice,
					TargetRank:          targetRank,
					TargetTraits:        targetTraits,
					TelegramBotInstance: telegramBotInstance,
					TelegramChatId:      telegramChatId,
				})
			}
		}
	}()

	// start the event watching cycle
	listingSignature := common.Bytes2Hex(ebisusbay.Contract.Methods[ebisusbay.MakeListingId].ID)
	go evmutils.WatchTransactions(evmutils.WatchTransactionsOpts{
		Events:      watchEvents,
		Methods:     []string{string(listingSignature)},
		ToAddresses: []string{constants.EbisusBayContractAddress},
	})

	waiter.Wait()
	if err != nil {
		return fmt.Errorf("failed to maintain watch on chain: %s", err)
	}

	return nil
}
