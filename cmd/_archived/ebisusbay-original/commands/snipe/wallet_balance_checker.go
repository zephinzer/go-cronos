package snipe

import (
	"context"
	"crypto/ecdsa"
	"fmt"
	"math/big"
	"time"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/common/math"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/ethereum/go-ethereum/ethclient"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"github.com/sirupsen/logrus"
	"github.com/zephinzer/go-cronos/cmd/ebisusbay-original/common/telegram"
	"github.com/zephinzer/go-cronos/pkg/cronos"
	"github.com/zephinzer/go-cronos/pkg/evmutils"
)

type walletBalanceCheckerOpts struct {
	Every               time.Duration
	EvmClient           *ethclient.Client
	PrivateKey          *ecdsa.PrivateKey
	TargetPriceCro      float64
	TelegramBotInstance *tgbotapi.BotAPI
	TelegramChatId      int64
}

func startWalletBalanceChecker(opts walletBalanceCheckerOpts) {
	userAddress := crypto.PubkeyToAddress(opts.PrivateKey.PublicKey)
	lastMessageId := 0
	for {
		if lastMessageId != 0 {
			opts.TelegramBotInstance.Send(tgbotapi.NewDeleteMessage(opts.TelegramChatId, lastMessageId))
			lastMessageId = 0
		}
		userBalance, getBalanceErr := opts.EvmClient.BalanceAt(context.Background(), userAddress, nil)
		userBalanceHuman := big.NewInt(0).Div(userBalance, math.BigPow(10, 18)).Int64()
		if getBalanceErr != nil {
			message := fmt.Errorf("failed to get user balance: %s", getBalanceErr)
			logrus.Warn(message)
			telegram.SendSystemLog(opts.TelegramBotInstance, opts.TelegramChatId, message.Error())
			<-time.After(opts.Every)
			continue
		}
		testTransaction, createTransactionErr := evmutils.CreateSignedTransaction(cronos.CreateTransactionOpts{
			Client:     opts.EvmClient,
			Data:       []byte{},
			GasLimit:   uint64(2000000),
			PrivateKey: opts.PrivateKey,
			To:         common.Address{},
			Value:      big.NewInt(0).Mul(big.NewInt(int64(opts.TargetPriceCro)), math.BigPow(10, 18)), // CRO has 18 digits
		})
		if createTransactionErr != nil {
			message := fmt.Errorf("failed to create test transaction: %s", createTransactionErr)
			logrus.Warn(message)
			telegram.SendSystemLog(opts.TelegramBotInstance, opts.TelegramChatId, message.Error())
			<-time.After(opts.Every)
			continue
		}
		if testTransaction.Cost().Cmp(userBalance) > -1 {
			message := fmt.Sprintf(
				"Insufficient funds *(%v CRO remaining)* for target price of %v CRO (total cost %v CRO) at current gas prices. Send %v more CRO",
				userBalanceHuman,
				int64(opts.TargetPriceCro),
				big.NewInt(0).Div(testTransaction.Cost(), math.BigPow(10, 18)).Int64(),
				big.NewInt(0).Div(big.NewInt(0).Sub(testTransaction.Cost(), userBalance), math.BigPow(10, 18)).Int64(),
			)
			logrus.Warn(message)
			lastMessageId = telegram.SendWarning(opts.TelegramBotInstance, opts.TelegramChatId, message)
		} else {
			excessValue := big.NewInt(0).Div(big.NewInt(0).Sub(userBalance, testTransaction.Cost()), math.BigPow(10, 18)).Int64()
			logrus.Infof("user has sufficient balance of %v cro (%v cro in excess)", userBalanceHuman, excessValue)
		}
		<-time.After(opts.Every)
	}
}
