package snipe

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"

	"github.com/zephinzer/go-cronos/cmd/ebisusbay-original/common/constants"
)

const (
	confirmationText = "yes"
)

func validateAndConfirmWithUser() error {
	targetAddress := conf.GetString(constants.FlagAddress)
	targetAddresses := conf.GetStringSlice(constants.FlagAddresses)
	targetPrice := conf.GetFloat(constants.FlagPrice)
	targetRank := conf.GetInt(constants.FlagRank)
	targetTraits := conf.GetStringSlice(constants.FlagTraits)

	isConfirmationRequired := !conf.GetBool(constants.FlagYes)

	if targetAddress == "" && len(targetAddresses) == 0 {
		return fmt.Errorf("failed to find target address(es), specify it with --%s", constants.FlagAddress)
	}

	if targetPrice <= 0 {
		return fmt.Errorf("failed to find a valid target price (must be above 0), specify it with --%s", constants.FlagPrice)
	}

	if targetRank < 0 {
		return fmt.Errorf("failed to find a valid rank (must be 0 or above), specify it with --%s", constants.FlagRank)
	}

	if !isConfirmationRequired {
		return nil
	}

	var confirmationMessage strings.Builder
	if len(targetAddresses) > 0 {
		confirmationMessage.WriteString("We will purchase a piece from any of collections[" + strings.Join(targetAddresses, ", ") + "] ")
	} else {
		confirmationMessage.WriteString("We will purchase a piece from collection[" + targetAddress + "] ")
	}
	if targetTraits != nil && len(targetTraits) > 0 {
		confirmationMessage.WriteString("with traits ['" + strings.Join(targetTraits, "', '"+"'] and "))
	}
	if targetRank > 0 {
		confirmationMessage.WriteString("with rank below " + strconv.Itoa(targetRank) + " ")
	}
	confirmationMessage.WriteString("when price is less than or equal to " + strconv.FormatFloat(targetPrice, 'f', 2, 64) + " cro")
	if len(targetAddresses) > 0 {
		for _, address := range targetAddresses {
			confirmationMessage.WriteString("\n\n[for collection " + address + "]")
			confirmationMessage.WriteString("\n> Verify collection on Cronoscan: https://cronoscan.com/address/" + address)
			confirmationMessage.WriteString("\n> Verify collection on EbisusBay: https://app.ebisusbay.com/collection/" + address)
		}
	} else {
		confirmationMessage.WriteString("\n> Verify collection on Cronoscan: https://cronoscan.com/address/" + targetAddress)
		confirmationMessage.WriteString("\n> Verify collection on EbisusBay: https://app.ebisusbay.com/collection/" + targetAddress)
	}
	confirmationMessage.WriteString("\n\nConfirm? (only '" + confirmationText + "' will be accepted) ")

	fmt.Println(confirmationMessage.String())
	userConfirmation := bufio.NewScanner(os.Stdin)
	if userConfirmation.Scan() {
		userInput := userConfirmation.Text()
		if userInput != confirmationText {
			return fmt.Errorf("failed to receive consent, refusing to proceed")
		}
	}

	return nil
}
