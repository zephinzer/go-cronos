package snipe

import (
	"bytes"
	"context"
	"crypto/ecdsa"
	"encoding/hex"
	"fmt"
	"math/big"
	"strconv"
	"strings"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/common/math"
	"github.com/ethereum/go-ethereum/ethclient"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"github.com/olekukonko/tablewriter"
	"github.com/sirupsen/logrus"
	"github.com/zephinzer/go-cronos/cmd/ebisusbay-original/common/constants"
	"github.com/zephinzer/go-cronos/cmd/ebisusbay-original/common/telegram"
	"github.com/zephinzer/go-cronos/cmd/ebisusbay-original/common/traits"
	"github.com/zephinzer/go-cronos/pkg/cronos"
	"github.com/zephinzer/go-cronos/pkg/ebisusbay"
	"github.com/zephinzer/go-cronos/pkg/ebisusbayapi"
	"github.com/zephinzer/go-cronos/pkg/evmutils"
)

type handleTransactionOpts struct {
	Client              *ethclient.Client
	Event               cronos.WatchEvent
	PrivateKey          *ecdsa.PrivateKey
	TargetAddress       string
	TargetAddresses     []string
	TargetPrice         float64
	TargetRank          int
	TargetTraits        []string
	TelegramBotInstance *tgbotapi.BotAPI
	TelegramChatId      int64
}

func handleTransaction(opts handleTransactionOpts) {
	client := opts.Client
	event := opts.Event
	privateKey := opts.PrivateKey
	targetAddress := opts.TargetAddress
	targetAddresses := opts.TargetAddresses
	targetPrice := opts.TargetPrice
	targetRank := opts.TargetRank
	targetTraits := opts.TargetTraits
	telegramBotInstance := opts.TelegramBotInstance
	telegramChatId := opts.TelegramChatId

	decodedData, err := hex.DecodeString(cronos.GetDataBody(event.TxData))
	if err != nil {
		logrus.Warnf("failed to decode txn data: %s", err)
		return
	}
	parsedData, err := ebisusbay.Contract.Methods["makeListing"].Inputs.Unpack(decodedData)
	if err != nil {
		logrus.Warnf("failed to parse txn data: %s", err)
		return
	}
	newListing := ebisusbay.MakeListingData{
		TokenAddress: "0x" + common.Bytes2Hex(parsedData[0].(common.Address).Bytes()),
		TokenId:      parsedData[1].(*big.Int).Int64(),
		Amount:       parsedData[2].(*big.Int),
	}
	listingPriceHuman := big.NewInt(0).Div(newListing.Amount, math.BigPow(10, 18)).Int64()

	// verify its from the collection we are watching
	if len(targetAddresses) > 0 {
		found := false
		for _, address := range targetAddresses {
			if strings.ToLower(newListing.TokenAddress) == strings.ToLower(address) {
				found = true
				break
			}
		}
		if !found {
			logrus.Infof("skipped listing of token id %v from collection '%s'", newListing.TokenId, newListing.TokenAddress)
			return
		}
	} else if strings.ToLower(newListing.TokenAddress) != strings.ToLower(targetAddress) {
		logrus.Infof("skipped listing of token id %v from collection '%s'", newListing.TokenId, newListing.TokenAddress)
		return
	}

	// get listing data
	listingData, err := ebisusbayapi.GetListings(ebisusbayapi.GetListingOpts{
		Address: newListing.TokenAddress,
		TokenId: newListing.TokenId,
	})
	if err != nil {
		logrus.Warnf("failed to get listing information: %s\n\n%v", err, listingData)
		fmt.Printf(
			"token #%v from collection '%s' listed for %v cro\n",
			newListing.TokenId,
			newListing.TokenAddress,
			listingPriceHuman,
		)
		fmt.Printf("> [cronoscan](https://cronoscan.com/tx/%s)\n", event.TxHash)
		fmt.Printf("> [ebisusbay collection](https://app.ebisusbay.com/collection/%s)\n", newListing.TokenAddress)
		fmt.Printf("> [ebisusbay item](https://app.ebisusbay.com/collection/%s/%v)\n", newListing.TokenAddress, newListing.TokenId)
		return
	}
	targetListing := listingData.GetNewest()
	if targetListing == nil {
		logrus.Infof("skipped token[%v] of collection[%s], no listings found\n", newListing.TokenId, newListing.TokenAddress)
		return
	}
	fmt.Printf(
		"%s (token id: %s) listed for %v cro\n",
		listingData.Nft.Name,
		listingData.Nft.NftId,
		listingPriceHuman,
	)
	fmt.Printf("> [cronoscan](https://cronoscan.com/tx/%s)\n", event.TxHash)
	fmt.Printf("> [ebisusbay collection](https://app.ebisusbay.com/collection/%s)\n", newListing.TokenAddress)
	fmt.Printf("> [ebisusbay item](https://app.ebisusbay.com/collection/%s/%v)\n", newListing.TokenAddress, newListing.TokenId)
	fmt.Printf("> [ebisusbay listing](https://app.ebisusbay.com/listing/%v)\n", targetListing.ListingId)

	// verify the price is right
	logrus.Infof("verifying price threshold...")
	if float64(listingPriceHuman) > targetPrice {
		logrus.Infof("skipping purchase because listed price (%v cro) is higher than target price (%v cro)", listingPriceHuman, strconv.FormatFloat(targetPrice, 'f', 0, 64))
		return
	}

	// verify the rank is right
	logrus.Infof("verifying rank threshold...")
	if targetRank > 0 && listingData.Nft.Rank > targetRank {
		logrus.Infof("skipping purchase, listing rank (%v) is less than target rank (%v)", listingData.Nft.Rank, targetRank)
		return
	}

	// verify the traits - if defined - are right
	logrus.Infof("verifying traits match...")
	targetTraitsMap, err := traits.ListToMap(targetTraits)
	if err != nil {
		logrus.Warnf("failed to convert traits list into a traits map: %s", err)
	}
	if len(targetTraits) > 0 {
		matchedTraitsCount := 0
		listingTraits := []string{}
		for _, attribute := range listingData.Nft.Attributes {
			listingTraits = append(listingTraits, fmt.Sprintf("%s=%s", attribute.TraitType, attribute.Value))
			if targetTraitsMap[attribute.TraitType] == attribute.Value {
				logrus.Infof("matched trait type '%s' with trait value '%s'", attribute.TraitType, attribute.Value)
				matchedTraitsCount += 1
			}
		}
		if matchedTraitsCount != len(targetTraits) {
			logrus.Infof("skipping purchase because the traits ('%s') do not match the target traits ('%s')", strings.Join(listingTraits, "', '"), strings.Join(targetTraits, "', '"))
			return
		}
	}

	// send notification if notifications are enabled
	if telegramBotInstance != nil {
		telegram.SendListingNotification(telegramBotInstance, telegramChatId, listingData, targetListing, listingPriceHuman)
	}

	// all is good, make the purchase
	packedData, err := ebisusbay.NewMakePurchaseData(targetListing.ListingId)
	if err != nil {
		logrus.Warnf("failed to create makePurchase transaction data: %s", err)
		return
	}

	// calculate nonce and gas price
	logrus.Infof("creating transaction...")
	transaction, err := evmutils.CreateSignedTransaction(cronos.CreateTransactionOpts{
		Client:     client,
		GasLimit:   uint64(2000000),
		PrivateKey: privateKey,
		Data:       packedData,
		Value:      newListing.Amount,
		To:         common.HexToAddress(constants.EbisusBayContractAddress),
	})
	if err != nil {
		logrus.Warnf("failed to create signed transaction: %s", err)
		return
	}

	if telegramBotInstance != nil {
		telegram.SendPurchaseNotification(telegramBotInstance, telegramChatId, listingData, targetListing, transaction)
	}

	tableData := bytes.NewBuffer(nil)
	table := tablewriter.NewWriter(tableData)
	table.SetHeader([]string{"property", "value"})
	table.Append([]string{"cost", transaction.Cost().String()})
	table.Append([]string{"value", transaction.Value().String()})
	table.Append([]string{"gas price", transaction.GasPrice().String()})
	table.Append([]string{"gas", strconv.FormatUint(transaction.Gas(), 10)})
	table.Append([]string{"raw data", common.Bytes2Hex(transaction.Data())})
	table.Append([]string{"nonce", strconv.FormatUint(transaction.Nonce(), 10)})
	table.Render()
	logrus.Infof("sending transaction...\n%s", tableData.String())

	if err := client.SendTransaction(context.Background(), transaction); err != nil {
		logrus.Warnf("failed to send transaction: %s", err)
		telegram.SendSystemLog(telegramBotInstance, telegramChatId, fmt.Sprintf("failed to send transaction: %s", err))
		return
	}

	logrus.Infof("successfully sent transaction: %s", transaction.Hash().Hex())
}
