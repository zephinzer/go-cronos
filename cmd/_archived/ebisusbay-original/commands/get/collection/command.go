package collection

import (
	"bytes"
	"fmt"
	"strconv"
	"strings"
	"sync"

	"github.com/olekukonko/tablewriter"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/zephinzer/go-cronos/pkg/ebisusbayapi"
)

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:   "collection <collection_address>",
		Short: "Retrieves information about collections and their pieces",
		RunE:  run,
	}
	return &command
}

func run(command *cobra.Command, args []string) error {
	if len(args) == 0 {
		return fmt.Errorf("failed to find a collection address, specify it as the argument and try again")
	}

	address := args[0]
	logrus.Infof("using collection at address '%s'", address)

	var waiter sync.WaitGroup

	var collectionsData *ebisusbayapi.CollectionSummariesResponse
	var collectionData *ebisusbayapi.CollectionResponse
	var raritiesData *ebisusbayapi.RarityResponse
	errs := []error{}

	waiter.Add(1)
	go func() {
		var err error
		collectionsData, err = ebisusbayapi.GetCollections(ebisusbayapi.GetCollectionsOpts{Address: address})
		if err != nil {
			errs = append(errs, fmt.Errorf("failed to get ccollection[%s] from ebisus bay: %s", address, err))
		}
		waiter.Done()
	}()

	waiter.Add(1)
	go func() {
		var err error
		collectionData, err = ebisusbayapi.GetCollection(ebisusbayapi.GetCollectionOpts{
			Address:   address,
			SortBy:    ebisusbayapi.SortKeyPrice,
			Direction: ebisusbayapi.DirectionAscending,
		})
		if err != nil {
			logrus.Warnf("failed to get the floor: %s", err)
		}
		waiter.Done()
	}()

	waiter.Add(1)
	go func() {
		var err error
		raritiesData, err = ebisusbayapi.GetRarity(address)
		if err != nil {
			logrus.Warnf("failed to get the rarity chart: %s", err)
		}
		waiter.Done()
	}()

	waiter.Wait()

	traitCategories := []string{}
	traitMap := map[string][]string{}
	for traitCategory, traits := range *raritiesData {
		traitCategories = append(traitCategories, traitCategory)
		for traitName, traitValues := range traits {
			traitMap[traitCategory] = append(traitMap[traitCategory], traitName+fmt.Sprintf(" (%s%%)", strconv.FormatFloat(traitValues.Occurrence*100, 'f', 2, 64)))
		}
	}

	collection := collectionsData.Collections[0]

	var tableData bytes.Buffer
	table := tablewriter.NewWriter(&tableData)
	table.SetHeader([]string{"property", "value"})
	table.SetHeaderAlignment(tablewriter.ALIGN_LEFT)
	table.SetAlignment(tablewriter.ALIGN_LEFT)
	table.SetColMinWidth(1, 80)
	table.AppendBulk([][]string{
		{"Address", collection.Collection},
		{"Total sales", collection.NumberOfSales},
		{"Number of sales (1-day)", collection.Sales1D},
		{"Number of sales (7-day)", collection.Sales7D},
		{"Number of sales (30-day)", collection.Sales30D},
		{"Average price", collection.AverageSalePrice},
		{"Floor price", collection.FloorPrice},
		{"Active listings", collection.NumberActive},
		{"Trait categories", "\"" + strings.Join(traitCategories, "\" ,\"") + "\""},
	})
	for traitName, traitOccurrences := range traitMap {
		table.Append([]string{fmt.Sprintf("Trait \"%s\"", traitName), traitOccurrences[0]})
		for _, traitOccurrence := range traitOccurrences[1:] {
			table.Append([]string{"", traitOccurrence})
		}
	}
	table.Append([]string{"Floor pieces", "- - -"})
	for _, nft := range collectionData.Nfts {
		table.Append([]string{"  " + nft.Name, nft.Market.Price + " CRO | " + nft.Market.URI})
	}

	table.Render()

	fmt.Println(tableData.String())

	return nil
}
