package get

import (
	"github.com/spf13/cobra"
	"github.com/zephinzer/go-cronos/cmd/ebisusbay-original/commands/get/collection"
)

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:   "get",
		Short: "Retrieves information about collections and their pieces",
		RunE:  run,
	}
	command.AddCommand(collection.GetCommand())
	return &command
}

func run(command *cobra.Command, args []string) error {
	command.Help()
	return nil
}
