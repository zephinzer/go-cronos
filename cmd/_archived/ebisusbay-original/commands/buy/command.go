package buy

import (
	"bytes"
	"context"
	"fmt"
	"math/big"
	"strconv"
	"time"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/common/math"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/olekukonko/tablewriter"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/usvc/go-config"
	"github.com/zephinzer/go-cronos/cmd/ebisusbay-original/common/constants"
	"github.com/zephinzer/go-cronos/pkg/ebisusbay"
	"github.com/zephinzer/go-cronos/pkg/ebisusbayapi"
)

const (
	ConfigAddress    = "address"
	ConfigPrivateKey = "private-key"
	ConfigTokenId    = "token-id"
)

var conf = config.Map{
	ConfigAddress: &config.String{
		Shorthand: "a",
		Usage:     "specifies the address of the collection on ebisus bay",
	},
	ConfigPrivateKey: &config.String{
		Shorthand: "P",
		Usage:     "specifies the private key of your account",
	},
	ConfigTokenId: &config.Int{
		Shorthand: "t",
		Usage:     "specifies the token id to purchase",
	},
}

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:   "buy",
		Short: "Purchases an item from Ebisus Bay",
		RunE:  run,
	}
	conf.ApplyToCobra(&command)
	return &command
}

func run(command *cobra.Command, args []string) error {
	targetAddress := conf.GetString(ConfigAddress)
	targetTokenId := int64(conf.GetInt(ConfigTokenId))
	listingData, err := ebisusbayapi.GetListings(ebisusbayapi.GetListingOpts{
		Address: targetAddress,
		TokenId: targetTokenId,
	})
	if err != nil {
		return fmt.Errorf("failed to get listing data: %s", err)
	}
	latestListingIndex := 0
	for index, listing := range listingData.Listings {
		if listing.ListingTime > listingData.Listings[latestListingIndex].ListingTime {
			latestListingIndex = index
		}
		fmt.Printf("id[%v] being sold for %s cro, listed at %v\n", listing.ListingId, listing.Price, time.Unix(listing.ListingTime, 0))
	}

	targetListing := listingData.Listings[latestListingIndex]

	client, dialErr := ethclient.Dial("https://rpc.vvs.finance")
	if dialErr != nil {
		return fmt.Errorf("failed to create a client: %s", dialErr)
	}

	// all is good, make the purchase
	makePurchase := ebisusbay.MakePurchaseData{
		ListingID: big.NewInt(targetListing.ListingId),
	}
	packedData, err := ebisusbay.Contract.Methods["makePurchase"].Inputs.Pack(makePurchase.ListingID)
	if err != nil {
		return fmt.Errorf("failed to pack data for makePurchase txn: %s", err)

	}
	packedData = append(ebisusbay.Contract.Methods["makePurchase"].ID, packedData...)

	// grab private key of the configured address (config.yaml address => ./secrets/keys.json key => get value)
	privateKey, err := crypto.HexToECDSA(conf.GetString(ConfigPrivateKey))
	if err != nil {
		return fmt.Errorf("failed to parse private keys: %s", err)
	}
	// calculate nonce and gas price
	logrus.Infof("retrieving transaction details...")
	txnFromAddress := crypto.PubkeyToAddress(privateKey.PublicKey)
	txnNonce, err := client.PendingNonceAt(context.Background(), txnFromAddress)
	if err != nil {
		logrus.Warnf("failed to get pending nonce: %s", err)
	}
	txnGasPrice, err := client.SuggestGasPrice(context.Background())
	if err != nil {
		logrus.Warnf("failed to get suggested gas price: %s", err)
	}
	txnGasLimit := uint64(2000000)
	price, err := strconv.ParseFloat(targetListing.Price, 64)
	if err != nil {
		return fmt.Errorf("failed to convert price to a big.Int: %s", err)
	}
	txnValue := big.NewInt(0).Mul(big.NewInt(int64(price)), math.BigPow(10, 18))

	chainId, err := client.NetworkID(context.Background())
	if err != nil {
		logrus.Warnf("failed to get chain network id: %s", err)
	}

	txnToAddress := common.HexToAddress(constants.EbisusBayContractAddress)
	txnData := types.DynamicFeeTx{
		ChainID:   chainId,
		Nonce:     txnNonce,
		To:        &txnToAddress,
		GasFeeCap: txnGasPrice,
		Gas:       txnGasLimit,
		Value:     txnValue,
		Data:      packedData,
	}
	signedTxnInstance, err := types.SignNewTx(privateKey, types.NewLondonSigner(chainId), &txnData)
	if err != nil {
		logrus.Warnf("failed to sign transaction: %s", err)
	}

	tableData := bytes.NewBuffer(nil)
	table := tablewriter.NewWriter(tableData)
	table.SetHeader([]string{"property", "value"})
	table.Append([]string{"cost", signedTxnInstance.Cost().String()})
	table.Append([]string{"value", signedTxnInstance.Value().String()})
	table.Append([]string{"gas price", signedTxnInstance.GasPrice().String()})
	table.Append([]string{"gas", strconv.FormatUint(signedTxnInstance.Gas(), 10)})
	table.Append([]string{"raw data", common.Bytes2Hex(signedTxnInstance.Data())})
	table.Append([]string{"nonce", strconv.FormatUint(signedTxnInstance.Nonce(), 10)})
	table.Render()
	logrus.Infof("sending transaction...\n%s", tableData.String())

	if err := client.SendTransaction(context.Background(), signedTxnInstance); err != nil {
		return fmt.Errorf("failed to send transaction: %s", err)
	}

	logrus.Infof("successfully sent transaction: %s", signedTxnInstance.Hash().Hex())

	return nil
}

func Buy() {

}
