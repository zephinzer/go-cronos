package main

import (
	"fmt"
	"strings"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/zephinzer/go-cronos/cmd/ebisusbay-original/commands/buy"
	"github.com/zephinzer/go-cronos/cmd/ebisusbay-original/commands/get"
	"github.com/zephinzer/go-cronos/cmd/ebisusbay-original/commands/snipe"
	"github.com/zephinzer/go-cronos/cmd/ebisusbay-original/commands/watch"
	"github.com/zephinzer/go-cronos/cmd/ebisusbay-original/common/constants"
)

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:   "ebisusbay",
		Short: "Utility tool to do things on Ebisus Bay",
		RunE:  run,
	}
	command.AddCommand(buy.GetCommand())
	command.AddCommand(get.GetCommand())
	command.AddCommand(snipe.GetCommand())
	command.AddCommand(watch.GetCommand())
	return &command
}

func run(command *cobra.Command, args []string) error {
	command.Help()
	return nil
}

func main() {
	fmt.Println(strings.Trim(constants.EbisusBayAsciiArt, "\n"))
	command := GetCommand()
	if err := command.Execute(); err != nil {
		command.Help()
		fmt.Println("")
		logrus.Errorf("failed to complete: %s", err)
	}
}
