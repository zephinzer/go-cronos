CREATE TABLE IF NOT EXISTS `ebz_transactions` (
  id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  address_token VARCHAR(44),
  address_triggerer VARCHAR(44) NOT NULL,
  ebisus_hash VARCHAR(128),
  listing_id BIGINT UNSIGNED,
  token_id BIGINT UNSIGNED,
  type VARCHAR(32) NOT NULL,
  value FLOAT NOT NULL DEFAULT(-1),
  last_modified TIMESTAMP NOT NULL DEFAULT NOW(),

  PRIMARY KEY(id)
);
