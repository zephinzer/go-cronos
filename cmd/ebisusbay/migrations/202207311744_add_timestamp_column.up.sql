ALTER TABLE `ebz_collection_stats`
  ADD COLUMN `timestamp` TIMESTAMP DEFAULT NOW() AFTER `last_modified`;
