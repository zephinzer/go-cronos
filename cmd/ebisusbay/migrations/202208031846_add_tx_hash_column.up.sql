ALTER TABLE `ebz_transactions`
  ADD COLUMN `transaction_hash` VARCHAR(66) AFTER `id`,
  ADD CONSTRAINT transaction_hash_unique UNIQUE(transaction_hash);
