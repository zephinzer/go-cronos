CREATE TABLE IF NOT EXISTS `ebz_collection_stats` (
  id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  last_modified TIMESTAMP NOT NULL DEFAULT NOW(),
  
  address_token VARCHAR(44),
  
  active_listings_count INT UNSIGNED,
  avg_sale_price FLOAT NOT NULL DEFAULT(-1),
  floor_price FLOAT NOT NULL DEFAULT(-1),
  sales_count_total INT UNSIGNED,
  sales_count_1d INT UNSIGNED,
  sales_count_7d INT UNSIGNED,
  sales_count_30d INT UNSIGNED,
  volume_total FLOAT NOT NULL DEFAULT(-1),
  volume_1d FLOAT NOT NULL DEFAULT(-1),
  volume_7d FLOAT NOT NULL DEFAULT(-1),
  volume_30d FLOAT NOT NULL DEFAULT(-1),

  PRIMARY KEY(id)
);
