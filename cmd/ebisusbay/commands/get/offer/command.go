package offer

import (
	"encoding/json"
	"fmt"

	"github.com/spf13/cobra"
	"github.com/zephinzer/go-cronos/internal/log"
	"github.com/zephinzer/go-cronos/pkg/ebisusbayapi"
)

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:   "offer <offer-hash>",
		Short: "Retrieves offer information from EbisusBay API given an offer hash",
		RunE:  runE,
	}
	return &command
}

func runE(command *cobra.Command, args []string) error {
	if len(args) != 1 {
		return fmt.Errorf("failed to receive 1 expected argument of <offer-hash>")
	}
	offerHash := args[0]
	log.Infof("retrieving offer information for offer[%s]", offerHash)

	offerResponse, err := ebisusbayapi.GetOfferByHash(offerHash)
	if err != nil {
		return fmt.Errorf("failed to retrieve data for offer[%s]: %s", offerHash, err)
	}
	offerResponseJson, err := json.MarshalIndent(offerResponse, "", "  ")
	if err != nil {
		return fmt.Errorf("failed to marshal offer data response into json: %s", err)
	}
	fmt.Println(string(offerResponseJson))
	return nil
}
