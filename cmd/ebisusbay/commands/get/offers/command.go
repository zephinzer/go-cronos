package offers

import (
	"encoding/json"
	"fmt"
	"strconv"

	"github.com/ethereum/go-ethereum/common"
	"github.com/spf13/cobra"
	"github.com/zephinzer/go-cronos/internal/log"
	"github.com/zephinzer/go-cronos/pkg/ebisusbayapi"
)

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:   "offers <address> <token-id>",
		Short: "Retrieves offer information from EbisusBay API given a collection address and token ID",
		RunE:  runE,
	}
	return &command
}

func runE(command *cobra.Command, args []string) error {
	if len(args) != 2 {
		return fmt.Errorf("failed to receive 2 expected arguments of <address> and <token-id>")
	}
	address := common.HexToAddress(args[0])
	tokenId, err := strconv.ParseInt(args[1], 10, 64)
	if err != nil {
		return fmt.Errorf("failed to parse token id[%v] into an int64: %s", tokenId, err)
	}
	log.Infof("retrieving offers for token[%v] of collection[%s]", tokenId, address)

	offersResponse, err := ebisusbayapi.GetOffers(ebisusbayapi.GetOffersOpts{
		Address: address.String(),
		TokenId: tokenId,
	})
	if err != nil {
		return fmt.Errorf("failed to retrieve offers for token[%v] of collection[%v]", tokenId, address)
	}
	offersResponseJson, err := json.Marshal(offersResponse)
	if err != nil {
		return fmt.Errorf("failed to marshal offers response into json: %s", err)
	}
	fmt.Println(string(offersResponseJson))
	return nil
}
