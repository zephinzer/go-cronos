package get

import (
	"github.com/spf13/cobra"
	"github.com/zephinzer/go-cronos/cmd/ebisusbay/commands/get/offer"
	"github.com/zephinzer/go-cronos/cmd/ebisusbay/commands/get/offers"
)

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:   "get",
		Short: "Retrieves stuff from EbisusBay API",
		RunE:  runE,
	}
	command.AddCommand(offer.GetCommand())
	command.AddCommand(offers.GetCommand())
	return &command
}

func runE(command *cobra.Command, args []string) error {
	return command.Help()
}
