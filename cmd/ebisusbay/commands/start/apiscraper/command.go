package apiscraper

import (
	"fmt"
	"strings"
	"time"

	"github.com/spf13/cobra"
	"github.com/zephinzer/go-cronos/cmd/ebisusbay/common/types"
	"github.com/zephinzer/go-cronos/internal/constants"
	"github.com/zephinzer/go-cronos/internal/database"
	"github.com/zephinzer/go-cronos/internal/healthcheck"
	"github.com/zephinzer/go-cronos/internal/log"
	"github.com/zephinzer/go-cronos/pkg/addressbook"
	"github.com/zephinzer/go-cronos/pkg/ebisusbayapi"
)

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:   "apiscraper",
		Short: "Starts the ebisusbay apiscraper service",
		RunE:  runE,
	}
	conf.ApplyToCobra(&command)
	return &command
}

func runE(command *cobra.Command, args []string) error {
	log.Infof("starting ebisusbay...")
	configurationInstance, configError := InitConfiguration(conf)
	if configError != nil {
		return fmt.Errorf("failed to receive a valid configuration: %s", configError)
	}

	selectedChain := addressbook.Chain[conf.GetString(ConfigKeyChain)]
	if selectedChain == nil {
		return fmt.Errorf("failed to retrieve chain[%s]", conf.GetString(ConfigKeyChain))
	}

	// TODO(joseph) - set these as some const/conf
	retryInterval := time.Second * 5
	dbMaxRetries := 10

	log.Infof("connecting to application db...")
	appDb, err := database.GetConnection(database.GetConnectionOpts{
		Username:      configurationInstance.DatabaseUsername,
		Password:      configurationInstance.DatabasePassword,
		Host:          configurationInstance.DatabaseHost,
		Port:          uint64(configurationInstance.DatabasePort),
		Database:      configurationInstance.DatabaseName,
		RetryInterval: retryInterval,
		MaxRetryCount: uint64(dbMaxRetries),
		Log:           func(l string, a ...any) { log.Infof(l, a...) },
		LogError:      func(l string, a ...any) { log.Warnf(l, a...) },
	})
	if err != nil {
		return fmt.Errorf("failed to establish db connection: %s", err)
	}

	go healthcheck.StartServer("0.0.0.0", 8000)

	timestampFormat := constants.TimestampYmdHMS
	for {
		timeStarted := time.Now()
		log.Infof("starting request at %v", timeStarted.Format(timestampFormat))

		collections, err := ebisusbayapi.GetCollections(ebisusbayapi.GetCollectionsOpts{
			SortBy:    ebisusbayapi.SortKeyTotalVolume,
			Direction: ebisusbayapi.DirectionDescending,
		})
		if err != nil {
			log.Warnf("failed to get collections: %s", err)
		}

		timeTaken := time.Since(timeStarted)
		log.Infof("request took %v ms", timeTaken.Milliseconds())
		timeStarted = time.Now()

		collectionSummaryList := types.CollectionSummaryList{}
		for _, collection := range collections.Collections {
			collectionAddress := strings.ToLower(collection.Collection)
			if dictEntry, ok := selectedChain.Directory[collectionAddress]; ok {
				log.Infof("added %s (%s...%s)", dictEntry.Label, dictEntry.Address[:6], dictEntry.Address[len(dictEntry.Address)-4:])
				collectionSummaryList = append(collectionSummaryList, types.CollectionSummary(collection))
			}
		}

		log.Infof("inserting %v collection summaries into the db...", len(collectionSummaryList))
		if err := collectionSummaryList.Insert(appDb); err != nil {
			return fmt.Errorf("failed to insert all collection summaries into the db: %s", err)
		}

		timeTaken = time.Since(timeStarted)
		log.Infof("processing took %v ms", timeTaken.Milliseconds())

		// log.Infof("queried %v txs from %s -> %s", len(transactions), lastQueriedString, time.Now().UTC().Format(timestampFormat))
		<-time.After(configurationInstance.IntervalDuration)
	}
	return nil
}
