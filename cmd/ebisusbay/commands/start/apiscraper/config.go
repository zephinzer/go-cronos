package apiscraper

import (
	"fmt"
	"strings"
	"time"

	"github.com/usvc/go-config"
	"github.com/zephinzer/go-cronos/cmd/ebisusbay/common/configuration"
	"github.com/zephinzer/go-cronos/pkg/addressbook"
)

const (
	ConfigKeyChain                   = "chain"
	ConfigKeyIntervalDurationSeconds = "interval-duration-s"
	DefaultIntervalDurationSeconds   = 30
)

var conf = config.Map{
	configuration.KeyDatabaseHost:     configuration.GetDatabaseHost(),
	configuration.KeyDatabasePort:     configuration.GetDatabasePort(),
	configuration.KeyDatabaseName:     configuration.GetDatabaseName(),
	configuration.KeyDatabaseUsername: configuration.GetDatabaseUsername(),
	configuration.KeyDatabasePassword: configuration.GetDatabasePassword(),
	ConfigKeyChain: &config.String{
		Shorthand: "c",
		Usage:     fmt.Sprintf("slug of chain (available options: [%s])", strings.Join(addressbook.AvailableChains, ", ")),
		Default:   "cronos",
	},
	ConfigKeyIntervalDurationSeconds: &config.Uint{
		Shorthand: "i",
		Usage:     "duration between scrapes of ebisus bay api data",
		Default:   DefaultIntervalDurationSeconds,
	},
}

func InitConfiguration(conf config.Map) (*commandConfiguration, error) {
	configurationInstance := &commandConfiguration{
		DatabaseHost:     conf.GetString(configuration.KeyDatabaseHost),
		DatabasePort:     conf.GetUint(configuration.KeyDatabasePort),
		DatabaseUsername: conf.GetString(configuration.KeyDatabaseUsername),
		DatabasePassword: conf.GetString(configuration.KeyDatabasePassword),
		DatabaseName:     conf.GetString(configuration.KeyDatabaseName),
		IntervalDuration: time.Duration(conf.GetUint(ConfigKeyIntervalDurationSeconds) * uint(time.Second)),
	}
	return configurationInstance, nil
}

type commandConfiguration struct {
	DatabaseHost     string
	DatabasePort     uint
	DatabaseUsername string
	DatabasePassword string
	DatabaseName     string
	IntervalDuration time.Duration
}

func (c *commandConfiguration) GetMySqlDsn() string {
	return fmt.Sprintf(
		"%s:%s@tcp(%s:%v)/%s?multiStatements=true",
		c.DatabaseUsername,
		c.DatabasePassword,
		c.DatabaseHost,
		c.DatabasePort,
		c.DatabaseName,
	)
}

func (c commandConfiguration) GetPublicMySqlConnectionString() string {
	return fmt.Sprintf(
		"%s@%s:%v/%s?multiStatements=true",
		c.DatabaseUsername,
		c.DatabaseHost,
		c.DatabasePort,
		c.DatabaseName,
	)
}
