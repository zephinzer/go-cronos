package start

import (
	"github.com/spf13/cobra"
	"github.com/zephinzer/go-cronos/cmd/ebisusbay/commands/start/apiscraper"
	"github.com/zephinzer/go-cronos/cmd/ebisusbay/commands/start/checks"
	"github.com/zephinzer/go-cronos/cmd/ebisusbay/commands/start/farmer"
)

var targetAddres string

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:   "start",
		Short: "Starts a worker of the ebisusbay function",
		RunE:  runE,
	}
	command.AddCommand(apiscraper.GetCommand())
	command.AddCommand(checks.GetCommand())
	command.AddCommand(farmer.GetCommand())
	return &command
}

func runE(command *cobra.Command, args []string) error {
	return command.Help()
}
