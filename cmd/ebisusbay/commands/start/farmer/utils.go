package farmer

import (
	"database/sql"
	"fmt"

	"github.com/go-sql-driver/mysql"
	"github.com/zephinzer/go-cronos/internal/log"
)

type logTransactionOpts struct {
	DB               *sql.DB
	TransactionId    uint64
	TransactionHash  string
	Hash             *string
	ListingId        *uint64
	TokenAddress     *string
	TokenId          *uint64
	TriggererAddress string
	Type             string
	Value            float64
}

func logTransaction(opts logTransactionOpts) error {
	stmt, err := opts.DB.Prepare(`
	INSERT INTO ebz_transactions (
		transaction_id,
		transaction_hash,
		address_token,
		address_triggerer,
		ebisus_hash,
		listing_id,
		token_id,
		type,
		value
	) VALUES (
		?,
		?,
		?,
		?,
		?,
		?,
		?,
		?,
		?
	)
`)
	if err != nil {
		return fmt.Errorf("failed to prepare database statement: %s", err)
	}
	defer stmt.Close()
	if _, err := stmt.Exec(
		opts.TransactionId,
		opts.TransactionHash,
		opts.TokenAddress,
		opts.TriggererAddress,
		opts.Hash,
		opts.ListingId,
		opts.TokenId,
		opts.Type,
		opts.Value,
	); err != nil {
		if mysqlError, ok := err.(*mysql.MySQLError); ok && mysqlError.Number == 1062 {
			log.Infof("transaction[%v] already exists", opts.TransactionHash)
			return nil
		}
		return fmt.Errorf("failed to execute insertion into transactions table: %s", err)
	}
	return nil
}
