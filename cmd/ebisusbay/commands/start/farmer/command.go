package farmer

import (
	"encoding/hex"
	"encoding/json"
	"fmt"
	"math/big"
	"strconv"
	"strings"
	"time"

	"github.com/ethereum/go-ethereum/common/math"

	"github.com/ethereum/go-ethereum/common"
	"github.com/spf13/cobra"
	"github.com/zephinzer/go-cronos/internal/database"
	"github.com/zephinzer/go-cronos/internal/healthcheck"
	"github.com/zephinzer/go-cronos/internal/log"
	"github.com/zephinzer/go-cronos/pkg/addressbook"
	"github.com/zephinzer/go-cronos/pkg/ebisusbay"
	"github.com/zephinzer/go-cronos/pkg/ebisusbayapi"
)

var targetAddres string

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:   "farmer",
		Short: "Starts the ebisusbay farmer service",
		RunE:  runE,
	}
	conf.ApplyToCobra(&command)
	return &command
}

func runE(command *cobra.Command, args []string) error {
	contractAddress, err := addressbook.GetAddress("cronos", "ebzbay")
	if err != nil {
		return fmt.Errorf("failed to find address of ebisus bay in the known addresses")
	}
	ebisusBayAddress := strings.ToLower(contractAddress.String())
	contractOffersAddress, err := addressbook.GetAddress("cronos", "ebzbay-offers")
	if err != nil {
		return fmt.Errorf("failed to find address of ebisus bay in the known addresses")
	}
	ebisusBayOffersAddress := strings.ToLower(contractOffersAddress.String())

	log.Infof("starting ebisusbay...")
	configurationInstance, configError := InitConfiguration(conf)
	if configError != nil {
		return fmt.Errorf("failed to receive a valid configuration: %s", configError)
	}

	// TODO(joseph) - set these as some const/conf
	retryInterval := time.Second * 5
	dbMaxRetries := 10

	log.Infof("connecting to application db...")
	appDb, err := database.GetConnection(database.GetConnectionOpts{
		Username:      configurationInstance.DatabaseUsername,
		Password:      configurationInstance.DatabasePassword,
		Host:          configurationInstance.DatabaseHost,
		Port:          uint64(configurationInstance.DatabasePort),
		Database:      configurationInstance.DatabaseName,
		RetryInterval: retryInterval,
		MaxRetryCount: uint64(dbMaxRetries),
		Log:           func(l string, a ...any) { log.Infof(l, a...) },
		LogError:      func(l string, a ...any) { log.Warnf(l, a...) },
	})

	log.Infof("connecting to master db...")
	masterDb, err := database.GetConnection(database.GetConnectionOpts{
		Username:      configurationInstance.MasterDatabaseUsername,
		Password:      configurationInstance.MasterDatabasePassword,
		Host:          configurationInstance.MasterDatabaseHost,
		Port:          uint64(configurationInstance.MasterDatabasePort),
		Database:      configurationInstance.MasterDatabaseName,
		RetryInterval: retryInterval,
		MaxRetryCount: uint64(dbMaxRetries),
		Log:           func(l string, a ...any) { log.Infof(l, a...) },
		LogError:      func(l string, a ...any) { log.Warnf(l, a...) },
	})
	if err != nil {
		return fmt.Errorf("failed to create db connection: %s", err)
	}

	log.Infof("getting last queried item timestamp...")
	var lastQueried time.Time
	lastQueriedTimestamp := appDb.QueryRow(`SELECT MAX(last_modified) FROM ebz_transactions`)
	if lastQueriedTimestamp.Err() != nil {
		return fmt.Errorf("failed to retrieve timestamp of the last known transaction: %s", err)
	}
	lastQueriedTimestamp.Scan(&lastQueried)
	log.Infof("continuing from timestamp[%v]", lastQueried)

	log.Infof("preparing query to get last processed id...")
	log.Infof("preparing query to get last processed id...")
	var lastProcessedId uint64
	getLastProcessedId := appDb.QueryRow(`SELECT MAX(transaction_id) FROM ebz_transactions`)
	if getLastProcessedId.Err() != nil {
		return fmt.Errorf("failed to retrieve transaction_id of the last known transaction: %s", err)
	}
	getLastProcessedId.Scan(&lastProcessedId)

	type transactionRecord struct {
		Id          uint64
		Hash        string
		AddressFrom *string
		AddressTo   *string
		BlockId     uint64
		Data        *string
		Cost        float64
		Value       float64
	}
	getAllTransactionsAfterId, err := masterDb.Prepare(`
		SELECT
			id,
			hash,
			address_from,
			address_to,
			block_number,
			data,
			cost,
			value
		FROM
			transactions
		WHERE
			(id > ?)
			AND
			(
				LOWER(address_from) IN (?,?) OR
				LOWER(address_to) IN (?,?)
			)
		ORDER BY
			id
	`)

	go healthcheck.StartServer("0.0.0.0", 8000)

	for {
		log.Infof("querying items[id > %v]", lastProcessedId)
		rows, err := getAllTransactionsAfterId.Query(
			lastProcessedId,
			ebisusBayAddress,
			ebisusBayOffersAddress,
			ebisusBayAddress,
			ebisusBayOffersAddress,
		)
		if err != nil {
			log.Warnf("failed to perform db query: %s", err)
		}
		transactionRecordsList := []transactionRecord{}
		for rows.Next() {
			var transactionRecordInstance transactionRecord
			if err := rows.Scan(
				&transactionRecordInstance.Id,
				&transactionRecordInstance.Hash,
				&transactionRecordInstance.AddressFrom,
				&transactionRecordInstance.AddressTo,
				&transactionRecordInstance.BlockId,
				&transactionRecordInstance.Data,
				&transactionRecordInstance.Cost,
				&transactionRecordInstance.Value,
			); err != nil {
				log.Warnf("failed to scan row: %s", err)
				continue
			}
			transactionRecordsList = append(transactionRecordsList, transactionRecordInstance)
			if lastProcessedId < transactionRecordInstance.Id {
				lastProcessedId = transactionRecordInstance.Id
			}
		}
		for _, transactionRecordInstance := range transactionRecordsList {
			log.Infof("block[%v]: tx[%s]", transactionRecordInstance.BlockId, transactionRecordInstance.Hash)
			if transactionRecordInstance.Data != nil && len(*transactionRecordInstance.Data) > 8 {
				data := *transactionRecordInstance.Data
				methodId := common.Hex2Bytes(data[:8])
				method, err := ebisusbay.Contract.MethodById(methodId)
				if err != nil {
					log.Warnf("failed to get method by id[%s]: %s", methodId, err)
					continue
				}
				decodedData, err := hex.DecodeString(data[8:])
				if err != nil {
					log.Warnf("failed to decode txn data: %s", err)
					continue
				}
				transactionData, err := method.Inputs.Unpack(decodedData)
				if err != nil {
					log.Warnf("failed to unpack '%s'", data[8:])
					continue
				}

				addressFrom := "(unknown)"
				if transactionRecordInstance.AddressFrom != nil && len(*transactionRecordInstance.AddressFrom) > 0 {
					addressFrom = *transactionRecordInstance.AddressFrom
				}

				switch method.Name {
				case "makeCollectionOffer":
					address := "0x" + common.Bytes2Hex(transactionData[0].(common.Address).Bytes())
					amount := transactionRecordInstance.Value
					log.Infof("collection offer of %v CRO made on collection[%s] by account[%s]",
						amount,
						address,
						addressFrom,
					)
					if err := logTransaction(logTransactionOpts{
						DB:               appDb,
						TransactionId:    transactionRecordInstance.Id,
						TransactionHash:  transactionRecordInstance.Hash,
						TokenAddress:     &address,
						TriggererAddress: addressFrom,
						Type:             "makeCollectionOffer",
						Value:            transactionRecordInstance.Value,
					}); err != nil {
						log.Warnf("failed to insert makeCollectionOffer tx[%s] to db: %s", transactionRecordInstance.Hash, err)
					}

				case "updateCollectionOffer":
					address := "0x" + common.Bytes2Hex(transactionData[0].(common.Address).Bytes())
					amount := transactionRecordInstance.Value
					log.Infof("collection offer on collection[%s] updated to %v by account[%s]",
						address,
						amount,
						addressFrom,
					)
					if err := logTransaction(logTransactionOpts{
						DB:               appDb,
						TransactionId:    transactionRecordInstance.Id,
						TransactionHash:  transactionRecordInstance.Hash,
						TokenAddress:     &address,
						TriggererAddress: addressFrom,
						Type:             "updateCollectionOffer",
						Value:            transactionRecordInstance.Value,
					}); err != nil {
						log.Warnf("failed to insert updateCollectionOffer tx[%s] to db: %s", transactionRecordInstance.Hash, err)
					}

				case "acceptCollectionOffer":
					tokenAddress := "0x" + common.Bytes2Hex(transactionData[0].(common.Address).Bytes())
					offerIndex := transactionData[1].(*big.Int).Int64()
					tokenId := transactionData[2].(*big.Int).Uint64()
					log.Infof(
						"collection offer[%v] on collection[%v]/token[%v] accepted by account[%s]",
						offerIndex,
						tokenAddress,
						tokenId,
						addressFrom,
					)
					if err := logTransaction(logTransactionOpts{
						DB:               appDb,
						TransactionId:    transactionRecordInstance.Id,
						TransactionHash:  transactionRecordInstance.Hash,
						TokenAddress:     &tokenAddress,
						TriggererAddress: addressFrom,
						TokenId:          &tokenId,
						Type:             "acceptOffer",
						Value:            -1,
					}); err != nil {
						log.Warnf("failed to insert acceptCollectionOffer tx[%s] to db: %s", transactionRecordInstance.Hash, err)
					}

				case "makeOffer":
					address := "0x" + common.Bytes2Hex(transactionData[0].(common.Address).Bytes())
					tokenId := transactionData[1].(*big.Int).Uint64()
					log.Infof("retrieving offers made for token id[%v]...", tokenId)
					offers, err := ebisusbayapi.GetOffers(ebisusbayapi.GetOffersOpts{
						Address: address,
						TokenId: int64(tokenId),
					})
					if err != nil {
						log.Warnf(
							"failed to get offer info for offer on token[%v] of collection[%s] by account[%s]",
							tokenId,
							address,
							addressFrom,
						)
					}
					offerHash := "UNKNOWN"
					if offers != nil {
						if offers.Data != nil && offers.Data.Offers != nil {
							offerInstance := offers.Data.Offers.GetLatestOfferBy(addressFrom)
							if offerInstance != nil {
								offerHash = offerInstance.Hash
							}
						} else {
							offersDebug, _ := json.Marshal(offers)
							log.Warnf("failed to receive valid offers data: %s", string(offersDebug))
						}
					}
					log.Infof(
						"offer made on token[%v] of collection[%s] for %v CRO by account[%s] (hash: %s)",
						tokenId,
						address,
						transactionRecordInstance.Value,
						addressFrom,
						offerHash,
					)
					if err := logTransaction(logTransactionOpts{
						DB:               appDb,
						TransactionId:    transactionRecordInstance.Id,
						TransactionHash:  transactionRecordInstance.Hash,
						Hash:             &offerHash,
						TokenAddress:     &address,
						TokenId:          &tokenId,
						TriggererAddress: addressFrom,
						Type:             "makeOffer",
						Value:            transactionRecordInstance.Value,
					}); err != nil {
						log.Warnf("failed to insert makeOffer tx[%s] to db: %s", transactionRecordInstance.Hash, err)
					}

				case "updateOffer":
					hashBytes := [32]byte(transactionData[0].([32]uint8))
					hash := "0x" + common.BytesToHash(hashBytes[:]).String()
					offerIndex := transactionData[1].(*big.Int).Int64()
					offers, err := ebisusbayapi.GetOfferByHash(hash)
					if err != nil {
						log.Warnf("failed to get offer by hash[%s]: %s", hash, err)
					}
					tokenAddress := "unknown"
					tokenId := "unknown"
					offerPrice := transactionRecordInstance.Value
					if offers.Data != nil && offers.Data.Offers != nil && len(offers.Data.Offers) > 0 {
						targetOffer := offers.Data.Offers[0]
						tokenAddress = targetOffer.NftAddress
						tokenId = targetOffer.NftId
					}
					log.Infof(
						"offer[index[%v] on hash[%s]] for token[%v] in collection[%v] was updated to %s CRO by account[%s]",
						offerIndex,
						hash,
						tokenId,
						tokenAddress,
						offerPrice,
						addressFrom,
					)
					var loggedTokenId *uint64 = nil
					parsedTokenId, err := strconv.ParseUint(tokenId, 10, 64)
					if err == nil {
						loggedTokenId = &parsedTokenId
					}
					if err := logTransaction(logTransactionOpts{
						DB:               appDb,
						TransactionId:    transactionRecordInstance.Id,
						TransactionHash:  transactionRecordInstance.Hash,
						Hash:             &hash,
						TokenAddress:     &tokenAddress,
						TriggererAddress: addressFrom,
						TokenId:          loggedTokenId,
						Type:             "updateOffer",
						Value:            offerPrice,
					}); err != nil {
						log.Warnf("failed to insert updateOffer tx[%s] to db: %s", transactionRecordInstance.Hash, err)
					}

				case "cancelOffer":
					hashBytes := [32]byte(transactionData[0].([32]uint8))
					hash := "0x" + common.BytesToHash(hashBytes[:]).String()
					offerIndex := transactionData[1].(*big.Int).Int64()
					offers, err := ebisusbayapi.GetOfferByHash(hash)
					if err != nil {
						log.Warnf("failed to get offer by hash[%s]: %s", hash, err)
					}
					tokenAddress := "unknown"
					tokenId := "unknown"
					offerPrice := "unknown"
					if offers.Data != nil && offers.Data.Offers != nil && len(offers.Data.Offers) > 0 {
						targetOffer := offers.Data.Offers[0]
						tokenAddress = targetOffer.NftAddress
						tokenId = targetOffer.NftId
						offerPrice = targetOffer.Price
					}
					log.Infof(
						"offer[index[%v] on hash[%s]] for token[%v] in collection[%v] for %s CRO cancelled by account[%s]",
						offerIndex,
						hash,
						tokenId,
						tokenAddress,
						offerPrice,
						addressFrom,
					)
					var loggedTokenId *uint64 = nil
					parsedTokenId, err := strconv.ParseUint(tokenId, 10, 64)
					if err == nil {
						loggedTokenId = &parsedTokenId
					}
					loggedOfferPrice, _ := strconv.ParseFloat(offerPrice, 64)
					if err := logTransaction(logTransactionOpts{
						DB:               appDb,
						TransactionId:    transactionRecordInstance.Id,
						TransactionHash:  transactionRecordInstance.Hash,
						Hash:             &hash,
						TokenAddress:     &tokenAddress,
						TriggererAddress: addressFrom,
						TokenId:          loggedTokenId,
						Type:             "cancelOffer",
						Value:            loggedOfferPrice,
					}); err != nil {
						log.Warnf("failed to insert cancelOffer tx[%s] to db: %s", transactionRecordInstance.Hash, err)
					}

				case "acceptOffer":
					hashBytes := [32]byte(transactionData[0].([32]uint8))
					hash := "0x" + common.BytesToHash(hashBytes[:]).String()
					offerIndex := transactionData[1].(*big.Int).Int64()
					offers, err := ebisusbayapi.GetOfferByHash(hash)
					if err != nil {
						log.Warnf("failed to get offer data from offer hash[%s]: %s", hash, err)
					}
					tokenAddress := "unknown"
					tokenId := "unknown"
					offerPrice := "unknown"
					if offers.Data != nil && offers.Data.Offers != nil && len(offers.Data.Offers) > 0 {
						targetOffer := offers.Data.Offers[0]
						tokenAddress = targetOffer.NftAddress
						tokenId = targetOffer.NftId
						offerPrice = targetOffer.Price
					}
					log.Infof(
						"offer[index[%v] on hash[%s]] for token[%v] of collection[%s] for %v CRO accepted by account[%s]",
						offerIndex,
						hash,
						tokenId,
						tokenAddress,
						offerPrice,
						addressFrom,
					)
					var loggedTokenId *uint64 = nil
					parsedTokenId, err := strconv.ParseUint(tokenId, 10, 64)
					if err == nil {
						loggedTokenId = &parsedTokenId
					}
					loggedOfferPrice, _ := strconv.ParseFloat(offerPrice, 64)
					if err := logTransaction(logTransactionOpts{
						DB:               appDb,
						TransactionId:    transactionRecordInstance.Id,
						TransactionHash:  transactionRecordInstance.Hash,
						Hash:             &hash,
						TokenAddress:     &tokenAddress,
						TriggererAddress: addressFrom,
						TokenId:          loggedTokenId,
						Type:             "acceptOffer",
						Value:            loggedOfferPrice,
					}); err != nil {
						log.Warnf("failed to insert acceptOffer tx[%s] to db: %s", transactionRecordInstance.Hash, err)
					}

				case "rejectOffer":
					hashBytes := [32]byte(transactionData[0].([32]uint8))
					hash := "0x" + common.BytesToHash(hashBytes[:]).String()
					offerIndex := transactionData[1].(*big.Int).Int64()
					log.Infof("offer[%s] was rejected", hash)
					offers, err := ebisusbayapi.GetOfferByHash(hash)
					if err != nil {
						log.Warnf("failed to get offer by hash[%s]: %s", hash, err)
					}
					tokenAddress := "unknown"
					tokenId := "unknown"
					offerPrice := "unknown"
					if offers.Data != nil && offers.Data.Offers != nil && len(offers.Data.Offers) > 0 {
						targetOffer := offers.Data.Offers[0]
						tokenAddress = targetOffer.NftAddress
						tokenId = targetOffer.NftId
						offerPrice = targetOffer.Price
					}
					log.Infof(
						"offer[index[%v] on hash[%s]] for token[%v] of collection[%s] for %v CRO rejected by account[%s]",
						offerIndex,
						hash,
						tokenId,
						tokenAddress,
						offerPrice,
						addressFrom,
					)
					var loggedTokenId *uint64 = nil
					parsedTokenId, err := strconv.ParseUint(tokenId, 10, 64)
					if err == nil {
						loggedTokenId = &parsedTokenId
					}
					loggedOfferPrice, _ := strconv.ParseFloat(offerPrice, 64)
					if err := logTransaction(logTransactionOpts{
						DB:               appDb,
						TransactionId:    transactionRecordInstance.Id,
						TransactionHash:  transactionRecordInstance.Hash,
						Hash:             &hash,
						TokenAddress:     &tokenAddress,
						TriggererAddress: addressFrom,
						TokenId:          loggedTokenId,
						Type:             "rejectOffer",
						Value:            loggedOfferPrice,
					}); err != nil {
						log.Warnf("failed to insert rejectOffer tx[%s] to db: %s", transactionRecordInstance.Hash, err)
					}

				case "cancelListing":
					listingId := transactionData[0].(*big.Int).Int64()
					listing, err := ebisusbayapi.GetListingById(strconv.FormatInt(listingId, 10))
					if err != nil {
						log.Warnf("failed to get cancelled listing[%v]: %s", listingId, err)
						break
					}
					tokenAddress := "UNKNOWN"
					var tokenId *uint64
					var listingPrice float64
					if listing.Listings != nil && len(listing.Listings) > 0 {
						targetListing := listing.Listings[0]
						tokenAddress = targetListing.NftAddress
						parsedTokenId, err := strconv.ParseUint(targetListing.NftId, 10, 64)
						if err != nil {
							log.Warnf("failed to parse nft with id[%s]: %s", targetListing.NftId, err)
							parsedTokenId = 0
						}
						tokenId = &parsedTokenId
						parsedPrice, err := strconv.ParseFloat(targetListing.Price, 64)
						if err == nil {
							listingPrice = parsedPrice
						}
					}
					if tokenId == nil {
						zero := uint64(0)
						tokenId = &zero
					}
					log.Infof(
						"listing[%v] for token[%v] of collection[%s] cancelled by account[%s]",
						listingId,
						*tokenId,
						tokenAddress,
						addressFrom,
					)
					if err := logTransaction(logTransactionOpts{
						DB:               appDb,
						TransactionId:    transactionRecordInstance.Id,
						TransactionHash:  transactionRecordInstance.Hash,
						TokenAddress:     &tokenAddress,
						TriggererAddress: addressFrom,
						TokenId:          tokenId,
						Type:             "cancelListing",
						Value:            listingPrice,
					}); err != nil {
						log.Warnf("failed to insert cancelListing tx[%s] to db: %s", transactionRecordInstance.Hash, err)
					}

				case "makeListing":
					address := "0x" + common.Bytes2Hex(transactionData[0].(common.Address).Bytes())
					tokenId := transactionData[1].(*big.Int).Uint64()
					amount := big.NewInt(0).Div(transactionData[2].(*big.Int), math.BigPow(10, 18))
					collectionLabel := "UNKNOWN"
					tokenAddress := address
					cronosChain := addressbook.Chain["cronos"]
					if directoryEntry, exists := cronosChain.Directory[strings.ToLower(address)]; exists {
						collectionLabel = directoryEntry.Label
					}
					log.Infof(
						"listing made for token[%v] of collection[%s] (%s) for %v CRO by account[%s]",
						tokenId,
						tokenAddress,
						collectionLabel,
						amount.String(),
						addressFrom,
					)
					var listing *ebisusbayapi.Listing = nil
					for attempts := 3; attempts >= 0; attempts-- {
						listings, err := ebisusbayapi.GetListings(ebisusbayapi.GetListingsOpts{
							Address: tokenAddress,
							TokenId: int64(tokenId),
						})
						if err != nil {
							log.Warnf("failed to get listings for token[%v] of collection[%s]: %s", tokenId, tokenAddress, err)
							break
						}
						listing = listings.GetNewest()
						if listing == nil {
							<-time.After(3 * time.Second)
							continue
						}
						listingId := uint64(listing.ListingId)
						if err := logTransaction(logTransactionOpts{
							DB:               appDb,
							TransactionId:    transactionRecordInstance.Id,
							TransactionHash:  transactionRecordInstance.Hash,
							ListingId:        &listingId,
							TokenAddress:     &address,
							TriggererAddress: addressFrom,
							TokenId:          &tokenId,
							Type:             "makeListing",
							Value:            float64(amount.Int64()),
						}); err != nil {
							log.Warnf("failed to insert makeListing tx[%s] to db: %s", transactionRecordInstance.Hash, err)
							continue
						}
						break
					}

				case "makePurchase":
					listingId := transactionData[0].(*big.Int).Uint64()
					log.Infof(
						"listing[%v] purchased for %v CRO by account[%s]",
						listingId,
						transactionRecordInstance.Value,
						addressFrom,
					)
					listing, err := ebisusbayapi.GetListingById(strconv.FormatUint(listingId, 10))
					if err != nil {
						log.Warnf("failed to get purchased listing[%v]: %s", listingId, err)
						break
					}
					if listing.Listings == nil || len(listing.Listings) == 0 {
						log.Warnf("failed to get listings data of listing[%v]", listingId)
					}
					listingInstance := listing.Listings[0]
					tokenAddress := listingInstance.NftAddress
					tokenId, err := strconv.ParseUint(listingInstance.NftId, 10, 64)
					if err != nil {
						log.Warnf("failed to parse token_id[%v]: %s", listingInstance.NftId, err)
					}
					if err := logTransaction(logTransactionOpts{
						DB:               appDb,
						TransactionId:    transactionRecordInstance.Id,
						TransactionHash:  transactionRecordInstance.Hash,
						ListingId:        &listingId,
						TokenAddress:     &tokenAddress,
						TriggererAddress: addressFrom,
						TokenId:          &tokenId,
						Type:             "makePurchase",
						Value:            transactionRecordInstance.Value,
					}); err != nil {
						log.Warnf("failed to insert makePurchase tx[%s] to db: %s", transactionRecordInstance.Hash, err)
					}

				case "withdrawPayments":
					payee := "0x" + common.Bytes2Hex(transactionData[0].(common.Address).Bytes())
					log.Infof(
						"payments withdrawn by account[%s]",
						payee,
					)
					if err := logTransaction(logTransactionOpts{
						DB:               appDb,
						TransactionId:    transactionRecordInstance.Id,
						TransactionHash:  transactionRecordInstance.Hash,
						TriggererAddress: addressFrom,
						Type:             "withdrawPayments",
						Value:            transactionRecordInstance.Value,
					}); err != nil {
						log.Warnf("failed to insert withdrawPayments tx[%s] to db: %s", transactionRecordInstance.Hash, err)
					}

				default:
					log.Infof("unknown method[%s] called", methodId)
				}
			}
		}
		// log.Infof("queried %v txs from %s -> %s", len(transactions), lastQueriedString, time.Now().UTC().Format(timestampFormat))
		<-time.After(time.Second * 5)
	}
	return nil
}
