package farmer

import (
	"fmt"

	"github.com/usvc/go-config"
	"github.com/zephinzer/go-cronos/cmd/ebisusbay/common/configuration"
)

var conf = config.Map{
	configuration.KeyMasterDatabaseHost:     configuration.GetMasterDatabaseHost(),
	configuration.KeyMasterDatabasePort:     configuration.GetMasterDatabasePort(),
	configuration.KeyMasterDatabaseUsername: configuration.GetMasterDatabaseUsername(),
	configuration.KeyMasterDatabasePassword: configuration.GetMasterDatabasePassword(),
	configuration.KeyMasterDatabaseName:     configuration.GetMasterDatabaseName(),
	configuration.KeyDatabaseHost:           configuration.GetDatabaseHost(),
	configuration.KeyDatabasePort:           configuration.GetDatabasePort(),
	configuration.KeyDatabaseName:           configuration.GetDatabaseName(),
	configuration.KeyDatabaseUsername:       configuration.GetDatabaseUsername(),
	configuration.KeyDatabasePassword:       configuration.GetDatabasePassword(),
}

func InitConfiguration(conf config.Map) (*commandConfiguration, error) {
	configurationInstance := &commandConfiguration{
		MasterDatabaseHost:     conf.GetString(configuration.KeyMasterDatabaseHost),
		MasterDatabasePort:     conf.GetUint(configuration.KeyMasterDatabasePort),
		MasterDatabaseUsername: conf.GetString(configuration.KeyMasterDatabaseUsername),
		MasterDatabasePassword: conf.GetString(configuration.KeyMasterDatabasePassword),
		MasterDatabaseName:     conf.GetString(configuration.KeyMasterDatabaseName),
		DatabaseHost:           conf.GetString(configuration.KeyDatabaseHost),
		DatabasePort:           conf.GetUint(configuration.KeyDatabasePort),
		DatabaseUsername:       conf.GetString(configuration.KeyDatabaseUsername),
		DatabasePassword:       conf.GetString(configuration.KeyDatabasePassword),
		DatabaseName:           conf.GetString(configuration.KeyDatabaseName),
	}
	return configurationInstance, nil
}

type commandConfiguration struct {
	MasterDatabaseHost     string
	MasterDatabasePort     uint
	MasterDatabaseUsername string
	MasterDatabasePassword string
	MasterDatabaseName     string
	DatabaseHost           string
	DatabasePort           uint
	DatabaseUsername       string
	DatabasePassword       string
	DatabaseName           string
}

func (c *commandConfiguration) GetMySqlDsn() string {
	return fmt.Sprintf(
		"%s:%s@tcp(%s:%v)/%s",
		c.DatabaseUsername,
		c.DatabasePassword,
		c.DatabaseHost,
		c.DatabasePort,
		c.DatabaseName,
	)
}

func (c commandConfiguration) GetPublicMySqlConnectionString() string {
	return fmt.Sprintf(
		"%s@%s:%v/%s",
		c.DatabaseUsername,
		c.DatabaseHost,
		c.DatabasePort,
		c.DatabaseName,
	)
}
