package checks

import (
	"fmt"

	"github.com/zephinzer/go-cronos/pkg/ebisusbayapi"
)

func testGetOffers(address string, tokenId int64) error {
	offers, err := ebisusbayapi.GetOffers(ebisusbayapi.GetOffersOpts{
		Address: address,
		TokenId: tokenId,
	})
	if err != nil {
		return fmt.Errorf("failed to .GetOffers: %s", err)
	}
	if offers.Data == nil {
		return fmt.Errorf("failed to receive .Data")
	}
	if offers.Data.Offers == nil || len(offers.Data.Offers) == 0 {
		return fmt.Errorf("failed to receive .Data.Offers")
	}
	offerInstance := offers.Data.Offers[0]
	if offerInstance.Buyer == nil {
		return fmt.Errorf("failed to receive a .Buyer")
	}
	if offerInstance.Hash == "" {
		return fmt.Errorf("failed to receive a .Hash")
	}
	if offerInstance.ID == "" {
		return fmt.Errorf("failed to receive an .ID")
	}
	if offerInstance.NftAddress == "" {
		return fmt.Errorf("failed to receive an .NftAddress")
	}
	if offerInstance.NftId == "" {
		return fmt.Errorf("failed to receive an .NftId")
	}
	if offerInstance.TimeCreated == nil {
		return fmt.Errorf("failed to receive a .TimeCreated")
	}
	return nil
}

func testGetWalletOffers(walletAddress string) error {
	offers, err := ebisusbayapi.GetWalletOffers(walletAddress)
	if err != nil {
		return fmt.Errorf("failed to .GetWalletOffers: %s", err)
	}
	if offers.Data == nil {
		return fmt.Errorf("failed to receive .Data")
	}
	if offers.Data.Offers == nil || len(offers.Data.Offers) == 0 {
		return fmt.Errorf("failed to receive .Data.Offers")
	}
	offerInstance := offers.Data.Offers[0]
	if offerInstance.Buyer == nil {
		return fmt.Errorf("failed to receive a .Buyer")
	}
	if offerInstance.Hash == "" {
		return fmt.Errorf("failed to receive a .Hash")
	}
	if offerInstance.ID == "" {
		return fmt.Errorf("failed to receive an .ID")
	}
	if offerInstance.NftAddress == "" {
		return fmt.Errorf("failed to receive an .NftAddress")
	}
	if offerInstance.NftId == "" {
		return fmt.Errorf("failed to receive an .NftId")
	}
	if offerInstance.TimeCreated == nil {
		return fmt.Errorf("failed to receive a .TimeCreated")
	}
	return nil
}

func testGetListings(address string, tokenId int64) error {
	listings, err := ebisusbayapi.GetListings(ebisusbayapi.GetListingsOpts{
		Address: address,
		TokenId: tokenId,
	})
	if err != nil {
		return fmt.Errorf("failed to .GetListings: %s", err)
	}
	if listings.Listings == nil || len(listings.Listings) == 0 {
		return fmt.Errorf("failed to get a valid item in .Listings")
	}
	listingInstance := listings.Listings[0]
	if listingInstance.ListingId == 0 {
		return fmt.Errorf("failed to get a .ListingId")
	}
	if listingInstance.NftAddress == "" {
		return fmt.Errorf("failed to get an .NftAddress")
	}
	if listingInstance.NftId == "" {
		return fmt.Errorf("failed to get an .NftId")
	}
	if listingInstance.Price == "" {
		return fmt.Errorf("failed to get a .Price")
	}
	if listingInstance.Seller == "" {
		return fmt.Errorf("failed to get a .Seller")
	}
	return nil
}

func testGetListingById(listingId string) error {
	listing, err := ebisusbayapi.GetListingById(listingId)
	if err != nil {
		return fmt.Errorf("failed to .GetListingById: %s", err)
	}
	if listing.Listings == nil || len(listing.Listings) == 0 {
		return fmt.Errorf("failed to get a valid item in .Listings")
	}
	listingInstance := listing.Listings[0]
	if listingInstance.ListingId == 0 {
		return fmt.Errorf("failed to get a .ListingId")
	}
	if listingInstance.Nft == nil {
		return fmt.Errorf("failed to get an .Nft in the listing items")
	}
	if listingInstance.NftAddress == "" {
		return fmt.Errorf("failed to get an .NftAddress")
	}
	if listingInstance.NftId == "" {
		return fmt.Errorf("failed to get an .NftId")
	}
	if listingInstance.Price == "" {
		return fmt.Errorf("failed to get a .Price")
	}
	if listingInstance.Seller == "" {
		return fmt.Errorf("failed to get a .Seller")
	}
	return nil
}

func testGetCollections(collectionAddress string) error {
	collections, err := ebisusbayapi.GetCollections(ebisusbayapi.GetCollectionsOpts{
		Address: collectionAddress,
	})
	if err != nil {
		return fmt.Errorf("failed to .GetCollections: %s", err)
	}
	if collections.Collections == nil {
		return fmt.Errorf("failed to receive a non-nil .Collections property")
	}
	collectionInstance := collections.Collections[0]
	if collectionInstance.AverageSalePrice == "" {
		return fmt.Errorf("failed to receive an .AverageSalePrice")
	}
	if collectionInstance.FloorPrice == "" {
		return fmt.Errorf("failed to receive a .FloorPrice")
	}
	if collectionInstance.NumberActive == "" {
		return fmt.Errorf("failed to receive a .NumberActive")
	}
	return nil
}

func testGetCollection(collectionAddress string) error {
	collection, err := ebisusbayapi.GetCollection(ebisusbayapi.GetCollectionOpts{
		Address:  collectionAddress,
		PageSize: 2,
		SortBy:   ebisusbayapi.SortKeyPrice,
	})
	if err != nil {
		return fmt.Errorf("failed to .GetCollection: %s", err)
	}
	if collection.Nfts == nil {
		return fmt.Errorf("failed to receive a non-nil .Nfts property")
	}
	if collection.Nfts[0].Address == "" {
		return fmt.Errorf("failed to receive a .Address property")
	}
	if collection.Nfts[0].ID == "" {
		return fmt.Errorf("failed to receive an .ID property")
	}
	return nil
}

func testGetRarity(collectionAddress string) error {
	rarity, err := ebisusbayapi.GetRarity(collectionAddress)
	if err != nil {
		return fmt.Errorf("failed to .GetCollections: %s", err)
	}
	if err != nil {
		return fmt.Errorf("failed to .GetRarity(): %s", err)
	}
	if rarity == nil {
		return fmt.Errorf("failed to .GetRarity() response: response is null")
	}
	rarityInstance := *rarity
	if _, ok := rarityInstance["Headwear"]; !ok {
		return fmt.Errorf("failed to receive expected 'Headwear' trait category")
	}
	return nil
}

func testGetSales(collectionAddress string) error {
	expectedPageSize := 2
	sales, err := ebisusbayapi.GetSales(ebisusbayapi.GetSalesOpts{
		Address:   collectionAddress,
		Direction: ebisusbayapi.DirectionDescending,
		PageSize:  uint64(expectedPageSize),
		SortBy:    ebisusbayapi.SortKeySaleTime,
	})
	if err != nil {
		return fmt.Errorf("failed to .GetSales: %s", err)
	}
	if sales.Listings == nil || len(sales.Listings) != expectedPageSize {
		return fmt.Errorf("failed to receive .Listings")
	}
	listingInstance := sales.Listings[0]
	if listingInstance.ListingID == 0 {
		return fmt.Errorf("failed to get a valid listing instance")
	}
	if listingInstance.Price == "" {
		return fmt.Errorf("failed to get a valid .Price")
	}
	if listingInstance.Nft == nil {
		return fmt.Errorf("failed to get a valid .Nft")
	}
	if listingInstance.Nft.NftAddress == "" {
		return fmt.Errorf("failed to get a valid .Nft nft id")
	}
	if listingInstance.Nft.NftID == "" {
		return fmt.Errorf("failed to get a valid .Nft nft id")
	}
	if listingInstance.ListingTime == 0 {
		return fmt.Errorf("failed to get a valid .ListingTime")
	}
	if listingInstance.SaleTime == 0 {
		return fmt.Errorf("failed to get a valid .SaleTime")
	}
	return nil
}
