package checks

import (
	"github.com/usvc/go-config"
)

const (
	ConfigKeyMinimumTestIntervalSeconds = "min-test-interval-seconds"
	ConfigKeyTestIntervalRangeSeconds   = "test-interval-range-seconds"
)

var conf = config.Map{
	ConfigKeyMinimumTestIntervalSeconds: &config.Uint{
		Shorthand: "t",
		Default:   3,
		Usage:     "minimum seconds between test intervals",
	},
	ConfigKeyTestIntervalRangeSeconds: &config.Uint{
		Shorthand: "r",
		Default:   60,
		Usage:     "range of seconds between test intervals, a random number between 0 and this will be generated and added to the minimum test interval",
	},
}
