package checks

import (
	"fmt"
	"math/rand"
	"sync"
	"time"

	"github.com/spf13/cobra"
	"github.com/zephinzer/go-cronos/internal/log"
	"github.com/zephinzer/go-cronos/pkg/addressbook"
)

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:   "checks",
		Short: "Starts continuous contract testing of the ebisusbay package",
		RunE:  runE,
	}
	return &command
}

func runE(command *cobra.Command, args []string) error {
	// seed the randomiser
	rand.Seed(time.Now().UnixNano())

	// friend's wallet hehe
	sampleWalletAddress := "0x0e65caf3b36fce31a0a01aa0baa7d67759f14427"

	// crokat's listing
	sampleCollectionAddress, err := addressbook.GetAddress("cronos", "mad-meerkat")
	if err != nil {
		return fmt.Errorf("failed to get collection[mad-meerkat] on chain[cronos]")
	}
	sampleListingId := "273012"
	sampleTokenId := int64(604)

	// offer made on my own nft - wtf right?
	sampleOffersAddress, err := addressbook.GetAddress("cronos", "red-saint")
	if err != nil {
		return fmt.Errorf("failed to get collection[red-saint] on chain [cronos]")
	}
	sampleOfferTokenId := int64(2977)

	// configuration for testing intervals
	minimumTestInterval := time.Second * time.Duration(conf.GetUint(ConfigKeyMinimumTestIntervalSeconds))
	testIntervalRangeSeconds := conf.GetUint(ConfigKeyTestIntervalRangeSeconds)

	contractTests := map[string]func() error{
		"GetCollection":   func() error { return testGetCollection(sampleCollectionAddress.String()) },
		"GetCollections":  func() error { return testGetCollections(sampleCollectionAddress.String()) },
		"GetListingById":  func() error { return testGetListingById(sampleListingId) },
		"GetListings":     func() error { return testGetListings(sampleCollectionAddress.String(), sampleTokenId) },
		"GetOffers":       func() error { return testGetOffers(sampleOffersAddress.String(), sampleOfferTokenId) },
		"GetRarity":       func() error { return testGetRarity(sampleCollectionAddress.String()) },
		"GetSales":        func() error { return testGetSales(sampleCollectionAddress.String()) },
		"GetWalletOffers": func() error { return testGetWalletOffers(sampleWalletAddress) },
	}

	var waiter sync.WaitGroup

	waiter.Add(1)

	// loop the tests and run indefinitely
	for contractTestId, contractTest := range contractTests {
		go func(intervalBetweenTests time.Duration, testId string, runTest func() error) {
			for {
				testIntervalRangeSeconds := rand.Intn(int(testIntervalRangeSeconds))
				nextRunInSeconds := minimumTestInterval + time.Duration(testIntervalRangeSeconds)*time.Second

				if err := runTest(); err != nil {
					log.Warnf("failed to verify %s(): %s", testId, err)
				} else {
					log.Infof("test[%s] passed, next run in %v", testId, nextRunInSeconds)
				}

				<-time.After(nextRunInSeconds)
			}
		}(minimumTestInterval, contractTestId, contractTest)
	}

	// cheat your feelings
	waiter.Wait()

	// never
	return nil
}
