package types

import (
	"database/sql"
	"fmt"
	"strconv"
	"strings"

	"github.com/zephinzer/go-cronos/internal/log"
	"github.com/zephinzer/go-cronos/pkg/ebisusbayapi"
)

const (
	InsertCollectionSummarytmtTemplate = `
REPLACE INTO ebz_collection_stats (
	address_token,
	active_listings_count,
	avg_sale_price,
	floor_price,
	sales_count_total,
	sales_count_1d,
	sales_count_7d,
	sales_count_30d,
	volume_total,
	volume_1d,
	volume_7d,
	volume_30d
) VALUES (
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?
)
`
)

func NewCollectionSummary() {

}

type CollectionSummaryList []CollectionSummary

func (cs CollectionSummaryList) Insert(db *sql.DB) error {
	stmt, err := db.Prepare(InsertCollectionSummarytmtTemplate)
	if err != nil {
		return fmt.Errorf("failed to prepare db stmt: %s", err)
	}
	defer stmt.Close()
	errors := []string{}
	for _, collectionStatistic := range cs {
		if err := collectionStatistic.Insert(db, stmt); err != nil {
			errors = append(errors, err.Error())
		}
	}
	if len(errors) > 0 {
		return fmt.Errorf("failed to insert all collection statistics into db:\n  - %s", strings.Join(errors, "\n  - "))
	}
	return nil
}

type CollectionSummary ebisusbayapi.CollectionSummary

func (cs CollectionSummary) Insert(db *sql.DB, stmt ...*sql.Stmt) error {
	var insertStmt *sql.Stmt
	if len(stmt) == 0 {
		var err error
		insertStmt, err = db.Prepare(InsertCollectionSummarytmtTemplate)
		if err != nil {
			return fmt.Errorf("failed to prepare db stmt: %s", err)
		}
		defer insertStmt.Close()
	} else {
		insertStmt = stmt[0]
	}
	floorPrice, err := strconv.ParseFloat(cs.FloorPrice, 64)
	if err != nil {
		log.Warnf("failed to parse floor price '%s' as a float: %s", cs.FloorPrice, err)
	}
	avgSalePrice, err := strconv.ParseFloat(cs.AverageSalePrice, 64)
	if err != nil {
		log.Warnf("failed to parse average sale price '%s' as a float: %s", cs.AverageSalePrice, err)
	}
	numberActive, err := strconv.ParseInt(cs.NumberActive, 10, 64)
	if err != nil {
		log.Warnf("failed to parse active listings count '%s' as an integer: %s", cs.NumberActive, err)
	}
	totalSales, err := strconv.ParseInt(cs.NumberOfSales, 10, 64)
	if err != nil {
		log.Warnf("failed to parse total sales count '%s' as an integer: %s", cs.NumberOfSales, err)
	}
	sales1d, err := strconv.ParseFloat(cs.Sales1D, 64)
	if err != nil {
		log.Warnf("failed to parse 1d sales '%s' as a flota: %s", cs.Sales1D, err)
	}
	sales7d, err := strconv.ParseFloat(cs.Sales7D, 64)
	if err != nil {
		log.Warnf("failed to parse 7d sales '%s' as a flota: %s", cs.Sales7D, err)
	}
	sales30d, err := strconv.ParseFloat(cs.Sales30D, 64)
	if err != nil {
		log.Warnf("failed to parse 30d sales '%s' as a flota: %s", cs.Sales30D, err)
	}
	totalVolume, err := strconv.ParseFloat(cs.TotalVolume, 64)
	if err != nil {
		log.Warnf("failed to parse total volume '%s' as a float: %s", cs.TotalVolume, err)
	}
	volume1d, err := strconv.ParseFloat(cs.Volume1D, 64)
	if err != nil {
		log.Warnf("failed to parse 1d volume '%s' as a float: %s", cs.Volume1D, err)
	}
	volume7d, err := strconv.ParseFloat(cs.Volume7D, 64)
	if err != nil {
		log.Warnf("failed to parse 7d volume '%s' as a float: %s", cs.Volume7D, err)
	}
	volume30d, err := strconv.ParseFloat(cs.Volume30D, 64)
	if err != nil {
		log.Warnf("failed to parse 30d volume '%s' as an integer: %s", cs.Volume30D, err)
	}
	if _, err := insertStmt.Exec(
		cs.Collection,
		numberActive,
		avgSalePrice,
		floorPrice,
		totalSales,
		sales1d,
		sales7d,
		sales30d,
		totalVolume,
		volume1d,
		volume7d,
		volume30d,
	); err != nil {
		return fmt.Errorf("failed to insert collection stats into db: %s", err)
	}

	return nil
}
