package main

import (
	"github.com/spf13/cobra"
	"github.com/zephinzer/go-cronos/cmd/seeder/commands/seed"
	"github.com/zephinzer/go-cronos/internal/constants"
	"github.com/zephinzer/go-cronos/internal/log"
)

func main() {
	err := GetCommand().Execute()
	if err != nil {
		log.Errorf("failed to execute successfully: %s", err)
	}
}

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:   "seeder <sub-command>",
		Short: "Tool to use to seed data",
		RunE:  runE,
	}
	command.Version = constants.Version
	command.AddCommand(seed.GetCommand())
	return &command
}

func runE(command *cobra.Command, args []string) error {
	command.Help()
	return nil
}
