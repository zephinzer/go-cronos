package seed

import (
	"github.com/spf13/cobra"
	"github.com/zephinzer/go-cronos/cmd/seeder/commands/seed/aliases"
	"github.com/zephinzer/go-cronos/internal/constants"
)

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:     "seed <data-type>",
		Aliases: []string{"s"},
		Short:   "Seeds data",
		RunE:    runE,
	}
	command.Version = constants.Version
	command.AddCommand(aliases.GetCommand())
	return &command
}

func runE(command *cobra.Command, args []string) error {
	command.Help()
	return nil
}
