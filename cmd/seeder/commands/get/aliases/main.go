package aliases

import (
	"github.com/spf13/cobra"
	"github.com/zephinzer/go-cronos/internal/constants"
)

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:     "aliases",
		Aliases: []string{"a"},
		Short:   "Dumps the aliases to stdout in JSON format",
		RunE:    runE,
	}
	command.Version = constants.Version
	return &command
}

func runE(command *cobra.Command, args []string) error {
	command.Help()
	return nil
}
