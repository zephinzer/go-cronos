package get

import (
	"github.com/spf13/cobra"
	"github.com/zephinzer/go-cronos/cmd/seeder/commands/get/aliases"
	"github.com/zephinzer/go-cronos/internal/constants"
)

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:     "get <data-type>",
		Aliases: []string{"g"},
		Short:   "Displays the type of data",
		RunE:    runE,
	}
	command.Version = constants.Version
	command.AddCommand(aliases.GetCommand())
	return &command
}

func runE(command *cobra.Command, args []string) error {
	command.Help()
	return nil
}
