# Default values for farmer.
# This is a YAML-formatted file.
# Declare variables to be passed into your templates.

image:
  repository: zephinzer/go-cronos-farmer
  pullPolicy: IfNotPresent
  # tag indicates the version of the application
  tag: v0.20.0

# config defines application-specific configuration
config:

  # env defines global environment variables that apply
  # across all pods that are spun up from this release
  env:

    # DB_HOST defines the database host which the farmer
    # service should use to perform write operations
    DB_HOST: database-mysql-primary

    # DB_NAME defines the name of the database schema to
    # use
    DB_NAME: farmer

    # BATCH_SIZE defines the number of transactions to
    # retrieve with every request
    BATCH_SIZE: "1000"

  # aliasSync contains configurations for the alias-sync
  # CronJob resource
  aliasSync:

    # image defines the Docker image configuration
    image:
      
      # repository is the URI to the Docker image
      repository: zephinzer/go-cronos-datasync
      
      # pullPolicy defines when the image should be pulled
      pullPolicy: IfNotPresent

      # tag defines the version of the datasync image to use
      tag: v0.20.8

    # isMigrationEnabled triggers the gORM migration when
    # it's set to true
    isMigrationEnabled: true

    # isPaused prevents the CronJob from running when it is
    # set to false
    isPaused: false

    # schedule defines the timings when the alias-sync
    # CronJob resource should be run
    schedule: "*/10 * * * *"

  # balances provides configuration for the balance harvester
  balances:

    # isPaused when set to true pauses the harvesting of
    # wallet balances
    isPaused: false

    # rpcUrl is the RPC to use
    rpcUrl: https://boomersquad.xstaking.sg
  
    # targets are a list of {token, holder} instances which
    # defines which wallet to look in (holder) for the token
    # (token). aliases here are matched against known_addresses.yaml
    # in this project
    targets:

      # ann/mmf
      - token: $ANN
        holder: $ANN/MMF

      # argo
      - token: $ARGO
        holder: Argo DAO Community Bounties
      - token: $ARGO
        holder: Argo DAO Marketing and Partnerships
      - token: $ARGO
        holder: Argo Incentives Programme
      - token: $ARGO
        holder: Argo Protocol Treasury
      - token: $ARGO
        holder: Argo Team Allocation
      - token: $ARGO
        holder: $ARGO>ARGO
      - token: $ARGO
        holder: $ARGO>ibARGO
      - token: $ARGO
        holder: $ARGO/USDC
      - token: $ARGO
        holder: $VVS/ARGO
      
      # betify
      - token: $BETIFY
        holder: $BETIFY/MMF

      # burrow
      - token: $BURROW
        holder: $BURROW/MMF
      - token: $BURROW
        holder: $BURROW/SVN
      - token: $BURROW
        holder: Mad Meerkat Finance Deployer
      - token: $BURROW
        holder: $BURROW>BURROW
      - token: $BURROW
        holder: $BURROW>BURROWv2
      
      # croissant
      - token: $CROISSANT
        holder: $CROISSANT/MMF
      
      # cros
      - token: $CROS
        holder: $CROS>CROS
      - token: $CROS
        holder: CroSwap Team 1
      - token: $CROS
        holder: CroSwap Team 2
      - token: $CROS
        holder: CroSwap Team 3
      - token: $CROS
        holder: $CROS/CRO
      - token: $CROS
        holder: $CROS/USDC

      # fira
      - token: $FIRA
        holder: $FIRA/WCRO
      - token: $FIRA
        holder: $MIMAS/FIRA

      # goal
      - token: $GOAL
        holder: $GOAL/MMF

      # hkn
      - token: $HKN
        holder: $HKN/SVN
      - token: $HKN
        holder: $MTT/HKN
      
      # mad
      - token: $MAD
        holder: $MAD/MMF
      - token: $MAD
        holder: $MAD/SVN
      - token: $SVN
        holder: $MAD/SVN
      
      # metf
      - token: $METF
        holder: METF DAO
      - token: $METF
        holder: Savanna Deployer
      - token: $METF
        holder: $METF/MMF
      - token: $MMF
        holder: $METF/MMF
      - token: $METF
        holder: $METF/SVN

      # mimas
      - token: $MIMAS
        holder: $MIMAS/FIRA
      - token: $MIMAS
        holder: $MIMAS/WCRO
      - token: $MIMAS
        holder: $MIMAS>MIMAS
      - token: $MIMAS
        holder: $rMIMAS>MIMAS
      - token: $MIMAS
        holder: Mimas Incentives
      - token: $MIMAS
        holder: Mimas Treasury
      - token: $MIMAS
        holder: Mimas Team Vesting
      - token: $MIMAS
        holder: Mimas IDO
      - token: $MIMAS
        holder: Mimas Team 1
      - token: $MIMAS
        holder: Mimas Team 2
      - token: $MIMAS
        holder: Mimas Team 3

      # mintme
      - token: $MINTME
        holder: $MINTME/MMF
      
      # mmf
      - token: $MMF
        holder: $MMF
      - token: $MMF
        holder: $MMF>MMF
      - token: $MMF
        holder: $MMF>MMFv2
      - token: $MMF
        holder: $MMF>pMMF
      - token: $MMF
        holder: $veMMF
      - token: $MMF
        holder: $MAD/MMF
      - token: $MMF
        holder: $MMF/WCRO
      - token: $MMF
        holder: $MMF/MUSD
      - token: $MMF
        holder: $MMF/USDC
      - token: $MMF
        holder: $MMF/USDT
      - token: $MMF
        holder: $MNG/MMF
      - token: $MMF
        holder: $MSHARE/MMF
      - token: $MMF
        holder: $SVN/MMF
      - token: $MMF
        holder: $CROISSANT/MMF
      - token: $MMF
        holder: $MINTME/MMF
      - token: $MMF
        holder: $BURROW/MMF
      - token: $MMF
        holder: $ANN/MMF
      - token: $MMF
        holder: $BETIFY/MMF

      # mmo
      - token: $MMO
        holder: METF DAO
      - token: $MMO
        holder: $MMO/WCRO
      
      # musd
      - token: $MUSD
        holder: $MMF/MUSD

      # mng
      - token: $MNG
        holder: $MNG/MMF
      - token: $MNG
        holder: $MNG/SVN

      # mshare dao
      - token: $MSHARE
        holder: Savanna Treasury
      - token: $MSHARE
        holder: MSHARE Rewards
      - token: $MSHARE
        holder: $MSHARE/MMF
      - token: $MSHARE
        holder: $SVN/MSHARE

      # mtt
      - token: $MTT
        holder: MTT Rewards
      - token: $MTT
        holder: $MTT/SVN
      - token: $MTT
        holder: $MTT/HKN

      # rlm
      - token: $RLM
        holder: Marbleverse Team
      - token: $RLM
        holder: Marbleverse Advisors Managers Community
      - token: $RLM
        holder: Marbleverse Collaborations
      - token: $RLM
        holder: Marbleverse Marketing
      - token: $RLM
        holder: Marbleverse RLM ICO
      - token: $RLM
        holder: Marbleverse RLM Private Sale
      - token: $RLM
        holder: Marbleverse P2E
      - token: $RLM
        holder: $RLM/WCRO

      # single
      - token: $SINGLE
        holder: SINGLE Boosting
      - token: $SINGLE
        holder: SINGLE Rewards 1
      - token: $SINGLE
        holder: SINGLE Rewards 2
      - token: $SINGLE
        holder: Single Team 1
      - token: $SINGLE
        holder: Single Team 2
      - token: $SINGLE
        holder: Single Team 3
      - token: $SINGLE
        holder: Single Team 4
      - token: $SINGLE
        holder: Single Team 5
      - token: $SINGLE
        holder: Single Team 6
      - token: $SINGLE
        holder: $SINGLE>SINGLE
      - token: $SINGLE
        holder: $SINGLE>anySINGLE
      - token: $SINGLE
        holder: $SINGLE>ibSINGLE
      - token: $SINGLE
        holder: $SINGLE/USDC (MMF)
      - token: $SINGLE
        holder: $SINGLE/USDC (VVS)
      - token: $SINGLE
        holder: $SINGLE/VVS
      
      # svn
      - token: $SVN
        holder: $BURROW/SVN
      - token: $SVN
        holder: $HKN/SVN
      - token: $SVN
        holder: $METF/SVN
      - token: $SVN
        holder: $SVN/MMF
      - token: $SVN
        holder: $MNG/SVN
      - token: $SVN
        holder: $SVN/MSHARE
      - token: $SVN
        holder: $MTT/SVN
      - token: $SVN
        holder: Mad Meerkat Arena Upgrader

      # tonic
      - token: $TONIC
        holder: TectonicSocket
      - token: $TONIC
        holder: Tectonic Staking Pool
      - token: $TONIC
        holder: Tectonic Timelock Vesting
      - token: $TONIC
        holder: Tectonic Vault
      - token: $TONIC
        holder: Tectonic Wallet (Community Fund)
      - token: $TONIC
        holder: Tectonic Wallet (Ecosystem Development/Reserve)
      - token: $TONIC
        holder: Tectonic Wallet (Network Security & Maintenance)
      - token: $TONIC
        holder: Tectonic Wallet (Team Vesting)
      - token: $TONIC
        holder: $tTONIC

      # tonic lps
      - token: $TONIC
        holder: $TONIC/USDC
      - token: $TONIC
        holder: $TONIC/WCRO
      - token: $TONIC
        holder: $V3TONIC/TONIC
      - token: $TONIC
        holder: $VVS/TONIC

      # vvs daos
      - token: $VVS
        holder: VVS Community Wallet
      - token: $VVS
        holder: VVS Craftsman
      - token: $VVS
        holder: VVS Deployer
      - token: $VVS
        holder: VVS Factory
      - token: $VVS
        holder: VVS Router
      - token: $VVS
        holder: VVS Workbench
      - token: $VVS
        holder: VVS VVSVault
      - token: $VVS
        holder: VVS Team Vesting
      - token: $VVS
        holder: VVS Community Wallet for Future Initiatives
      - token: $VVS
        holder: VVS Ecosystem Development
      - token: $VVS
        holder: VVS Network Security and Maintenance
      - token: $VVS
        holder: VVS Community Market Makers Incentives
      - token: $VVS
        holder: VVS Trade and Referrers Incentives
      - token: $VVS
        holder: VVS Bling Swap Fee Collector
      - token: $VVS
        holder: $SINGLE/VVS
      - token: $VVS
        holder: $VVS/ARGO
      - token: $VVS
        holder: $VVS/USDC
      - token: $VVS
        holder: $VVS/USDT
      - token: $VVS
        holder: $VVS/TONIC
      - token: $VVS
        holder: $VVS/V3S
      - token: $VVS
        holder: $VVS/WCRO
      - token: $VVS
        holder: $VVS>VVS

      # wcro
      - token: $WCRO
        holder: $MMF/WCRO
      
      # ------------
      # BURN WALLETS
      # ------------
      # $argo
      - token: $ARGO
        holder: $BURN1
      - token: $ARGO
        holder: $BURN2
      # $burrow
      - token: $BURROW
        holder: $BURN1
      - token: $BURROW
        holder: $BURN2
      # $mad
      - token: $MAD
        holder: $BURN1
      - token: $MAD
        holder: $BURN2
      # $metf
      - token: $METF
        holder: $BURN1
      - token: $METF
        holder: $BURN2
      # $mmf
      - token: $MMF
        holder: $BURN1
      - token: $MMF
        holder: $BURN2
      # $mmo
      - token: $MMO
        holder: $BURN1
      - token: $MMO
        holder: $BURN2
      # $mshare
      - token: $MSHARE
        holder: $BURN1
      - token: $MSHARE
        holder: $BURN2
      # $svn
      - token: $SVN
        holder: $BURN1
      - token: $SVN
        holder: $BURN2
      # $vvs
      - token: $VVS
        holder: $BURN1
      - token: $VVS
        holder: $BURN2
  
  # erc20events defines configurations for the series of
  # cronjobs which scrape erc20 token events
  erc20events:
    
    # addresses defines a list of {label, address} instances
    # that define the erc20 events to scrape. the `label`
    # parameter is only used in the naming of k8s resources
    addresses:
    - label: ann
      address: 0x98936bde1cf1bff1e7a8012cee5e2583851f2067
    - label: argo
      address: 0x47A9D630dc5b28F75d3AF3be3AAa982512Cd89Aa
    - label: burrow
      address: 0x49ab7ca4D2Cf6777c8C56C5E9Edb7D1218AE1299
    - label: cpb
      address: 0xf52F39819c01f25C609bEf57626A2520462c9bc1
    - label: cros
      address: 0x1Ba477CA252C0FF21c488d41759795E7E7812aB4
    - label: fer
      address: 0x39bC1e38c842C60775Ce37566D03B41A7A66C782
    - label: fira
      address: 0x7ABa852082b6F763E13010CA33B5D9Ea4EeE2983
    - label: grve
      address: 0x9885488cD6864DF90eeBa6C5d07B35f08CEb05e9
    - label: goal
      address: 0x00fe915a5209e74D5a88334cC2daA4541AEC8278
    - label: hakuna
      address: 0xa60943a1B19395C999ce6c21527b6B278F3f2046
    - label: mad
      address: 0x212331e1435A8df230715dB4C02B2a3A0abF8c61
    - label: metf
      address: 0xB8Df27c687c6af9aFE845A2aFAD2D01e199f4878
    - label: mimas
      address: 0x10C9284E6094b71D3CE4E38B8bFfc668199da677
    - label: mmf
      address: 0x97749c9B61F878a880DfE312d2594AE07AEd7656
    - label: mmo
      address: 0x50c0C5bda591bc7e89A342A3eD672FB59b3C46a7
    - label: mshare
      address: 0xf8b9facB7B4410F5703Eb29093302f2933D6E1Aa
    - label: mtd
      address: 0x0224010BA2d567ffa014222eD960D1fa43B8C8E1
    - label: mtt
      address: 0x388c07066AA6cEa2Be4db58e702333df92c3A074
    - label: musd
      address: 0x95aEaF383E2e86A47c11CffdE1F7944eCB2C38C2
    - label: opl
      address: 0x28D292A914eDaeb6DAA13642231333bd157632B2
    - label: phnx
      address: 0x57d06bB1e3B60C875cD3A4445a53217F9B44d390
    - label: rlm
      address: 0x19258a1df9E929D02b34621CF52797998aE1Aa27
    - label: single
      address: 0x0804702a4E749d39A35FDe73d1DF0B1f1D6b8347
    - label: svn
      address: 0x654bAc3eC77d6dB497892478f854cF6e8245DcA9
    - label: tonic
      address: 0xDD73dEa10ABC2Bff99c60882EC5b2B81Bb1Dc5B2
    - label: usdc
      address: 0xc21223249ca28397b4b6541dffaecc539bff0c59
    - label: usdt
      address: 0x66e428c3f67a68878562e79A0234c1F83c208770
    - label: vvs
      address: 0x2D03bECE6747ADC00E1a131BBA1469C15fD11e03
    - label: wcro
      address: 0x5C7F8A570d578ED84E63fdFA7b1eE72dEae1AE23
    
    # historical defines configurations for the historical
    # erc20 token events retrieval
    historical:

      # fromBlock defines which block we should start retrieving
      # erc20 token events from
      fromBlock: "4900000"

      rpcUrl: "https://rpc.vvs.finance"
    
    # isPaused when set to true does not run any of the jobs
    isPaused: false
  
  # erc20stats holds configurations for the erc20 statistics
  # job
  erc20info:

    # rpcUrl defines which rpc should be used
    rpcUrl: https://boomersquad.xstaking.sg

    config:

      global:
        burnAddresses:
        - $BURN1
        - $BURN2
      tokens:
      - id: $ARGO
        ssContracts:
          - $ARGO>ARGO
          - $ARGO>ibARGO
        lpContracts:
          - $ARGO/USDC
          - $VVS/ARGO
        burnAddresses: []
        daoAddresses:
          - Argo DAO Community Bounties
          - Argo DAO Marketing and Partnerships
          - Argo Incentives Programme
          - Argo Protocol Treasury
          - Argo Team Allocation

      - id: $BURROW
        ssContracts:
          - $BURROW>BURROW
          - $BURROW>BURROWv2
        lpContracts:
          - $BURROW/MMF
          - $BURROW/SVN
        burnAddresses: []
        daoAddresses:
          - Mad Meerkat Finance Deployer

      - id: $CROS
        ssContracts:
          - $CROS>CROS
        lpContracts:
          - $CROS/CRO
          - $CROS/USDC
        burnAddresses: []
        daoAddresses:
          - CroSwap Team 1
          - CroSwap Team 2
          - CroSwap Team 3
      
      - id: $FIRA
        lpContracts:
          - $FIRA/DEFIRABLUEFRUIT
          - $FIRA/WCRO
          - $MIMAS/FIRA
        burnAddresses: []

      - id: $HKN
        ssContracts:
          - $HKN>pMMF
        lpContracts:
          - $HKN/SVN
          - $MTT/HKN
        burnAddresses: []
        daoAddresses:
          - METF DAO
          - Savanna DAO

      - id: $MAD
        ssContracts:
          - $MAD>MAD
        lpContracts:
          - $MAD/MMF
          - $MAD/SVN
        burnAddresses: []
        daoAddresses:
          - $MAD
          - MAD Rewards
          - Treehouse Rewards

      - id: $METF
        ssContracts:
          - $METF>pMMF
          - $METF>METF
        lpContracts:
          - $METF/MMF
          - $METF/SVN
        burnAddresses: []
        daoAddresses:
          - Mad Meerkat Finance Deployer
          - METF DAO
          - METF DAO SPES
          - Savanna Deployer

      - id: $MIMAS
        ssContracts:
          - $MIMAS>MIMAS
          - $rMIMAS>MIMAS
        lpContracts:
          - $MIMAS/FIRA
          - $MIMAS/WCRO
        burnAddresses: []
        daoAddresses:
          - Mimas Incentives
          - Mimas Treasury
          - Mimas Team Vesting
          - Mimas IDO
          - Mimas Team 1
          - Mimas Team 2
          - Mimas Team 3

      - id: $MMF
        ssContracts:
          - $MMF
          - $MMF>MMF
          - $MMF>MMFv2
          - $MMF>pMMF
          - $MMF>VERSA
          - $veMMF
        lpContracts:
          - $ANN/MMF
          - $BETIFY/MMF
          - $BURROW/MMF
          - $CROISSANT/MMF
          - $GOAL/MMF
          - $METF/MMF
          - $MINTME/MMF
          - $MNG/MMF
          - $MSHARE/MMF
          - $MMF/MUSD
          - $SVN/MMF
          - $MMF/USDC
          - $MMF/USDT
          - $MMF/WCRO
        burnAddresses: []
        daoAddresses:
          - Mad Meerkat Finance Deployer

      - id: $MMO
        ssContracts:
          - $MMO
          - $MMO>pMMO
          - $MMO>WCRO
        lpContracts:
          - $MMO/WCRO
        burnAddresses: []
        daoAddresses:
          - METF DAO

      - id: $MSHARE
        ssContracts:
          - $MSHARE>SVN
        lpContracts:
          - $MSHARE/MMF
          - $SVN/MSHARE
        burnAddresses: []
        daoAddresses:
          - Savanna DAO
          - Savanna Treasury
          - MSHARE Rewards

      - id: $MTT
        ssContracts:
          - $MTT>HKN
        lpContracts:
          - $MTT/HKN
          - $MTT/SVN
        burnAddresses: []
        daoAddresses:
          - METF DAO
          - Savanna DAO
          - MTT Rewards
          - MSHARE Rewards

      - id: $RLM
        ssContracts: []
        lpContracts:
          - $RLM/WCRO
        burnAddresses: []
        daoAddresses:
          - Marbleverse Team
          - Marbleverse Advisors Managers Community
          - Marbleverse Collaborations
          - Marbleverse Marketing
          - Marbleverse RLM ICO
          - Marbleverse RLM Private Sale
          - Marbleverse P2E

      - id: $SCRATCH
        ssContracts:
          - BoomRoom Staking
        lpContracts:
          - $SCRATCH/WCRO
        burnAddresses: []
        daoAddresses:
          - Boomer Squad Deployer          

      - id: $SINGLE
        ssContracts:
          - $SINGLE>SINGLE
          - $SINGLE>anySINGLE
          - $SINGLE>ibSINGLE
          - SINGLE Boosting
        lpContracts:
          - $SINGLE/USDC (MMF)
          - $SINGLE/USDC (VVS)
          - $SINGLE/VVS
        burnAddresses: []
        daoAddresses:
          - SINGLE Rewards 1
          - SINGLE Rewards 2
          - SINGLE Team 1
          - SINGLE Team 2
          - SINGLE Team 3
          - SINGLE Team 4
          - SINGLE Team 5
          - SINGLE Team 6

      - id: $SVN
        ssContracts:
          - $SVN>pMMF
        lpContracts:
          - $BURROW/SVN
          - $HKN/SVN
          - $MAD/SVN
          - $METF/SVN
          - $MNG/SVN
          - $SVN/MMF
          - $SVN/MSHARE
        burnAddresses: []
        daoAddresses:
          - Mad Meerkat Finance Deployer
          - METF DAO
          - Savanna DAO
          - Savanna Treasury
          - Mad Meerkat Arena Upgrader
      
      - id: $TONIC
        lpContracts:
          - $TONIC/USDC
          - $TONIC/WCRO
          - $V3TONIC/TONIC
          - $VVS/TONIC
        burnAddresses: []
        daoAddresses:
          - TectonicSocket
          - Tectonic Staking Pool
          - Tectonic Timelock Vesting
          - Tectonic Vault
          - Tectonic Wallet (Community Fund)
          - Tectonic Wallet (Ecosystem Development/Reserve)
          - Tectonic Wallet (Network Security & Maintenance)
          - Tectonic Wallet (Team Vesting)

      - id: $VVS
        ssContracts:
          - $VVS>VVS
        lpContracts:
          - $VVS/ARGO
          - $VVS/TONIC
          - $VVS/USDC
          - $VVS/USDT
          - $VVS/V3S
          - $VVS/WCRO
        burnAddresses: []
        daoAddresses:
          - VVS Bling Swap Fee Collector
          - VVS Community Market Makers Incentives
          - VVS Community Wallet for Future Initiatives
          - VVS Ecosystem Development
          - VVS Network Security and Maintenance
          - VVS Team Vesting
          - VVS Trade and Referrers Incentives
          - VVS Workbench

  erc20stats:

    # targets is a list of token addresses/aliases found in
    # known_addresses.yaml
    targets:
      - $ANN
      - $anyMSHARE
      - $anyMTT
      - $ARGO
      - $BURROW
      - $bCRO
      - $FER
      - $FIRA
      - $GOAL
      - $HKN
      - $MAD
      - $METF
      - $MIMAS
      - $MINTME
      - $MMF
      - $MMFX
      - $MMO
      - $MMOX
      - $MNG
      - $MSHARE
      - $MTT
      - $MUSD
      - $PHNX
      - $RLM
      - $SINGLE
      - $SVN
      - $TONIC
      - $tCRO
      - $tDAI
      - $tETH
      - $tTONIC
      - $tUSDC
      - $tUSDT
      - $tWBTC
      - $veMMF
      - $VVS
      - $WCRO
      - $xARGO
  
  # erc721events holds configurations for the erc721 series
  # of cronjobs
  erc721events:
    
    # addresses is a list of {label, address} instances which
    # defines addresses for which erc721 events should be
    # retrieved
    addresses:
    - label: boomroom
      address: 0x8Dd9975c78423480a6188F55B0767fbF4478b001
    - label: boomer
      address: 0xb650279e3d726B0c75C984cAA55341cB87A7F501
    - label: cronosverse
      address: 0x0aCDA31Cf1F301a7Eb8f988D47F708FbA058F8f5
    - label: croskull
      address: 0xF87A517A5CaecaA03d7cCa770789BdB61e09e05F
    - label: dgpals-genesis
      address: 0xeFc73c41EF829C0B87E3245aCF033b867cCD0F84
    - label: dgpals-unlimited
      address: 0x86AABE05E885108A8797314e817488C26F0601A1
    - label: mad-sacks
      address: 0x23721073592FB452C556fB9322bA4dF6A6675050
    - label: marbleverse
      address: 0xc427e0b2e4C9b79F856391b1EF37fD7ee7457d8D
    - label: mmb
      address: 0x89dBC8Bd9a6037Cbd6EC66C4bF4189c9747B1C56
    - label: mmd
      address: 0xA19bFcE9BaF34b92923b71D487db9D0D051a88F8
    - label: mme
      address: 0x23721073592FB452C556fB9322bA4dF6A6675050
    - label: mmt
      address: 0xDC5bBDb4A4b051BDB85B959eB3cBD1c8C0d0c105
    - label: vvs-miner-mole
      address: 0x06596eD89aC4609DE47A21aF7E36b38b2dF57C26

    # historical defines configurations for the historical
    # erc721 token events retrieval
    historical:

      # fromBlock defines which block we should start retrieving
      # erc721 token events from
      fromBlock: "3800000"
    
    # isPaused when set to true does not run any of the jobs
    isPaused: false
  
  # flags refer to feature flags
  flags:

    # isMigrationEnabled defines whether the database migrator
    # will be run, set this to false if there is some faulty
    # migration but you want the app to come up anyway
    isMigrationEnabled: true
  
  # sweeper defines `farmer sweep`-specific configuration
  sweeper:

    # rpcUrl is the RPC to use
    rpcUrl: https://rpc.vvs.finance

    # schedule defines the crontab schedule for the job to run,
    # this MUST be considered together with the blockCount below
    # so that all blocks will definitely be accounted for
    schedule: "*/10 * * * *"

    # blockCount determines the number of blocks in the past to
    # check for missing blocks
    blockCount: 200

replicaCount: 1

imagePullSecrets: []
nameOverride: ""
fullnameOverride: ""

serviceAccount:
  # Specifies whether a service account should be created
  create: true
  # Annotations to add to the service account
  annotations: {}
  # The name of the service account to use.
  # If not set and create is true, a name is generated using the fullname template
  name: ""

podAnnotations: {}

podSecurityContext: {}
  # fsGroup: 2000

securityContext: {}
  # capabilities:
  #   drop:
  #   - ALL
  # readOnlyRootFilesystem: true
  # runAsNonRoot: true
  # runAsUser: 1000

service:
  type: ClusterIP
  port: 8000

ingress:
  enabled: false
  className: ""
  annotations: {}
    # kubernetes.io/ingress.class: nginx
    # kubernetes.io/tls-acme: "true"
  hosts:
    - host: chart-example.local
      paths:
        - path: /
          pathType: ImplementationSpecific
  tls: []
  #  - secretName: chart-example-tls
  #    hosts:
  #      - chart-example.local

resources:
  limits:
    cpu: 80m
    memory: 100Mi
  requests:
    cpu: 50m
    memory: 60Mi

autoscaling:
  enabled: false
  minReplicas: 1
  maxReplicas: 100
  targetCPUUtilizationPercentage: 80
  # targetMemoryUtilizationPercentage: 80

nodeSelector: {}

tolerations: []

affinity: {}
