app := ebisusbay
image_registry := docker.io
image_owner := zephinzer
image_path := go-cronos

-include ./Makefile.properties

image_url ?= $(image_registry)/$(image_owner)/$(image_path)-$(app)

git_available := $(shell git rev-parse HEAD 2> /dev/null)
ifdef git_available
release_version = $$(git describe --tags --abbrev=0)
release_tag = $$(git rev-parse HEAD | head -c 8)
endif
release_version ?= v0.0.1
release_tag ?= latest

# this adjusts the binary extension for when building with windows
bin_ext := 
ifeq "${GOOS}" "windows"
bin_ext := .exe
endif

env:
	@docker-compose -f ./deploy/local-env/docker-compose.yml up --remove-orphans -d
env_down:
	@docker-compose -f ./deploy/local-env/docker-compose.yml down
env_purge:
	-@docker-compose -f ./deploy/local-env/docker-compose.yml down --remove-orphans
	sudo rm -rf ./data/docker/mysql/*
k8s:
	kind create cluster --config ./deploy/local-k8s/kind.config.yaml --name go-cronos
k8s_down:
	kind delete cluster --name go-cronos
deps:
	@go mod vendor
start:
	@go run . start $(app)
test:
	mkdir -p ./coverage
	go test -v -mod=mod -cover -covermode=atomic -coverpkg=./... -coverprofile=./coverage/golang.all.out ./...
	grep -v ".pb.go" ./coverage/golang.all.out > ./coverage/golang.out
	go tool cover -func ./coverage/golang.out
lint:
	go vet ./...
build:
	CGO_ENABLED=0 go build \
		-ldflags " \
			-X github.com/zephinzer/go-cronos/internal/constants.Version=$(release_version)-$(release_tag) \
			-extldflags 'static' -s -w \
		" \
		-o ./bin/$(app)_$(release_version)-$(release_tag)_$$(go env GOOS)_$$(go env GOARCH)$(bin_ext) \
		./cmd/$(app)
	cd ./bin && sha256sum $(app)_$(release_version)-$(release_tag)_$$(go env GOOS)_$$(go env GOARCH)$(bin_ext) > $(app)_$(release_version)-$(release_tag)_$$(go env GOOS)_$$(go env GOARCH)$(bin_ext).sha256
image:
	docker build \
		--network host \
		--file ./Dockerfile \
		--build-arg APP=$(app) \
		--build-arg RELEASE_TAG=$(release_tag) \
		--build-arg RELEASE_VERSION=$(release_version) \
		--tag $(image_url):latest \
		.
image_test:
	container-structure-test test image --image $(image_url):latest --config ./cmd/$(app)/specs.yaml
release: image
	docker tag $(image_url):latest $(image_url):$(release_tag)
ifdef release_version
	docker tag $(image_url):latest $(image_url):$(release_version)
	docker push $(image_url):$(release_version)
endif
	docker push $(image_url):latest
	docker push $(image_url):$(release_tag)
release_git:
	@$(MAKE) release release_tag=$$(git rev-parse HEAD | head -c 8) release_version=$$(git describe --tags)
utils_image:
	docker build \
		--network host \
		--file ./build/$(app)/Dockerfile \
		--tag $(image_url):latest \
		.	
utils_image_test:
	container-structure-test test image --image $(image_url):latest --config ./build/$(app)/specs.yaml
utils_release: utils_image
	docker tag $(image_url):latest $(image_url):$(release_tag)
	docker push $(image_url):latest
	docker push $(image_url):$(release_tag)
ifdef release_version
	docker tag $(image_url):latest $(image_url):$(release_version)
	docker push $(image_url):$(release_version)
endif
install:
	go install .
